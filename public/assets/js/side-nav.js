$('.menu-toggle').on('click', function () {

    if ($(this).hasClass('show')) {
        $(this).removeClass('show');
        $(".side-nav-bar ul li span").animate({
            "width": "0px"
        });
        $(".side-nav-bar ul li i").animate({
            "marginLeft": "10px"
        });
        $('.content').css({
            "marginLeft": "7rem"
        })
    } else {
        $(".side-nav-bar ul li span").css({
            "width": "auto"
        });
        $(".side-nav-bar ul li i").animate({
            "marginLeft": "0px"
        });
        $('.content').css({
            "marginLeft": "15rem"
        });
        $(this).addClass('show');
    }
});