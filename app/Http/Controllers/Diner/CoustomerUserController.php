<?php

namespace App\Http\Controllers\Diner;

use App\Http\Requests\Diner\CustomerUserRequest;
use App\Http\Services\Common\CountryService;
use App\Http\Services\Diner\CustomerUserServices;
use App\Http\Services\Admin\CustomerServices;
use App\Http\Services\Common\Helper;
use Illuminate\Http\Request;
Use URL;
use Illuminate\Support\Facades\Hash;

class CoustomerUserController extends DinerController
{
    private $customeruserObj;
    private $countryObj;
    private $helperObj;
    private $customerObj;

    public function __construct(CustomerServices $customerObj, CustomerUserServices $customeruserObj, CountryService $countryObj, Helper $helperObj)
    {
        parent::__construct();
        $this->customeruserObj = $customeruserObj;
        $this->helperObj = $helperObj;
        $this->countryObj = $countryObj;
        $this->customerObj = $customerObj;
    }

    public function index(Request $request)
    {
        $customerID = (int)$request->venue_id ? (int)$request->venue_id : $this->data['current_user']->id;
        $conditions = ['customer_id'=> $customerID];

        $per_page = $request->per_page ? $request->per_page : 20;
        $this->data['customers'] = $this->customeruserObj->getAll($conditions, $per_page);

        $this->data['title'] = (object)['name' => 'Waiters', 'url' => URL::to('diner/waiters'),
            'child' => (object)['name' => 'Create Waiters', 'url' => URL::to('diner/waiters/create')]
        ];

        return view('diner.customeruser.index', $this->data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['title'] = (object)['name' => 'Create Waiter', 'url' => URL::to('diner/waiters'),
            'parent' => (object)['name' => 'Waiter']];

        return view('diner.customeruser.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function store(CustomerUserRequest $request)
    {
        $data = $request->except(['_token']);

//        if ($request->hasFile('profile_pic')) {
//            $data['profile_pic'] = $this->helperObj->upload($request->file('profile_pic'));
//        }
        $data['customer_id'] = (int)$request->venue_id ? (int)$request->venue_id : $this->data['current_user']->id;
        $data['password'] = Hash::make($data['password']);  
        $vendor = $this->customeruserObj->insert($data);
        if ($vendor) {
            return redirect()->to('diner/waiters')->with(['success' => 'added successfully']);
        } else {
            return redirect()->back()->with('fail', 'Whoops something wrong');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->data['current_customer'] = $this->customeruserObj->find(['id' => $id]);
        if ($this->data['current_customer']) {
            $this->data['title'] = (object)['name' => 'Waiter Details', 'url' => URL::to('diner/waiters'),
                // 'additional' => (object)['name' => 'Login AS ' . $this->data['current_customer']->name, 'url' => URL::to('diner/login/customer/' . $this->data['current_customer']->id), 'class' => 'white-btn'],
                'child' => (object)['name' => 'Edit Waiter', 'url' => URL::to('diner/waiters/' . $this->data['current_customer']->id . '/edit'), 'subtitle' => $this->data['current_customer']->name]
            ];
            $this->data['countries'] = $this->countryObj->getAll([['id', '>', 0]]);

            return view('diner.customeruser.show', $this->data);
        } else {
            return view('diner.404');
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['current_customer'] = $this->customeruserObj->find(['id' => $id]);
        if ($this->data['current_customer']) {
            $this->data['title'] = (object)['name' => 'Edit Customer', 'url' => URL::to('diner/customer'),
                'parent' => (object)['name' => 'Customer']];
            $this->data['countries'] = $this->countryObj->getAll([['id', '>', 0]]);

            return view('diner.customeruser.edit', $this->data);
        } else {
            return view('diner.404');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */

    public function update(CustomerUserRequest $request, $id)
    {
        $current_user = $this->customeruserObj->find(['id' => $id]);
        if ($current_user) {
            $data = $request->except(['_token', '_method']);

//            if ($request->hasFile('profile_pic') && $current_user->profile_pic != '') {
//                $data['profile_pic'] = $this->helperObj->upload($request->file('profile_pic'), 'edit', $current_user->profile_pic);
//            }

            $current_vendor = $this->customeruserObj->update(['id' => $id], $data);

            if ($current_vendor) {
                return redirect()->back()->with(['success' => 'updated successfully']);
            } else {
                return redirect()->back()->with(['fail' => 'whoops something wrong']);
            }
        } else {
            return view('diner.404');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->customeruserObj->delete(['id' => $id]);
        return redirect()->route('diner.waiters.index')->with('success', 'Customer was removed successfully');
    }

    public function getvenueMenu($id){
        $venue = $this->customerObj->find(['id' => $id]);
        return $venue->Menus;
    }

}
