<?php

namespace App\Http\Controllers\Diner;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;
use App\Http\Models\Configration;
use Illuminate\Http\Request;
use App\Http\Services\Diner\FeedbackInteractionService;
use App\Http\Services\Diner\CampaignService;
use App\Http\Services\Diner\OrderService;
use App\Http\Services\Diner\ItemService;
use App\Http\Services\Diner\CartService;
use App\Http\Services\Diner\CartItemService;
use Illuminate\Support\Facades\Hash;

class DinerController extends Controller
{
    public $data;
    private $feedbackinteractionObj;
    private $campaignObj;
    private $orderObj;
    private $itemObj;
    private $cartitemObj;
    private $cartObj;

    public function __construct(CartService $cartObj = null, FeedbackInteractionService $feedbackinteractionObj = null, CampaignService $campaignObj = null, OrderService $orderObj = null, ItemService $itemObj = null, CartItemService $cartitemObj = null)
    {

        Carbon::setWeekStartsAt(Carbon::SATURDAY);
        Carbon::setWeekEndsAt(Carbon::FRIDAY);

        $this->feedbackinteractionObj = $feedbackinteractionObj;
        $this->campaignObj = $campaignObj;
        $this->orderObj = $orderObj;
        $this->itemObj = $itemObj;
        $this->cartitemObj = $cartitemObj;
        $this->cartObj = $cartObj;

        $this->middleware(function ($request, $next) {
            if(Auth::guard('diner')->check()){
                $customerID = (int)$request->venue_id ? (int)$request->venue_id : Auth::guard('diner')->user()->id;

                $navigations = [
                    'Dashbord' =>(object)[
                        'name' => 'Dashbord', 'url' => $customerID ? URL::to('diner?venue_id='.$customerID) : URL::to('diner'),
                        'class' => 'fas fa-clipboard-list'
                    ],
                    'Menus' => (object)[
                        'name' => 'Menus', 'url' => $customerID ? URL::to('diner/menu?venue_id='.$customerID) : URL::to('diner/menu'),
                        'class' => 'fas fa-clipboard-list'
                    ],
                    'Orders' => (object)[
                        'name' => 'Orders', 'url' => $customerID ? URL::to('diner/order?venue_id='.$customerID) : URL::to('diner/order'),
                        'class' => 'fas fa-clipboard-list'
                    ],
                    'Tables' => (object)[
                        'name' => 'Tables', 'url' => $customerID ? URL::to('diner/tables?venue_id='.$customerID) : URL::to('diner/tables'),
                        'class' => 'fas fa-clipboard-list'
                    ],
                    'Waiters' => (object)[
                        'name' => 'Waiters', 'url' => $customerID ? URL::to('diner/waiters?venue_id='.$customerID) : URL::to('diner/waiters'),
                        'class' => 'fas fa-clipboard-list'
                    ],
                    'Campaigns' => (object)[
                        'name' => 'Campaigns', 'url' => $customerID ? URL::to('diner/campaign?venue_id='.$customerID) : URL::to('diner/campaign'),
                        'class' => 'fas fa-clipboard-list'
                    ],
                    'Survey Forms' => (object)[
                        'name' => 'Survey Forms', 'url' => $customerID ? URL::to('diner/feedback?venue_id='.$customerID) : URL::to('diner/feedback'),
                        'class' => 'fas fa-clipboard-list'
                    ],
                    'Survey Results' => (object)[
                        'name' => 'Survey Results', 'url' => $customerID ? URL::to('diner/interaction?venue_id='.$customerID) : URL::to('diner/interaction'),
                        'class' => 'fas fa-clipboard-list'
                    ],
                    'Devices' => (object)[
                        'name' => 'Devices', 'url' => $customerID ? URL::to('diner/devices?venue_id='.$customerID) : URL::to('diner/devices'),
                        'class' => 'fas fa-clipboard-list'
                    ],
                    'App Configration' => (object)[
                        'name' => 'App Configration', 'url' => $customerID ? URL::to('diner/cofigrations?venue_id='.$customerID) : URL::to('diner/cofigrations'),
                        'class' => 'fas fa-clipboard-list'
                    ],
                    'Localization' => (object)[
                        'name' => 'Localization', 'url' => $customerID ? URL::to('diner/localization?venue_id='.$customerID) : URL::to('diner/localization'),
                        'class' => 'fas fa-clipboard-list'
                    ],
                    'Modifiers' => (object)[
                        'name' => 'Modifiers', 'url' => $customerID ? route('diner.modifiers').'?venue_id='.$customerID : route('diner.modifiers'),
                        'class' => 'fas fa-clipboard-list'
                    ],
                ];
                View::share('navigations', $navigations);
                $user = Auth::guard('diner')->user();
                $userBanch = $user;

                $this->data['venue_id'] = $customerID;

                $this->data['current_user'] = $user;
                $this->data['user_branch'] = $userBanch;
                $this->data['mainMenus'] = $userBanch->parent_id ? [] : $userBanch->mainMenus();
                $this->data['mainBranch'] = $userBanch->parent_id ? $userBanch->mainBranch : $userBanch;
                $this->data['venues'] = $userBanch->parent_id ? [] : $userBanch->venues;

                $this->data['config'] = Configration::where('customer_id', $customerID)->first() ?? null;
            }
            return $next($request);
        });


    }


    public function index(Request $request){
        $topViews = $request->filled('top_views') ? $request->top_views : true;
        $customerID = (int)$request->venue_id ? (int)$request->venue_id : Auth::guard('diner')->user()->id;
        $this->data['venue_id'] =$customerID;
        $currentWeek = [Carbon::now()->startOfWeek(),Carbon::now()->endOfWeek()];

        $orderedCartsIDs = $this->cartObj->getAll(['customer_id' => $customerID,'ordered' => 1])->pluck('id')->toArray();
        $itemsInOrderedCarts = $this->cartitemObj->getAll([], [], false, ['key' => 'cart_id', 'values' => $orderedCartsIDs])->pluck('item_id')->toArray();
        $this->data['item_Selled'] = count(array_unique($itemsInOrderedCarts));

        if(isset($request->from_date) && isset($request->to_date)){
            $dates = [$request->from_date, $request->to_date];
            $this->data['open_servay'] = count($this->feedbackinteractionObj->getAllInTime(['status' => 1, 'customer_id' => $customerID],$dates));
            $this->data['closed_feedback'] = count($this->feedbackinteractionObj->getAllinTime(['status' => 0, 'customer_id' => $customerID],$dates));
            $this->data['running_campain'] = count($this->campaignObj->getAllinTime(['customer_id' => $customerID,'active' => 1], $dates));
            $this->data['open_orders'] = count($this->orderObj->getAllinTime(['status' => 'open'], $dates));
            $this->data['closed_orders'] = count($this->orderObj->getAllinTime(['status' => 'close'], $dates));
            //$this->data['item_Selled'] = count($this->orderObj->getAllinTime(['status' => 'close'], $dates));
            $this->data['items_viewed'] = $topViews ? $this->itemObj->Topview([]) : $this->itemObj->Topview([], false);
            $this->data['recent_feedback'] = $this->feedbackinteractionObj->recentfeedback($dates);
            $this->data['recent_order'] = $this->orderObj->recentorder($dates);

            $this->data['orders_chart'] = $this->orderObj->getAllinTime([], $currentWeek, true);

        }
        else{
            $this->data['open_servay'] = count($this->feedbackinteractionObj->getAll(['status' => 1, 'customer_id' => $customerID]));
            $this->data['closed_feedback'] = count($this->feedbackinteractionObj->getAll(['status' => 0, 'customer_id' => $customerID]));
            $this->data['running_campain'] = count($this->campaignObj->getAll(['customer_id' => $customerID,'active' => 1]));
            $this->data['open_orders'] = count($this->orderObj->getAll(['status' => 'open']));
            $this->data['closed_orders'] = count($this->orderObj->getAll(['status' => 'close']));
            //$this->data['item_Selled'] = count($this->orderObj->getAll(['status' => 'close']));
            $this->data['items_viewed'] = $topViews ? $this->itemObj->Topview([]) : $this->itemObj->Topview([], false);
            $this->data['recent_feedback'] = $this->feedbackinteractionObj->recentfeedback(['customer_id' => $customerID]);
            $this->data['recent_order'] = $this->orderObj->recentorder([]);

            $this->data['orders_chart'] = $this->orderObj->getAllinTime([], $currentWeek, true);
        }

        $this->data['days'] = ['Sat','Sun','Mon','Tue','Wed','Thu','Fri'];

        $this->data['title'] = (object)[
            'name' => 'Dashbord', 'url' => URL::to('admin'),
        ];
        //return $this->data;
        return view('diner.dashbord', $this->data);
    }

    public function preview(Request $request) {
        return view('diner.preview');
    }

}
