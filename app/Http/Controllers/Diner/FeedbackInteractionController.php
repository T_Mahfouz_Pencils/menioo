<?php

namespace App\Http\Controllers\Diner;

use App\Exports\SurveyExport;
use App\Http\Services\Diner\FeedbackService;
use App\Http\Services\Diner\FeedbackInteractionService;
use App\Http\Services\Diner\FeedbackQuestionService;
use App\Http\Services\Admin\CustomerServices;
use App\Http\Services\Common\Helper;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
Use URL;

class FeedbackInteractionController extends DinerController
{
    private $feedbackObj;
    private $feedbackInteractionObj;
    private $fbQuestionObj;
    private $customerObj;
    private $helperObj;

    public function __construct(
        FeedbackService $feedbackObj,
        FeedbackInteractionService $feedbackInteractionObj,
        FeedbackQuestionService $fbQuestionObj,
        CustomerServices $customerObj,
        Helper $helperObj
    )
    {
        parent::__construct();

        $this->feedbackObj = $feedbackObj;
        $this->feedbackInteractionObj = $feedbackInteractionObj;
        $this->customerObj = $customerObj;
        $this->fbQuestionObj = $fbQuestionObj;
        $this->helperObj = $helperObj;
    }

    public function index(Request $request)
    {
        $customer =  (int)$request->venue_id ? $this->customerObj->find(['id'=>(int)$request->venue_id]): $this->data['user_branch'];
        $conditions['customer_id'] = $customer->id;
        $this->data['venue_id'] = (int)$request->venue_id ? (int)$request->venue_id : 0;

        if ($request->filled('active')) {
            $conditions['active'] = $request->active;
        }
        if ($request->filled('name')) {
            $conditions['name'] = $request->name;
        }
        
        if ($request->filled('interaction_status')) {
            $conditions['status'] = $request->interaction_status;
        }
        if ($request->filled('form_id')) {
            $conditions['form_id'] = $request->form_id;
        }
        
        $interactionFormConditions = $dateConditions = [];

        if ($request->filled('interaction_name')) {
            $interactionFormConditions[] = ['form_name','LIKE','%'.$request->interaction_name.'%'];
        }
        $formIDs = $this->feedbackObj->getall($interactionFormConditions)->pluck('id')->toArray();
        

        if ($request->filled('interaction_from')) {
            $dateConditions[] = $request->interaction_from;
        }
        if ($request->filled('interaction_to')) {
            $dateConditions[] = $request->interaction_to;
        }

        $per_page = $request->per_page ? $request->per_page : 100;
        $this->data['interactions'] = $this->feedbackInteractionObj->getAll($conditions, ['question','customer','form'], $per_page, $dateConditions, $formIDs);
        

        //return response()->json(count($this->data['interactions'][0]->interactions));

        $total_count = count($this->data['interactions']);

        $this->data['title'] = (object)[
            'name' => 'Survey Results',
            //'url' => URL::to('admin/feedback'),
//            'child' => (object)[
//                'name' => 'Create Feedback Form',
//                'url' => $this->data['venue_id'] ? URL::to('diner/interaction/create?venue_id='.$this->data['venue_id']) : URL::to('diner/interaction/create')
//            ]
        ];

        return view('diner.interaction.index', $this->data);
    }

    public function create(Request $request)
    {
        $customerID = (int)$request->venue_id ? (int)$request->venue_id : 0;

        $customer = (int)$request->venue_id ? $this->customerObj->find(['id'=>(int)$request->venue_id]): $this->data['user_branch'];
        $this->data['items'] = $customer ? $customer->items() : [];

        $this->data['title'] = (object)[
            'name' => 'Create New Feedback Form',
            'url' => $customerID ? URL::to('diner/feedback?venue_id='.$customerID) : URL::to('diner/feedback'),
            'parent' => (object)['name' => 'Campaigns']];
        //$this->data['countries'] = $this->countryObj->getAll([['id', '>', 0]]);

        $this->data['venue_id'] = (int)$request->venue_id ? (int)$request->venue_id : 0;
        $this->data['customer_id'] = (int)$request->venue_id ? (int)$request->venue_id : $this->data['user_branch']->id;
        return view('diner.feedback.create', $this->data);
    }

    public function store(Request $request)
    {
        //return $request->all();
        $data = $request->only(['customer_id','form_name','form_header']);

        $feedback = $this->feedbackObj->insert($data);
        if ($feedback) {
            if(count($request->questions)) {
                $questions = [];
                foreach ($request->questions as $question) {
                    if($question['content'] != null) {
                        $item = [];
                        $item['customer_id'] = $data['customer_id'];
                        $item['feedback_id'] = $feedback->id;
                        $item['value'] = $question['content'];
                        $item['type'] = $question['type'];
                        $item['mandatory'] = isset($question['mandatory']) ? 1 : 0;
                        $questions[] = $item;
                    }
                }
                if(count($questions))
                    $this->fbQuestionObj->bulkInsert($questions);
            }
            return $this->data['user_branch']->id != $feedback->customer_id ?
                redirect()->route('diner.feedback.index', ['venue_id' => $feedback->customer_id])->with(['success' => 'added successfully']) :
                redirect()->route('diner.feedback.index')->with(['success' => 'added successfully']);
        } else {
            return redirect()->back()->with('fail', 'Whoops something wrong');
        }

    }

    public function show($id)
    {
        $this->data['current_venue'] = $this->customerObj->find(['id' => $id]);
        if ($this->data['current_venue']) {
            $this->data['title'] = (object)['name' => 'Customer Details', 'url' => URL::to('admin/customer'),
                'additional' => (object)['name' => 'Login AS ' . $this->data['current_venue']->name, 'url' => URL::to('admin/login/customer/' . $this->data['current_venue']->id), 'class' => 'white-btn'],
                'child' => (object)['name' => 'Edit Customer', 'url' => URL::to('admin/customer/' . $this->data['current_venue']->id . '/edit'), 'subtitle' => $this->data['current_venue']->name]
            ];
            return view('diner.venue.show', $this->data);
        } else {
            return view('admin.404');
        }

    }

    public function edit($id)
    {
        $customer = (int)request()->venue_id ? $this->customerObj->find(['id'=>(int)request()->venue_id]): $this->data['user_branch'];
        $this->data['customer_id'] = $customer->id;
        $customerID = (int)request()->venue_id ? (int)request()->venue_id : 0;
        $currentFeedback = $this->feedbackObj->find(['id' => $id],['questions']);
        $this->data['current_feedback'] = $currentFeedback;

        if ($this->data['current_feedback']) {
            $this->data['title'] = (object)[
                'name' => 'Edit '.$currentFeedback->form_name,
                'url' => $customerID ? URL::to('diner/feedback?venue_id='.$customerID) : URL::to('diner/feedback'),
                'parent' => (object)['name' => 'Feedback']
            ];

            return view('diner.feedback.edit', $this->data);
        } else {
            return view('diner.404');
        }
    }

    public function update(Request $request, $id)
    {
        $data = $request->except(['_token','_method','questions']);

        $feedback = $this->feedbackObj->find(['id'=>$id]);
        if ($feedback) {
            $this->feedbackObj->update(['id'=>$id],$data);
            $this->fbQuestionObj->delete(['feedback_id'=>$id]);

            if(count($request->questions)) {
                $questions = [];
                foreach ($request->questions as $question) {
                    if($question['content'] != null) {
                        $item = [];
                        $item['customer_id'] = $data['customer_id'];
                        $item['feedback_id'] = $id;
                        $item['value'] = $question['content'];
                        $item['type'] = $question['type'];
                        $item['mandatory'] = isset($question['mandatory']) ? 1 : 0;
                        $questions[] = $item;
                    }
                }
                if(count($questions))
                    $this->fbQuestionObj->bulkInsert($questions);
            }

            return $this->data['user_branch']->id != $feedback->customer_id ?
                redirect()->route('diner.feedback.index', ['venue_id' => $feedback->customer_id])->with(['success' => 'Updated successfully']) :
                redirect()->route('diner.feedback.index')->with(['success' => 'Updated successfully']);
        } else {
            return redirect()->back()->with('fail', 'Whoops something wrong');
        }
    }

    public function openClose($id)
    {
        $interaction = $this->feedbackInteractionObj->find(['identifier'=>$id]);
        if($interaction) {
            $status = $interaction->status ? 0 : 1;
            $this->feedbackInteractionObj->update(['identifier'=>$id],['status'=>$status]);
            return redirect()->back()->with(['success' => 'Status changed successfully.']);
        }
        return redirect()->back()->with(['fail' => 'This interactive not found']);
    }

    public function openCloseAll($ids,$status){
        $ids = array_map ('intval', explode(",",$ids));
        $interactions = $this->feedbackInteractionObj->findall($ids);
        
        if($interactions) {
            $interactions->update(['status'=>$status]);
            return ['success' => 'Status changed successfully.'];
        }
        return ['fail' => 'This interactive not found'];
    }

    public function destroy($id)
    {
        //$customer = (int)request()->venue_id ? $this->customerObj->find(['id'=>request()->venue_id]) : $this->data['user_branch'];
        $feedback = $this->feedbackObj->find(['id'=>$id]);
        $this->feedbackObj->delete(['id' => $id]);
        $this->fbQuestionObj->delete(['feedback_id'=>$id]);
        return $this->data['user_branch']->id != $feedback->customer_id ?
            redirect()->route('diner.feedback.index', ['venue_id' => $feedback->customer_id])->with(['success' => 'Feedback was removed successfully']) :
            redirect()->route('diner.feedback.index')->with(['success' => 'Feedback was removed successfully']);
    }

    public function export(Request $request)
    {
        $customer =  (int)$request->venue_id ? $this->customerObj->find(['id'=>(int)$request->venue_id]): $this->data['user_branch'];
        $conditions['customer_id'] = $customer->id;


        if ($request->filled('active')) {
            $conditions['active'] = $request->active;
        }
        if ($request->filled('name')) {
            $conditions['name'] = $request->name;
        }
        if ($request->filled('form_id')) {
            $conditions['form_id'] = $request->form_id;
        }

        $dateConditions = [];
        if ($request->filled('interaction_from')) {
            $dateConditions[] = $request->interaction_from;
        }
        if ($request->filled('interaction_to')) {
            $dateConditions[] = $request->interaction_to;
        }

        $per_page = 0;

        $data = $this->feedbackInteractionObj->getAll($conditions, ['question','customer','form'], $per_page, $dateConditions);
        //return $data;
        $dataSheet = [];
        if(count($data)) {
            foreach($data as $form){
                $form = (object)$form;
                $item = [];
                $item['form'] = $form->form_name;
                $item['status'] = $form->status ? 'open' : 'close';
                $item['sent_at'] = date('d/m/Y',strtotime($form->interactions[0]->created_at));
                $item['fqa'] = $form->interactions;
                foreach($form->interactions as $index => $si) {
                    $item['Q'.($index+1)] = $si->question->value;
                    $item['type'.($index+1)] = $si->question->type;
                    $item['A'.($index+1)] = $si->answer;
                }
                $dataSheet[] = (object)$item;
            }
        }
        //return $dataSheet;


        return Excel::download(new SurveyExport($dataSheet), time().'-feedback-export.csv');
    }
}
