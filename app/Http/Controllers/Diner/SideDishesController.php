<?php

namespace App\Http\Controllers\Diner;

use Illuminate\Http\Request;
use App\Http\Services\Diner\SideDishService;
use Auth;
use App\Http\Services\Common\Helper;
use App\Http\Services\Common\CommonService;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class SideDishesController extends DinerController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $sidedish;
    private $helperObj;
    private $serviceObj;

    public function __construct(Helper $helperObj, SideDishService $sidedish, CommonService $serviceObj)
    {
        parent::__construct();
        $this->sidedish = $sidedish;
        $this->helperObj = $helperObj;
        $this->serviceObj = $serviceObj;
    }

    public function index(Request $request)
    {
        $customerID = (int)$request->venue_id ? (int)$request->venue_id : $this->data['current_user']->id;
        $this->data['venue_id'] = $customerID;
        $this->data['title'] = (object)[
            'name' => 'App Modifiers'
        ];
        $this->data['sideDishes'] = $this->serviceObj->getAll('SideDish', ['customer_id' => $customerID])->map(function($item){
            $item->label = $item->name;
            $item->value = $item->name;
            $item->photo = $item->FullThumbnailImagePath('300X300');
            return $item;
        });
        return view('diner.sidedishes.index',$this->data);
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
        $customerID = (int)$request->venue_id ? (int)$request->venue_id : Auth::guard('diner')->user()->id;

        $data = $request->except(['_token', 'image']);

        $sideDishitem = $this->sidedish->find(['name' => $data['name']]);

        if ($request->has('image')) {

            $media = $this->helperObj->upload($request->file('image'), ['title' => $data['name'], 'customer_id' => $customerID]);

            $data['media_id'] = $media->id;
        }
        $data['customer_id'] = $customerID;
        if($sideDishitem){
            $sideDishitem = $this->sidedish->update(['name' => $data['name']],$data);
        }
        else{
            $sideDishitem = $this->sidedish->insert($data);

        }

        if ($sideDishitem) {
            return redirect()->route('diner.sidedishes.index')->with(['success' => 'added successfully']);
        } else {
            return redirect()->back()->with('fail', 'Whoops something wrong');
        }

    }
    public function show($id)
    {
        //
    }
    public function edit($id)
    {
        $sidedish = $this->serviceObj->find('SideDish' ,['id' => $id]);
        $sidedish->image_path = $sidedish->FullImagePath();
        if(!$sidedish){
            $message = ['success' => 'Modifier not found!'];
            return redirect()->back()->with($message);
        }
        $this->data['title'] = (object)[
            'name' => 'Edit Modifier'
        ];
        $this->data['sidedish'] = $sidedish;
        return view('diner.sidedishes.edit',$this->data);
    }
    public function update(Request $request, $id)
    {
        $customerID = (int)$request->venue_id ? (int)$request->venue_id : Auth::guard('diner')->user()->id;
        $valid = Validator::make($request->all(), [
            'name' => [
                'required',
                Rule::unique('side_dishes')->where(function ($query) use($customerID, $request,$id) {
                    return $query->where('name', $request->name)->where('customer_id', $customerID)->where('id','!=',$id);
                }),
            ],
            'price' => 'required',
        ]);
        if($valid->fails()) {
            $message = ['fail' => $valid->errors()->first()];
            return redirect()->back()->with($message);
        }
        $sideDishData = $request->only(['name','price']);
        $this->serviceObj->update('SideDish',['id' => $id],$sideDishData);


        $message = ['success' => 'Successfully modified.'];
        return redirect()->back()->with($message);
    }
    public function destroy($id)
    {
        return $id;
        $deleted = $this->sidedish->delete(['id' => $id]);
        if($deleted){
            return ['success' => 'SideDish was removed successfully'];

        }
        else{
            return ['Error' => 'SideDish Faild to delete'];

        }
        return redirect()->route('diner.sidedishes.index')->with(['success' => 'SideDish was removed successfully']);


    }




    public function modifiers(Request $request)
    {
        $customerID = (int)$request->venue_id ? (int)$request->venue_id : $this->data['current_user']->id;
        $this->data['venue_id'] = $customerID;
        $this->data['title'] = (object)[
            'name' => 'App Modifiers'
        ];
        $options = $this->serviceObj->getAll('Option',['customer_id' => $customerID], ['values']);

        $this->data['modifiers'] = $options;

        return view('diner.configuration.modifiers',$this->data);
    }
    public function addModifier(Request $request)
    {
        $this->data['title'] = (object)[
            'name' => 'Add Modifier'
        ];
        return view('diner.configuration.add-modifier',$this->data);
    }
    public function saveModifier(Request $request)
    {
        $customerID = (int)$request->venue_id ? (int)$request->venue_id : Auth::guard('diner')->user()->id;

        Validator::make($request->all(), [
            'name' => [
                'required',
                Rule::unique('options')->where(function ($query) use($customerID, $request) {
                    return $query->where('name', $request->name)->where('customer_id', $customerID);
                }),
            ],
            'type' => 'required',
        ]);

        $optionData = $request->only(['name','type']);
        $optionData['customer_id'] = $customerID;
        $option = $this->serviceObj->create('Option',$optionData);

        if($request->filled('optionValues')) {
            $optionValuesData = [];
            foreach($request->optionValues as $val){
                if(isset($val['name']) && isset($val['price'])) {
                    $optVal = [];
                    $optVal['option_id'] = $option->id;
                    $optVal['value'] = $val['name'];
                    $optVal['price'] = $val['price'];
                    $optionValuesData[] = $optVal;
                }
            };
            $this->serviceObj->bulkInsert('OptionValue',$optionValuesData);
        }
        return redirect()->to('diner/sidedishes');
    }
    public function editModifier($id)
    {
        $modifier = $this->serviceObj->find('Option' ,['id' => $id], ['values']);
        if(!$modifier){
            $message = ['success' => 'Modifier not found!'];
            return redirect()->back()->with($message);
        }
        $this->data['title'] = (object)[
            'name' => 'Edit Modifier'
        ];
        $this->data['modifier'] = $modifier;
        return view('diner.configuration.edit-modifier',$this->data);
    }
    public function updateModifier(Request $request)
    {
        $customerID = (int)$request->venue_id ? (int)$request->venue_id : Auth::guard('diner')->user()->id;
        $id = $request->id;
        Validator::make($request->all(), [
            'name' => [
                'required',
                Rule::unique('options')->where(function ($query) use($customerID, $request, $id) {
                    return $query->where('name', $request->name)->where('customer_id', $customerID)->where('id','!=',$id);
                }),
            ],
            'type' => 'required',
        ]);

        $optionData = $request->only(['name','type']);
        $this->serviceObj->update('Option',['id' => $request->id],$optionData);
        $option = $this->serviceObj->find('Option',['id' => $request->id]);
        $this->serviceObj->destroy('OptionValue',['option_id' => $request->id]);

        if($request->filled('optionValues') && $request->type == 'checkbox') {
            $optionValuesData = [];
            foreach($request->optionValues as $val){
                if(isset($val['name']) && isset($val['price'])) {
                    $optVal = [];
                    $optVal['option_id'] = $request->id;
                    $optVal['value'] = $val['name'];
                    $optVal['price'] = $val['price'];
                    $optionValuesData[] = $optVal;
                }
            };
            $this->serviceObj->bulkInsert('OptionValue',$optionValuesData);
        }
        return redirect()->route('diner.modifiers');
    }
    public function deleteModifier($id)
    {
        $deleted = $this->serviceObj->destroy('Option' ,['id' => $id]);
        if($deleted){
            $this->serviceObj->destroy('OptionValue' , ['option_id' => $id]);
            $message = ['success' => 'Modifier was removed successfully'];
        }
        else{
            $message = ['Error' => 'Modifier Faild to delete'];
        }
        return redirect()->back()->with($message);
    }

}
