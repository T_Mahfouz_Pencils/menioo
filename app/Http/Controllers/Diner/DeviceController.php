<?php

namespace App\Http\Controllers\Diner;

use Illuminate\Http\Request;
use App\Http\Services\Diner\DeviceService;
Use URL;

class DeviceController extends DinerController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $deviceObj ;

    public function __construct(DeviceService $deviceObj){
        parent::__construct();

        $this->deviceObj = $deviceObj;
    }

    public function index(Request $request)
    {
        //
        $customerID = (int)$request->venue_id ? (int)$request->venue_id : $this->data['current_user']->id;
        $this->data['title'] = (object)[
            'name' => 'Devices',
           
        ];
        $this->data['devices'] = $this->deviceObj->getAll(['customer_id' => $customerID]);
        return view('diner.devices.index',$this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->deviceObj->delete(['id' => $id]);
    }

    public function deleteall($ids){
        $ids = array_map ('intval', explode(",",$ids));
        foreach ($ids as $id) {
            $this->destroy($id);
        }
        return ['success' => 'Devices deleted successfully.'];
    }
}
