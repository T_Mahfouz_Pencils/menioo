<?php

namespace App\Http\Controllers\Diner;

use App\Http\Services\Admin\CustomerServices;
use App\Http\Services\Diner\ItemService;
use App\Http\Services\Diner\OrderService;
use App\Http\Services\Diner\CartService;
use App\Http\Services\Diner\CartItemService;
use App\Http\Services\Diner\CartItemOptionService;
use App\Http\Services\Diner\CartItemSideDishService;

use App\Http\Services\Common\Helper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
Use URL;
use Carbon\Carbon;

class OrderController extends DinerController
{
    private $customerObj;
    private $itemObj;
    private $itemOptionObj;
    private $orderObj;
    private $itemSideDishObj;
    private $itemPriceObj;
    private $cartObj;
    private $cartItemObj;
    private $cartItemOptionObj;
    private $cartItemSideDishObj;
    private $helperObj;

    public function __construct(
        CustomerServices $customerObj,
        ItemService $itemObj,
        OrderService $orderObj,
        CartService $cartObj,
        CartItemService $cartItemObj,
        CartItemOptionService $cartItemOptionObj,
        CartItemSideDishService $cartItemSideDishObj,
        Helper $helperObj
    )
    {
        parent::__construct();

        $this->customerObj = $customerObj;
        $this->itemObj = $itemObj;
        $this->orderObj = $orderObj;
        $this->cartObj = $cartObj;
        $this->cartItemObj = $cartItemObj;
        $this->cartItemOptionObj = $cartItemOptionObj;
        $this->cartItemSideDishObj = $cartItemSideDishObj;
        $this->helperObj = $helperObj;
    }

    public function index(Request $request)
    {
        $customerID = (int)$request->venue_id ? (int)$request->venue_id : $this->data['current_user']->id;
        $venueID = (int)$request->venue_id ? (int)$request->venue_id : 0;

        $venue = $this->customerObj->find(['id'=>$customerID]);

        $key = 'customer_user_id';
        $arr = $venue->Users->pluck('id')->toArray();//$this->data['current_user']->siblinsIDs();

        $conditions = [['id', '>', 0]];
        $dates = [];
        $with = [];

        if ($request->keyword) {
            $conditions[] = ['id', '=', (int)$request->keyword];
        }

        if ($request->status) {
            $conditions[] = ['status', '=', $request->status];
        }

        if ($request->order_date_from) {
            $dates[] = $request->order_date_from;
            if(!$request->order_date_to)
              $dates[] = Carbon::now();
            else
              $dates[] = $request->order_date_to;
        }

        //$orders = $this->opened = $this->orderObj->getAllWhereIn($key,$arr,[],['cart']);
        $orders = $this->orderObj->getAllWhereIn($key,$arr,$conditions,$dates,$with,10);
        if(count($orders)) {
            foreach($orders as $order) {
                $cart = $order->cart;
                if($cart) {
                    //$items = $cart->items;
                    $cartItems = $cart->cartItems;
                      if(count($cartItems)) {
                          foreach($cartItems as $cartItem) {
                            $cartItem['item'] = $cartItem->item;
                          }
                          $cart['items'] = $cartItems;
                      }
                }
            }
        }
        $this->data['venue_id'] = $venueID;

        $this->data['orders'] = $orders;
        $this->data['title'] = (object)[
          'name' => 'Orders',
          //'child' => (object)['name' => 'Create Menu', 'url' => URL::to('diner/menu/create')]
        ];

        //return $this->data;
        return view('diner.order.index', $this->data);
    }

    public function show(Request $request, $id)
    {
        $customerID = (int)$request->venue_id ? (int)$request->venue_id : 0;

        $order = $this->orderObj->find(['id' => $id]);
        $cart = $order->cart;
        if($cart) {
            //$items = $cart->items;
            $cartItems = $cart->cartItems;
              if(count($cartItems)) {
                  foreach($cartItems as $cartItem) {
                    $cartItem['item'] = $cartItem->item;
                  }
                  $cart['items'] = $cartItems;
              }
        }

        $this->data['waiter'] = $order->customerUser;
        $this->data['current_order'] = $order;

        if ($this->data['current_order']) {
            $this->data['title'] = (object)[
                'name' => 'Order Details',
                'back' => (object)['name' => 'Orders', 'url' => $customerID ? URL::to('diner/order?venue_id'.$customerID) : URL::to('diner/order')],
                'parent' => (object)['name' => 'Orders'],
            ];

            //return $this->data;
            return view('diner.order.show', $this->data);
        } else {
            return view('admin.404');
        }
    }

    public function openedOrders(Request $request)
    {
        try {
            $statusCode = 200;
            $message = "Done.";
            $this->opened = $this->orderObj->getAll(['status' => 'open','customer_user_id'=>$this->data['current_user']->id]);

        } catch (\Exception $e) {
            $statusCode = 500;
            $message = $e->getMessage();
        } finally {
            return parent::jsonResponse($statusCode,$message,$this->opened);
        }
    }

    public function closeOrder(Request $request,$id)
    {
        try {
            $statusCode = 200;
            $message = "Done.";
            $this->order = $this->orderObj->find([
                'id' => $id,
                'status' => 'open',
                'customer_user_id' => $this->data['current_user']->id
            ]);
            if(!$this->order)
                throw new \Exception('Not Found');

            $this->orderObj->update([
                'id' => $request->id,
                'status' => 'open',
                'customer_user_id' => $this->data['current_user']->id
            ],['status' => "close"]);

            $this->order = $this->orderObj->find(['id' => $id]);

        } catch (\Exception $e) {
            $statusCode = 500;
            $message = $e->getMessage();
        } finally {
            return parent::jsonResponse($statusCode,$message,$this->order);
        }
    }

}
