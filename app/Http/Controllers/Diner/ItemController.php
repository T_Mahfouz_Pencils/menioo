<?php

namespace App\Http\Controllers\Diner;

use App\Http\Services\Diner\ItemIngredientWarningService;
use App\Http\Services\Diner\ItemOptionService;
use App\Http\Services\Diner\ItemOptionValueService;
use App\Http\Services\Diner\ItemSideDishService;
use App\Http\Services\Diner\IngredientWarningService;
use Illuminate\Http\Request;
use App\Http\Requests\Diner\ItemRequest;
use App\Http\Services\Diner\MenuService;
use App\Http\Services\Diner\ItemPriceService;
use App\Http\Services\Diner\ItemService;
use App\Http\Services\Diner\SideDishService;
use App\Http\Services\Diner\ArrangmentService;
use App\Http\Services\Diner\OptionService;
use App\Http\Services\Diner\ItemViewService;
use App\Http\Services\Common\Helper;
use Illuminate\Support\Facades\Auth;
Use URL;

class ItemController extends DinerController
{
    private $itemObj;
    private $helperObj;
    private $menuObj;
    private $itemPriceObj;
    private $itemIngredientWarningObj;
    private $itemSideDishObj;
    private $itemOptionObj;
    private $itemOptionValuesObj;
    private $ingredientWarningObj;
    private $sideDishObj;
    private $optionObj;
    private $arrangeObj;
    private $itemviewObj;


    public function __construct(
        ArrangmentService $arrangeObj,
        ItemService $itemObj,
        SideDishService $sideDishObj,
        OptionService $optionObj,
        MenuService $menuObj,
        ItemPriceService $itemPriceObj,
        IngredientWarningService $ingredientWarningObj,
        ItemIngredientWarningService $itemIngredientWarningObj,
        ItemSideDishService $itemSideDishObj,
        ItemOptionService $itemOptionObj,
        ItemOptionValueService $itemOptionValuesObj,
        Helper $helperObj,
        ItemViewService $itemviewObj
    )
    {
        parent::__construct();
        $this->menuObj = $menuObj;
        $this->itemObj = $itemObj;
        $this->helperObj = $helperObj;
        $this->itemPriceObj = $itemPriceObj;
        $this->ingredientWarningObj = $ingredientWarningObj;
        $this->itemIngredientWarningObj = $itemIngredientWarningObj;
        $this->itemSideDishObj = $itemSideDishObj;
        $this->itemOptionObj = $itemOptionObj;
        $this->itemOptionValuesObj = $itemOptionValuesObj;
        $this->sideDishObj = $sideDishObj;
        $this->optionObj = $optionObj;
        $this->arrangeObj = $arrangeObj;
        $this->itemviewObj = $itemviewObj;
    }

    public function index(Request $request)
    {
        //return view('diner.item.index');
        try {
            $customerID = (int)$request->venue_id ? (int)$request->venue_id : 0;
            $currentSubmenu = $this->menuObj->find(['id'=>$request->menu_id]);
            $parentID = $currentSubmenu->parent_id;

            $menuID = $request->menu_id ? (int)$request->menu_id : 0;

            $this->data['message'] = "data received";
            $this->data['items'] = [];

            $items = $this->itemObj->getAll([['menu_id', '=', $menuID]]);
            if (count($items)) {
                foreach ($items as $item) {
                    $obj = new \stdClass();
                    $obj->id = $item->id;
                    $obj->is_active = $item->is_active;
                    $obj->menu_id = $menuID;
                    $obj->name = $item->name;
                    $obj->description = $item->description;
                    $obj->portion = $item->portion;
                    $obj->preparation_time = $item->preparation_time;
                    $obj->calories = $item->calories;
                    $obj->notes = $item->notes;
                    $obj->item_image = $item->FullThumbnailImagePath('500X500');
                    $this->data['items'][] = $obj;
                }
            }

            $backUrl = $customerID ? $parentID ? URL::to('diner/menu/'.$parentID.'?venue_id='.$customerID) : URL::to('diner/menu/?venue_id='.$customerID) : $parentID ? URL::to('diner/menu/'.$parentID) : URL::to('diner/menu/'.$menuID);

            $this->data['title'] = (object)[
              'name' => 'Items',
              'url' => URL::to('diner/menu'),
              'back' => (object)[
                  'name' => ' Back',
                  'url' => $backUrl
                  //'url' => url()->previous()//str_replace(url('/'), '', url()->previous())
              ],
              'child' => (object)[
                  'name' => 'Create Item',
                  'url' => $customerID ? URL::to('diner/menu/'.$menuID.'/item/create?venue_id='.$customerID) : URL::to('diner/menu/'.$menuID.'/item/create'),
                ]
            ];

        } catch (\Exception $e) {
            return view('diner.error.500', $this->data);
        } finally {
            //return $this->data['title'];
            return view('diner.item.index', $this->data);
        }
    }

    public function show($menu_id, $id)
    {
        try {
            $customerID = (int)request()->venue_id ? (int)request()->venue_id : 0;
            $item = $this->itemObj->find(['id' => $id, 'menu_id' => $menu_id], ['menu', 'itemOptions', 'priceList', 'sideDishes', 'ingredientWarnings']);
            //$this->data['item'] = $item;
            if ($item) {
                $menuID = $item->menu_id;
                $obj = new \stdClass();
                $obj->id = $item->id;
                $obj->menu_id = $menuID;
                $obj->name = $item->name;
                $obj->description = $item->description;
                $obj->portion = $item->portion;
                $obj->preparation_time = $item->preparation_time;
                $obj->calories = $item->calories;
                $obj->notes = $item->notes;
                $obj->item_image = $item->FullThumbnailImagePath('500X500');
                $obj->item_video = $item->FullVideoPath();
                $obj->ingredientWarnings = [];
                $obj->options = [];
                $obj->prices = [];
                $obj->sideDishes = [];
                $ingredientWarnings_arr = [];
                $options_arr = [];
                $prices_arr = [];
                $sideDishes_arr = [];
                $this->itemviewObj->insert(['item_id' => $item->id]);
                if (count($item->ingredientWarnings)) {
                    foreach ($item->ingredientWarnings as $ingredientWarning) {
                        $ingredientWarning_obj = new \stdClass();
                        $ingredientWarning_obj->id = $ingredientWarning->id;
                        $ingredientWarning_obj->name = $ingredientWarning->name;
                        $ingredientWarnings_arr[] = $ingredientWarning_obj;
                    }
                    $obj->ingredientWarnings = $ingredientWarnings_arr;
                }
                if (count($item->itemOptions)) {
                    foreach ($item->itemOptions as $itemOption) {
                        $itemOption_obj = new \stdClass();
                        $itemOption_obj->id = $itemOption->id;
                        $itemOption_obj->name = $itemOption->name;
                        $options_arr[] = $itemOption_obj;
                    }
                    $obj->ingredientWarnings = $options_arr;
                }
                if (count($item->priceList)) {
                    foreach ($item->priceList as $price) {
                        $price_obj = new \stdClass();
                        $price_obj->price = $price->price;
                        $price_obj->description = $price->description;
                        $prices_arr[] = $price_obj;
                    }
                    $obj->prices = $prices_arr;
                }
                if (count($item->sideDishes)) {
                    foreach ($item->sideDishes as $sideDishe) {
                        $sd = $this->sideDishObj->find(['id'=>$sideDishe->side_dish_id]);
                        $sideDishe_obj = new \stdClass();
                        $sideDishe_obj->name = $sideDishe->side_dishe->name;
                        $sideDishe_obj->price = $sideDishe->price;
                        $sideDishe_obj->sideDishe_image = $sd ? $sd->FullThumbnailImagePath('100X100') : '';
                        $sideDishes_arr[] = $sideDishe_obj;
                    }
                    $obj->sideDishes = $sideDishes_arr;
                }
                $this->data['item'] = $obj;

                $this->data['title'] = (object)[
                  'name' => $item->name,
                  //'url' => URL::to('diner/menu'),
                  'back' => (object)[
                      'name' => ' Main Courses',
                      'url' => $customerID ? URL::to('diner/menu/'.$menuID.'?venue_id='.$customerID) : URL::to('diner/menu/'.$menuID)
                  ],
                  'parent' => (object)['name' => 'Items'],
                  'child' => (object)[
                      'name' => 'Edit',
                      'url' => URL::to('diner/menu/'.$menuID.'/item/'.$item->id.'/edit'),
                    ]
                ];

            }
        } catch (\Exception $e) {
            return view('diner.error.500', $this->data);
        } finally {
            //return $this->data;
            return view('diner.item.show', $this->data);
        }
    }

    public function create(Request $request, $menu_id)
    {
        $customerID = (int)$request->venue_id ? (int)$request->venue_id : $this->data['current_user']->id;
        $this->data['menu_id'] = $menu_id;
        $this->data['ingredient_warnings'] = $this->itemIngredientWarningObj->getAll([['id', '>', 0]]);
        $this->data['current_menu'] = $this->menuObj->find(['id' => $menu_id]);
        $this->data['title'] = (object)[
            'name' => 'Create Item',
            'url' => URL::to('diner/menu/' . $menu_id . '/item'),
            'parent' => (object)[
                'name' => 'Menus'
            ],
            'back' => (object)[
              'name' => ' ' . $this->data['current_menu']->name,
              'url' => URL::to('diner/menu/' . $menu_id.'/item')
            ]
        ];
        $this->data['sideDishes'] = $this->sideDishObj->getAll(['customer_id' => $customerID]);
            
        $this->data['options'] = $this->optionObj->getAll(['customer_id' => $customerID]);
        $menus = $this->menuObj->getAll(['customer_id' => $customerID]);
        $this->data['items'] = collect();
        foreach($menus as $menu){
            $this->data['items'] = $this->data['items']->merge($menu->items);
        }
        $this->data['items'] = $this->data['items']->all();
        $this->data['ingredientWarnings'] = $this->ingredientWarningObj->getAll();

        return view('diner.item.create', $this->data);
    }

    public function store(ItemRequest $request, $menu_id)
    {
        $prices = [];
        $prices = $request->prices ?? [];
        $pricestags = $request->price_tags ?? [];

        if(
          (!$prices[0] && !$pricestags[0]) 
        ) {
          return redirect()->back()->with(['fail' => 'Add one price at least.']);
        }

        $data = $request->only([
          'name','description','calories','portion','preparation_time','notes',
        ]);
        $data['menu_id'] = $menu_id;


        $menu = $this->menuObj->find(['id' => $menu_id]);
        if (!$menu) {
            return view('diner.error.404');
        }
        $customerID = Auth::guard('diner')->user() ? Auth::guard('diner')->user()->id : $menu->customer_id;

        if ($request->hasfile('image')) {
            $media = $this->helperObj->upload($request->file('image'), ['title' => $data['name'], 'customer_id' => $customerID]);
            $data['image_id'] = $media->id;
        }
        if ($request->hasfile('video')) {
            $video = $this->helperObj->upload($request->file('video'), ['title' => $data['name'].time(), 'customer_id' => $customerID]);
            $data['video_id'] = $video->id;
        }


        $item = $this->itemObj->insert($data);
        if ($item) {
            if (count($prices)) {
                $prices_insert = [];
                foreach ($prices as $key=>$price) {
                    $obj = [];
                    $obj['item_id'] = $item->id;
                    $obj['price'] = $price?? 0;
                    $obj['description'] = $pricestags[$key]?? '';
                    if($obj['price'] && $obj['description'])
                      $prices_insert[] = $obj;
                }
                if (count($prices_insert)) {
                    $this->itemPriceObj->bulkInsert($prices_insert);
                }
            }

            $side_dishes = $request->side_dishes;
            if (count($side_dishes) && array_slice($side_dishes, 0, 1)[0]['id']) {
                $side_dishes_insert = [];
                foreach ($side_dishes as $side_dish) {
                    $obj = [];
                    $originSideDish = $this->sideDishObj->find(['id'=>$side_dish['id']]);
                    if($originSideDish) {
                        $obj['item_id'] = $item->id;
                        $obj['side_dish_id'] = $originSideDish->id;
                        // $obj['name'] = $originSideDish->name;
                        $obj['price'] = (float)$side_dish['price'] ? $side_dish['price'] : $originSideDish->price;
                        $side_dishes_insert[] = $obj;
                    }
                }
                if (count($side_dishes_insert)) {
                    $this->itemSideDishObj->bulkInsert($side_dishes_insert);
                }
            }

            /* OPTIONS */
            $options = $request->options;
            if (count($options) && array_slice($options, 0, 1)[0]['id']) {
                $options_insert = [];
                foreach ($options as $option) {
                    $obj = [];
                    $originoOption = $this->optionObj->find(['id'=>$option['id']]);
                    if($originoOption && count($option['values']) ) {
                        $obj['item_id'] = $item->id;
                        $obj['option_id'] = $option['id'];
                        // $obj['name'] = $originoOption->name;
                        // $obj['type'] = $option['type'];
                        $singleItemOption = $this->itemOptionObj->insert($obj);

                        $opvalues = [];
                        foreach($option['values'] as $key => $optionValue) {
                          $opv = [];
                          $opv['item_option_id'] = $singleItemOption->id;
                          $opv['value'] = $optionValue;
                          $opv['price'] = $option['prices'][$key] ?? 0;
                          if($optionValue)
                              $opvalues[] = $opv;
                        }

                        $this->itemOptionValuesObj->bulkInsert($opvalues);

                    }
                }
            }


            $ingredient_warning_ids = $request->ingredient_warning_ids;
            if ($request->filled('ingredient_warning_ids') && count($ingredient_warning_ids)) {
                foreach ($ingredient_warning_ids as $key => $value) {
                    $warning = $this->ingredientWarningObj->find(['id'=>$value]);
                    if($warning) {
                        $obj = [];
                        $obj['item_id'] = $item->id;
                        $obj['ingredient_warning_id'] = $warning->id;
                        $obj['name'] = $warning->name;
                        $this->itemIngredientWarningObj->insert($obj);
                    }
                }
                
            }

            if($request->recommend){
                $item->recommendeditems()->attach($request->recommend);
            }

            /*$ingredient_warning_id = $request->ingredient_warning_id;
            if (count($ingredient_warnings)) {
                $warnings_insert = [];
                foreach ($ingredient_warnings as $warning) {
                    $obj = [];
                    $obj['item_id'] = $item->id;
                    $obj['ingredient_warning_id'] = $warning['ingredient_warning_id'];
                    $warnings_insert[] = $obj;
                }
                if (count($warnings_insert)) {
                    $this->itemIngredientWarningObj->bulkInsert($warnings_insert);
                }
            }

            $item_options = $request->item_options;
            if (count($item_options)) {
                foreach ($item_options as $key => $item_option) {
                    $obj = [];
                    $option_values = [];
                    $obj['item_id'] = $item->id;
                    $obj['name'] = $item_option['name'];
                    $obj['type_id'] = $item_option['type_id'];
                    $option = $this->itemOptionObj->insert($obj);
                    if (isset($item_option['values']) && count($item_option['values'])) {
                        foreach ($item_option['values'] as $value) {
                            $option_values[] = ['item_id' => $item->id, 'option_id' => $option->id, 'value' => $value];
                        }
                        $this->itemOptionValuesObj->bulkInsert($option_values);
                    }

                }
            }
            */

            return redirect()->route('diner.item.show', ['menu_id' => $menu_id, 'id' => $item->id])->with(['success' => 'added successfully']);
        } else {
            return redirect()->back()->with('fail', 'Whoops something wrong');
        }

    }

    public function edit($menu_id, $id)
    {
            $customerID = (int)request()->venue_id ? (int)request()->venue_id : $this->data['current_user']->id;
            $item = $this->itemObj->find(['id' => $id, 'menu_id' => $menu_id], ['menu', 'image', 'video', 'priceList', 'sideDishes', 'ingredientWarnings', 'recommendeditems']);
            $this->data['current_item'] = $item;
            if ($this->data['current_item']) {
              $this->data['title'] = (object)[
                  'name' => 'Edit ' . $this->data['current_item']->name,
                  'url' => $customerID ? URL::to('diner/menu/' . $menu_id.'?venue_id='.$customerID) : URL::to('diner/menu/' . $menu_id),
                  'parent' => (object)['name' => 'Menus'],
                  'back' => (object)[
                      'name' => ' ' . $this->data['current_item']->menu->name,
                      'url' => $customerID ? URL::to('diner/menu/' . $menu_id.'?venue_id='.$customerID) : URL::to('diner/menu/' . $menu_id)
                  ]
              ];

            $this->data['sideDishes'] = $this->sideDishObj->getAll(['customer_id' => $customerID]);          
            $this->data['items'] = $this->itemObj->getAll(['menu_id' => $menu_id]);

          $availableOptions = $this->optionObj->getAll();
          $options = $this->itemOptionObj->getAll(['item_id' => $id]);
          if(count($options)) {
            foreach($options as $option) {
              $option['values'] = $this->itemOptionValuesObj->getAll(['item_option_id'=>$option->id]);
            }
          }
          $this->data['options'] = $options;
          $this->data['availableOptions'] = $this->optionObj->getAll(['customer_id' => $customerID]);
          $this->data['ingredientWarnings'] = $this->ingredientWarningObj->getAll();

          $this->data['itemIngredientWarnings'] = $item->ingredientWarnings;
          //return  $item->ingredientWarnings;
          //return $this->data;

          return view('diner.item.edit', $this->data);
        } else {
            
            return view('admin.404');
        }
    }

    //public function update(ItemRequest $request, $menu_id, $id)
    public function update(ItemRequest $request, $menu_id, $id)
    {
        $prices = [];
        $request->all();
        $prices = $request->prices ?? [];
        $pricestags = $request->price_tags ?? [];

        if(
            (!$prices[0] && !$pricestags[0]) 
            ) {
            return redirect()->back()->with(['fail' => 'Add one price at least.']);
        }

        $current_item = $this->itemObj->find(['id' => $id, 'menu_id' => $menu_id]);
        if ($current_item) {
            $data = $request->only([
              'name','description','calories','portion','preparation_time','notes',
            ]);
            //$data = $request->except(['_token', 'file', 'video', 'prices', 'ingredient_warnings', 'side_dishes', 'item_options']);


            $menu = $this->menuObj->find(['id' => $menu_id]);
            if (!$menu) {
                return view('diner.error.404');
            }

            $customerID = Auth::guard('diner')->user() ? Auth::guard('diner')->user()->id : $menu->customer_id;

            if ($request->has('image')) {
                if ($current_item->image) {
                    $media = $this->helperObj->upload($request->file('image'), ['title' => $data['name'], 'customer_id' => $customerID], 'edit', $current_item->image);
                } else {
                    $media = $this->helperObj->upload($request->file('image'), ['title' => $data['name'], 'customer_id' => $customerID]);
                }
                $data['image_id'] = $media->id;
            }
            if ($request->has('video')) {
                if ($current_item->image) {
                    $video = $this->helperObj->upload($request->file('video'), ['title' => $data['name'], 'customer_id' => $customerID], 'edit', $current_item->video->media_path);
                } else {
                    $video = $this->helperObj->upload($request->file('video'), ['title' => $data['name'], 'customer_id' => $customerID]);
                }
                $data['video_id'] = $video->id;
            }


            $item = $this->itemObj->update(['id' => $id, 'menu_id' => $menu_id], $data);

            if ($item) {

                $optionEditedIDs = $this->itemOptionObj->getAll(['item_id'=>$id])->pluck('id')->toArray();
                $this->itemOptionValuesObj->deleteFrom('item_option_id',$optionEditedIDs);
                $this->itemPriceObj->delete(['item_id'=>$id]);
                $this->itemIngredientWarningObj->delete(['item_id'=>$id]);
                $this->itemSideDishObj->delete(['item_id'=>$id]);
                $this->itemOptionObj->delete(['item_id'=>$id]);

                if (count($prices)) {
                    $prices_insert = [];
                    foreach ($prices as $key=>$price) {
                        $obj = [];
                        $obj['item_id'] = $current_item->id;
                        $obj['price'] = $price?? 0;
                        $obj['description'] = $pricestags[$key]?? '';
                        if($obj['price'] && $obj['description'])
                          $prices_insert[] = $obj;
                    }
                    if (count($prices_insert)) {
                        $this->itemPriceObj->bulkInsert($prices_insert);
                    }
                }

                $side_dishes = $request->side_dishes;
                if ($side_dishes && count($side_dishes) && array_slice($side_dishes, 0, 1)[0]['id']) {
                    $side_dishes_insert = [];
                    foreach ($side_dishes as $side_dish) {
                        $obj = [];
                        $originSideDish = $this->sideDishObj->find(['id'=>$side_dish['id']]);
                        //return $originSideDish;
                        if($originSideDish) {
                            $obj['item_id'] = $current_item->id;
                            $obj['side_dish_id'] = $originSideDish->id;
                            // $obj['name'] = $originSideDish->name;
                            $obj['price'] = (float)$side_dish['price'] ? $side_dish['price'] : $originSideDish->price;
                            $side_dishes_insert[] = $obj;
                        }
                    }
                    if (count($side_dishes_insert)) {
                        $this->itemSideDishObj->bulkInsert($side_dishes_insert);
                    }
                }

                /* OPTIONS */
                $options = $request->options;

                if ($options && count($options) && array_slice($options, 0, 1)[0]['id']) {
                    foreach ($options as $option) {
                        if(isset($option['id'])) {
                            $obj = [];
                            $originoOption = $this->optionObj->find(['id'=>$option['id']]);
                            //return $option;
                            if($originoOption && count($option['values']) ) {
                                $obj['item_id'] = $current_item->id;
                                $obj['option_id'] = $option['id'];
                                // $obj['name'] = $originoOption->name;
                                // $obj['type'] = $option['type'];
                                $singleItemOption = $this->itemOptionObj->insert($obj);

                                $opvalues = [];
                                foreach($option['values'] as $key => $optionValue) {
                                  $opv = [];
                                  $opv['item_option_id'] = $singleItemOption->id;
                                  $opv['value'] = $optionValue;
                                  $opv['price'] = $option['prices'][$key] ?? 0;
                                  if($optionValue)
                                      $opvalues[] = $opv;
                                }
                                $this->itemOptionValuesObj->bulkInsert($opvalues);
                            }
                        }

                    }
                }

                $ingredient_warning_ids = $request->ingredient_warning_ids;
                if ($request->filled('ingredient_warning_ids') && count($ingredient_warning_ids)) {
                    foreach ($ingredient_warning_ids as $key => $value) {
                        $warning = $this->ingredientWarningObj->find(['id'=>$value]);
                        if($warning) {
                            $obj = [];
                            $obj['item_id'] = $item->id;
                            $obj['ingredient_warning_id'] = $warning->id;
                            $obj['name'] = $warning->name;
                            $this->itemIngredientWarningObj->insert($obj);
                        }
                    }
                    
                }


                if($request->recommend){
                    $current_item->recommendeditems()->detach();
                    $current_item->recommendeditems()->attach($request->recommend);
                }
                else{
                    $current_item->recommendeditems()->detach();
                }


                /*$prices = $request->prices;
                if (count($prices)) {
                    $prices_insert = [];
                    foreach ($prices as $price) {
                        $obj = [];
                        $obj['item_id'] = $id;
                        $obj['price'] = $price['value'];
                        $obj['description'] = $price['description'];
                        $prices_insert[] = $obj;
                    }
                    if (count($prices_insert)) {
                        $this->itemPriceObj->bulkInsert($prices_insert);
                    }
                }

                $ingredient_warnings = $request->ingredient_warnings;
                if (count($ingredient_warnings)) {
                    $warnings_insert = [];
                    foreach ($ingredient_warnings as $warning) {
                        $obj = [];
                        $obj['item_id'] = $item->id;
                        $obj['ingredient_warning_id'] = $warning['ingredient_warning_id'];
                        $warnings_insert[] = $obj;
                    }
                    if (count($warnings_insert)) {
                        $this->itemIngredientWarningObj->bulkInsert($warnings_insert);
                    }
                }

                $side_dishes = $request->side_dishes;
                if (count($side_dishes)) {
                    $side_dishes_insert = [];
                    foreach ($side_dishes as $side_dish) {
                        $obj = [];
                        $obj['item_id'] = $item->id;
                        $obj['side_dish_id'] = $side_dish['side_dish_id'];
                        $obj['price'] = $side_dish['price'];
                        $side_dishes_insert[] = $obj;
                    }
                    if (count($side_dishes_insert)) {
                        $this->itemSideDishObj->bulkInsert($side_dishes_insert);
                    }
                }


                $item_options = $request->item_options;
                if (count($item_options)) {
                    foreach ($item_options as $key => $item_option) {
                        $obj = [];
                        $option_values = [];
                        $obj['item_id'] = $item->id;
                        $obj['name'] = $item_option['name'];
                        $obj['type_id'] = $item_option['type_id'];
                        $option = $this->itemOptionObj->insert($obj);
                        if (isset($item_option['values']) && count($item_option['values'])) {
                            foreach ($item_option['values'] as $value) {
                                $option_values[] = ['item_id' => $item->id, 'option_id' => $option->id, 'value' => $value];
                            }
                            $this->itemOptionValuesObj->bulkInsert($option_values);
                        }

                    }
                }*/


                return redirect()->route('diner.item.show', ['menu_id' => $menu_id, 'id' => $current_item->id])->with(['success' => 'added successfully']);
            } else {
                return redirect()->back()->with('fail', 'Whoops something wrong');
            }
        } else {
            return view('admin.404');
        }
    }

    public function destroy($menu_id, $id)
    {
        $deleted = $this->itemObj->delete(['id' => $id, 'menu_id' => $menu_id]);
        if($deleted) {
          $optionIDs = $this->itemOptionObj->getAll(['item_id'=>$id])->pluck('id')->toArray();
          $this->itemOptionValuesObj->deleteFrom('item_option_id',$optionIDs);
          $this->itemPriceObj->delete(['item_id'=>$id]);
          $this->itemIngredientWarningObj->delete(['item_id'=>$id]);
          $this->itemSideDishObj->delete(['item_id'=>$id]);
          $this->itemOptionObj->delete(['item_id'=>$id]);
          $this->arrangeObj->delete(['arrangmentable_id' => $id ]);
        }
        return redirect()->route('diner.item.index', ['menu_id' => $menu_id])->with(['success' => 'Item was removed successfully']);
    }

}
