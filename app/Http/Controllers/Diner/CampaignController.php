<?php

namespace App\Http\Controllers\Diner;

use App\Http\Requests\Diner\CampaignRequest;
use App\Http\Services\Diner\CampaignService;
use App\Http\Services\Admin\CustomerServices;
use App\Http\Services\Diner\ItemService;
use App\Http\Services\Common\Helper;

use Illuminate\Http\Request;
Use URL;

class CampaignController extends DinerController
{
    private $campaignObj;
    private $customerObj;
    private $itemObj;
    private $helperObj;

    public function __construct(
        CampaignService $campaignObj,
        CustomerServices $customerObj,
        ItemService $itemObj,
        Helper $helperObj
    )
    {
        parent::__construct();

        $this->campaignObj = $campaignObj;
        $this->customerObj = $customerObj;
        $this->itemObj = $itemObj;
        $this->helperObj = $helperObj;
    }

    public function index(Request $request)
    {
        $conditions = [['id', '>', 0]];
        $customer = $this->data['user_branch'];
        $conditions['customer_id'] = $customer->id;
        $customerID = (int)$request->venue_id ? (int)$request->venue_id : 0;
        $this->data['venue_id'] = (int)$request->venue_id ? (int)$request->venue_id : $customer->id;


        if ($request->filled('item_id')) {
            $conditions['id'] = $request->item_id;
        }
        if ($request->filled('active')) {
            $conditions['active'] = $request->active;
        }
        if ($request->filled('venue_id')) {
            $conditions['customer_id'] = $request->venue_id;
        }
        $per_page = $request->per_page ? $request->per_page : 20;

        $this->data['campaigns'] = $this->campaignObj->getAll($conditions, [], $per_page);


        $total_count = count($this->data['campaigns']);

        $this->data['title'] = (object)[
            'name' => 'Campaigns',
            //'url' => URL::to('admin/campaign'),
            'child' => (object)[
                'name' => 'Create Campaign',
                'url' => $customerID ? URL::to('diner/campaign/create?venue_id='.$customerID) : URL::to('diner/campaign/create')
            ]
        ];

        return view('diner.campaign.index', $this->data);

    }

    public function create(Request $request)
    {
        $customerID = (int)$request->venue_id ? (int)$request->venue_id : 0;

        $customer = (int)$request->venue_id ? $this->customerObj->find(['id'=>(int)$request->venue_id]): $this->data['user_branch'];
        $this->data['items'] = $customer ? $customer->items() : [];

        $this->data['title'] = (object)[
            'name' => 'Create New Campaign',
            'url' => $customerID ? URL::to('diner/campaign?venue_id='.$customerID) : URL::to('diner/campaign'),
            'parent' => (object)['name' => 'Campaigns']];
        //$this->data['countries'] = $this->countryObj->getAll([['id', '>', 0]]);

        $this->data['venue_id'] = (int)$request->venue_id ? (int)$request->venue_id : 0;
        $this->data['customer_id'] = (int)$request->venue_id ? (int)$request->venue_id : $this->data['user_branch']->id;
        return view('diner.campaign.create', $this->data);
    }

    public function store(CampaignRequest $request)
    {
        $data = $request->except(['_token','image','video']);

        $campaign = $this->campaignObj->insert($data);
        if ($campaign) {
            if ($request->hasFile('image')) {
              $media = $this->helperObj->upload($request->file('image'),['title' => $data['title'], 'customer_id' => $campaign->customer_id]);
              $this->campaignObj->update(['id'=>$campaign->id],['image_id'=>$media->id]);
            }
            if ($request->hasFile('video')) {
              $media = $this->helperObj->upload($request->file('video'),['title' => $data['title'], 'customer_id' => $campaign->customer_id]);
              $this->campaignObj->update(['id'=>$campaign->id],['video_id'=>$media->id]);
            }
            return $this->data['user_branch']->id != $campaign->customer_id ?
                redirect()->route('diner.campaign.index', ['venue_id' => $campaign->customer_id])->with(['success' => 'added successfully']) :
                redirect()->route('diner.campaign.index')->with(['success' => 'added successfully']);
        } else {
            return redirect()->back()->with('fail', 'Whoops something wrong');
        }

    }

    public function show($id)
    {
        $this->data['current_venue'] = $this->customerObj->find(['id' => $id]);
        if ($this->data['current_venue']) {
            $this->data['title'] = (object)['name' => 'Customer Details', 'url' => URL::to('admin/customer'),
                'additional' => (object)['name' => 'Login AS ' . $this->data['current_venue']->name, 'url' => URL::to('admin/login/customer/' . $this->data['current_venue']->id), 'class' => 'white-btn'],
                'child' => (object)['name' => 'Edit Customer', 'url' => URL::to('admin/customer/' . $this->data['current_venue']->id . '/edit'), 'subtitle' => $this->data['current_venue']->name]
            ];
            return view('diner.venue.show', $this->data);
        } else {
            return view('admin.404');
        }

    }

    public function edit($id)
    {
        $customerID = (int)request()->venue_id ? (int)request()->venue_id : 0;

            //$customer = $this->data['user_branch'];
        $customer = (int)request()->venue_id ? $this->customerObj->find(['id'=>(int)request()->venue_id]): $this->data['user_branch'];
        $this->data['customer_id'] = $customer->id;
        $this->data['items'] = $customer ? $customer->items() : [];

        $currentCampaign = $this->campaignObj->find(['id' => $id]);
        $this->data['current_campaign'] = $currentCampaign;

        if ($this->data['current_campaign']) {
            $this->data['title'] = (object)[
                'name' => 'Edit '.$currentCampaign->title,
                'url' => $customerID ? URL::to('diner/campaign?venue_id=').$customerID : URL::to('diner/campaign').$customerID,
                'parent' => (object)['name' => 'Campaign'
            ]];

            return view('diner.campaign.edit', $this->data);
        } else {
            return view('diner.404');
        }
    }

    public function update(CampaignRequest $request, $id)
    {
        $data = $request->except(['_token','_method','image','video','content_type']);

        $campaign = $this->campaignObj->find(['id'=>$id]);
        if ($campaign) {
            if ($request->hasfile('image')) {
                $media = $this->helperObj->upload($request->file('image'), ['title' => $data['title'], 'customer_id' => $campaign->customer_id],'edit', $campaign->image);
                $data['image_id'] = $media->id;
            }
            if ($request->hasfile('video')) {
                $media = $this->helperObj->upload($request->file('video'), ['title' => $data['title'], 'customer_id' => $campaign->customer_id],'edit', $campaign->video);
                $data['video_id'] = $media->id;
            }

            switch($request->content_type) {
                case "url" : {
                    $data['image_id'] = NULL;
                    $data['video_id'] = NULL;
                }break;
                case "image" : {
                    $data['url'] = NULL;
                    $data['video_id'] = NULL;
                }break;
                case "video" : {
                    $data['url'] = NULL;
                    $data['image_id'] = NULL;
                }break;
            }

            $this->campaignObj->update(['id'=>$id],$data);
            return $this->data['user_branch']->id != $campaign->customer_id ?
                redirect()->route('diner.campaign.index', ['venue_id' => $campaign->customer_id])->with(['success' => 'added successfully']) :
                redirect()->route('diner.campaign.index')->with(['success' => 'added successfully']);
        } else {
            return redirect()->back()->with('fail', 'Whoops something wrong');
        }
    }


    public function destroy($id)
    {
        //$customer = (int)request()->venue_id ? $this->customerObj->find(['id'=>request()->venue_id]) : $this->data['user_branch'];
        $campaign = $this->campaignObj->find(['id'=>$id]);
        $this->campaignObj->delete(['id' => $id]);
        return $this->data['user_branch']->id != $campaign->customer_id ?
            redirect()->route('diner.campaign.index', ['venue_id' => $campaign->customer_id])->with(['success' => 'Campaign was removed successfully']) :
            redirect()->route('diner.campaign.index')->with(['success' => 'Campaign was removed successfully']);
    }

}
