<?php

namespace App\Http\Controllers\Diner;

use App\Http\Services\Diner\FeedbackService;
use App\Http\Services\Diner\FeedbackQuestionService;
use App\Http\Services\Diner\FeedbackInteractionService;
use App\Http\Services\Admin\CustomerServices;
use App\Http\Services\Common\Helper;

use Illuminate\Http\Request;
Use URL;

class FeedbackController extends DinerController
{
    private $feedbackObj;
    private $fbQuestionObj;
    private $fbInteractionObj;
    private $customerObj;
    private $helperObj;

    public function __construct(
        FeedbackService $feedbackObj,
        FeedbackQuestionService $fbQuestionObj,
        FeedbackInteractionService $fbInteractionObj,
        CustomerServices $customerObj,
        Helper $helperObj
    )
    {
        parent::__construct();

        $this->feedbackObj = $feedbackObj;
        $this->fbQuestionObj = $fbQuestionObj;
        $this->fbInteractionObj = $fbInteractionObj;
        $this->customerObj = $customerObj;
        $this->helperObj = $helperObj;
    }

    public function index(Request $request)
    {
        $customer =  (int)$request->venue_id ? $this->customerObj->find(['id'=>(int)$request->venue_id]): $this->data['user_branch'];
        $conditions['customer_id'] = $customer->id;
        $this->data['venue_id'] = (int)$request->venue_id ? (int)$request->venue_id : 0;

        if ($request->filled('active')) {
            $conditions['active'] = $request->active;
        }
        if ($request->filled('name')) {
            $conditions['name'] = $request->name;
        }
        if ($request->filled('venue_id')) {
            $conditions['customer_id'] = $request->venue_id;
        }
        $per_page = $request->per_page ? $request->per_page : 20;

        $this->data['feedbacks'] = $this->feedbackObj->getAll($conditions, ['questions'], $per_page);

        //return $this->data['feedbacks'];
        $total_count = count($this->data['feedbacks']);

        $this->data['title'] = (object)[
            'name' => 'Feedback',
            //'url' => URL::to('admin/feedback'),
            'child' => (object)[
                'name' => 'Create a form',
                'url' => $this->data['venue_id'] ? URL::to('diner/feedback/create?venue_id='.$this->data['venue_id']) : URL::to('diner/feedback/create')
            ]
        ];

        return view('diner.feedback.index', $this->data);
    }

    public function create(Request $request)
    {
        $customerID = (int)$request->venue_id ? (int)$request->venue_id : 0;

        $customer = (int)$request->venue_id ? $this->customerObj->find(['id'=>(int)$request->venue_id]): $this->data['user_branch'];
        $this->data['items'] = $customer ? $customer->items() : [];

        $this->data['title'] = (object)[
            'name' => 'Create New Feedback Form',
            'url' => $customerID ? URL::to('diner/feedback?venue_id='.$customerID) : URL::to('diner/feedback'),
            'parent' => (object)['name' => 'Campaigns']];
        //$this->data['countries'] = $this->countryObj->getAll([['id', '>', 0]]);

        $this->data['venue_id'] = (int)$request->venue_id ? (int)$request->venue_id : 0;
        $this->data['customer_id'] = (int)$request->venue_id ? (int)$request->venue_id : $this->data['user_branch']->id;
        return view('diner.feedback.create', $this->data);
    }

    public function store(Request $request)
    {
        //return $request->all();
        $data = $request->only(['customer_id','form_name','form_header']);

        $feedback = $this->feedbackObj->insert($data);
        if ($feedback) {
            if(count($request->questions)) {
                $questions = [];
                foreach ($request->questions as $question) {
                    if($question['content'] != null) {
                        $item = [];
                        $item['customer_id'] = $data['customer_id'];
                        $item['feedback_id'] = $feedback->id;
                        $item['value'] = $question['content'];
                        $item['type'] = $question['type'];
                        $item['mandatory'] = isset($question['mandatory']) ? 1 : 0;
                        $questions[] = $item;
                    }
                }
                if(count($questions))
                    $this->fbQuestionObj->bulkInsert($questions);
            }
            return $this->data['user_branch']->id != $feedback->customer_id ?
                redirect()->route('diner.feedback.index', ['venue_id' => $feedback->customer_id])->with(['success' => 'added successfully']) :
                redirect()->route('diner.feedback.index')->with(['success' => 'added successfully']);
        } else {
            return redirect()->back()->with('fail', 'Whoops something wrong');
        }

    }

    public function show($id)
    {
        $this->data['current_venue'] = $this->customerObj->find(['id' => $id]);
        if ($this->data['current_venue']) {
            $this->data['title'] = (object)['name' => 'Customer Details', 'url' => URL::to('admin/customer'),
                'additional' => (object)['name' => 'Login AS ' . $this->data['current_venue']->name, 'url' => URL::to('admin/login/customer/' . $this->data['current_venue']->id), 'class' => 'white-btn'],
                'child' => (object)['name' => 'Edit Customer', 'url' => URL::to('admin/customer/' . $this->data['current_venue']->id . '/edit'), 'subtitle' => $this->data['current_venue']->name]
            ];
            return view('diner.venue.show', $this->data);
        } else {
            return view('admin.404');
        }

    }

    public function edit($id)
    {
        $customer = (int)request()->venue_id ? $this->customerObj->find(['id'=>(int)request()->venue_id]): $this->data['user_branch'];
        $this->data['customer_id'] = $customer->id;
        $customerID = (int)request()->venue_id ? (int)request()->venue_id : 0;
        $currentFeedback = $this->feedbackObj->find(['id' => $id],['questions']);
        $this->data['current_feedback'] = $currentFeedback;

        if ($this->data['current_feedback']) {
            $this->data['title'] = (object)[
                'name' => 'Edit '.$currentFeedback->form_name,
                'url' => $customerID ? URL::to('diner/feedback?venue_id='.$customerID) : URL::to('diner/feedback'),
                'parent' => (object)['name' => 'Feedback']
            ];

            return view('diner.feedback.edit', $this->data);
        } else {
            return view('diner.404');
        }
    }

    public function update(Request $request, $id)
    {
        $data = $request->except(['_token','_method','questions']);
        $feedback = $this->feedbackObj->find(['id'=>$id]);
        if ($feedback) {
            $this->feedbackObj->update(['id'=>$id],$data);
            
            if(count($request->questions)) {
                $this->fbQuestionObj->deleteQuestion(array_keys($request->questions), $id);
                $oldquestions = $request->questions;
                unset($oldquestions[0]);
                foreach ($oldquestions as $key => $question) {
                    if($question['content'] != null) {
                        $check = $this->fbQuestionObj->find(['id' => $key, 'feedback_id' => $id]);
                        if($check){
                            $this->fbQuestionObj->update(['id' => $key, 'feedback_id' => $id], [
                                'value' => $question['content'],
                                'type'  => $question['type']
                            ]);
                        }
                        else{
                            $item = [];
                            $item['customer_id'] = $data['customer_id'];
                            $item['feedback_id'] = $id;
                            $item['type'] = $question['type'];
                            $item['mandatory'] = isset($question['mandatory']) ? 1 : 0;
                            $newitem = $this->fbQuestionObj->insert($item);
                            $newitem->{'value:en'} = $question['content'];
                            $newitem->save();

                        }
                        
                    }
                }    
            }
            return $this->data['user_branch']->id != $feedback->customer_id ?
                redirect()->route('diner.feedback.index', ['venue_id' => $feedback->customer_id])->with(['success' => 'Updated successfully']) :
                redirect()->route('diner.feedback.index')->with(['success' => 'Updated successfully']);
        } else {
            return redirect()->back()->with('fail', 'Whoops something wrong');
        }
    }

    public function openClose($id)
    {
        $feedback = $this->feedbackObj->find(['id'=>$id]);
        if($feedback) {
            $active = $feedback->active ? 0 : 1;
            $this->feedbackObj->update(['id'=>$id],['active'=>$active]);
            return redirect()->back()->with(['success' => 'Status changed successfully.']);
        }
        return redirect()->back()->with(['fail' => 'This interactive not found']);
    }

    public function destroy($id)
    {
        //$customer = (int)request()->venue_id ? $this->customerObj->find(['id'=>request()->venue_id]) : $this->data['user_branch'];
        $feedback = $this->feedbackObj->find(['id'=>$id]);
        $this->feedbackObj->delete(['id' => $id]);
        $this->fbQuestionObj->delete(['feedback_id'=>$id]);
        $this->fbInteractionObj->delete(['form_id'=>$id]);
        return $this->data['user_branch']->id != $feedback->customer_id ?
            redirect()->route('diner.feedback.index', ['venue_id' => $feedback->customer_id])->with(['success' => 'Feedback was removed successfully']) :
            redirect()->route('diner.feedback.index')->with(['success' => 'Feedback was removed successfully']);
    }

    public function getsurvey($id){
        $obj = $this->feedbackObj->find(['id' => $id], ['questions']);
        return $obj;
    }

}
