<?php

namespace App\Http\Controllers\Diner;

use App\Http\Requests\Diner\MenuRequest;
use App\Http\Services\Diner\ItemService;
use App\Http\Services\Diner\MenuService;
use App\Http\Services\Diner\ArrangmentService;
use App\Http\Services\Common\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
Use URL;

class MenuController extends DinerController
{
    private $menuObj;
    private $itemObj;
    private $arrangmentObj;
    private $helperObj;
    private $customer_id;

    public function __construct(MenuService $menuObj, ItemService $itemObj, Helper $helperObj,ArrangmentService $arrangmentObj)
    {
        parent::__construct();
        $this->menuObj = $menuObj;
        $this->itemObj = $itemObj;
        $this->arrangmentObj = $arrangmentObj;
        $this->helperObj = $helperObj;
    }

    public function index(Request $request)
    {
        // return $this->data['current_user'];
        //try{
            $customerID = (int)$request->venue_id ? (int)$request->venue_id : $this->data['current_user']->id;

            //return $this->data['current_user'];

            $this->data['venue_id'] = (int)$request->venue_id ? (int)$request->venue_id : 0;

            $conditions = ['customer_id' => $customerID, 'parent_id' => 0];


            $this->data['menus'] = $this->menuObj->getAll($conditions);
            $total_count = $this->menuObj->getTotalCount($conditions);

            $this->data['title'] = (object)[
                'name' => 'Menu', 'url' => route('diner.menu.index',['venue_id' => $customerID]),
                'child' => (object)['name' => 'Create Menu', 'url' => URL::to('diner/menu/create?venue_id='.$customerID), 'subtitle' => $total_count . ' Menus'],
            ];
            return view('diner.menu.index', $this->data);
            //$message = "asd";
        //}catch(\Exception $e){
            //$message = $e->getMessage();
        //} finally {
            //dd($message) ;
            //return view('diner.menu.index', $this->data);
        //}
        

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->data['venue_id'] = (int)$request->venue_id ? (int)$request->venue_id : 0;

        $this->data['title'] = (object)['name' => 'Create Menu', 'url' => URL::to('diner/menu'),
            'parent' => (object)['name' => 'Menus'], 'back' => (object)['name' => 'Menus', 'url' => URL::to('diner/menu')]];

        return view('diner.menu.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function store(MenuRequest $request)
    {
        $customerID = (int)$request->venue_id ? (int)$request->venue_id : Auth::guard('diner')->user()->id;

        $data = $request->except(['_token', 'file', 'video']);

        if ($request->has('file')) {

            $media = $this->helperObj->upload($request->file('file'), ['title' => $data['name'], 'customer_id' => Auth::guard('diner')->user()->id]);

            $data['media_id'] = $media->id;
        }
        if ($request->has('video')) {
            $video = $this->helperObj->upload($request->file('video'), ['title' => $data['name'], 'customer_id' => Auth::guard('diner')->user()->id]);
            $data['video_id'] = $video->id;
        }

        $data['customer_id'] = $customerID;

        $menu = $this->menuObj->insert($data);

        if ($menu) {
            return redirect()->route('diner.menu.edit', ['id' => $menu->id])->with(['success' => 'added successfully']);
        } else {
            return redirect()->back()->with('fail', 'Whoops something wrong');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $customerID = (int)$request->venue_id ? (int)$request->venue_id : 0;
        
        $this->data['venue_id'] = (int)$request->venue_id ? (int)$request->venue_id : Auth::guard('diner')->user()->id;
        $this->data['current_menu'] = $this->menuObj->find(['id' => $id]);
        $parentID = $this->data['current_menu']->parent_id;

        #======================= Items Part
        $this->data['items'] = [];
        //$currentSubmenu = $this->menuObj->find(['id'=>$id]);
        $menuID = $id ? $id : 0;
        $items = $this->itemObj->getAll([['menu_id', '=', $menuID]]);
        if (count($items)) {
            foreach ($items as $item) {
                $obj = new \stdClass();
                $obj->id = $item->id;
                $obj->is_active = $item->is_active;
                $obj->menu_id = $menuID;
                $obj->name = $item->name;
                $obj->description = $item->description;
                $obj->portion = $item->portion;
                $obj->preparation_time = $item->preparation_time;
                $obj->calories = $item->calories;
                $obj->notes = $item->notes;
                $obj->item_image = $item->FullThumbnailImagePath('500X500');
                $this->data['items'][] = $obj;
            }
        }
        #======================= End Items Part
        $backUrl = $customerID ?
            ($parentID ?
                URL::to('diner/menu/'.$parentID.'?venue_id='.$customerID) :
                URL::to('diner/menu?venue_id='.$customerID))
            :
            ($parentID ?
                URL::to('diner/menu/'.$parentID) :
                URL::to('diner/menu'));
        
        if ($this->data['current_menu']) {
            $this->data['title'] = (object)['name' => $this->data['current_menu']->name . ' Menu',
                'back' => (object)[
                    'name' => 'Menus',
                    'url' => $backUrl
                ],
                'parent' => (object)['name' => 'Menus'],
                'child' => (object)['name' => 'NEW SUBCATEGORY', 'url' => $customerID ? URL::to('diner/menu/' . $id . '/sub-menu/create?venue_id='.$customerID) : URL::to('diner/menu/' . $id . '/sub-menu/create')],
                'extra' => (object)[
                    'name' => 'Create Item',
                    'url' => $customerID ? URL::to('diner/menu/'.$id.'/item/create?venue_id='.$customerID) : URL::to('diner/menu/'.$id.'/item/create'),
                    'subtitle' => ''
                ],
            ];
        $this->data['arrangmentall'] = $this->data['current_menu']->arrangmentall
                ->map(function($arrangment) use ($customerID){
                    $arrangment->name = $arrangment->arrangmentable->name;
                    $arrangment->col_id = $arrangment->arrangmentable->id;
                    $arrangment->type = (strpos($arrangment->arrangmentable_type, 'Menu') !== false) ? 'Menu' : 'Item';
                    $arrangment->editUrl = ($arrangment->type == 'Menu') ? 
                                URL::to('diner/menu/'.$this->data['current_menu']->id.'/sub-menu/'.$arrangment->arrangmentable->id.'/edit?venue_id='.$this->data['venue_id']) : 
                                URL::to('diner/menu/' . $arrangment->arrangmentable->menu_id . '/item/'.$arrangment->arrangmentable->id.'/edit');
                    $arrangment->showUrl = ($arrangment->type == 'Menu') ? 
                                ($customerID ? 
                                    URL::to('diner/menu/'.$arrangment->arrangmentable->id .'?venue_id='.$customerID) : 
                                    URL::to('diner/menu/'.$arrangment->arrangmentable->id)  
                                ) : 
                                ($customerID ?
                                    URL::to('diner/menu/' . $arrangment->arrangmentable->menu_id . '/item/'.$arrangment->arrangmentable->id . '?venue_id='.$customerID):                               
                                    URL::to('diner/menu/' . $arrangment->arrangmentable->menu_id . '/item/'.$arrangment->arrangmentable->id)                               
                                ) ;
                                
                    $arrangment->image = $arrangment->arrangmentable->FullThumbnailImagePath('300X300');
                    $arrangment->is_active = $arrangment->arrangmentable->is_active;
                    $arrangment->is_last_child = $arrangment->arrangmentable->is_last_child ? true : false;
                    return $arrangment;
                });
            return view('diner.menu.show', $this->data);
        } else {
            return view('admin.404');
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['current_menu'] = $this->menuObj->find(['id' => $id]);
        if ($this->data['current_menu']) {
            $this->data['title'] = (object)['name' => 'Edit ' . $this->data['current_menu']->name . ' Menu', 'url' => URL::to('diner/menu'),
                'parent' => (object)['name' => 'Menus'], 'back' => (object)['name' => 'Menus', 'url' => URL::to('diner/menu')]];

            return view('diner.menu.edit', $this->data);
        } else {
            return view('diner.error.404');
        }
    }
    public function duplicate($id)
    {
        $this->data['current_menu'] = $this->menuObj->find(['id' => $id]);
        if ($this->data['current_menu']) {
            $this->data['title'] = (object)['name' => 'Edit ' . $this->data['current_menu']->name . ' Menu', 'url' => URL::to('diner/menu'),
                'parent' => (object)['name' => 'Menus'], 'back' => (object)['name' => 'Menus', 'url' => URL::to('diner/menu')]];

            return view('diner.menu.duplicate', $this->data);
        } else {
            return view('diner.error.404');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */

    public function update(MenuRequest $request, $id)
    {
        $current_menu = $this->menuObj->find(['id' => $id]);
        if ($current_menu) {

            $data = $request->except(['_token', 'file', 'video', '_method']);
            $data['display_similar_items'] = 0;
            $data['mark_as_new'] = 0;
            $data['mark_as_signature'] = 0;
            if ($request->display_similar_items) {
                $data['display_similar_items'] = 1;
            }
            if ($request->mark_as_new) {
                $data['mark_as_new'] = 1;
            }
            if ($request->mark_as_signature) {
                $data['mark_as_signature'] = 1;
            }

            if ($request->has('file')) {
                $old_media = $current_menu->MenuImage;
                $media = $this->helperObj->upload($request->file('file'), ['title' => $data['name'], 'customer_id' => Auth::guard('diner')->user()->id], true, $old_media);

                $data['media_id'] = $media->id;
            }
            if ($request->has('video')) {
                $old_media = $current_menu->MenuVideo;
                $video = $this->helperObj->upload($request->file('video'), ['title' => $data['name'], 'customer_id' => Auth::guard('diner')->user()->id], true, $old_media);
                $data['video_id'] = $video->id;
            }

            $current_menu = $this->menuObj->update(['id' => $id], $data);

            if ($current_menu) {
                return redirect()->back()->with(['success' => 'updated successfully']);
            } else {
                return redirect()->back()->with(['fail' => 'whoops something wrong']);
            }
        } else {
            return view('admin.404');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->menuObj->delete(['id' => $id]);
        $this->arrangmentObj->delete(['arrangmentable_id' => $id ]);
        return redirect()->route('diner.menu.index')->with('success', 'Menu was removed successfully');
    }

}
