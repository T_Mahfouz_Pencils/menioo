<?php

namespace App\Http\Controllers\Diner;

use App\Http\Requests\Diner\SubMenuRequest;
use App\Http\Services\Diner\SubMenuService;
use App\Http\Services\Diner\MenuService;
use App\Http\Services\Diner\MenuSectionsService;
use App\Http\Services\Diner\ArrangmentService;
use App\Http\Services\Common\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
Use URL;

class SubMenuController extends DinerController
{
    private $parentMenuObj;
    private $menuObj;
    private $menuSectionObj;
    private $helperObj;
    private $customer_id;
    private $arrangeObj;

    public function __construct(ArrangmentService $arrangeObj, SubMenuService $menuObj, MenuService $parentMenuObj, Helper $helperObj, MenuSectionsService $menuSectionObj)
    {
        parent::__construct();
        $this->menuObj = $menuObj;
        $this->parentMenuObj = $parentMenuObj;
        $this->helperObj = $helperObj;
        $this->menuSectionObj = $menuSectionObj;
        $this->arrangeObj = $arrangeObj;
        //$this->data['parent_id'] = Route::current()->parameter('parent_id');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $parent_id)
    {
        $customerID = (int)$request->venue_id ? (int)$request->venue_id : 0;
        $this->data['venue_id'] = $customerID;

        $section_id = $request->section_id;
        if ($section_id > 0) {
            $section = $this->menuSectionObj->find(['id' => $section_id, 'menu_id' => $parent_id]);
            if (!$section) {
                return view('admin.404');
            }
        }
        $this->data['section_id'] = $section_id;
        $this->data['parent_id'] = $parent_id;
        $parent_menu = $this->parentMenuObj->find(['id' => $parent_id]);

        $this->data['title'] = (object)['name' => 'Create SubCategory', 'url' => URL::to('diner/menu'),
            'parent' => (object)['name' => 'Menus'], 'back' => (object)['name' => ' ' . $parent_menu->name, 'url' => $customerID ? URL::to('diner/menu/' . $parent_menu->id.'?venue_id='.$customerID) : URL::to('diner/menu/' . $parent_menu->id)]];
        return view('diner.sub-menu.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function store(SubMenuRequest $request, $parent_id)
    {
        $customerID = (int)$request->venue_id ? (int)$request->venue_id : Auth::guard('diner')->user()->id;

        $data = $request->except(['_token', 'file', 'video', 'section_id']);

        if ($request->has('file')) {

            $media = $this->helperObj->upload($request->file('file'), ['title' => $data['name'], 'customer_id' => Auth::guard('diner')->user()->id]);

            $data['media_id'] = $media->id;
        }

        if ($request->has('video')) {
            $video = $this->helperObj->upload($request->file('video'), ['title' => $data['name'], 'customer_id' => Auth::guard('diner')->user()->id]);
            $data['video_id'] = $video->id;
        }

        $data['customer_id'] = $customerID;
        $data['parent_id'] = $parent_id;

        if ($request->section_id > 0) {
            $data['section_id'] = $request->section_id;
        } else {
            $meu_section = $this->menuSectionObj->getLastOne(['menu_id' => $parent_id]);
            if ($meu_section) {
                $data['section_id'] = $meu_section->id;
            }
        }

        $menu = $this->menuObj->insert($data);

        if ($menu) {
            return redirect()->route('diner.sub-menu.edit', ['parent_id' => $parent_id, 'id' => $menu->id])->with(['success' => 'added successfully']);
        } else {
            return redirect()->back()->with('fail', 'Whoops something wrong');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($parent_id, $id)
    {
        $customerID = (int)request()->venue_id ? (int)request()->venue_id : $this->data['user_branch']->id;
        $this->data['current_menu'] = $this->menuObj->find(['id' => $id, 'parent_id' => $parent_id, 'customer_id' => $customerID]);
        if ($this->data['current_menu']) {
            $this->data['title'] = (object)['name' => 'Menu Details', 'url' => URL::to('admin/menu'),
                'child' => (object)['name' => 'Edit Menu', 'url' => URL::to('diner/menu/' . $this->data['current_menu']->id . '/edit'), 'subtitle' => $this->data['current_menu']->name]
            ];


            return view('diner.sub-menu.show', $this->data);
        } else {
            return view('admin.404');
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $parent_id, $id)
    {
        $customerID = (int)$request->venue_id ? (int)$request->venue_id : Auth::guard('diner')->user()->id;
        $this->data['venue_id'] = $customerID;
        $this->data['current_menu'] = $this->menuObj->find(['id' => $id, 'parent_id' => $parent_id, 'customer_id' => $customerID]);

        if ($this->data['current_menu']) {
            $parent_menu = $this->parentMenuObj->find(['id' => $parent_id]);
            $this->data['title'] = (object)['name' => 'Edit ' . $this->data['current_menu']->name . ' Menu', 'url' => URL::to('diner/menu/' . $parent_id),
                'parent' => (object)['name' => 'Menus'], 'back' => (object)['name' => ' ' . $parent_menu->name, 'url' => URL::to('diner/menu/' . $parent_id)]];
            $this->data['parent_id'] = $parent_id;

            return view('diner.sub-menu.edit', $this->data);
        } else {
            return view('diner.error.404');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */

    public function update(SubMenuRequest $request, $parent_id, $id)
    {
        $customerID = (int)$request->venue_id ? (int)$request->venue_id : Auth::guard('diner')->user()->id;

        $current_menu = $this->menuObj->find(['id' => $id, 'parent_id' => $parent_id, 'customer_id' => $customerID]);
        if ($current_menu) {

            $data = $request->except(['_token', 'file', 'video', '_method','venue_id']);
            $data['display_similar_items'] = 0;
            $data['mark_as_new'] = 0;
            $data['mark_as_signature'] = 0;
            if ($request->display_similar_items) {
                $data['display_similar_items'] = 1;
            }
            if ($request->mark_as_new) {
                $data['mark_as_new'] = 1;
            }
            if ($request->mark_as_signature) {
                $data['mark_as_signature'] = 1;
            }

            if ($request->has('file')) {
                $old_media = $current_menu->MenuImage;
                $media = $this->helperObj->upload($request->file('file'), ['title' => $data['name'], 'customer_id' => $customerID], true, $old_media);

                $data['media_id'] = $media->id;
            }
            if ($request->has('video')) {
                $old_media = $current_menu->MenuVideo;
                $video = $this->helperObj->upload($request->file('video'), ['title' => $data['name'], 'customer_id' => $customerID], true, $old_media);
                $data['video_id'] = $video->id;
            }

            if (!$current_menu->section_id) {
                $meu_section = $this->menuSectionObj->getLastOne(['menu_id' => $parent_id]);
                if ($meu_section) {
                    $data['section_id'] = $meu_section->id;
                }

            }
            //dd($data);
            $current_menu = $this->menuObj->update(['id' => $id, 'parent_id' => $parent_id], $data);

            if ($current_menu) {
                return redirect()->back()->with(['success' => 'updated successfully']);
            } else {
                return redirect()->back()->with(['fail' => 'whoops something wrong']);
            }
        } else {
            return view('admin.404');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($parent_id, $id)
    {
        $this->menuObj->delete(['id' => $id, 'parent_id' => $parent_id]);
        $this->arrangeObj->delete(['arrangmentable_id' => $id]);
        return redirect()->route('diner.menu', ['parent_id' => $parent_id])->with(['success' => 'Subcategory was removed successfully']);

    }

}
