<?php

namespace App\Http\Controllers\API;

use App\Http\Services\Admin\CustomerServices;
use App\Http\Services\Admin\CustomerUserServices;
use App\Http\Services\Diner\ConfigrationService;
use App\Http\Services\Diner\TableService;
use App\Http\Services\Diner\DeviceService;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
//use JWTAuth;
use Tymon\JWTAuth\Facades\JWTAuth;

class PublicController extends Controller
{
    private $confObj;
    private $deviceObj;
    private $customerObj;
    private $wiaterObj;
    private $tableObj;
    //private $aboutObj;
    public function __construct(ConfigrationService $confObj, CustomerServices $customerObj, DeviceService $deviceObj, CustomerUserServices $wiaterObj, TableService $tableObj)
    {
        $this->confObj = $confObj;
        $this->deviceObj = $deviceObj;
        $this->customerObj = $customerObj;
        $this->wiaterObj = $wiaterObj;
        $this->tableObj = $tableObj;
        //$this->aboutObj = $aboutObj;
    }

    public function login(Request $request)
    {
        //return $request->all();
        $credentials = [
            'customer_id' => $request->branch_id,
            'id' => $request->bincode,
        ];

        $waiter = $this->wiaterObj->find($credentials);

        if (!$waiter) {
            $message = 'credential error!';
            return parent::jsonResponse(401, $message);
        }

        if (! $token = Auth::guard('api')->login($waiter)) {
            $message = 'credential error!';
            return parent::jsonResponse(401, $message);
        }

        $auth = $this->respondWithToken($token,'api');

        $data = array_merge($waiter->toArray(), $auth);

        $message = 'Done';
        return parent::jsonResponse(200, $message, $data);
    }

    public function venueLogin(Request $request)
    {
        $deviceInfo = $request->only(['uid','model','app_version','os']);

        $credentials = [
            'id' => $request->branch_id,
            'email' => $request->email,
        ];
        $customer = $this->customerObj->find($credentials);

        if (!$customer) {
            $message = 'credential error!';
            return parent::jsonResponse(401, $message);
        }

        $cond = [];
        $cond['customer_id'] = $request->branch_id;
        if(isset($deviceInfo['uid'])) {
            $cond['uid'] = $deviceInfo['uid'];
        }
        $device = $this->deviceObj->find($cond);

        if(!$device) {
            if(count($customer->devices) >= 3) {
                $message = 'You have exceeded the limit of available devices number!';
                //return parent::jsonResponse(400, $message);
            } else {
                $deviceInfo['customer_id'] = $request->branch_id;
                $this->deviceObj->insert($deviceInfo);
            }
        }

        if (! $token = Auth::guard('venue')->login($customer)) {
            $message = 'credential error!';
            return parent::jsonResponse(401, $message);
        }

        $auth = $this->respondWithToken($token,'venue');
        $customer['customer_id'] = $customer->id;

        $data = array_merge($customer->toArray(), $auth);

        $message = 'Done';
        return parent::jsonResponse(200, $message, $data);
    }

    protected function respondWithToken($token,$type)
    {
        return [
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => Auth::guard($type)->factory()->getTTL() * 3600
        ];
    }

    public function getCustomerConfig(Request $request)
    {
        try {
            $statusCode = 200;
            $message = "Done.";
            $customerID = $request->customer_id;
            $conf = $this->confObj->find(['customer_id' => $customerID]);

            $tables = $this->tableObj->getAll(['customer_id' => $customerID]);
            $tables->map(function($table) {
                $table->makeHidden(['id','customer_id']);
            });
            $conf['tables'] = $tables;
        }catch (\Exception $ex) {
            $statusCode = 500;
            $message = $ex->getMessage();
            $conf = [];
        }finally{
            return parent::jsonResponse($statusCode,$message,$conf);
        }

    }

    /*public function about(Request $request) {
        try {
            $statusCode = 200;
            $message = "Done.";
            $about = $this->aboutObj->getFirst();

        }catch (\Exception $ex) {
            $statusCode = 500;
            $message = $ex->getMessage();
            $about = [];
        }finally{
            return parent::jsonResponse($statusCode,$message,$about);
        }
    }*/

    public function changeConfig(Request $request)
    {
        //return $request->all();
        $conditions = ['customer_id' => $request->customer_id];
        $data = [
            'order_option' => $request->order_option,
            'order_submmiting' => $request->order_submmiting,
        ];

        $this->confObj->update($conditions, $data);
        $result = $this->confObj->find($conditions);

        $message = 'Done';
        return parent::jsonResponse(200, $message, $result);
    }

}
