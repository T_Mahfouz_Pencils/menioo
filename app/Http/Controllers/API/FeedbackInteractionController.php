<?php

namespace App\Http\Controllers\API;

use App\Http\Services\Diner\FeedbackService;
use App\Http\Services\Diner\FeedbackInteractionService;
use App\Http\Services\Diner\FeedbackQuestionService;
use App\Http\Services\Admin\CustomerServices;
use App\Http\Services\Common\Helper;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
Use URL;

class FeedbackInteractionController extends APIController
{
    private $feedbackObj;
    private $feedbackInteractionObj;
    private $fbQuestionObj;
    private $customerObj;
    private $helperObj;

    public function __construct(
        FeedbackService $feedbackObj,
        FeedbackInteractionService $feedbackInteractionObj,
        FeedbackQuestionService $fbQuestionObj,
        CustomerServices $customerObj,
        Helper $helperObj
    )
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::guard('api')->user() ?? null;
            return $next($request);
        });

        $this->feedbackObj = $feedbackObj;
        $this->fbQuestionObj = $fbQuestionObj;
        $this->feedbackInteractionObj = $feedbackInteractionObj;
        $this->customerObj = $customerObj;
        $this->helperObj = $helperObj;
    }

    public function getForm(Request $request)
    {
        try{
            $locale =  $request->locale;

            $statusCode = 0;
            $message = 'Done!';
            $questions = [];
            $form = $this->feedbackObj->find(['active'=>1,'customer_id'=>$request->customer_id]);
            if(!$form) {
                $statusCode = 404;
                $message = 'There\'re no form available!';
                throw new \Exception($message);
            }
            $questions = $this->fbQuestionObj->getAll(['feedback_id'=>$form->id]);
            $statusCode = 200;

            if($questions) {
                  $questions->map(function($question) use ($locale){
                      $question->form_id = $question->feedback_id;
                      $question->question = $question->translateOrNew($locale,true)->value;
                      $question->makeHidden(['value','feedback_id','created_at','updated_at','translations']);
                  });
            }

        }   catch(\Exception $e) {
            $statusCode = $statusCode ? $statusCode : 500;
            $message = $e->getMessage().' '.$e->getFile().' '.$e->getLine();
        }   finally{
            return parent::jsonResponse($statusCode,$message,$questions);
        }

    }

    public function create(Request $request)
    {
        try {
            $statusCode = 201;
            $message = 'Done!';
            $data = [];
            $identifier = $this->feedbackInteractionObj->getIdentifier();
            if(count($request->answers)) {
                $identifier = $this->feedbackInteractionObj->getIdentifier();

                foreach($request->answers as $answer) {
                    $answer['status'] = 1;
                    $answer['customer_user_id'] = $this->user->id ?? null;
                    $answer['identifier'] = $identifier;
                    $data[] = $answer;
                }
            }
            $this->feedbackInteractionObj->bulkInsert($data);
            $statusCode = 201;

        } catch(\Exception $e) {
            $message = $e->getMessage().' => '.$e->getLine();
        } finally {
            return parent::jsonResponse($statusCode,$message,$data);
        }
    }

}
