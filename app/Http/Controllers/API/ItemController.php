<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Requests\Diner\ItemRequest;
use App\Http\Services\Diner\MenuService;
use App\Http\Services\Diner\ItemPriceService;
use App\Http\Services\Diner\ItemService;
use App\Http\Services\Common\Helper;
use Illuminate\Support\Facades\Auth;
Use URL;

class ItemController extends APIController
{
    private $itemObj;
    private $helperObj;
    private $menuObj;
    private $itemPriceService;

    public function __construct(ItemService $itemObj, Helper $helperObj,MenuService $menuObj,ItemPriceService $itemPriceService)
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::guard('api')->user();
            $this->customer = Auth::guard('venue')->user();
            $this->customerId = $this->customer ? $this->customer->id : $this->user->customer_id;
            return $next($request);
        });

        $this->menuObj = $menuObj;
        $this->itemObj = $itemObj;
        $this->helperObj = $helperObj;
        $this->itemPriceService = $itemPriceService;
    }

    public function index(Request $request)
    {
        try {
            $menuID = $request->menu_id ? (int)$request->menu_id : 0;
            $with = ['ingredientWarnings','sideDishes','priceList','itemOptions','campaigns'];

            $statusCode = 200;
            $message = "Done.";
            $this->data = [];
            $items = $this->itemObj->getAll([['menu_id', '=', $menuID], 'is_active' => 1],$with);
            if (count($items)) {
                foreach ($items as $item) {
                    $obj = new \stdClass();
                    $obj->id = $item->id;
                    $obj->name = $item->translateOrNew($request->locale, true)->name;
                    $obj->description = $item->translateOrNew($request->locale, true)->description;
                    $obj->portion = $item->portion;
                    $obj->preparation_time = $item->preparation_time;
                    $obj->calories = $item->calories;
                    $obj->notes = $item->translateOrNew($request->locale, true)->notes;
                    $obj->item_image = $item->FullImagePath();
                    $obj->campaigns = $item->campaigns;

                    $ingredientWarnings_arr = [];
                    $options_arr = [];
                    $prices_arr = [];
                    $sideDishes_arr = [];

                    /*if (count($item->ingredientWarnings)) {
                        foreach ($item->ingredientWarnings as $ingredientWarning) {
                            $ingredientWarning_obj = new \stdClass();
                            $ingredientWarning_obj->id = $ingredientWarning->ingredient_warning_id;
                            $ingredientWarning_obj->name = $ingredientWarning->name;
                            $ingredientWarnings_arr[] = $ingredientWarning_obj;
                        }
                        $obj->ingredients_warnings = $ingredientWarnings_arr;
                    }*/
                    $obj->ingredients_warnings = explode(',', $item->ingredients_warnings);

                    if (count($item->itemOptions)) {
                        $itemOptions = $item->itemOptions;
                        $itemOptions->map(function($io){
                            $io->name = $io->option->name ?? "";
                            $io->values = $io->optionValues;
                            $io->makeHidden(['option','item_id','option_id','created_at','updated_at','values']);
                        });
                        $obj->options = $itemOptions;
                    }
                    if (count($item->sideDishes)) {
                        foreach ($item->sideDishes as $sideDish) {
                            $sideDishObj = new \stdClass();
                            $sideDishObj->id = $sideDish->side_dish_id;
                            if($sideDish->side_dishe) {
                                $sideDishObj->name = $sideDish->side_dishe->name;
                                $sideDishObj->sideDish_image = $sideDish->side_dishe->FullThumbnailImagePath('335X165');
                            }
                            else {
                                $sideDishObj->sideDish_image = "";
                                $sideDishObj->name = "";
                            }
                            $sideDishObj->price = $sideDish->price;

                            $sideDishes_arr[] = $sideDishObj;
                        }
                        $obj->side_dishes = $sideDishes_arr;
                    }
                    if (count($item->priceList)) {
                        foreach ($item->priceList as $price) {
                            $priceObj = new \stdClass();
                            $priceObj->price = $price->price;
                            $priceObj->description = $price->description;
                            $prices[] = $priceObj;
                        }
                        $obj->prices = $prices;
                    }


                    /*$obj->prices = $item->priceList ? $item->priceList : [];
                    $obj->ingredients_warnings = $item->ingredientWarnings ? $item->ingredientWarnings : [];
                    $obj->side_dishes = $item->sideDishes ? $item->sideDishes : [];
                    $obj->options = $item->itemOptions ? $item->itemOptions : [];*/

                    $this->data[] = $obj;
                }
            }
        } catch (\Exception $e) {
            $statusCode = 500;
            $message = $e->getMessage();
        } finally {
            return parent::jsonResponse($statusCode,$message,$this->data);
        }

    }

    public function show($locale,$id)
    {
        try {
            $statusCode = 200;
            $message = 'Done.';
            $sideDishes_arr = [];

            $item = $this->itemObj->find(['id' => $id, 'is_active' => 1],['campaigns','recommendeditems' => function($sql) use ($locale){
                return $sql->with(['translations' => function($sql)  use ($locale){
                    return $sql->whereLocale($locale);
                }]);
            }]);

            if ($item) {
                $obj = new \stdClass();
                $obj->id = $item->id;
                $obj->name = $item->translateOrNew($locale, true)->name;
                $obj->description = $item->translateOrNew($locale, true)->description;
                $obj->portion = $item->portion;
                $obj->preparation_time = $item->preparation_time;
                $obj->calories = $item->calories;
                $obj->notes = $item->translateOrNew($locale, true)->notes;
                $obj->campaigns = $item->campaigns;
                $obj->item_image = base64_encode(file_get_contents($item->FullImagePath()));
                //$obj->item_image_base64 = base64_encode(file_get_contents($item->FullImagePath()));
                $obj->prices = $item->priceList ? $item->priceList : [];
                /*$obj->ingredients_warnings = $item->ingredientWarnings ? $item->ingredientWarnings : [];*/
                $obj->ingredients_warnings = explode(',', $item->ingredients_warnings);
                
                //$obj->side_dishes = $item->sideDishes ? $item->sideDishes : [];
                //$obj->options = $item->itemOptions ? $item->itemOptions : [];

                if (count($item->sideDishes)) {
                    foreach ($item->sideDishes as $sideDish) {
                        $sideDishObj = new \stdClass();
                        $sideDishObj->id = $sideDish->side_dish_id;
                        if($sideDish->side_dishe) {
                            $sideDishObj->name = $sideDish->side_dishe->name;
                            $sideDishObj->sideDish_image = base64_encode(file_get_contents($sideDish->side_dishe->FullThumbnailImagePath('335X165')));
                        }
                        else {
                            $sideDishObj->sideDish_image = "";
                            $sideDishObj->name = "";
                        }
                        $sideDishObj->price = $sideDish->price;

                        $sideDishes_arr[] = $sideDishObj;
                    }
                    $obj->side_dishes = $sideDishes_arr;
                }

                if (count($item->itemOptions)) {
                    $itemOptions = $item->itemOptions;
                    $itemOptions->map(function($io){
                        $io->name = $io->option->name ?? "";
                        $io->type = $io->type ?? "";
                        $io->values = $io->optionValues;
                        $io->makeHidden(['option','item_id','option_id','created_at','updated_at','values']);
                    });
                    $obj->options = $itemOptions;
                } else {
                    $obj->options = [];
                }

                $obj->recommended = [];
                if(count($item->recommendeditems)) {
                    foreach($item->recommendeditems as $recommended) {
                        $recomObj = new \stdClass();
                        $recomObj->id = $recommended->id;
                        $recomObj->name = $recommended->translateOrNew($locale, true)->name;
                        $recomObj->description = $recommended->translateOrNew($locale, true)->description;
                        $recomObj->portion = $recommended->portion;
                        $recomObj->preparation_time = $recommended->preparation_time;
                        $recomObj->calories = $recommended->calories;
                        $recomObj->notes = $recommended->translateOrNew($locale, true)->notes;
                        $recomObj->is_active = (bool)$recommended->is_active;
                        $recomObj->campaigns = $recommended->campaigns;
                        $recomObj->item_image = base64_encode(file_get_contents($recommended->FullImagePath()));
                        $recomObj->prices = $recommended->priceList ? $recommended->priceList : [];
                        if($recomObj->is_active)
                            $obj->recommended[] = $recomObj;
                    }
                }

                $this->data = $obj;
            }

        } catch (\Exception $e) {
            $statusCode = 500;
            $message = $e->getMessage();
        } finally {
            return parent::jsonResponse($statusCode,$message,$this->data);
        }
    }

    public function create(Request $request, $menu_id)
    {
        //dd($menu_id);
        $section_id = $request->section_id;
        if ($section_id > 0) {
            $section = $this->menuSectionObj->find(['id' => $section_id, 'menu_id' => menu_id]);
            if (!$section) {
                return view('admin.404');
            }
        }
        $this->data['section_id'] = $section_id;
        $parent_menu = $this->parentMenuObj->find(['id' => $parent_id]);

        $this->data['title'] = (object)['name' => 'Create SubCategory', 'url' => URL::to('diner/menu'),
            'parent' => (object)['name' => 'Menus'], 'back' => (object)['name' => ' ' . $parent_menu->name, 'url' => URL::to('diner/menu/' . $parent_menu->id)]];

        return view('diner.sub-menu.create', $this->data);
    }

    public function store(ItemRequest $request, $menu_id)
    {
        $data = $request->except(['_token', 'file', 'video','prices']);

        $menuID = $request->menu_id;
        if ($menuID > 0) {
            $menu = $this->menuObj->find(['id' => $menuID]);
            if (!$menu) {
                return view('admin.404');
            }
        }
        $data['menu_id'] = $menuID;

        $customerID = Auth::guard('diner')->user() ? Auth::guard('diner')->user()->customer_id : $menu->customer_id;

        if ($request->has('file')) {
            $media = $this->helperObj->upload($request->file('file'), ['title' => $data['name'], 'customer_id' => $customerID]);
            $data['image_id'] = $media->id;
        }
        if ($request->has('video')) {
            $video = $this->helperObj->upload($request->file('video'));
            $data['video_id'] = $video->id;
        }


        $item = $this->itemObj->insert($data);
        //return $item;

        if ($item) {
            $prices = $request->prices;
            if(count($prices)) {
                foreach ($prices as $price) {
                    $obj = [];
                    $obj['item_id'] = $item->id;
                    $obj['price'] = $price['value'];
                    $obj['description'] = $price['description'];
                    $this->itemPriceService->insert($obj);
                }
            }
            return redirect()->route('show-item', ['id' => $item->id])->with(['success' => 'added successfully']);
        } else {
            return redirect()->back()->with('fail', 'Whoops something wrong');
        }

    }

    public function edit($parent_id, $id)
    {

        $this->data['current_menu'] = $this->menuObj->find(['id' => $id, 'parent_id' => $parent_id, 'customer_id' => Auth::guard('diner')->user()->customer_id]);
        if ($this->data['current_menu']) {
            $parent_menu = $this->parentMenuObj->find(['id' => $parent_id]);
            $this->data['title'] = (object)['name' => 'Edit ' . $this->data['current_menu']->name . ' Menu', 'url' => URL::to('diner/menu/' . $parent_id),
                'parent' => (object)['name' => 'Menus'], 'back' => (object)['name' => ' ' . $parent_menu->name, 'url' => URL::to('diner/menu/' . $parent_id)]];

            return view('diner.sub-menu.edit', $this->data);
        } else {
            return view('admin.404');
        }
    }

    public function update(SubMenuRequest $request, $parent_id, $id)
    {
        $current_menu = $this->menuObj->find(['id' => $id, 'parent_id' => $parent_id, 'customer_id' => Auth::guard('diner')->user()->customer_id]);
        if ($current_menu) {

            $data = $request->except(['_token', 'file', 'video', '_method']);
            $data['display_similar_items'] = 0;
            $data['mark_as_new'] = 0;
            $data['mark_as_signature'] = 0;
            if ($request->display_similar_items) {
                $data['display_similar_items'] = 1;
            }
            if ($request->mark_as_new) {
                $data['mark_as_new'] = 1;
            }
            if ($request->mark_as_signature) {
                $data['mark_as_signature'] = 1;
            }

            if ($request->has('file')) {
                $old_media = $current_menu->MenuImage;
                $media = $this->helperObj->upload($request->file('file'), ['title' => $data['name'], 'customer_id' => Auth::guard('diner')->user()->customer_id], true, $old_media);

                $data['media_id'] = $media->id;
            }
            if ($request->has('video')) {
                $old_media = $current_menu->MenuVideo;
                $video = $this->helperObj->upload($request->file('video'), ['title' => $data['name'], 'customer_id' => Auth::guard('diner')->user()->customer_id], true, $old_media);
                $data['video_id'] = $video->id;
            }

            if (!$current_menu->section_id) {
                $meu_section = $this->menuSectionObj->getLastOne(['menu_id' => $parent_id]);
                if ($meu_section) {
                    $data['section_id'] = $meu_section->id;
                }

            }
            //dd($data);
            $current_menu = $this->menuObj->update(['id' => $id, 'parent_id' => $parent_id], $data);

            if ($current_menu) {
                return redirect()->back()->with(['success' => 'updated successfully']);
            } else {
                return redirect()->back()->with(['fail' => 'whoops something wrong']);
            }
        } else {
            return view('admin.404');
        }
    }

    public function destroy($parent_id, $id)
    {
        $this->menuObj->delete(['id' => $id, 'parent_id' => $parent_id]);
        return redirect()->route('diner.menu', ['parent_id' => $parent_id])->with(['success' => 'Subcategory was removed successfully']);

    }

}
