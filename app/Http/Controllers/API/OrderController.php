<?php

namespace App\Http\Controllers\API;

use App\Http\Services\Diner\ItemService;
use App\Http\Services\Diner\OrderService;
use App\Http\Services\Diner\CartService;
use App\Http\Services\Diner\CartItemService;
use App\Http\Services\Diner\CartItemOptionService;
use App\Http\Services\Diner\CartItemSideDishService;

use App\Http\Services\Common\Helper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
Use URL;

class OrderController extends Controller
{
    private $itemObj;
    private $itemOptionObj;
    private $orderObj;
    private $itemSideDishObj;
    private $itemPriceObj;
    private $cartObj;
    private $cartItemObj;
    private $cartItemOptionObj;
    private $cartItemSideDishObj;
    private $helperObj;

    public function __construct(
        ItemService $itemObj,
        OrderService $orderObj,
        CartService $cartObj,
        CartItemService $cartItemObj,
        CartItemOptionService $cartItemOptionObj,
        CartItemSideDishService $cartItemSideDishObj,
        Helper $helperObj
    )
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::guard('api')->user();
            $this->customer = Auth::guard('venue')->user();
            $this->customerId = $this->customer ? $this->customer->id : $this->user->customer_id;
            return $next($request);
        });
        $this->itemObj = $itemObj;
        $this->orderObj = $orderObj;
        $this->cartObj = $cartObj;
        $this->cartItemObj = $cartItemObj;
        $this->cartItemOptionObj = $cartItemOptionObj;
        $this->cartItemSideDishObj = $cartItemSideDishObj;
        $this->helperObj = $helperObj;
    }

    public function submitOrder(Request $request)
    {
        //$customer = $this->user->Customer;
        //$statusCode = 200;
        //$message = "Done.";
        //
        //$serviceChargePercentag = 12;
        //$tipPercentag = 0;
        //$taxPercentag = 0;
        //
        //$id = $request->cart_id;
        //$conditions = [
        //'id' => $id,
        //'ordered' => 0,
        //'customer_id' => $customer->id
        //];
        //
        //$cart = $this->cartObj->find($conditions,['items']);
        //return $cart;
        try {
            $customer = $this->user->Customer ?? $this->customer;
            $statusCode = 200;
            $message = "Done.";

            $serviceChargePercentag = 0.12;
            $taxPercentag = 0.14;
            $tipPercentag = 0;

            $id = $request->cart_id;
            $conditions = [
                'id' => $id,
                'ordered' => 0,
                'customer_id' => $customer->id
            ];

            $cart = $this->cartObj->find($conditions,['items']);
            if($cart) {
                $serviceCharge = $cart->total_cost * $serviceChargePercentag;
                $tax = $cart->total_cost * $taxPercentag;

                $total = $cart->total_cost + $serviceCharge +$tax;
                $orderData = [
                    'cart_id' => $id,
                    'table_number' => $cart->table_number,
                    'customer_user_id' => $this->user->id ?? null,
                    'customer_id' => $this->customerId,
                    'service_charge' => $serviceCharge,
                    'tax' => $tax,
                    'tip' => $cart->total_cost * $tipPercentag,
                    'total_cost' => $total,
                    'status' => 'close',
                ];
                $this->orderObj->insert($orderData);

                $this->cartObj->update($conditions,['ordered'=>1]);

            } else {
                $statusCode = 404;
                $message = "Cart not found.";
            }
        } catch (\Exception $e) {
            $statusCode = 500;
            $message = $e->getMessage();
        } finally {
            return parent::jsonResponse($statusCode,$message);
        }
    }

    public function openedOrders(Request $request)
    {
        try {
            $statusCode = 200;
            $message = "Done.";
            $conditions = array();
            $conditions['status'] = 'open';
            $conditions['customer_id'] = $this->customerId;
            if($this->user)
                $conditions['customer_user_id'] = $this->user->id;
            $result = $this->orderObj->getAll($conditions);

        } catch (\Exception $e) {
            $statusCode = 500;
            $message = $e->getMessage();
            $result = [];
        } finally {
            return parent::jsonResponse($statusCode,$message,$result);
        }
    }

    public function closeOrder(Request $request,$id)
    {
        try {
            $statusCode = 200;
            $message = "Done.";
            $this->order = $this->orderObj->find([
                'id' => $id,
                'status' => 'open',
                'customer_user_id' => $this->user->id
            ]);
            if(!$this->order)
                throw new \Exception('Not Found');

            $this->orderObj->update([
                'id' => $request->id,
                'status' => 'open',
                'customer_user_id' => $this->user->id
            ],['status' => "close"]);

            $this->order = $this->orderObj->find(['id' => $id]);

        } catch (\Exception $e) {
            $statusCode = 500;
            $message = $e->getMessage();
        } finally {
            return parent::jsonResponse($statusCode,$message,$this->order);
        }
    }

}
