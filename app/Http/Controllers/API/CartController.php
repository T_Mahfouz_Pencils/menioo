<?php

namespace App\Http\Controllers\API;

use App\Http\Services\Diner\ItemService;
use App\Http\Services\Diner\OrderService;
use App\Http\Services\Diner\ItemOptionService;
use App\Http\Services\Diner\ItemSideDishService;
use App\Http\Services\Diner\ItemPriceService;
use App\Http\Services\Diner\CartService;
use App\Http\Services\Diner\CartItemService;
use App\Http\Services\Diner\CartItemOptionService;
use App\Http\Services\Diner\CartItemSideDishService;

use App\Http\Services\Common\Helper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
Use URL;

class CartController extends Controller
{
    private $itemObj;
    private $itemOptionObj;
    private $orderObj;
    private $itemSideDishObj;
    private $itemPriceObj;
    private $cartObj;
    private $cartItemObj;
    private $cartItemOptionObj;
    private $cartItemSideDishObj;
    private $helperObj;

    public function __construct(
        ItemService $itemObj,
        OrderService $orderObj,
        ItemOptionService $itemOptionObj,
        ItemSideDishService $itemSideDishObj,
        ItemPriceService $itemPriceObj,
        CartService $cartObj,
        CartItemService $cartItemObj,
        CartItemOptionService $cartItemOptionObj,
        CartItemSideDishService $cartItemSideDishObj,
        Helper $helperObj
    )
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::guard('api')->user();
            $this->customer = Auth::guard('venue')->user();
            $this->customerId = $this->customer ? $this->customer->id : $this->user->customer_id;
            return $next($request);
        });
        $this->itemObj = $itemObj;
        $this->orderObj = $orderObj;
        $this->itemOptionObj = $itemOptionObj;
        $this->itemSideDishObj = $itemSideDishObj;
        $this->itemPriceObj = $itemPriceObj;
        $this->cartObj = $cartObj;
        $this->cartItemObj = $cartItemObj;
        $this->cartItemOptionObj = $cartItemOptionObj;
        $this->cartItemSideDishObj = $cartItemSideDishObj;
        $this->helperObj = $helperObj;
    }

    public function index(Request $request)
    {
        try {
            $statusCode = 200;

            $servicePer = 0.12;
            $vatPer     = 0.14;

            $message = "Done.";
            $with = ['items'];
            $items= [];

            $this->data = [];
            $conditions = array();
            $conditions['ordered'] = 0;
            $conditions['customer_id'] = $this->customerId;
            if($this->user)
                $conditions['customer_user_id'] = $this->user->id;
            //return Auth::guard('api')->user();
            $carts = $this->cartObj->getAll($conditions,$with);

            if (count($carts)) {
                foreach ($carts as $cart) {
                    $obj = new \stdClass();
                    $obj->id = $cart->id;
                    $obj->table_number = $cart->table_number;
                    $obj->customer_id = $cart->customer_id;
                    $obj->total_cost = round($cart->total_cost + ($cart->total_cost * $servicePer) + ($cart->total_cost * $vatPer));

                    $obj->items = [];

                    //$items = $cart->items;
                    $items = $cart->cartItems;
                    //return $items;

                    if (count($items)) {
                        foreach ($items as $item) {
                            //$cartItem = $this->cartItemObj->find(['item_id'=>$item->id,'cart_id'=>$cart->id]);
                            $cartItem = $this->itemObj->find(['id'=>$item->item_id]);
                            $itemRef = $this->cartItemObj->find(['item_id'=>$item->item_id,'cart_id'=>$cart->id]);
                            //return $cartItem;
                            if($cartItem) {
                                $itemObj = new \stdClass();
                                $itemObj->id = $cartItem->id;
                                $itemObj->name = $cartItem->translateOrNew($request->locale, true)->name;
                                $itemObj->item_status = $itemRef->item_status;
                                $itemObj->description = $cartItem->translateOrNew($request->locale, true)->description;
                                $itemObj->portion = $cartItem->portion;
                                $itemObj->preparation_time = $cartItem->preparation_time;
                                $itemObj->calories = $cartItem->calories;
                                $itemObj->notes = $cartItem->translateOrNew($request->locale, true)->notes;
                                $itemObj->quantity = $item ? $item->quantity : 1;
                                $itemObj->item_image = base64_encode(file_get_contents($cartItem->FullThumbnailImagePath('335X165')));
                                if(count($cartItem->priceList)) {
                                    $allPrices = $cartItem->priceList;
                                    $allPrices->map(function($origin){
                                        $origin->makeHidden(['item_id','created_at','updated_at']);
                                    });
                                    $itemObj->prices = $cartItem->priceList;
                                } else {
                                    $itemObj->prices = [];
                                }

                                $itemObj->selected_price = $itemRef->item_price;
                                $itemObj->ingredients_warnings = $cartItem->ingredientWarnings ? $cartItem->ingredientWarnings : [];
                                //$itemObj->prices = $item->priceList ? $cartItem->priceList : [];
                                //$itemObj->side_dishes = $cartItem->sideDishes ? $cartItem->sideDishes : [];
                                //$itemObj->options = $cartItem->itemOptions ? $cartItem->itemOptions : [];
                                if(count($cartItem->sideDishes)) {
                                    $allSideDishes = $cartItem->sideDishes;
                                    $allSideDishes->map(function($origin) {
                                        $origin->side_dishe;
                                        $origin->id = $origin->side_dishe->id ?? "";
                                        $origin->name = $origin->side_dishe->name ?? "";
                                        $origin->image = base64_encode(file_get_contents($origin->side_dishe->image));
                                        $origin->makeHidden(['side_dishe','item_id','side_dish_id','media_id','created_at','updated_at','image']);
                                    });
                                    $itemObj->side_dishes = $allSideDishes;
                                } else {
                                    $itemObj->side_dishes = [];
                                }

                                if (count($cart->itemSideDishes($cartItem->id))) {
                                    $sideDishes_arr = [];
                                    foreach ($cart->itemSideDishes($cartItem->id) as $sideDish) {
                                        $side =  $sideDish->itemSideDish;

                                        $sideDishObj = new \stdClass();
                                        $sideDishObj->id = $side->side_dish_id;
                                        if($side->side_dishe) {
                                            $sideDishObj->name = $side->side_dishe->name;
                                            $sideDishObj->sideDish_image = base64_encode(file_get_contents($side->side_dishe->FullThumbnailImagePath('335X165')));
                                        }
                                        else {
                                            $sideDishObj->sideDish_image = "";
                                            $sideDishObj->name = "";
                                        }
                                        $sideDishObj->price = $side->price;

                                        $sideDishes_arr[] = $sideDishObj;
                                    }
                                    $itemObj->selected_side_dishes = $sideDishes_arr;
                                }

                                if(count($cartItem->itemOptions)) {
                                    $allOptions = $cartItem->itemOptions;
                                    $allOptions->map(function($origin) {
                                        $origin->option;
                                        $origin->id = $origin->option->id ?? "";
                                        $origin->name = $origin->option->name ?? "";
                                        $origin->values = $origin->option->values ?? [];
                                        $origin->makeHidden(['option','item_id','option_id','created_at','updated_at']);
                                    });
                                    $itemObj->options = $allOptions;
                                } else {
                                    $itemObj->options = [];
                                }

                                if (count($cart->itemOptions($cartItem->id))) {
                                    $itemOptions = $cart->itemOptions($cartItem->id);
                                    //$itemOptions[0]->itemOption->option->values
                                    $itemOptions->map(function($io){
                                        $io->id = $io->item_option_id;
                                        //$io->option = $io->itemOption;
                                        $io->option->name = $io->itemOption->option->name ?? "";
                                        $io->option->makeHidden(['option','item_id','option_id','created_at','updated_at']);
                                        $io->values = $io->option->optionValues;
                                        $io->values->makeHidden(['created_at','updated_at']);
                                        $io->makeHidden(['cart_id','item_option_id','itemOption','item_id','option_id','created_at','updated_at','values']);
                                    });
                                    $itemObj->selected_options = $itemOptions;
                                }
                                $obj->items[] = $itemObj;
                            }
                        }
                    }
                    if(count($obj->items))
                        $this->data[] = $obj;
                }
            }
        } catch (\Exception $e) {
            $statusCode = 500;
            $message = $e->getMessage().' => '.$e->getLine();
        } finally {
            return parent::jsonResponse($statusCode,$message,$this->data);
        }
    }

    public function createOrEdit(Request $request)
    {
        try {
            $servicePer = 0.12;
            $vatPer     = 0.14;
            if($request->filled('cart_id')) {
                $statusCode = 201;
                $message = 'Done!';
                $cart = $this->cartObj->find(['id'=>$request->cart_id,'ordered' => 0]);
                if(!$cart) {
                    $statusCode = 404;
                    $message = 'Cart not found!';
                    $cartInfo = [];
                    goto end;
                    //throw new \Exception('Cart not found!');
                }
                $this->cartItemObj->delete(['cart_id'=>$request->cart_id]);

                $cartInfo = [];

                # STEP 1: ADD ITEMS TO CART ===============
                $cartData = $cartItemsData = [];
                $totalCost = $itemPrice = 0;
                $items = $request->items;
                if(count($items)) {
                    foreach($items as $singleItem) {
                        $singleItem = (object)$singleItem;
                        $item = $this->itemObj->find(['id'=>$singleItem->item_id]);
                        if($item) {
                            $price = $this->itemPriceObj->find(['id'=>$singleItem->item_price_id]);
                            if($price) {
                                $totalCost += $singleItem->quantity * $price->price;
                                $itemPrice = $price->price;
                            }

                            $this->cartObj->update(['id' => $request->cart_id],['table_number'=>$request->table_number]);

                            $cartItemData['cart_id'] = $cart->id;
                            $cartItemData['item_id'] = $item->id;
                            $cartItemData['item_price'] = $itemPrice;
                            $cartItemData['quantity'] = $singleItem->quantity;
                            $cartItemData['item_status'] = $singleItem->status;
                            $this->cartItemObj->insert($cartItemData);

                            $optionIDs = $singleItem->option_ids;
                            $sideDishesIDs = $singleItem->side_dishes_ids;
                            if(count($optionIDs)) {

                                $this->cartItemOptionObj->delete(['item_id'=> $singleItem->item_id,'cart_id'=>$request->cart_id]);

                                foreach($optionIDs as $optionID) {
                                    $options = [];
                                    $option = $this->itemOptionObj->find(['id'=>$optionID]);
                                    if($option) {
                                        $options['cart_id'] = $cart->id;
                                        $options['item_id'] = $item->id;
                                        $options['item_option_id'] = $optionID;
                                        $this->cartItemOptionObj->insert($options);
                                    }
                                }
                            }
                            if(count($sideDishesIDs)) {

                                $this->cartItemSideDishObj->delete(['item_id'=> $singleItem->item_id,'cart_id'=>$request->cart_id]);

                                foreach($sideDishesIDs as $sideDishesID) {
                                    $sideDishes = [];
                                    $sideDish = $this->itemSideDishObj->find(['id'=>$sideDishesID]);
                                    if($sideDish) {
                                        $totalCost += $sideDish->price;
                                        $sideDishes['cart_id'] = $cart->id;
                                        $sideDishes['item_id'] = $item->id;
                                        $sideDishes['item_side_dish_id'] = $sideDishesID;
                                        $sideDishes['price'] = $sideDish->price;
                                        $this->cartItemSideDishObj->insert($sideDishes);
                                    }
                                }
                            }
                            //$totalCost = $totalCost + ($totalCost * $servicePer) + ($totalCost * $vatPer);
                            $this->cartObj->update(['id'=>$cart->id],['total_cost'=>$totalCost]);

                            $cartInfo = $this->cartObj->find(['id'=>$cart->id],['items']);
                        }
                    }

                } else {
                    $statusCode = 404;
                    throw new \Exception('There\'re no items!');
                }
            }
            else{
                $statusCode = 201;
                $message = 'Done!';
                $cartInfo = [];
                $tableNumber = $request->table_number;

                # STEP 1: ADD ITEMS TO CART ===============
                $cartData = $cartItemsData = [];
                $totalCost = $itemPrice = 0;
                $items = $request->items;

                if(count($items)) {
                    $cartData['customer_id'] = $this->customerId;
                    $cartData['table_number'] = $request->table_number;
                    $cartData['customer_user_id'] = $this->user->id ?? null;
                    $cartData['ordered'] = 0;
                    $cartData['total_cost'] = $totalCost;
                    $cart = $this->cartObj->insert($cartData);
                    //$existedOne = $this->cartObj->getLastHandled(['customer_id'=>$this->user->id,'ordered'=>0]);
                    //$cart = $existedOne ? $existedOne : $this->cartObj->insert($cartData);

                    foreach($items as $singleItem) {
                        $singleItem = (object)$singleItem;
                        $item = $this->itemObj->find(['id'=>$singleItem->item_id]);
                        if($item) {
                            $price = $this->itemPriceObj->find(['id'=>$singleItem->item_price_id]);
                            if($price) {
                                $totalCost += $singleItem->quantity * $price->price;
                                $itemPrice = $price->price;
                            }
                            $cartItemData['cart_id'] = $cart->id;
                            $cartItemData['item_id'] = $item->id;
                            $cartItemData['item_price'] = $itemPrice;
                            $cartItemData['quantity'] = $singleItem->quantity;
                            $cartItemData['item_status'] = $singleItem->status;
                            $cartitem = $this->cartItemObj->insert($cartItemData);

                            $optionIDs = $singleItem->option_ids;
                            $sideDishesIDs = $singleItem->side_dishes_ids;
                            if(count($optionIDs)) {
                                foreach($optionIDs as $optionID) {
                                    $options = [];
                                    $option = $this->itemOptionObj->find(['id'=>$optionID]);
                                    if($option) {
                                        $options['cart_id'] = $cart->id;
                                        $options['item_id'] = $item->id;
                                        $options['item_option_id'] = $optionID;
                                        $this->cartItemOptionObj->insert($options);
                                    }
                                }
                            }
                            if(count($sideDishesIDs)) {
                                foreach($sideDishesIDs as $sideDishesID) {
                                    $sideDishes = [];
                                    $sideDish = $this->itemSideDishObj->find(['id'=>$sideDishesID]);
                                    if($sideDish) {
                                        $totalCost += $sideDish->price;
                                        $sideDishes['cart_id'] = $cart->id;
                                        $sideDishes['item_id'] = $item->id;
                                        $sideDishes['item_side_dish_id'] = $sideDishesID;
                                        $sideDishes['price'] = $sideDish->price;
                                        $this->cartItemSideDishObj->insert($sideDishes);
                                    }
                                }
                            }
                            //$totalCost = $totalCost + ($totalCost * $servicePer) + ($totalCost * $vatPer);
                            $this->cartObj->update(['id'=>$cart->id],['total_cost'=>$totalCost]);

                            $cartInfo = $this->cartObj->find(['id'=>$cart->id],['items']);
                        }
                    }

                } else {
                    $statusCode = 404;
                    throw new \Exception('There\'re no items!');
                }
            }

            end:
        }
        catch(\Exception $e) {
            $statusCode = $statusCode == 201 ? 500 : $statusCode;
            $message = $e->getMessage().": ".$e->getLine();
            $cartInfo = [];
        }
        finally {
            return parent::jsonResponse($statusCode,$message,[]);
            //return parent::jsonResponse($statusCode,$message,$cartInfo);
        }
    }

    public function edit(Request $request)
    {
        try {
            $statusCode = 200;
            $message = 'Done!';

            $cartItemData= [];
            $itemPrice = 0;

            $item = $this->itemObj->find(['id'=>$request->item_id]);
            $cart = $this->cartObj->find(['id'=>$request->cart_id]);
            $cartItem = $this->cartItemObj->find(['cart_id'=>$request->cart_id,'item_id'=>$request->item_id]);

            if($cart && $item && $cartItem) {
                $cartItemData['cart_id'] = $cart->id;
                $cartItemData['item_id'] = $item->id;
                $cartItemData['item_price'] = $itemPrice;
                $cartItemData['quantity'] = $request->quantity;
                $this->cartItemObj->update(['cart_id'=>$cart->id,'item_id'=>$item->id],$cartItemData);

                $optionIDs = explode(',',$request->option_ids);
                $sideDishesIDs = explode(',',$request->side_dishes_ids);
                if(count($optionIDs)) {
                    foreach($optionIDs as $optionID) {
                        $options = [];
                        $option = $this->itemOptionObj->find(['id'=>$optionID]);
                        if($option) {
                            $options['cart_id'] = $cart->id;
                            $options['item_id'] = $item->id;
                            $options['name'] = $option->name;
                            $options['value'] = $option->value;
                            $options['item_option_id'] = $optionID;
                            $this->cartItemOptionObj->update(['cart_id'=>$cart->id,'item_id'=>$item->id],$options);
                        }
                    }
                }
                if(count($sideDishesIDs)) {
                    foreach($sideDishesIDs as $sideDishesID) {
                        $sideDishes = [];
                        $sideDish = $this->itemSideDishObj->find(['id'=>$sideDishesID]);
                        if($sideDish) {
                            $sideDishes['cart_id'] = $cart->id;
                            $sideDishes['item_id'] = $item->id;
                            $sideDishes['item_side_dish_id'] = $sideDishesID;
                            $sideDishes['name'] = $sideDish->name;
                            $sideDishes['price'] = $sideDish->price;
                            $sideDishes['image_id'] = $sideDish->image_id;
                            $this->cartItemSideDishObj->update(['cart_id'=>$cart->id,'item_id'=>$item->id],$sideDishes);
                        }
                    }
                }

                $cartInfo = $this->cartObj->find(['id'=>$cart->id],['items']);
                //$cartInfo = $this->cartObj->find(['id'=>$cart->id],['cartItems']);
                //if(count($cartInfo->cartItems)) {
                //    foreach($cartInfo->cartItems as $sItem) {
                //        $cartItem = $this->itemObj->find(['id'=>$item->item_id]);
                //        $sItem['id'] = $cartItem->id;
                //        $sItem['name'] = $cartItem->name;
                //        $sItem['description'] = $cartItem->description;
                //        $sItem['portion'] = $cartItem->portion;
                //        $sItem['preparation_time'] = $cartItem->preparation_time;
                //        $sItem['calories'] = $cartItem->calories;
                //        $sItem['notes'] = $cartItem->notes;
                //        $sItem['quantity'] = $item ? $sItem->quantity : 1;
                //        $sItem['item_image'] = $cartItem->FullThumbnailImagePath('335X165');
                //        $sItem['prices'] = $sItem->priceList ? $cartItem->priceList : [];
                //        $sItem['ingredients_warnings'] = $cartItem->ingredientWarnings ? $cartItem->ingredientWarnings : [];
                //        $sItem['side_dishes'] = $cartItem->sideDishes ? $cartItem->sideDishes : [];
                //        $sItem['options'] = $cartItem->itemOptions ? $cartItem->itemOptions : [];
                //        $sItem['quantity'] = $this->cartItemObj->find(['item_id'=>$sItem->id,'cart_id'=>$cartInfo->id])->quantity;
                //    }
                //}
            } else {
                throw new \Exception('Not Found!');
            }
        } catch (\Exception $e) {
            $statusCode = 500;
            $message = $e->getMessage().' => '.$e->getLine();
        } finally {
            return parent::jsonResponse($statusCode,$message,[]);
            //return parent::jsonResponse($statusCode,$message,$cartInfo);
        }
    }

    public function submit(Request $request)
    {
        $statusCode = 200;
        $message = 'Done.';

        $servicePer = 0.12;
        $vatPer     = 0.14;

        $serviceChargePercentag = 0.12;
        $tipPercentag = 0;
        $taxPercentag = 0.14;
        $serviceCharge = 0;

        $id = $request->cart_id;
        $tableNumber = $request->table_number;
        $conditions = [
            'id' => $id,
            'ordered' => 0,
            'customer_id' => $this->customerId
        ];
        $data = ['ordered' => 1];
        $cart = $this->cartObj->find($conditions,['items']);
        if($cart) {
            $tip = $cart->total_cost * $tipPercentag / 100;
            $tax = $cart->total_cost * $taxPercentag / 100;
            $total = $cart->total_cost + ($cart->total_cost * $servicePer) + ($cart->total_cost * $vatPer);
            $orderData = [
                'cart_id' => $id,
                'table_number' => $cart->table_number,
                'customer_id' => $this->customerId,
                'customer_user_id' => $this->customer->id,
                'service_charge' => $cart->total_cost * $serviceChargePercentag,
                'tax' => $cart->total_cost* $taxPercentag,
                'tip' => $tip,
                'total_cost' => $total,
                'status' => 'open',
            ];
            $this->orderObj->insert($orderData);
            $this->cartObj->update($conditions,$data);

            return parent::jsonResponse($statusCode,$message,$this->cartObj->find([
                'id' => $id,
                'customer_id' => $this->customerId
            ],['items']));
        }

        $statusCode = 404;
        $message = 'not found!';
        return parent::jsonResponse($statusCode,$message);
    }

    public function delete(Request $request)
    {
        try {
            $code = 200;
            $message = "Done!";
            $data = [];

            $cartId = $request->cart_id;
            $itemId = $request->item_id;
            $cart = $this->cartObj->find(['id' => $cartId]);
            $item = $this->cartObj->find(['id' => $itemId]);
            if(!$cart)
                throw new \Exception('Cart not found!');
            if($itemId) {
                if(!$item)
                    throw new \Exception('Item not found!');
                $this->cartItemOptionObj->delete(['cart_id'=>$cartId,'item_id'=>$itemId]);
                $this->cartItemSideDishObj->delete(['cart_id'=>$cartId,'item_id'=>$itemId]);
                $this->cartItemObj->delete(['cart_id'=>$cartId,'item_id'=>$itemId]);

                $stillItems = $this->cartItemObj->getAll(['cart_id'=>$cartId]);
                if(!count($stillItems))
                    $this->cartObj->delete(['id' => $cartId]);
            } else {
                $this->cartItemOptionObj->delete(['cart_id'=>$cartId,'item_id'=>$itemId]);
                $this->cartItemSideDishObj->delete(['cart_id'=>$cartId,'item_id'=>$itemId]);
                $this->cartItemObj->delete(['cart_id'=>$cartId,'item_id'=>$itemId]);
                $this->cartObj->delete(['id' => $cartId]);
            }
        } catch (\Exception $e) {
            $code = 500;
            $message = $e->getMessage();
        } finally {
            return parent::jsonResponse($code,$message,$data);
        }
    }
}
