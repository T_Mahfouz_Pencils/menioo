<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;


class AdminController extends Controller
{
    public $data;
    

    public function __construct() 
    {
       
        $this->middleware(function ($request, $next) {
            $navigations = [(object)['name' => 'Customers', 'url' => URL::to('admin/customer'), 'class' => 'fas fa-utensils'],
                (object)['name' => 'Subscriptions', 'url' => URL::to('admin/subscription'), 'class' => 'fas fa-file-invoice-dollar']
            ];
            View::share('navigations', $navigations);
            $this->data['current_user'] = Auth::guard('admin')->user();
            return $next($request);
        });

    }

    

}
