<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Common\LoginRequest;
use App\Http\Services\Admin\AdminLoginService;
use App\Http\Services\Diner\DinerLoginService;
use App\Http\Services\Diner\User As CustomerUser;
use Illuminate\Support\Facades\Auth;
Use URL;

class AdminLoginController extends AdminController
{
    private $adminLoginObj;
    private $dinerLoginObj;
    private $customerUserobj;

    public function __construct(AdminLoginService $adminLoginObj, DinerLoginService $dinerLoginObj, CustomerUser $customerUserobj)
    {
        $this->adminLoginObj = $adminLoginObj;
        $this->dinerLoginObj = $dinerLoginObj;
        $this->customerUserobj = $customerUserobj;
    }

    public function login()
    {
        if (!Auth::guard('admin')->user()) {
            $this->data['title'] = (object)['name' => 'Login'];
            return view('admin.auth.login', $this->data);
        }
        return redirect()->route('admin.customer.index');
    }

    public function loginAsDiner($id)
    {
        return "ddddd";
        $user = $this->customerobj->find(['id' => $id]);
        if ($this->dinerLoginObj->loginAs($user)) {
            return redirect()->route('diner.menu.index')->with(['success' => 'Logged in successfully']);
        } else {
            return redirect()->back()->with('fail', 'Whoops something wrong');
        }
    }


    public function doLogin(LoginRequest $request)
    {

        $credentials = $request->except(['_token', 'remember']);

        $isLogin = $this->adminLoginObj->login($credentials, $request->remember);


        if ($isLogin) {
            return redirect()->route('admin.customer.index')->with(['success' => 'Logged in successfully']);
        } else {
            return redirect()->back()->with('fail', 'Whoops something wrong');
        }

    }

    public function logout()
    {
        $this->adminLoginObj->logout();
        return redirect()->route('admin.login');
    }
}
