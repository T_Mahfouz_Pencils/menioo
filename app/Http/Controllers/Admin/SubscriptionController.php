<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\SubscriptionRequest;
use App\Http\Services\Admin\SubscriptionServices;
use App\Http\Services\Common\Helper;
use Illuminate\Http\Request;
Use URL;

class SubscriptionController extends AdminController
{
    private $planObj;
    private $countryObj;
    private $helperObj;

    public function __construct(SubscriptionServices $planObj, Helper $helperObj)
    {
        parent::__construct();
        $this->planObj = $planObj;
        $this->helperObj = $helperObj;
    }

    public function index(Request $request)
    {
        $conditions = [];

        if ($request->keyword) {
            $conditions[] = ['name', 'like', '%' . $request->keyword . '%'];
        }

        $per_page = $request->per_page ? $request->per_page : 20;
        $this->data['plans'] = $this->planObj->getAll($conditions, $per_page);

        $this->data['title'] = (object)['name' => 'Subscription Plans', 'url' => URL::to('admin/subscription'),
            'child' => (object)['name' => 'New Plan', 'url' => URL::to('admin/subscription/create'),'subtitle' => '']
        ];

        return view('admin.subscription.index', $this->data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['title'] = (object)['name' => 'Create New Plan', 'url' => URL::to('admin/subscription'),
            'parent' => (object)['name' => 'subscription']];
        $this->data['types'] = ['Normal', 'Premium'];

        return view('admin.subscription.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function store(SubscriptionRequest $request)
    {
        $data = $request->except(['_token']);
        $data['features'] = json_encode ($data['features']);
        $data['order_option'] = isset($data['order_option']) ? 1 : 0 ;
        $plan = $this->planObj->insert($data);
        if ($plan) {
            return redirect()->route('admin.subscription.edit', ['id' => $plan->id])->with(['success' => 'added successfully']);
        } else {
            return redirect()->back()->with('fail', 'Whoops something wrong');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->data['current_plan'] = $this->planObj->find(['id' => $id]);
        if ($this->data['current_plan']) {

            $this->data['title'] = (object)[
                'name' => $this->data['current_plan']->name,
                'back' => (object)[
                    'name' => ' Subscriptions',
                    'url' => URL::to('admin/subscription/')
                ],
                'parent' => (object)['name' => 'Plans'],
                'child' => (object)[
                    'name' => 'Edit',
                    'url' => URL::to('admin/subscription/'.$id.'/edit'),
                ]
            ];

            return view('admin.subscription.show', $this->data);
        } else {
            return view('admin.404');
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['current_plan'] = $this->planObj->find(['id' => $id]);
        $this->data['types'] = ['Normal', 'Premium'];
        if ($this->data['current_plan']) {
            $this->data['title'] = (object)['name' => 'Edit ' . $this->data['current_plan']->name, 'url' => URL::to('admin/subscription/'),
                'parent' => (object)['name' => 'Subscriptions']];

            return view('admin.subscription.edit', $this->data);
        } else {
            return view('admin.404');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */

    public function update(SubscriptionRequest $request, $id)
    {
        //return $request->all();
        $current_user = $this->planObj->find(['id' => $id]);
        if ($current_user) {
            $data = $request->except(['_token', '_method']);
            $data['features'] = json_encode ($data['features']);
            $data['order_option'] = isset($data['order_option']) ? 1 : 0 ;

//            if ($request->hasFile('profile_pic') && $current_user->profile_pic != '') {
//                $data['profile_pic'] = $this->helperObj->upload($request->file('profile_pic'), 'edit', $current_user->profile_pic);
//            }

            $current_vendor = $this->planObj->update(['id' => $id], $data);

            if ($current_vendor) {
                return redirect()->back()->with(['success' => 'updated successfully']);
            } else {
                return redirect()->back()->with(['fail' => 'whoops something wrong']);
            }
        } else {
            return view('admin.404');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->planObj->delete(['id' => $id]);
        return redirect()->route('admin.subscription.index')->with('success', 'Plan was removed successfully');
    }

}
