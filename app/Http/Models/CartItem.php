<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class CartItem extends Model
{
    protected $guarded = [];

    public function cart()
    {
        return $this->belongsTo(Cart::class,'cart_id');
    }

    public function item()
    {
        return $this->belongsTo(Item::class,'item_id');
    }

    public function itemPrice()
    {
        return $this->belongsTo(ItemPrice::class,'item_price');
    }

    public function options()
    {
        return $this->hasMany(CartItemOption::class,'cart_item_id');
    }
    public function sideDishes()
    {
      return $this->hasMany(CartItemSideDish::class,'cart_item_id');
    }
    /*public function sideDishes()
    {
        return $this->belongsToMany(ItemSideDish::class,'side_dishes','item_id','item_side_dishe_id');
    }*/

    public static function boot()
    {
        parent::boot();
        static::deleting(function($cartItem){
            CartItemOption::where('item_id',$cartItem->id)->delete();
            CartItemSideDish::where('item_id',$cartItem->id)->delete();
        });
    }

}
