<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class ItemView extends Model
{
    //

    protected $table = 'item_views';

    protected $fillable = ['item_id' , 'view'];

    public function item()
    {
        return $this->belongsTo(Item::class, 'item_id');
    }
}
