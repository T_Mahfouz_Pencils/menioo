<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class ItemOptionValue extends Model
{
    protected $table = 'item_option_values';

    protected $guarded = [];


    public function itemOption()
    {
        return $this->belongsTo(ItemOption::class, 'item_option_id');
    }

}
