<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class ItemOption extends Model
{
    protected $table = 'item_options';

    protected $guarded = [];


    public function item()
    {
        return $this->belongsTo(Item::class, 'item_id');
    }
    public function option()
    {
        return $this->belongsTo(Option::class, 'option_id');
    }
    public function optionValues()
    {
        return $this->hasMany(ItemOptionValue::class, 'item_option_id');
    }


}
