<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Table extends Model
{
    protected $fillable = ['customer_id', 'number', 'description',];
    protected $hidden = ['created_at', 'updated_at',];

    public function Customer()
    {
        return $this->belongsTo(Customer::class, 'id', 'customer_id');
    }

}
