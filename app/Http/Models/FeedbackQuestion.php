<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;

class FeedbackQuestion extends Model
{
    use Translatable;


    protected $guarded = [];
    protected $casts = [
        'mandatory' => 'boolean'
    ];

    public $translatedAttributes = ['value'];
    public $translationModel = 'App\Http\Models\QuestionTranslation';


    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }

    public function feedback()
    {
        return $this->belongsTo(Feedback::class, 'feedback_id');
    }

//    public function translate()
//    {
//        return $this->hasMany(\App\Http\Models\QuestionTranslation::class, 'question_id');
//    }
}
