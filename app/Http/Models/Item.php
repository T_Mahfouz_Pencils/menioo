<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
Use URL;
use Dimsav\Translatable\Translatable;


class Item extends Model
{
    use Translatable, SoftDeletes;

    protected $table = 'items';
    public $translationModel = 'App\Http\Models\ItemTranslation';
    public $translatedAttributes = ['name', 'description', 'notes'];


    protected $guarded = [];
//    public function translate()
//    {
//        return $this->hasMany(\App\Http\Models\ItemTranslation::class);
//    }
    public function menu()
    {
        return $this->belongsTo(Menu::class,'menu_id');
    }
    public function campaigns()
    {
        return $this->hasMany(Campaign::class,'item_id');
    }
    public function priceList()
    {
      return $this->hasMany(ItemPrice::class, 'item_id');
    }

    public function ingredientWarnings()
    {
        return $this->hasMany(ItemIngredientWarning::class, 'item_id');
    }
    public function sideDishes()
    {
        return $this->hasMany(ItemSideDish::class, 'item_id');
    }
    public function itemOptions()
    {
        return $this->hasMany(ItemOption::class, 'item_id');
    }

    /*public function ingredientWarnings()
    {
        return $this->belongsToMany(IngredientWarning::class, 'item_ingredient_warnings', 'item_id', 'ingredient_warning_id');
    }
    public function sideDishes()
    {
        return $this->belongsToMany(SideDish::class, 'item_side_dishes', 'item_id', 'side_dish_id');
    }
    public function itemOptions()
    {
        return $this->hasMany(ItemOption::class, 'item_id', 'id');
    }*/

    public function image()
    {
        return $this->belongsTo(Media::class, 'image_id');
    }

    public function video()
    {
        return $this->belongsTo(Media::class, 'video_id');
    }

    public function FullImagePath()
    {
        return $this->image ? URL::to('media/' . $this->image->media_path) : '';
    }

    public function FullThumbnailImagePath($size = '500X500')
    {
        return $this->image ? URL::to('thumbnail/' . str_replace('.' . $this->image->media_ext, '-' . $size . '.' . $this->image->media_ext, $this->image->media_path)) : '';
    }

    public function FullVideoPath()
    {
        return $this->video ? URL::to('media/' . $this->video->media_path) : '';
    }

    public function arrangment()
    {
        return $this->morphOne('App\Http\Models\Arrangment', 'arrangmentable');
    }


    public static function boot()
    {
        parent::boot();
        static::deleting(function($item){
            $item->translate()->delete();
        });
    }

    public function recommendeditems()
    {
        return $this->belongsToMany('App\Http\Models\Item', 'recommended_items','item_id', 'recommended_item_id');
    }
    public function recommendedtoitem()
    {
        return $this->belongsToMany('App\Http\Models\Item', 'recommended_items','recommended_item_id', 'item_id');
    }

    public function views()
    {
        return $this->hasMany(ItemView::class, 'item_id');
    }

}
