<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class CartItemSideDish extends Model
{
    protected $guarded = [];

    public function item()
    {
        return $this->belongsTo(Item::class,'item_id');
    }

    public function itemSideDish()
    {
        return $this->belongsTo(ItemSideDish::class,'item_side_dish_id');
    }

}
