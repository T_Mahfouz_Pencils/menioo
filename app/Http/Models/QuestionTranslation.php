<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionTranslation extends Model
{
    protected $fillable = [
        'feedback_question_id', 'value', 'locale'
    ];
}
