<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class ItemIngredientWarning extends Model
{
    protected $table = 'item_ingredient_warnings';

    protected $guarded = [];

    public function item()
    {
        return $this->belongsTo(Item::class, 'item_id');
    }
    public function ingredientWarning()
    {
        return $this->belongsTo(IngredientWarning::class, 'ingredient_warning_id');
    }


}
