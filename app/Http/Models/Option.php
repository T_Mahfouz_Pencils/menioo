<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    protected $table = 'options';

    protected $fillable = [
        'name', 'type', 'customer_id'
    ];

    protected $guarded = [];

    public function values(){
        return $this->hasMany(OptionValue::class, 'option_id');
    }
}
