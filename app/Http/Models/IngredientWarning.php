<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class IngredientWarning extends Model
{
    protected $table = 'ingredients_warnings';

    protected $fillable = [
        'name',
    ];
}
