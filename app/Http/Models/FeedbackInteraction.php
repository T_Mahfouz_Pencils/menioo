<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class FeedbackInteraction extends Model
{
    protected $guarded = [];

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }

    public function form()
    {
        return $this->belongsTo(Feedback::class, 'form_id');
    }

    public function question()
    {
        return $this->belongsTo(FeedbackQuestion::class, 'question_id');
    }


    
}
