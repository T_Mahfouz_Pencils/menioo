<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Configration extends Model
{
    //
    protected $table = 'configrations';

    protected $fillable = [
        'customer_id',
        'default_language',
        'other_languages',
        'show_language_icon',
        'screen_orintation',
        'currency',
        'show_info_icon',
        'show_feedback_icon',
        'show_labels',
        'order_option',
        'font_size',
        'font_style',
        'order_submmiting',
        'calories',
        'emails',
        'welcome_message'
    ];

    protected $casts = [
        'show_language_icon' => 'bool',
        'show_info_icon' => 'bool',
        'show_feedback_icon' => 'bool',
        'show_labels' => 'bool',
        'other_languages' => 'array',
    ];
}
