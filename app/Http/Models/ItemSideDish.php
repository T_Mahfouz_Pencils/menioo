<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use URL;

class ItemSideDish extends Model
{
    protected $table = 'item_side_dishes';

    protected $guarded = [];
    protected $appends = ['side_dish_image'];

    public function item()
    {
        return $this->belongsTo(Item::class, 'item_id');
    }
    public function side_dishe()
    {
        return $this->belongsTo(SideDish::class, 'side_dish_id');
    }

    public function getSideDishImageAttribute()
    {
        return $this->side_dishe ? $this->side_dishe->FullImagePath() : '';
    }

    public function FullThumbnailImagePath($size = '500X500')
    {
        return $this->image ? URL::to('thumbnail/' . str_replace('.' . $this->image->media_ext, '-' . $size . '.' . $this->image->media_ext, $this->image->media_path)) : '';
    }

}
