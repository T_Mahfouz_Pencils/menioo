<?php namespace App\Http\Services\Diner;

use App\Http\Models\Item;
use App\Http\Models\Menu;
use App\Http\Models\Arrangment;
use App\Http\Services\Diner\ArrangmentService;
use Illuminate\Database\Eloquent\Builder;

class ItemService
{
    private $model;
    private $menuModel;
    private $arrang;
    public function __construct(ArrangmentService $arrang, Item $model,Menu $menuModel)
    {
        $this->arrang = $arrang;
        $this->model = $model;
        $this->menuModel = $menuModel;
    }

    public function getAll($conditions, $with=[], $per_page = false)
    {
        $items = count($with) ? $this->model->with($with) : $this->model;
        return $per_page ? $items->where($conditions)->paginate($per_page) : $items->where($conditions)->get();
    }

    public function Topview($dates = [], $top = true)
    {
        $items =$this->model;
        if(count($dates))
            $items = $items->whereHas('views', function(Builder $query) use ($dates)
            {
                return $query->whereBetween('created_at',$dates);
            })->withCount('views');
        $items = $items->withCount('views');
        $items = $top ? $items->orderBy('views_count', 'Desc')->take(5)->get() : $items->orderBy('views_count', 'Asc')->take(5)->get();
        $items->map(function($item){
            $item->image_path = $item->FullImagePath();
            $item->makeHidden(['translations','image','updated_at','deleted_at','image_id','video_id']);
        });
        return $items;
    }

    public function insert($data)
    {
        $obj = $this->model->create($data);
        $this->arrang->insert(
            [
                'menu_id' => $obj->menu_id,
                'customer_id' => $obj->menu->customer_id,
                'order' => 0,
                'arrangmentable_type' => 'App\Http\Models\Item',
                'arrangmentable_id' => $obj->id
            ]
        );
        return $obj;
    }

    public function update($conditions, $data)
    {
        return $this->model->where($conditions)->update($data);
    }

    public function find($conditions,$with=[])
    {
        if(count($with))
            return $this->model->with($with)->where($conditions)->first();
        else
            return $this->model->where($conditions)->first();
    }

    public function getSuggestions($id,$with=[])
    {
        $item = $this->model->find($id);
        $menuID = $item[0]->menu_id;
        $subMenu = $this->menuModel->find($menuID);
        $items = $this->model->where([['id','!=',$id]])->limit(2)->get();
        if(count($items)) {
          $items->map(function($item){
              $item->makeHidden(['image','created_at','updated_at','deleted_at','video_id','image_id','priceList']);
              $item->item_image = $item->FullImagePath();
              $item->prices = $item->priceList;
          });
        }
        return $items;
    }

    public function delete($conditions)
    {
        
        $items = $this->model->where($conditions)->get();
        foreach ($items as $key => $obj) {
            $this->arrang->delete([
                'arrangmentable_type' => 'App\Http\Models\Item',
                'arrangmentable_id' => $obj->id
            ]);
        }
        return $this->model->where($conditions)->delete();
    }

}
