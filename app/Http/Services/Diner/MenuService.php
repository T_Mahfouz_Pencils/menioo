<?php namespace App\Http\Services\Diner;

use App\Http\Models\Menu;


class MenuService
{

    private $model;
    public function __construct(Menu $model)
    {
        $this->model = $model;
    }

    public function insert($data)
    {
        return $this->model->create($data);
    }


    public function update($conditions, $data)
    {
        return $this->model->where($conditions)->update($data);
    }

    public function updateOrCreate($conditions, $data)
    {
        return $this->model->where($conditions)->updateOrCreate($data);
    }

    public function find($conditions)
    {
        return $this->model->with(['MenuImage', 'MenuVideo', 'Sections', 'Sections.SubMenus', 'SubMenus'])->where($conditions)->first();
    }

    public function getAll($conditions, $per_page = false)
    {
        return $per_page ? $this->model->with(['MenuImage'])->where($conditions)->paginate($per_page) : $this->model->with(['MenuImage'])->where($conditions)->get();
    }

    public function findIn($conditions, $inArray)
    {
        return $this->model->where($conditions)->whereIn('id',$inArray)->first();
    }

    public function delete($conditions)
    {
        return $this->model->where($conditions)->delete();
    }

    public function getTotalCount($conditions = [])
    {
        return count($conditions) ? $this->model->where($conditions)->count() : $this->model->count();
    }
}
