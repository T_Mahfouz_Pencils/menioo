<?php

namespace App\Http\Services\Diner;

use App\Http\Models\ItemTranslation;


class ItemTranslationService
{

    private $model;
    public function __construct(ItemTranslation $model)
    {
        $this->model = $model;
    }
    public function insert($data)
    {
        return $this->model->create($data);
    }

    public function update($conditions, $data)
    {
        return $this->model->where($conditions)->update($data);
    }

    public function find($conditions,$with=[])
    {
        if(count($with))
            return $this->model->with($with)->where($conditions)->first();
        else
            return $this->model->where($conditions)->first();
    }

    public function delete($conditions)
    {
        return $this->model->where($conditions)->delete();
    }


    public function copy($type, $id){
        $conditions = ($type == 'Menu') ?
                       ['arrangmentable_type' => 'App\Http\Models\Menu', 'arrangmentable_id' => $id] :
                       ['arrangmentable_type' => 'App\Http\Models\Item', 'arrangmentable_id' => $id] ;
        $arrangment = $this->find($conditions);
        if($arrangment){
            $newarrangment = $arrangment->replicate();
            $newarrangment->save();
        }
        
    }

}