<?php

namespace App\Http\Services\Diner;

use App\Http\Models\QuestionTranslation;


class QuestionTranslationService
{

    private $model;
    public function __construct(QuestionTranslation $model)
    {
        $this->model = $model;
    }
    public function insert($data)
    {
        return $this->model->create($data);
    }

    public function update($conditions, $data)
    {
        return $this->model->where($conditions)->update($data);
    }

    public function find($conditions,$with=[])
    {
        if(count($with))
            return $this->model->with($with)->where($conditions)->first();
        else
            return $this->model->where($conditions)->first();
    }

    public function delete($conditions)
    {
        return $this->model->where($conditions)->delete();
    }


    

}