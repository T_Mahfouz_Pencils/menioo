<?php namespace App\Http\Services\Diner;

use App\Http\Models\ItemOptionValue;


class ItemOptionValueService
{

    private $model;

    public function __construct(ItemOptionValue $model)
    {
        $this->model = $model;
    }

    public function getAll($conditions, $per_page = false)
    {
        return $per_page ? $this->model->where($conditions)->paginate($per_page) : $this->model->where($conditions)->get();
    }

    public function insert($data)
    {
        return $this->model->create($data);
    }

    public function bulkInsert($data)
    {
        return $this->model->insert($data);
    }

    public function update($conditions, $data)
    {
        return $this->model->where($conditions)->update($data);
    }

    public function find($conditions, $with = [])
    {
        if (count($with))
            return $this->model->with($with)->where($conditions)->first();
        else
            return $this->model->where($conditions)->first();
    }

    public function delete($conditions)
    {
        return $this->model->where($conditions)->delete();
    }

    public function deleteFrom($key, $arr)
    {
        return $this->model->whereIn($key,$arr)->delete();
    }
}
