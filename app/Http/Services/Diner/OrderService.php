<?php

namespace App\Http\Services\Diner;

use App\Http\Models\Order;
use Carbon\Carbon;


class OrderService
{

    private $model;
    public function __construct(Order $model)
    {
        $this->model = $model;
    }

    public function getAll($conditions, $with=[], $per_page = false)
    {
        $items = count($with) ? $this->model->with($with) : $this->model;
        return $per_page ? $items->where($conditions)->paginate($per_page) : $items->where($conditions)->get();
    }

    public function getAllinTime($conditions, $dates, $groupByDays = false){
        $items = $this->model;
        $items = $items->where($conditions)->whereBetween('created_at', $dates);
        $items = $groupByDays ? $items->orderBy('created_at')->get() : $items->get();
        $items = $groupByDays ? $items->groupBy(function($item) {
            return Carbon::parse($item->created_at)->format('D');
        })->toArray() : $items;

        if($groupByDays) {
            $days = ['Sat','Sun','Mon','Tue','Wed','Thu','Fri'];
            $existedDays = array_keys($items);
            $result = [];
            foreach($days as $day) {
                if(!in_array($day, $existedDays)) {
                    $result[$day] = 0;
                }
                else {
                    $result[$day] = round(collect($items[$day])->sum('total_cost'));
                }
            }
        } else
            $result = $items;
        return $result;
    }

    public function getAllWhereIn($key, $arr, $conditions=[], $dates=[], $with=[], $per_page = false)
    {
        $items = count($with) ? $this->model->with($with) : $this->model;
        $items = count($conditions) ? $items->where($conditions) : $items;
        $items = count($dates) ? $items->whereBetween('created_at',$dates) : $items;

        return $per_page ? $items->whereIn($key, $arr)->paginate($per_page) : $items->whereIn($key, $arr)->get();
    }

    public function insert($data)
    {
        return $this->model->create($data);
    }

    public function update($conditions, $data)
    {
        return $this->model->where($conditions)->update($data);
    }

    public function find($conditions,$with=[])
    {
        if(count($with))
            return $this->model->with($with)->where($conditions)->first();
        else
            return $this->model->where($conditions)->first();
    }

    public function delete($conditions)
    {
        return $this->model->where($conditions)->delete();
    }

    public function recentorder($dates = []){
        if(! count($dates))
            $dates = [Carbon::now(), Carbon::now()->addDay()];
        return $this->model->whereBetween('created_at', $dates)->get();
        
    }

}
