<?php namespace App\Http\Services\Diner;

use Illuminate\Support\Facades\Auth;


class DinerLoginService
{

    private $auth;
    public function __construct()
    {
        $this->auth = Auth::guard('diner');
    }

    public function login($credentials, $remember = 0)
    {
        return $this->auth->attempt($credentials, $remember);
    }

    public function loginAs($diner)
    {
        return $this->auth->loginUsingId($diner->id);
    }

    public function logout()
    {
        return $this->auth->logout();
    }
}
