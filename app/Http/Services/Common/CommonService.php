<?php

namespace App\Http\Services\Common;

/**
 *  Tarek Mahfouz
 */

class CommonService
{
    private $model;
    private static $namespace = 'App\Http\Models\\';
    /**
     * @param mixed $model
     */

    public function getAll($model ,$conditions = [], $with = [], $per_page = 0, $whereIn = [], $reverse = true, $orderBy = 'id', $take = 0, $whereDate = null)
    {
        $class = self::$namespace.$model;
        $this->model = new $class();

        $items = count($with) ? $this->model->with($with) : $this->model;
        $items = (count($whereIn) && isset($whereIn['key']) && isset($whereIn['values'])) ? $items->whereIn($whereIn['key'], $whereIn['values']) : $items;
        $items = $items->where($conditions);
        $items = $whereDate ? $items->whereDate($whereDate['0'],$whereDate['1'],$whereDate['2']) : $items;
        $items = $reverse ?
            (
                $per_page ?
                    $items->orderBy($orderBy,'DESC')->paginate($per_page) :
                    $items->orderBy($orderBy,'DESC')->get()
                    //$items->orderBy('id','DESC')->toSql()
            )
            :
            (
                $per_page ?
                    $items->paginate($per_page) :
                    $items->get()
            );
        $items = $take ? $items->take($take) : $items;
        return $items;
    }

    public function search($model , $keys, $value, $per_page = 0, $with = [], $reverse = true, $orderBy = 'id', $take = 0, $whereDate = null)
    {
        $class = self::$namespace.$model;
        $this->model = new $class();

        $items = count($with) ? $this->model->with($with) : $this->model;

        $items = $items->where(function($sql) use($keys, $value){
            $len = count($keys);
            $sql->where($keys[0], 'LIKE', '%'.$value.'%');
            if($len > 1) {
                for($i = 1; $i < $len - 1; $i++) {
                    $sql->orWhere($keys[$i], 'LIKE', '%'.$value.'%');
                }
            }
        });
        $items = $whereDate ? $items->whereDate($whereDate['0'],$whereDate['1'],$whereDate['2']) : $items;
        $items = $items->orderBy($orderBy,'DESC');
        $items = $per_page ? $items->paginate($per_page) : $items->get();

        $items = $take ? $items->take($take) : $items;
        return $items;
    }

    public function find($model ,$conditions = [], $with = [], $whereIn = [])
    {
        $class = self::$namespace.$model;
        $this->model = new $class();

        $item = count($with) ? $this->model->with($with) : $this->model;
        $item = (count($whereIn) && isset($whereIn['key']) && isset($whereIn['values'])) ? $item->whereIn($whereIn['key'], $whereIn['values'])->where($conditions) : $item->where($conditions);
        $item = $item->first();

        return $item;
    }

    public function getById($model ,$conditions)
    {
        $class = self::$namespace.$model;
        $this->model = new $class();

        $item = $this->model->where($conditions)->first();
        return $item;
    }

    public function create($model ,array $data)
    {
        $class = self::$namespace.$model;
        $this->model = new $class();

        $item = $this->model->create($data);
        return $item;
    }

    public function bulkInsert($model ,array $data)
    {
        $class = self::$namespace.$model;
        $this->model = new $class();

        $item = $this->model->insert($data);
        return $item;
    }

    public function update($model ,$conditions, array $data, $whereIn = [])
    {
        $class = self::$namespace.$model;
        $this->model = new $class();

        $items = $this->model;
        $items = (count($whereIn) && isset($whereIn['key']) && isset($whereIn['values'])) ? $items->whereIn($whereIn['key'], $whereIn['values'])->where($conditions) : $items->where($conditions);
        $items = $items->update($data);

        $item = $this->model->where($conditions)->first();
        return $item;
    }

    public function destroy($model ,$condition = [], $whereIn = [])
    {
        $class = self::$namespace.$model;
        $this->model = new $class();

        if((count($whereIn) && isset($whereIn['key']) && isset($whereIn['values'])) && !count($condition))
            return $this->model->whereIn($whereIn['key'], $whereIn['values'])->delete();
        elseif(!count($whereIn) && count($condition))
            return $this->model->where($condition)->delete();
        elseif((count($whereIn) && isset($whereIn['key']) && isset($whereIn['values'])) && count($condition))
            return $this->model->where($condition)->whereIn($whereIn['key'], $whereIn['values'])->delete();

        return false;
    }


}
