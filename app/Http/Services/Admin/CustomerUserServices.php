<?php namespace App\Http\Services\Admin;

use App\Http\Models\CustomerUser;


class CustomerUserServices
{

    private $model;
    public function __construct(CustomerUser $model)
    {
        $this->model = $model;
    }

    public function insert($data)
    {
        return $this->model->create($data);
    }

    public function update($conditions, $data)
    {
        return $this->model->where($conditions)->update($data);
    }

    public function updateOrCreate($conditions, $data)
    {
        return $this->model->where($conditions)->updateOrCreate($data);
    }

    public function find($conditions)
    {
        return $this->model->where($conditions)->first();
    }

    public function getAll($conditions, $per_page = false)
    {
        return $per_page ? $this->model->where($conditions)->paginate($per_page) : $this->model->where($conditions)->get();
    }

    public function delete($conditions)
    {
        return $this->model->where($conditions)->delete();
    }
}
