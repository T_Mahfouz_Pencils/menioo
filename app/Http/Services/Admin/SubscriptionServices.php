<?php namespace App\Http\Services\Admin;

use App\Http\Models\Subscription;


class SubscriptionServices
{

    private $model;
    public function __construct(Subscription $model)
    {
        $this->model = $model;
    }

    public function insert($data)
    {
        $response = $this->createonPaddle($data);
        $data['plan_id'] = $response->response->product_id;
        return $this->model->create($data);
    }


    public function update($conditions, $data)
    {
        return $this->model->where($conditions)->update($data);
    }

    public function updateOrCreate($conditions, $data)
    {
        return $this->model->where($conditions)->updateOrCreate($data);
    }

    public function find($conditions)
    {
        return $this->model->where($conditions)->first();
        //return $this->model->with(['Country', 'City','venues'])->where($conditions)->first();
    }

    public function getAll($conditions, $per_page = false)
    {
        return $per_page ? $this->model->where($conditions)->orderBy('order')->paginate($per_page) : $this->model->where($conditions)->orderBy('order')->get();
        //return $per_page ? $this->model->with([])->where($conditions)->paginate($per_page) : $this->model->with(['Country', 'City'])->where($conditions)->get();
    }

    public function delete($conditions)
    {
        return $this->model->where($conditions)->delete();
    }


    public function createonPaddle($data){
        $data['plan_type'] = 'month';
    //     $response = \Ixudra\Curl\Facades\Curl::to('https://vendors.paddle.com/api/2.0/subscription/plans_create ')
    //     ->withData( [
    //         'vendor_id' => '52571',
    //         'vendor_auth_code' => '76a7b9870e96b92ebfb9796e3699342788aa125567b37bcddd',
    //         'plan_name' => $data['name'],
    //         'plan_trial_days' => 0,
    //         'plan_length' => $data['billing_period'],
    //         'plan_type' => $data['plan_type'],
    //         'main_currency_code' => 'USD',
    //         'recurring_price_usd' => $data['price']
    //     ] )
    //     ->asJson( true )
    //     ->post();
    // return $response;
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://vendors.paddle.com/api/2.0/subscription/plans_create",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "vendor_id=52571&vendor_auth_code=76a7b9870e96b92ebfb9796e3699342788aa125567b37bcddd&plan_name=".$data['name']."&plan_type=".$data['plan_type']."&plan_length=12&main_currency_code=USD&recurring_price_usd=".$data['price'],
            CURLOPT_HTTPHEADER => array(
                "content-type: application/x-www-form-urlencoded"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
        return  "cURL Error #:" . $err;
        } else {
        return  json_decode($response);
        }
    }

}
