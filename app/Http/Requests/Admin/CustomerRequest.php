<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class CustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
                {
                    return [];
                }
            case 'POST':
                {
                    return [
                        'name' => 'required',
                        'company_name' => 'required',
                        // 'occupation' => 'required',
                        'email' => 'required|unique:customers',
                        // 'mobile_number' => 'required|unique:customers',
                        // 'country_id' => 'required|numeric',
                        // 'city_id' => 'required|numeric',
                        // 'state' => 'required'
                    ];
                }
            case 'PATCH':
            case 'PUT':
                {
                    return [

                        'name' => 'required',
                        'company_name' => 'required',
                        // 'occupation' => 'required',
                        'email' => 'required|email|unique:customers,email,' . $this->segment(3) . ',id',
                        // 'mobile_number' => 'required|unique:customers,email,' . $this->segment(3) . ',id',
                        // 'country_id' => 'required|numeric',
                        // 'city_id' => 'required|numeric',
                        // 'state' => 'required'
                    ];
                }
            default:
                break;
        }
    }
}
