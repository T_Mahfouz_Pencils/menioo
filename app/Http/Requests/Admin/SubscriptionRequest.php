<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class SubscriptionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
                {
                    return [];
                }
            case 'POST':
                {
                    return [
                        'order' => 'required|unique:plans',
                        'name' => 'required',
                        'price' => 'required',
                        'type' => 'required',
                        'tablets_number' => 'required',
                    ];
                }
            case 'PATCH':
            case 'PUT':
                {
                    return [
                        'order' => 'required|unique:plans,order,'.$this->segment(3),
                        'name' => 'required',
                        'price' => 'required',
                        'type' => 'required',
                        'tablets_number' => 'required',
                    ];
                }
            default:
                break;
        }
    }
}
