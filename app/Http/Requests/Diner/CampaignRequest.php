<?php

namespace App\Http\Requests\Diner;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class CampaignRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
                {
                    return [];
                }
            case 'POST': {
                return [
                    'title' => 'required',
                    'customer_id' => 'required',
                    'item_id' => 'required_if:triggered_after,==,null',
                    'triggered_after' => 'required_if:item_id,==,null',
                    'active' => 'required',
                    'end_after' => 'required_if:automatically_end,==,1',
                ];
            }
            case 'PATCH':
            case 'PUT':
                {
                    return [
                        'title' => 'required',
                        'customer_id' => 'required',
                        'item_id' => 'required_if:triggered_after,==,null',
                        'triggered_after' => 'required_if:item_id,==,null',
                        'active' => 'required',
                        'end_after' => 'required_if:automatically_end,==,1',
                    ];
                }
            default:
                break;
        }
    }

    public function messages()
    {
        return [
            'title.required' => 'Must define campaign title',
            'customer_id.required' => 'Must define customer',
            'item_id.required' => 'Must define item if not triggered exists',
            'triggered_after.required' => 'Must define item if not item exists',
            'active.required' => 'Must define active or not',
            //'active.required' => 'Must define active or not',
        ];
    }
}
