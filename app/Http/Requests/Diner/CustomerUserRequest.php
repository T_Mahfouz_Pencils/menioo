<?php

namespace App\Http\Requests\Diner;

use Illuminate\Foundation\Http\FormRequest;

class CustomerUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
                {
                    return [];
                }
            case 'POST':
                {
                    return [
                        'name' => 'required',
                        'email' => 'required|unique:customer_users',
                        'password' => ['required', 'string', 'min:8', 'confirmed']
                    ];
                }
            case 'PATCH':
            case 'PUT':
                {
                    return [

                        'name' => 'required',
                        'email' => 'required|email|unique:customer_users,email,' . $this->segment(3) . ',id'
                        
                        
                    ];
                }
            default:
                break;
        }
    }
}
