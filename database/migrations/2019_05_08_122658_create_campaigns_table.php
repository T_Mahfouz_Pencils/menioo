<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaigns', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('customer_id');
            $table->unsignedInteger('item_id')->nullable();
            $table->integer('triggered_after')->nullable()->default(10);
            $table->string('title');
            $table->text('description')->nullable();
            $table->boolean('active')->default(0);
            $table->string('url')->nullable();
            $table->unsignedInteger('image_id')->nullable();
            $table->unsignedInteger('video_id')->nullable();
            $table->boolean('automatically_end')->default(1);
            $table->integer('end_after')->default(10);
            $table->integer('repetation')->default(1);
            $table->timestamps();
            $table->foreign('image_id')->references('id')->on('media')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('video_id')->references('id')->on('media')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('customer_id')->references('id')->on('customers')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('item_id')->references('id')->on('items')->onUpdate('cascade')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaigns');
    }
}
