<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('parent_id')->default(0);
            $table->string('name', 2000);
            $table->string('company_name', 2000);
            $table->string('occupation', 2000);
            $table->string('mobile_number');
            $table->string('email');
            $table->integer('country_id')->unsigned()->nullable();
            $table->integer('city_id')->unsigned()->nullable();
            $table->string('state', 2000);
            $table->string('email_token', 255)->nullable();
            $table->tinyInteger('is_active')->default(0);
            $table->text('description')->nullable();
            $table->unsignedInteger('plan_id')->nullable();
            $table->unsignedInteger('logo_id')->nullable();
            $table->tinyInteger('waiter_login')->default(1);
            $table->string('address')->nullable();
            $table->string('region')->nullable();
            $table->string('timezone')->nullable();
            $table->string('website')->nullable();
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('instagram')->nullable();
            $table->string('foursquare')->nullable();
            $table->softDeletes();
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('parent_id')->references('id')->on('customers')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('logo_id')->references('id')->on('media')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('plan_id')->references('id')->on('plans')->onUpdate('cascade')->onDelete('set null');
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('city_id')->references('id')->on('countries')->onDelete('set null')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
