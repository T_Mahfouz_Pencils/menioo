<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedbackInteractionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feedback_interactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('identifier');
            //$table->string('name')->nullable();
            //$table->string('email')->nullable();
            $table->unsignedInteger('customer_id');
            $table->unsignedInteger('customer_user_id')->nullable();
            $table->unsignedInteger('form_id');
            $table->unsignedInteger('question_id');
            $table->text('answer');
            $table->boolean('status')->default(1);
            $table->timestamps();
            $table->foreign('customer_id')->references('id')->on('customers')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('customer_user_id')->references('id')->on('customer_users')->onUpdate('cascade')->onDelete('set null');
            $table->foreign('form_id')->references('id')->on('feedback')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('question_id')->references('id')->on('feedback_questions')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feedback_interactions');
    }
}
