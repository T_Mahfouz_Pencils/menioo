<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configrations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id')->unsigned();
            $table->string('default_language');
            $table->string('other_languages');
            $table->tinyInteger('show_language_icon')->default(1);
            $table->string('screen_orintation');
            $table->string('currency');
            $table->tinyInteger('show_info_icon')->default(1);
            $table->tinyInteger('show_feedback_icon')->default(1);
            $table->tinyInteger('show_labels')->default(1);
            $table->string('order_option')->default('Browse');
            $table->integer('font_size');
            $table->string('font_style');
            $table->string('order_submmiting')->default('Waiter');
            $table->string('calories')->default('Cal');
            $table->json('emails')->nullable();
            $table->string('welcome_message')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('configrations');
    }
}
