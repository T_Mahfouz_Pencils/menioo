<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::enableForeignKeyConstraints();
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('menu_id');
            $table->string('name');
            $table->text('description');
            $table->string('portion');
            $table->string('preparation_time');
            $table->string('calories');
            $table->text('notes')->nullable();
            $table->text('ingredients_warnings')->nullable();
            $table->unsignedInteger('image_id')->nullable();
            $table->unsignedInteger('video_id')->nullable();
            $table->boolean('is_active')->default(0);
            $table->integer('order')->after('is_active')->default(1);
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('menu_id')->references('id')->on('menu')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('image_id')->references('id')->on('media')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('video_id')->references('id')->on('media')->onDelete('set null')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
