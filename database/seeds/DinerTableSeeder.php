<?php

use Illuminate\Database\Seeder;

class DinerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('customer_users')->insert([
            'name' => 'Mahmoud Abdelsamad',
            'email' => 'mahmoud@lucilles.com',
            'password' => bcrypt('admin'),
            'customer_id' => 1
        ]);
    }
}
