<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $obj = new \App\Http\Services\Common\MediaService();
        $helperObj = new \App\Http\Services\Common\Helper($obj);
        $data = [];
        $response = $helperObj->parseDataFromOutSource('http://restcountries.eu/rest/v2/all');
        //dd($response);
        if ($response) {
            foreach ($response as $one) {
                $data[] = ['name' => $one->name, 'parent_id' => 0];
            }
            if (count($data))
                DB::table('countries')->insert($data);
        }
    }
}
