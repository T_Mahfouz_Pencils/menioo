<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Locales
    |--------------------------------------------------------------------------
    |
    | Contains an array with the applications available locales.
    |
    */
    'locales' => array(
        'ace' => 'Aceh', 
        'af' => 'Afrikaans', 
        'agq' => 'Aghem', 
        'ak' => 'Akan' , 
        'an' => 'aragonés' , 
        'cch' => 'Atsam', 
        'gn' => 'Avañe’ẽ', 
        'ae' => 'avesta', 
        'ay' => 'aymar aru' , 
        'az' => 'azərbaycanca' , 
        'id' => 'Bahasa Indonesia' , 
        'ms' => 'Bahasa Melayu' , 
        'bm' => 'bamanakan', 
        'jv' => 'Basa Jawa', 
        'su' => 'Basa Sunda', 
        'bh' => 'Bihari', 
        'bi' => 'Bislama', 
        'nb' => 'Bokmål' , 
        'bs' => 'bosanski' , 
        'br' => 'brezhoneg' , 
        'ca' => 'català' , 
        'ch' => 'Chamoru', 
        'ny' => 'chiCheŵa', 
        'kde' => 'Chimakonde', 
        'sn' => 'chiShona', 
        'co' => 'corsu', 
        'cy' => 'Cymraeg' , 
        'da' => 'dansk' , 
        'se' => 'davvisámegiella' , 
        'de' => 'Deutsch' , 
        'luo' => 'Dholuo', 
        'nv' => 'Diné bizaad', 
        'dua' => 'duálá', 
        'et' => 'eesti' , 
        'na' => 'Ekakairũ Naoero', 
        'guz' => 'Ekegusii', 
        'en' => 'English' , 
        'en-AU' => 'Australian English' , 
        'en-GB' => 'British English' , 
        'en-US' => 'U.S. English' , 
        'es' => 'español' , 
        'eo' => 'esperanto', 
        'eu' => 'euskara' , 
        'ewo' => 'ewondo', 
        'ee' => 'eʋegbe', 
        'fil' => 'Filipino' , 
        'fr' => 'français' , 
        'fr-CA' => 'français canadien' , 
        'fy' => 'frysk' , 
        'fur' => 'furlan' , 
        'fo' => 'føroyskt' , 
        'gaa' => 'Ga', 
        'ga' => 'Gaeilge' , 
        'gv' => 'Gaelg' , 
        'sm' => 'Gagana fa’a Sāmoa', 
        'gl' => 'galego' , 
        'ki' => 'Gikuyu', 
        'gd' => 'Gàidhlig' , 
        'ha' => 'Hausa' , 
        'bez' => 'Hibena', 
        'ho' => 'Hiri Motu', 
        'hr' => 'hrvatski' , 
        'bem' => 'Ichibemba' , 
        'io' => 'Ido', 
        'ig' => 'Igbo' , 
        'rn' => 'Ikirundi', 
        'ia' => 'interlingua' , 
        'iu-Latn' => 'Inuktitut' , 
        'sbp' => 'Ishisangu', 
        'nd' => 'isiNdebele', 
        'nr' => 'isiNdebele' , 
        'xh' => 'isiXhosa' , 
        'zu' => 'isiZulu' , 
        'it' => 'italiano' , 
        'ik' => 'Iñupiaq' , 
        'dyo' => 'joola', 
        'kea' => 'kabuverdianu', 
        'kaj' => 'Kaje', 
        'mh' => 'Kajin M̧ajeļ' , 
        'kl' => 'kalaallisut' , 
        'kln' => 'Kalenjin', 
        'kr' => 'Kanuri', 
        'kcg' => 'Katab', 
        'kw' => 'kernewek' , 
        'naq' => 'Khoekhoegowab', 
        'rof' => 'Kihorombo', 
        'kam' => 'Kikamba', 
        'kg' => 'Kikongo', 
        'jmc' => 'Kimachame', 
        'rw' => 'Kinyarwanda' , 
        'asa' => 'Kipare', 
        'rwk' => 'Kiruwa', 
        'saq' => 'Kisampur', 
        'ksb' => 'Kishambaa', 
        'swc' => 'Kiswahili ya Kongo', 
        'sw' => 'Kiswahili' , 
        'dav' => 'Kitaita', 
        'teo' => 'Kiteso', 
        'khq' => 'Koyra ciini', 
        'ses' => 'Koyraboro senni', 
        'mfe' => 'kreol morisien', 
        'ht' => 'Kreyòl ayisyen' , 
        'kj' => 'Kwanyama', 
        'ksh' => 'Kölsch', 
        'ebu' => 'Kĩembu', 
        'mer' => 'Kĩmĩrũ', 
        'lag' => 'Kɨlaangi', 
        'lah' => 'Lahnda', 
        'la' => 'latine', 
        'lv' => 'latviešu' , 
        'to' => 'lea fakatonga', 
        'lt' => 'lietuvių' , 
        'li' => 'Limburgs' , 
        'ln' => 'lingála', 
        'lg' => 'Luganda' , 
        'luy' => 'Luluhia', 
        'lb' => 'Lëtzebuergesch' , 
        'hu' => 'magyar' , 
        'mgh' => 'Makua', 
        'mg' => 'Malagasy' , 
        'mt' => 'Malti' , 
        'mtr' => 'Mewari', 
        'mua' => 'Mundang', 
        'mi' => 'Māori' , 
        'nl' => 'Nederlands' , 
        'nmg' => 'ngumba', 
        'yav' => 'nuasue', 
        'nn' => 'nynorsk' , 
        'oc' => 'occitan' , 
        'ang' => 'Old English', 
        'xog' => 'Olusoga', 
        'om' => 'Oromoo' , 
        'ng' => 'OshiNdonga', 
        'hz' => 'Otjiherero', 
        'uz-Latn' => 'oʼzbekcha' , 
        'nds' => 'Plattdüütsch' , 
        'pl' => 'polski' , 
        'pt' => 'português' , 
        'pt-BR' => 'português do Brasil' , 
        'ff' => 'Pulaar' , 
        'pi' => 'Pāli', 
        'aa' => 'Qafar' , 
        'ty' => 'Reo Māohi', 
        'ksf' => 'rikpa', 
        'ro' => 'română' , 
        'cgg' => 'Rukiga', 
        'rm' => 'rumantsch', 
        'qu' => 'Runa Simi', 
        'nyn' => 'Runyankore', 
        'ssy' => 'Saho', 
        'sc' => 'sardu' , 
        'de-CH' => 'Schweizer Hochdeutsch' , 
        'gsw' => 'Schwiizertüütsch', 
        'trv' => 'Seediq', 
        'seh' => 'sena', 
        'nso' => 'Sesotho sa Leboa' , 
        'st' => 'Sesotho' , 
        'tn' => 'Setswana' , 
        'sq' => 'shqip' , 
        'sid' => 'Sidaamu Afo' , 
        'ss' => 'Siswati' , 
        'sk' => 'slovenčina' , 
        'sl' => 'slovenščina' , 
        'so' => 'Soomaali' , 
        'sr-Latn' => 'Srpski' , 
        'sh' => 'srpskohrvatski', 
        'fi' => 'suomi' , 
        'sv' => 'svenska' , 
        'sg' => 'Sängö', 
        'tl' => 'Tagalog' , 
        'tzm-Latn' => 'Tamazight', 
        'kab' => 'Taqbaylit' , 
        'twq' => 'Tasawaq senni', 
        'shi' => 'Tashelhit', 
        'nus' => 'Thok Nath', 
        'vi' => 'Tiếng Việt' , 
        'tg-Latn' => 'tojikī' , 
        'lu' => 'Tshiluba' , 
        've' => 'Tshivenḓa', 
        'tw' => 'Twi', 
        'tr' => 'Türkçe' , 
        'ale' => 'Unangax tunuu', 
        'ca-valencia' => 'valencià', 
        'vai-Latn' => 'Viyamíĩ', 
        'vo' => 'Volapük', 
        'fj' => 'vosa Vakaviti', 
        'wa' => 'Walon' , 
        'wae' => 'Walser' , 
        'wen' => 'Wendic', 
        'wo' => 'Wolof' , 
        'ts' => 'Xitsonga' , 
        'dje' => 'Zarmaciine', 
        'yo' => 'Èdè Yorùbá' , 
        'de-AT' => 'Österreichisches Deutsch' , 
        'is' => 'íslenska' , 
        'cs' => 'čeština' , 
        'bas' => 'Ɓàsàa', 
        'mas' => 'ɔl-Maa', 
        'haw' => 'ʻŌlelo Hawaiʻi', 
        'el' => 'Ελληνικά' , 
        'uz' => 'Ўзбек' , 
        'az-Cyrl' => 'Азәрбајҹан' , 
        'ab' => 'Аҧсуа', 
        'os' => 'Ирон' , 
        'ky' => 'Кыргыз' , 
        'sr' => 'Српски' , 
        'av' => 'авар мацӀ', 
        'ady' => 'адыгэбзэ', 
        'ba' => 'башҡорт теле', 
        'be' => 'беларуская' , 
        'bg' => 'български' , 
        'kv' => 'коми кыв', 
        'mk' => 'македонски' , 
        'mn' => 'монгол' , 
        'ce' => 'нохчийн мотт' , 
        'ru' => 'русский' , 
        'sah' => 'саха тыла', 
        'tt' => 'татар теле' , 
        'tg' => 'тоҷикӣ' , 
        'tk' => 'түркменче' , 
        'uk' => 'українська' , 
        'cv' => 'чӑваш чӗлхи' , 
        'cu' => 'ѩзыкъ словѣньскъ', 
        'kk' => 'қазақ тілі' , 
        'hy' => 'Հայերեն' , 
        'yi' => 'ייִדיש' , 
        'he' => 'עברית' , 
        'ug' => 'ئۇيغۇرچە' , 
        'ur' => 'اردو' , 
        'ar' => 'العربية' , 
        'uz-Arab' => 'اۉزبېک', 
        'tg-Arab' => 'تاجیکی' , 
        'sd' => 'سنڌي' , 
        'fa' => 'فارسی' , 
        'pa-Arab' => 'پنجاب' , 
        'ps' => 'پښتو' , 
        'ks' => 'کأشُر' , 
        'ku' => 'کوردی' , 
        'dv' => 'ދިވެހިބަސް' , 
        'ks-Deva' => 'कॉशुर' , 
        'kok' => 'कोंकणी' , 
        'doi' => 'डोगरी' , 
        'ne' => 'नेपाली', 
        'pra' => 'प्राकृत', 
        'brx' => 'बड़ो' , 
        'bra' => 'ब्रज भाषा', 
        'mr' => 'मराठी' , 
        'mai' => 'मैथिली' , 
        'raj' => 'राजस्थानी', 
        'sa' => 'संस्कृतम्' , 
        'hi' => 'हिन्दी' , 
        'as' => 'অসমীয়া' , 
        'bn' => 'বাংলা' , 
        'mni' => 'মৈতৈ' , 
        'pa' => 'ਪੰਜਾਬੀ' , 
        'gu' => 'ગુજરાતી' , 
        'or' => 'ଓଡ଼ିଆ' , 
        'ta' => 'தமிழ்' , 
    ),

    /*
    |--------------------------------------------------------------------------
    | Locale separator
    |--------------------------------------------------------------------------
    |
    | This is a string used to glue the language and the country when defining
    | the available locales. Example: if set to '-', then the locale for
    | colombian spanish will be saved as 'es-CO' into the database.
    |
    */
    'locale_separator' => '-',

    /*
    |--------------------------------------------------------------------------
    | Default locale
    |--------------------------------------------------------------------------
    |
    | As a default locale, Translatable takes the locale of Laravel's
    | translator. If for some reason you want to override this,
    | you can specify what default should be used here.
    | If you set a value here it will only use the current config value
    | and never fallback to the translator one.
    |
    */
    'locale' => 'en',

    /*
    |--------------------------------------------------------------------------
    | Use fallback
    |--------------------------------------------------------------------------
    |
    | Determine if fallback locales are returned by default or not. To add
    | more flexibility and configure this option per "translatable"
    | instance, this value will be overridden by the property
    | $useTranslationFallback when defined
    |
    */
    'use_fallback' => true,

    /*
    |--------------------------------------------------------------------------
    | Use fallback per property
    |--------------------------------------------------------------------------
    |
    | The property fallback feature will return the translated value of
    | the fallback locale if the property is empty for the selected
    | locale. Note that 'use_fallback' must be enabled.
    |
     */
    'use_property_fallback' => true,

    /*
    |--------------------------------------------------------------------------
    | Fallback Locale
    |--------------------------------------------------------------------------
    |
    | A fallback locale is the locale being used to return a translation
    | when the requested translation is not existing. To disable it
    | set it to false.
    |
    */
    'fallback_locale' => 'en',

    /*
    |--------------------------------------------------------------------------
    | Translation Model Namespace
    |--------------------------------------------------------------------------
    |
    | Defines the default 'Translation' class namespace. For example, if
    | you want to use App\Translations\CountryTranslation instead of App\CountryTranslation
    | set this to 'App\Translations'.
    |
    */
    'translation_model_namespace' => null,

    /*
    |--------------------------------------------------------------------------
    | Translation Suffix
    |--------------------------------------------------------------------------
    |
    | Defines the default 'Translation' class suffix. For example, if
    | you want to use CountryTrans instead of CountryTranslation
    | application, set this to 'Trans'.
    |
    */
    'translation_suffix' => 'Translation',

    /*
    |--------------------------------------------------------------------------
    | Locale key
    |--------------------------------------------------------------------------
    |
    | Defines the 'locale' field name, which is used by the
    | translation model.
    |
    */
    'locale_key' => 'locale',

    /*
    |--------------------------------------------------------------------------
    | Always load translations when converting to array
    |--------------------------------------------------------------------------
    | Setting this to false will have a performance improvement but will
    | not return the translations when using toArray(), unless the
    | translations relationship is already loaded.
    |
     */
    'to_array_always_loads_translations' => true,
];
