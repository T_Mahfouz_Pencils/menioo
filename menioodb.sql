-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 17, 2019 at 05:15 AM
-- Server version: 5.6.40-84.0-log
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pencilde_menioo`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`id`, `title`, `content`, `image`, `created_at`, `updated_at`) VALUES
(1, 'title', 'content', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Super Admin', 'admin@menioo.com', '$2y$10$TX0x2AB2sXlkdsLq2pexset2.b2oql2niQYHK0G0ngywtHjKmjjMK', '8z1UUBKEydzORaJr9nubaZ5XwT19ayDxe5S4s0BCKY2c3D7ZtDibDZPB0pB7', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `arrangments`
--

CREATE TABLE `arrangments` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `arrangmentable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `arrangmentable_id` bigint(20) UNSIGNED NOT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `arrangments`
--

INSERT INTO `arrangments` (`id`, `menu_id`, `customer_id`, `arrangmentable_type`, `arrangmentable_id`, `order`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 2, 1, 'App\\Http\\Models\\Item', 2, 4, NULL, '2019-10-25 04:37:35', NULL),
(10, 8, 1, 'App\\Http\\Models\\Item', 7, 0, '2019-09-15 16:56:19', '2019-09-15 16:56:19', NULL),
(13, 8, 1, 'App\\Http\\Models\\Item', 10, 0, '2019-09-15 17:08:44', '2019-09-15 17:08:44', NULL),
(18, 1, 1, 'App\\Http\\Models\\Item', 15, 2, '2019-10-24 17:59:05', '2019-10-25 04:36:13', NULL),
(19, 1, 1, 'App\\Http\\Models\\Item', 16, 3, '2019-10-24 18:01:06', '2019-10-25 04:36:13', NULL),
(20, 1, 1, 'App\\Http\\Models\\Item', 17, 4, '2019-10-24 18:04:09', '2019-10-25 04:36:13', NULL),
(21, 1, 1, 'App\\Http\\Models\\Item', 18, 5, '2019-10-24 18:22:16', '2019-10-25 04:36:13', NULL),
(22, 2, 1, 'App\\Http\\Models\\Item', 19, 1, '2019-10-24 19:31:48', '2019-10-25 04:37:35', NULL),
(23, 2, 1, 'App\\Http\\Models\\Item', 20, 2, '2019-10-24 19:35:20', '2019-10-25 04:37:35', NULL),
(24, 2, 1, 'App\\Http\\Models\\Item', 21, 3, '2019-10-24 19:40:26', '2019-10-25 04:37:35', NULL),
(26, 7, 1, 'App\\Http\\Models\\Menu', 12, 0, '2019-10-24 19:49:42', '2019-10-24 19:49:42', NULL),
(29, 10, 1, 'App\\Http\\Models\\Menu', 15, 0, '2019-10-24 20:00:45', '2019-10-24 20:00:45', NULL),
(30, 10, 1, 'App\\Http\\Models\\Menu', 16, 0, '2019-10-24 20:02:16', '2019-10-24 20:02:16', NULL),
(31, 15, 1, 'App\\Http\\Models\\Item', 22, 1, '2019-10-24 21:00:18', '2019-10-25 04:39:02', NULL),
(32, 15, 1, 'App\\Http\\Models\\Item', 23, 2, '2019-10-24 21:02:41', '2019-10-25 04:39:02', NULL),
(33, 16, 1, 'App\\Http\\Models\\Item', 24, 0, '2019-10-24 21:15:56', '2019-10-24 21:15:56', NULL),
(34, 16, 1, 'App\\Http\\Models\\Item', 25, 0, '2019-10-24 21:19:08', '2019-10-24 21:19:08', NULL),
(35, 17, 1, 'App\\Http\\Models\\Item', 26, 1, '2019-10-24 21:23:14', '2019-10-24 21:35:54', NULL),
(36, 17, 1, 'App\\Http\\Models\\Item', 27, 2, '2019-10-24 21:25:43', '2019-10-24 21:35:54', NULL),
(37, 17, 1, 'App\\Http\\Models\\Item', 28, 3, '2019-10-24 21:31:05', '2019-10-24 21:35:54', NULL),
(38, 17, 1, 'App\\Http\\Models\\Item', 29, 4, '2019-10-24 21:34:38', '2019-10-24 21:35:54', NULL),
(39, 1, 1, 'App\\Http\\Models\\Item', 30, 0, '2019-11-05 23:42:33', '2019-11-05 23:42:33', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `campaigns`
--

CREATE TABLE `campaigns` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `item_id` int(10) UNSIGNED DEFAULT NULL,
  `repeats` int(11) NOT NULL DEFAULT '1',
  `triggered_after` int(11) DEFAULT '10',
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image_id` int(10) UNSIGNED DEFAULT NULL,
  `video_id` int(10) UNSIGNED DEFAULT NULL,
  `automatically_end` tinyint(1) NOT NULL DEFAULT '1',
  `end_after` int(11) NOT NULL DEFAULT '10',
  `repetation` int(11) NOT NULL DEFAULT '1',
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `campaigns`
--

INSERT INTO `campaigns` (`id`, `customer_id`, `item_id`, `repeats`, `triggered_after`, `title`, `description`, `active`, `created_at`, `updated_at`, `image_id`, `video_id`, `automatically_end`, `end_after`, `repetation`, `url`) VALUES
(4, 1, 2, 1, 5, 'New Dish', 'Offers Allowed', 1, '2019-11-06 00:42:05', '2019-11-06 00:42:05', 79, NULL, 1, 5, 4, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `table_number` int(11) DEFAULT NULL,
  `total_cost` double(8,2) NOT NULL DEFAULT '0.00',
  `ordered` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `customer_user_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `carts`
--

INSERT INTO `carts` (`id`, `customer_id`, `table_number`, `total_cost`, `ordered`, `created_at`, `updated_at`, `customer_user_id`) VALUES
(1, 1, 20, 40.00, 1, '2019-09-17 18:00:33', '2019-09-17 18:00:58', NULL),
(2, 1, 20, 40.00, 1, '2019-09-17 18:02:13', '2019-09-17 18:22:53', NULL),
(3, 1, 40, 75.00, 1, '2019-09-18 00:03:13', '2019-09-19 15:45:43', 1),
(4, 1, 40, 20.00, 1, '2019-09-19 15:45:35', '2019-09-19 15:45:51', NULL),
(5, 1, 40, 45.00, 1, '2019-09-20 14:14:42', '2019-10-20 22:36:38', NULL),
(6, 1, 13, 303.00, 1, '2019-10-01 16:49:59', '2019-10-20 22:45:26', NULL),
(7, 1, 13, 70.00, 1, '2019-10-08 17:16:38', '2019-10-20 22:45:30', NULL),
(8, 1, 13, 70.00, 1, '2019-10-08 17:54:25', '2019-10-20 22:45:33', NULL),
(9, 1, 0, 65.00, 1, '2019-10-17 02:08:09', '2019-10-20 22:45:38', 1),
(10, 1, 0, 70.00, 1, '2019-10-17 02:25:51', '2019-10-20 22:45:44', NULL),
(11, 1, 40, 70.00, 1, '2019-10-17 02:30:35', '2019-10-20 22:45:54', NULL),
(12, 1, 40, 70.00, 1, '2019-10-17 02:33:22', '2019-10-20 22:45:58', NULL),
(13, 1, 40, 70.00, 1, '2019-10-17 02:50:05', '2019-10-20 22:46:02', NULL),
(14, 1, 0, 70.00, 1, '2019-10-19 16:06:34', '2019-10-20 22:46:06', NULL),
(15, 1, 40, 75.00, 1, '2019-10-20 14:59:55', '2019-10-20 22:46:09', NULL),
(16, 1, 0, 140.00, 1, '2019-10-20 15:17:38', '2019-10-20 22:46:13', NULL),
(17, 1, 0, 140.00, 1, '2019-10-20 15:17:38', '2019-10-20 22:46:17', NULL),
(18, 1, 0, 73.00, 1, '2019-10-20 15:19:14', '2019-10-20 22:46:20', NULL),
(19, 1, 0, 50.00, 1, '2019-10-20 19:20:13', '2019-10-20 22:46:24', NULL),
(20, 1, 0, 90.00, 1, '2019-10-20 19:20:56', '2019-10-20 22:46:36', NULL),
(21, 1, 40, 40.00, 1, '2019-10-20 19:23:43', '2019-10-20 22:46:41', NULL),
(22, 1, 0, 150.00, 1, '2019-10-20 20:38:10', '2019-10-20 22:46:45', NULL),
(23, 1, 0, 179.00, 1, '2019-10-20 21:48:33', '2019-10-20 22:46:49', NULL),
(24, 1, 0, 40.00, 1, '2019-10-20 22:06:12', '2019-10-20 22:47:03', NULL),
(25, 1, 0, 23.00, 1, '2019-10-20 22:06:16', '2019-10-20 22:47:07', NULL),
(26, 1, 1, 0.00, 1, '2019-10-20 22:10:35', '2019-10-20 22:47:12', 2),
(27, 1, 1, 0.00, 1, '2019-10-20 22:19:10', '2019-10-20 22:47:16', NULL),
(28, 1, 0, 210.00, 1, '2019-10-20 22:35:03', '2019-10-20 22:47:19', NULL),
(29, 1, 0, 45.00, 1, '2019-10-20 22:45:15', '2019-10-20 22:47:22', NULL),
(30, 1, 0, 45.00, 1, '2019-10-20 22:47:41', '2019-10-20 22:48:09', NULL),
(31, 1, 40, 40.00, 1, '2019-10-20 22:48:44', '2019-10-20 22:49:03', NULL),
(32, 1, 40, 45.00, 1, '2019-10-20 22:50:45', '2019-10-22 17:44:08', NULL),
(33, 1, 40, 320.00, 1, '2019-10-20 22:52:33', '2019-10-22 20:44:23', NULL),
(34, 1, 0, 70.00, 1, '2019-10-21 00:21:44', '2019-10-22 11:02:02', 1),
(35, 1, 0, 28.00, 0, '2019-10-22 11:01:52', '2019-10-22 11:01:52', 1),
(36, 1, 0, 70.00, 1, '2019-10-22 17:42:22', '2019-10-26 22:37:07', NULL),
(37, 1, 0, 195.00, 0, '2019-10-22 17:45:44', '2019-10-22 17:45:44', NULL),
(38, 1, 0, 28.00, 0, '2019-10-22 17:49:49', '2019-10-22 17:49:49', NULL),
(39, 1, 0, 70.00, 1, '2019-10-22 17:57:40', '2019-10-26 22:37:18', NULL),
(40, 1, 0, 70.00, 0, '2019-10-22 17:58:39', '2019-10-22 17:58:39', NULL),
(41, 1, 0, 50.00, 1, '2019-10-22 18:00:20', '2019-10-26 22:44:24', NULL),
(42, 1, 13, 0.00, 0, '2019-10-22 18:57:36', '2019-10-22 18:57:36', 2),
(43, 1, 0, 335.00, 1, '2019-10-22 20:36:57', '2019-10-27 19:01:36', NULL),
(44, 1, 0, 180.00, 1, '2019-10-22 20:41:49', '2019-10-27 19:07:41', NULL),
(45, 1, 0, 40.00, 0, '2019-10-22 20:45:32', '2019-10-22 20:45:32', NULL),
(46, 1, 0, 552.00, 1, '2019-10-25 22:20:33', '2019-10-27 19:07:45', NULL),
(47, 1, 0, 224.00, 1, '2019-10-26 21:13:38', '2019-10-27 19:07:49', NULL),
(48, 1, 0, 207.00, 1, '2019-10-26 22:23:36', '2019-10-27 19:14:32', NULL),
(49, 1, 0, 207.00, 1, '2019-10-26 22:24:29', '2019-11-06 00:58:23', NULL),
(50, 1, 0, 207.00, 1, '2019-10-26 22:24:44', '2019-11-06 01:00:05', NULL),
(51, 1, 0, 207.00, 0, '2019-10-26 22:25:29', '2019-10-26 22:25:29', NULL),
(52, 1, 0, 69.00, 0, '2019-10-26 22:29:13', '2019-10-26 22:29:13', NULL),
(53, 1, 0, 0.00, 0, '2019-10-26 22:31:12', '2019-10-26 22:31:12', NULL),
(54, 1, 0, 0.00, 0, '2019-10-26 22:31:28', '2019-10-26 22:31:28', NULL),
(55, 1, 0, 276.00, 0, '2019-10-26 22:36:13', '2019-10-26 22:36:13', NULL),
(56, 1, 0, 0.00, 0, '2019-10-26 22:39:28', '2019-10-26 22:39:28', NULL),
(57, 1, 0, 138.00, 0, '2019-10-26 22:40:01', '2019-10-26 22:40:01', NULL),
(58, 1, 0, 345.00, 0, '2019-10-26 22:43:47', '2019-10-26 22:43:47', NULL),
(59, 1, 0, 466.00, 1, '2019-10-27 18:11:11', '2019-10-27 19:12:11', NULL),
(60, 1, 0, 448.00, 1, '2019-10-27 19:00:47', '2019-10-27 19:11:09', NULL),
(61, 1, 0, 138.00, 0, '2019-10-30 04:05:43', '2019-10-30 04:05:43', 1),
(62, 1, 0, 180.00, 0, '2019-11-02 13:34:37', '2019-11-02 13:34:37', NULL),
(63, 1, 0, 180.00, 0, '2019-11-03 18:14:25', '2019-11-03 18:14:25', NULL),
(64, 1, 0, 180.00, 0, '2019-11-06 00:58:02', '2019-11-06 00:58:02', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cart_items`
--

CREATE TABLE `cart_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `cart_id` int(10) UNSIGNED NOT NULL,
  `item_id` int(10) UNSIGNED NOT NULL,
  `item_price` int(10) UNSIGNED NOT NULL,
  `quantity` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `item_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'new'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cart_items`
--

INSERT INTO `cart_items` (`id`, `cart_id`, `item_id`, `item_price`, `quantity`, `created_at`, `updated_at`, `item_status`) VALUES
(1, 1, 11, 40, 1, '2019-09-17 18:00:33', '2019-09-17 18:00:33', 'old'),
(2, 2, 11, 40, 1, '2019-09-17 18:02:13', '2019-09-17 18:02:13', 'old'),
(3, 3, 3, 70, 1, '2019-09-18 00:03:13', '2019-09-18 00:03:13', 'old'),
(4, 4, 9, 20, 1, '2019-09-19 15:45:35', '2019-09-19 15:45:35', 'old'),
(5, 5, 1, 40, 1, '2019-09-20 14:14:42', '2019-09-20 14:14:42', 'old'),
(6, 6, 6, 65, 3, '2019-10-01 16:49:59', '2019-10-01 16:49:59', 'old'),
(7, 6, 8, 30, 1, '2019-10-01 16:49:59', '2019-10-01 16:49:59', 'old'),
(8, 6, 4, 70, 1, '2019-10-01 16:49:59', '2019-10-01 16:49:59', 'old'),
(9, 7, 4, 70, 1, '2019-10-08 17:16:38', '2019-10-08 17:16:38', 'old'),
(10, 8, 2, 70, 1, '2019-10-08 17:54:25', '2019-10-08 17:54:25', 'old'),
(11, 9, 6, 65, 1, '2019-10-17 02:08:09', '2019-10-17 02:08:09', 'old'),
(12, 10, 4, 70, 1, '2019-10-17 02:25:51', '2019-10-17 02:25:51', 'old'),
(13, 11, 4, 70, 1, '2019-10-17 02:30:35', '2019-10-17 02:30:35', 'old'),
(14, 12, 4, 70, 1, '2019-10-17 02:33:22', '2019-10-17 02:33:22', 'old'),
(15, 13, 4, 70, 1, '2019-10-17 02:50:05', '2019-10-17 02:50:05', 'old'),
(16, 14, 4, 70, 1, '2019-10-19 16:06:34', '2019-10-19 16:06:34', 'old'),
(17, 15, 3, 70, 1, '2019-10-20 14:59:55', '2019-10-20 14:59:55', 'old'),
(18, 16, 4, 70, 2, '2019-10-20 15:17:38', '2019-10-20 15:17:38', 'old'),
(19, 17, 4, 70, 2, '2019-10-20 15:17:38', '2019-10-20 15:17:38', 'old'),
(20, 18, 4, 70, 1, '2019-10-20 15:19:14', '2019-10-20 15:19:14', 'old'),
(21, 19, 8, 50, 1, '2019-10-20 19:20:13', '2019-10-20 19:20:13', 'old'),
(22, 20, 8, 50, 1, '2019-10-20 19:20:56', '2019-10-20 19:20:56', 'old'),
(23, 20, 11, 40, 1, '2019-10-20 19:20:56', '2019-10-20 19:20:56', 'old'),
(24, 21, 11, 40, 1, '2019-10-20 19:23:43', '2019-10-20 19:23:43', 'old'),
(25, 22, 8, 50, 3, '2019-10-20 20:38:11', '2019-10-20 20:38:11', 'old'),
(26, 23, 13, 23, 2, '2019-10-20 21:48:33', '2019-10-20 21:48:33', 'old'),
(27, 23, 3, 70, 1, '2019-10-20 21:48:33', '2019-10-20 21:48:33', 'old'),
(28, 23, 1, 40, 1, '2019-10-20 21:48:33', '2019-10-20 21:48:33', 'old'),
(29, 23, 13, 23, 1, '2019-10-20 21:48:33', '2019-10-20 21:48:33', 'old'),
(30, 24, 11, 40, 1, '2019-10-20 22:06:12', '2019-10-20 22:06:12', 'old'),
(31, 25, 13, 23, 1, '2019-10-20 22:06:16', '2019-10-20 22:06:16', 'old'),
(32, 26, 1, 0, 3, '2019-10-20 22:10:35', '2019-10-20 22:10:35', 'old'),
(33, 27, 1, 0, 3, '2019-10-20 22:19:10', '2019-10-20 22:19:10', 'old'),
(34, 28, 2, 70, 3, '2019-10-20 22:35:03', '2019-10-20 22:35:03', 'old'),
(35, 29, 11, 40, 1, '2019-10-20 22:45:15', '2019-10-20 22:45:15', 'old'),
(36, 30, 11, 40, 1, '2019-10-20 22:47:41', '2019-10-20 22:47:41', 'old'),
(37, 31, 11, 40, 1, '2019-10-20 22:48:44', '2019-10-20 22:48:44', 'old'),
(43, 32, 11, 40, 1, '2019-10-20 23:05:37', '2019-10-20 23:05:37', 'new'),
(39, 33, 11, 40, 3, '2019-10-20 22:52:33', '2019-10-20 22:52:33', 'old'),
(40, 33, 12, 40, 2, '2019-10-20 22:52:33', '2019-10-20 22:52:33', 'old'),
(41, 33, 12, 40, 2, '2019-10-20 22:52:33', '2019-10-20 22:52:33', 'old'),
(42, 33, 11, 40, 1, '2019-10-20 22:52:33', '2019-10-20 22:52:33', 'old'),
(44, 34, 2, 70, 1, '2019-10-21 00:21:44', '2019-10-21 00:21:44', 'old'),
(45, 35, 13, 23, 1, '2019-10-22 11:01:52', '2019-10-22 11:01:52', 'old'),
(46, 36, 2, 70, 1, '2019-10-22 17:42:22', '2019-10-22 17:42:22', 'old'),
(47, 37, 6, 65, 3, '2019-10-22 17:45:44', '2019-10-22 17:45:44', 'old'),
(48, 38, 13, 23, 1, '2019-10-22 17:49:49', '2019-10-22 17:49:49', 'old'),
(49, 39, 2, 70, 1, '2019-10-22 17:57:40', '2019-10-22 17:57:40', 'old'),
(50, 40, 3, 70, 1, '2019-10-22 17:58:39', '2019-10-22 17:58:39', 'old'),
(51, 41, 8, 50, 1, '2019-10-22 18:00:20', '2019-10-22 18:00:20', 'old'),
(52, 42, 1, 0, 3, '2019-10-22 18:57:36', '2019-10-22 18:57:36', 'old'),
(53, 43, 6, 65, 3, '2019-10-22 20:36:57', '2019-10-22 20:36:57', 'old'),
(54, 43, 2, 70, 2, '2019-10-22 20:36:57', '2019-10-22 20:36:57', 'old'),
(55, 44, 11, 40, 1, '2019-10-22 20:41:49', '2019-10-22 20:41:49', 'old'),
(56, 44, 8, 50, 1, '2019-10-22 20:41:49', '2019-10-22 20:41:49', 'old'),
(57, 44, 11, 40, 1, '2019-10-22 20:41:49', '2019-10-22 20:41:49', 'old'),
(58, 44, 8, 50, 1, '2019-10-22 20:41:49', '2019-10-22 20:41:49', 'old'),
(59, 45, 11, 40, 1, '2019-10-22 20:45:32', '2019-10-22 20:45:32', 'old'),
(60, 46, 19, 69, 4, '2019-10-25 22:20:33', '2019-10-25 22:20:33', 'old'),
(61, 46, 19, 69, 4, '2019-10-25 22:20:33', '2019-10-25 22:20:33', 'old'),
(62, 47, 22, 224, 1, '2019-10-26 21:13:38', '2019-10-26 21:13:38', 'old'),
(63, 48, 14, 69, 2, '2019-10-26 22:23:36', '2019-10-26 22:23:36', 'old'),
(64, 48, 17, 69, 1, '2019-10-26 22:23:36', '2019-10-26 22:23:36', 'old'),
(65, 49, 14, 69, 2, '2019-10-26 22:24:29', '2019-10-26 22:24:29', 'old'),
(66, 49, 17, 69, 1, '2019-10-26 22:24:29', '2019-10-26 22:24:29', 'old'),
(67, 50, 14, 69, 2, '2019-10-26 22:24:44', '2019-10-26 22:24:44', 'old'),
(68, 50, 17, 69, 1, '2019-10-26 22:24:44', '2019-10-26 22:24:44', 'old'),
(69, 51, 14, 69, 2, '2019-10-26 22:25:29', '2019-10-26 22:25:29', 'old'),
(70, 51, 17, 69, 1, '2019-10-26 22:25:29', '2019-10-26 22:25:29', 'old'),
(71, 52, 14, 69, 1, '2019-10-26 22:29:13', '2019-10-26 22:29:13', 'old'),
(72, 55, 14, 69, 2, '2019-10-26 22:36:13', '2019-10-26 22:36:13', 'old'),
(73, 55, 17, 69, 2, '2019-10-26 22:36:13', '2019-10-26 22:36:13', 'old'),
(74, 57, 17, 69, 2, '2019-10-26 22:40:01', '2019-10-26 22:40:01', 'old'),
(75, 58, 14, 69, 3, '2019-10-26 22:43:47', '2019-10-26 22:43:47', 'old'),
(76, 58, 17, 69, 2, '2019-10-26 22:43:47', '2019-10-26 22:43:47', 'old'),
(77, 59, 14, 69, 1, '2019-10-27 18:11:11', '2019-10-27 18:11:11', 'old'),
(78, 59, 17, 69, 3, '2019-10-27 18:11:11', '2019-10-27 18:11:11', 'old'),
(79, 59, 26, 190, 1, '2019-10-27 18:11:11', '2019-10-27 18:11:11', 'old'),
(80, 60, 26, 172, 1, '2019-10-27 19:00:47', '2019-10-27 19:00:47', 'old'),
(81, 60, 14, 69, 4, '2019-10-27 19:00:47', '2019-10-27 19:00:47', 'old'),
(82, 61, 15, 69, 1, '2019-10-30 04:05:43', '2019-10-30 04:05:43', 'old'),
(83, 61, 18, 69, 1, '2019-10-30 04:05:43', '2019-10-30 04:05:43', 'old'),
(84, 62, 27, 180, 1, '2019-11-02 13:34:37', '2019-11-02 13:34:37', 'old'),
(85, 63, 27, 180, 1, '2019-11-03 18:14:25', '2019-11-03 18:14:25', 'old'),
(86, 64, 27, 180, 1, '2019-11-06 00:58:02', '2019-11-06 00:58:02', 'old');

-- --------------------------------------------------------

--
-- Table structure for table `cart_item_options`
--

CREATE TABLE `cart_item_options` (
  `id` int(10) UNSIGNED NOT NULL,
  `item_id` int(10) UNSIGNED NOT NULL,
  `cart_id` int(10) UNSIGNED NOT NULL,
  `item_option_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cart_item_options`
--

INSERT INTO `cart_item_options` (`id`, `item_id`, `cart_id`, `item_option_id`, `created_at`, `updated_at`) VALUES
(1, 1, 5, 2, '2019-09-20 14:14:42', '2019-09-20 14:14:42');

-- --------------------------------------------------------

--
-- Table structure for table `cart_item_side_dishes`
--

CREATE TABLE `cart_item_side_dishes` (
  `id` int(10) UNSIGNED NOT NULL,
  `item_id` int(10) UNSIGNED NOT NULL,
  `cart_id` int(10) UNSIGNED NOT NULL,
  `item_side_dish_id` int(10) UNSIGNED NOT NULL,
  `price` double(8,2) NOT NULL DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cart_item_side_dishes`
--

INSERT INTO `cart_item_side_dishes` (`id`, `item_id`, `cart_id`, `item_side_dish_id`, `price`, `created_at`, `updated_at`) VALUES
(1, 3, 3, 3, 5.00, '2019-09-18 00:03:13', '2019-09-18 00:03:13'),
(2, 1, 5, 3, 5.00, '2019-09-20 14:14:42', '2019-09-20 14:14:42'),
(3, 4, 6, 2, 3.00, '2019-10-01 16:49:59', '2019-10-01 16:49:59'),
(4, 4, 6, 3, 5.00, '2019-10-01 16:49:59', '2019-10-01 16:49:59'),
(5, 3, 15, 3, 5.00, '2019-10-20 14:59:55', '2019-10-20 14:59:55'),
(6, 4, 18, 2, 3.00, '2019-10-20 15:19:14', '2019-10-20 15:19:14'),
(7, 11, 29, 3, 5.00, '2019-10-20 22:45:15', '2019-10-20 22:45:15'),
(8, 11, 30, 3, 5.00, '2019-10-20 22:47:41', '2019-10-20 22:47:41'),
(9, 11, 32, 3, 5.00, '2019-10-20 23:05:37', '2019-10-20 23:05:37'),
(10, 13, 35, 3, 5.00, '2019-10-22 11:01:52', '2019-10-22 11:01:52'),
(11, 13, 38, 3, 5.00, '2019-10-22 17:49:49', '2019-10-22 17:49:49');

-- --------------------------------------------------------

--
-- Table structure for table `configrations`
--

CREATE TABLE `configrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `default_language` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `other_languages` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `show_language_icon` tinyint(4) NOT NULL DEFAULT '1',
  `screen_orintation` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `show_info_icon` tinyint(4) NOT NULL DEFAULT '1',
  `show_feedback_icon` tinyint(4) NOT NULL DEFAULT '1',
  `show_labels` tinyint(4) NOT NULL DEFAULT '1',
  `order_option` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'browse',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `font_size` int(11) NOT NULL DEFAULT '14',
  `font_style` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'Arial',
  `order_submmiting` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Waiter',
  `calories` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Cal',
  `emails` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `welcome_message` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `configrations`
--

INSERT INTO `configrations` (`id`, `customer_id`, `default_language`, `other_languages`, `show_language_icon`, `screen_orintation`, `currency`, `show_info_icon`, `show_feedback_icon`, `show_labels`, `order_option`, `created_at`, `updated_at`, `font_size`, `font_style`, `order_submmiting`, `calories`, `emails`, `welcome_message`) VALUES
(30, 1, 'en', '\"[\\\"fr-CA\\\",\\\"ar\\\"]\"', 1, 'Portrait', 'EUR', 0, 0, 0, 'Order', '2019-11-06 00:56:03', '2019-11-06 00:56:03', 14, '1', 'Waiter', 'Cal', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `name`, `parent_id`, `created_at`, `updated_at`) VALUES
(1, 'Afghanistan', 0, NULL, NULL),
(2, 'Åland Islands', 0, NULL, NULL),
(3, 'Albania', 0, NULL, NULL),
(4, 'Algeria', 0, NULL, NULL),
(5, 'American Samoa', 0, NULL, NULL),
(6, 'Andorra', 0, NULL, NULL),
(7, 'Angola', 0, NULL, NULL),
(8, 'Anguilla', 0, NULL, NULL),
(9, 'Antarctica', 0, NULL, NULL),
(10, 'Antigua and Barbuda', 0, NULL, NULL),
(11, 'Argentina', 0, NULL, NULL),
(12, 'Armenia', 0, NULL, NULL),
(13, 'Aruba', 0, NULL, NULL),
(14, 'Australia', 0, NULL, NULL),
(15, 'Austria', 0, NULL, NULL),
(16, 'Azerbaijan', 0, NULL, NULL),
(17, 'Bahamas', 0, NULL, NULL),
(18, 'Bahrain', 0, NULL, NULL),
(19, 'Bangladesh', 0, NULL, NULL),
(20, 'Barbados', 0, NULL, NULL),
(21, 'Belarus', 0, NULL, NULL),
(22, 'Belgium', 0, NULL, NULL),
(23, 'Belize', 0, NULL, NULL),
(24, 'Benin', 0, NULL, NULL),
(25, 'Bermuda', 0, NULL, NULL),
(26, 'Bhutan', 0, NULL, NULL),
(27, 'Bolivia (Plurinational State of)', 0, NULL, NULL),
(28, 'Bonaire, Sint Eustatius and Saba', 0, NULL, NULL),
(29, 'Bosnia and Herzegovina', 0, NULL, NULL),
(30, 'Botswana', 0, NULL, NULL),
(31, 'Bouvet Island', 0, NULL, NULL),
(32, 'Brazil', 0, NULL, NULL),
(33, 'British Indian Ocean Territory', 0, NULL, NULL),
(34, 'United States Minor Outlying Islands', 0, NULL, NULL),
(35, 'Virgin Islands (British)', 0, NULL, NULL),
(36, 'Virgin Islands (U.S.)', 0, NULL, NULL),
(37, 'Brunei Darussalam', 0, NULL, NULL),
(38, 'Bulgaria', 0, NULL, NULL),
(39, 'Burkina Faso', 0, NULL, NULL),
(40, 'Burundi', 0, NULL, NULL),
(41, 'Cambodia', 0, NULL, NULL),
(42, 'Cameroon', 0, NULL, NULL),
(43, 'Canada', 0, NULL, NULL),
(44, 'Cabo Verde', 0, NULL, NULL),
(45, 'Cayman Islands', 0, NULL, NULL),
(46, 'Central African Republic', 0, NULL, NULL),
(47, 'Chad', 0, NULL, NULL),
(48, 'Chile', 0, NULL, NULL),
(49, 'China', 0, NULL, NULL),
(50, 'Christmas Island', 0, NULL, NULL),
(51, 'Cocos (Keeling) Islands', 0, NULL, NULL),
(52, 'Colombia', 0, NULL, NULL),
(53, 'Comoros', 0, NULL, NULL),
(54, 'Congo', 0, NULL, NULL),
(55, 'Congo (Democratic Republic of the)', 0, NULL, NULL),
(56, 'Cook Islands', 0, NULL, NULL),
(57, 'Costa Rica', 0, NULL, NULL),
(58, 'Croatia', 0, NULL, NULL),
(59, 'Cuba', 0, NULL, NULL),
(60, 'Curaçao', 0, NULL, NULL),
(61, 'Cyprus', 0, NULL, NULL),
(62, 'Czech Republic', 0, NULL, NULL),
(63, 'Denmark', 0, NULL, NULL),
(64, 'Djibouti', 0, NULL, NULL),
(65, 'Dominica', 0, NULL, NULL),
(66, 'Dominican Republic', 0, NULL, NULL),
(67, 'Ecuador', 0, NULL, NULL),
(68, 'Egypt', 0, NULL, NULL),
(69, 'El Salvador', 0, NULL, NULL),
(70, 'Equatorial Guinea', 0, NULL, NULL),
(71, 'Eritrea', 0, NULL, NULL),
(72, 'Estonia', 0, NULL, NULL),
(73, 'Ethiopia', 0, NULL, NULL),
(74, 'Falkland Islands (Malvinas)', 0, NULL, NULL),
(75, 'Faroe Islands', 0, NULL, NULL),
(76, 'Fiji', 0, NULL, NULL),
(77, 'Finland', 0, NULL, NULL),
(78, 'France', 0, NULL, NULL),
(79, 'French Guiana', 0, NULL, NULL),
(80, 'French Polynesia', 0, NULL, NULL),
(81, 'French Southern Territories', 0, NULL, NULL),
(82, 'Gabon', 0, NULL, NULL),
(83, 'Gambia', 0, NULL, NULL),
(84, 'Georgia', 0, NULL, NULL),
(85, 'Germany', 0, NULL, NULL),
(86, 'Ghana', 0, NULL, NULL),
(87, 'Gibraltar', 0, NULL, NULL),
(88, 'Greece', 0, NULL, NULL),
(89, 'Greenland', 0, NULL, NULL),
(90, 'Grenada', 0, NULL, NULL),
(91, 'Guadeloupe', 0, NULL, NULL),
(92, 'Guam', 0, NULL, NULL),
(93, 'Guatemala', 0, NULL, NULL),
(94, 'Guernsey', 0, NULL, NULL),
(95, 'Guinea', 0, NULL, NULL),
(96, 'Guinea-Bissau', 0, NULL, NULL),
(97, 'Guyana', 0, NULL, NULL),
(98, 'Haiti', 0, NULL, NULL),
(99, 'Heard Island and McDonald Islands', 0, NULL, NULL),
(100, 'Holy See', 0, NULL, NULL),
(101, 'Honduras', 0, NULL, NULL),
(102, 'Hong Kong', 0, NULL, NULL),
(103, 'Hungary', 0, NULL, NULL),
(104, 'Iceland', 0, NULL, NULL),
(105, 'India', 0, NULL, NULL),
(106, 'Indonesia', 0, NULL, NULL),
(107, 'Côte d\'Ivoire', 0, NULL, NULL),
(108, 'Iran (Islamic Republic of)', 0, NULL, NULL),
(109, 'Iraq', 0, NULL, NULL),
(110, 'Ireland', 0, NULL, NULL),
(111, 'Isle of Man', 0, NULL, NULL),
(112, 'Israel', 0, NULL, NULL),
(113, 'Italy', 0, NULL, NULL),
(114, 'Jamaica', 0, NULL, NULL),
(115, 'Japan', 0, NULL, NULL),
(116, 'Jersey', 0, NULL, NULL),
(117, 'Jordan', 0, NULL, NULL),
(118, 'Kazakhstan', 0, NULL, NULL),
(119, 'Kenya', 0, NULL, NULL),
(120, 'Kiribati', 0, NULL, NULL),
(121, 'Kuwait', 0, NULL, NULL),
(122, 'Kyrgyzstan', 0, NULL, NULL),
(123, 'Lao People\'s Democratic Republic', 0, NULL, NULL),
(124, 'Latvia', 0, NULL, NULL),
(125, 'Lebanon', 0, NULL, NULL),
(126, 'Lesotho', 0, NULL, NULL),
(127, 'Liberia', 0, NULL, NULL),
(128, 'Libya', 0, NULL, NULL),
(129, 'Liechtenstein', 0, NULL, NULL),
(130, 'Lithuania', 0, NULL, NULL),
(131, 'Luxembourg', 0, NULL, NULL),
(132, 'Macao', 0, NULL, NULL),
(133, 'Macedonia (the former Yugoslav Republic of)', 0, NULL, NULL),
(134, 'Madagascar', 0, NULL, NULL),
(135, 'Malawi', 0, NULL, NULL),
(136, 'Malaysia', 0, NULL, NULL),
(137, 'Maldives', 0, NULL, NULL),
(138, 'Mali', 0, NULL, NULL),
(139, 'Malta', 0, NULL, NULL),
(140, 'Marshall Islands', 0, NULL, NULL),
(141, 'Martinique', 0, NULL, NULL),
(142, 'Mauritania', 0, NULL, NULL),
(143, 'Mauritius', 0, NULL, NULL),
(144, 'Mayotte', 0, NULL, NULL),
(145, 'Mexico', 0, NULL, NULL),
(146, 'Micronesia (Federated States of)', 0, NULL, NULL),
(147, 'Moldova (Republic of)', 0, NULL, NULL),
(148, 'Monaco', 0, NULL, NULL),
(149, 'Mongolia', 0, NULL, NULL),
(150, 'Montenegro', 0, NULL, NULL),
(151, 'Montserrat', 0, NULL, NULL),
(152, 'Morocco', 0, NULL, NULL),
(153, 'Mozambique', 0, NULL, NULL),
(154, 'Myanmar', 0, NULL, NULL),
(155, 'Namibia', 0, NULL, NULL),
(156, 'Nauru', 0, NULL, NULL),
(157, 'Nepal', 0, NULL, NULL),
(158, 'Netherlands', 0, NULL, NULL),
(159, 'New Caledonia', 0, NULL, NULL),
(160, 'New Zealand', 0, NULL, NULL),
(161, 'Nicaragua', 0, NULL, NULL),
(162, 'Niger', 0, NULL, NULL),
(163, 'Nigeria', 0, NULL, NULL),
(164, 'Niue', 0, NULL, NULL),
(165, 'Norfolk Island', 0, NULL, NULL),
(166, 'Korea (Democratic People\'s Republic of)', 0, NULL, NULL),
(167, 'Northern Mariana Islands', 0, NULL, NULL),
(168, 'Norway', 0, NULL, NULL),
(169, 'Oman', 0, NULL, NULL),
(170, 'Pakistan', 0, NULL, NULL),
(171, 'Palau', 0, NULL, NULL),
(172, 'Palestine, State of', 0, NULL, NULL),
(173, 'Panama', 0, NULL, NULL),
(174, 'Papua New Guinea', 0, NULL, NULL),
(175, 'Paraguay', 0, NULL, NULL),
(176, 'Peru', 0, NULL, NULL),
(177, 'Philippines', 0, NULL, NULL),
(178, 'Pitcairn', 0, NULL, NULL),
(179, 'Poland', 0, NULL, NULL),
(180, 'Portugal', 0, NULL, NULL),
(181, 'Puerto Rico', 0, NULL, NULL),
(182, 'Qatar', 0, NULL, NULL),
(183, 'Republic of Kosovo', 0, NULL, NULL),
(184, 'Réunion', 0, NULL, NULL),
(185, 'Romania', 0, NULL, NULL),
(186, 'Russian Federation', 0, NULL, NULL),
(187, 'Rwanda', 0, NULL, NULL),
(188, 'Saint Barthélemy', 0, NULL, NULL),
(189, 'Saint Helena, Ascension and Tristan da Cunha', 0, NULL, NULL),
(190, 'Saint Kitts and Nevis', 0, NULL, NULL),
(191, 'Saint Lucia', 0, NULL, NULL),
(192, 'Saint Martin (French part)', 0, NULL, NULL),
(193, 'Saint Pierre and Miquelon', 0, NULL, NULL),
(194, 'Saint Vincent and the Grenadines', 0, NULL, NULL),
(195, 'Samoa', 0, NULL, NULL),
(196, 'San Marino', 0, NULL, NULL),
(197, 'Sao Tome and Principe', 0, NULL, NULL),
(198, 'Saudi Arabia', 0, NULL, NULL),
(199, 'Senegal', 0, NULL, NULL),
(200, 'Serbia', 0, NULL, NULL),
(201, 'Seychelles', 0, NULL, NULL),
(202, 'Sierra Leone', 0, NULL, NULL),
(203, 'Singapore', 0, NULL, NULL),
(204, 'Sint Maarten (Dutch part)', 0, NULL, NULL),
(205, 'Slovakia', 0, NULL, NULL),
(206, 'Slovenia', 0, NULL, NULL),
(207, 'Solomon Islands', 0, NULL, NULL),
(208, 'Somalia', 0, NULL, NULL),
(209, 'South Africa', 0, NULL, NULL),
(210, 'South Georgia and the South Sandwich Islands', 0, NULL, NULL),
(211, 'Korea (Republic of)', 0, NULL, NULL),
(212, 'South Sudan', 0, NULL, NULL),
(213, 'Spain', 0, NULL, NULL),
(214, 'Sri Lanka', 0, NULL, NULL),
(215, 'Sudan', 0, NULL, NULL),
(216, 'Suriname', 0, NULL, NULL),
(217, 'Svalbard and Jan Mayen', 0, NULL, NULL),
(218, 'Swaziland', 0, NULL, NULL),
(219, 'Sweden', 0, NULL, NULL),
(220, 'Switzerland', 0, NULL, NULL),
(221, 'Syrian Arab Republic', 0, NULL, NULL),
(222, 'Taiwan', 0, NULL, NULL),
(223, 'Tajikistan', 0, NULL, NULL),
(224, 'Tanzania, United Republic of', 0, NULL, NULL),
(225, 'Thailand', 0, NULL, NULL),
(226, 'Timor-Leste', 0, NULL, NULL),
(227, 'Togo', 0, NULL, NULL),
(228, 'Tokelau', 0, NULL, NULL),
(229, 'Tonga', 0, NULL, NULL),
(230, 'Trinidad and Tobago', 0, NULL, NULL),
(231, 'Tunisia', 0, NULL, NULL),
(232, 'Turkey', 0, NULL, NULL),
(233, 'Turkmenistan', 0, NULL, NULL),
(234, 'Turks and Caicos Islands', 0, NULL, NULL),
(235, 'Tuvalu', 0, NULL, NULL),
(236, 'Uganda', 0, NULL, NULL),
(237, 'Ukraine', 0, NULL, NULL),
(238, 'United Arab Emirates', 0, NULL, NULL),
(239, 'United Kingdom of Great Britain and Northern Ireland', 0, NULL, NULL),
(240, 'United States of America', 0, NULL, NULL),
(241, 'Uruguay', 0, NULL, NULL),
(242, 'Uzbekistan', 0, NULL, NULL),
(243, 'Vanuatu', 0, NULL, NULL),
(244, 'Venezuela (Bolivarian Republic of)', 0, NULL, NULL),
(245, 'Viet Nam', 0, NULL, NULL),
(246, 'Wallis and Futuna', 0, NULL, NULL),
(247, 'Western Sahara', 0, NULL, NULL),
(248, 'Yemen', 0, NULL, NULL),
(249, 'Zambia', 0, NULL, NULL),
(250, 'Zimbabwe', 0, NULL, NULL),
(251, 'Cairo', 68, NULL, NULL),
(252, 'Giza', 68, NULL, NULL),
(253, 'Alexandria', 68, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_name` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `occupation` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_id` int(10) UNSIGNED NOT NULL,
  `city_id` int(10) UNSIGNED DEFAULT NULL,
  `state` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `timezone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foursquare` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plan_id` int(10) UNSIGNED DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '0',
  `active_deactivate_date` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `logo_id` int(10) UNSIGNED DEFAULT NULL,
  `waiter_login` tinyint(4) NOT NULL DEFAULT '1',
  `email_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `company_name`, `occupation`, `mobile_number`, `email`, `country_id`, `city_id`, `state`, `website`, `facebook`, `address`, `timezone`, `twitter`, `instagram`, `foursquare`, `plan_id`, `password`, `remember_token`, `created_at`, `updated_at`, `is_active`, `active_deactivate_date`, `deleted_at`, `description`, `parent_id`, `logo_id`, `waiter_login`, `email_token`) VALUES
(1, 'lucilles', 'Asd', 'asd', '012312312', 'admin@lucilles.com', 68, NULL, 'Cairo', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '$2y$10$kOa6SKV2qR/dM17lXQtLbOo8qYZxH7ZaWxrUrQBrfAE3MNMaPE6lS', 'EX13NTU8QPFWILMscBfTuHbpP68swC7mAC2sAiGv5qGD0y393qBbgCaNy1Pl', '2019-05-13 09:02:23', '2019-07-24 11:13:18', 0, NULL, NULL, 'Customer About Us Content', 0, NULL, 1, NULL),
(2, 'My new venue 2', 'Asd', 'asd', '012312312', 'asd@asd.sad', 68, NULL, 'Cairo', 'Website.com', 'https://www.facebook.com', 'Address', 'Timezone', 'https://www.twitter.com', 'https://www.instagram.com', 'Foursquare', NULL, NULL, NULL, '2019-06-13 20:48:33', '2019-06-13 20:50:21', 0, NULL, NULL, NULL, 0, NULL, 1, NULL),
(3, 'My new venue 3', 'Asd', 'asd', '012312312', 'asd@asd.sad', 68, NULL, 'Cairo', 'Website.com', 'https://www.facebook.com', 'Address', 'Timezone', 'https://www.twitter.com', 'https://www.instagram.com', 'Foursquare', NULL, NULL, NULL, '2019-06-13 20:49:32', '2019-06-17 20:36:09', 0, NULL, NULL, NULL, 0, NULL, 1, NULL),
(4, 'Oak Egypt', 'Asd', 'asd', '012312312', 'asd@asd.sad', 68, NULL, 'Cairo', 'Test@test.com', 'Test@facebook.com', 'Cairo', 'Cairo', 'Test@twitter', 'Test@instagram.com', 'Test@foursquare', NULL, NULL, NULL, '2019-06-13 22:01:24', '2019-06-13 22:01:24', 0, NULL, NULL, NULL, 0, NULL, 1, NULL),
(5, 'Ayman Ali', 'Giza Systems', 'PM', '0101010', 'dev@dev.com', 68, 251, 'Cairo', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-06-16 17:08:24', '2019-06-16 17:08:36', 1, '2019-06-16 17:08:36', NULL, NULL, 0, NULL, 1, NULL),
(6, 'Samarqan Noodels', 'Asd', 'asd', '012312312', 'admin@lucilles.com', 68, NULL, 'Cairo', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-09 16:12:23', '2019-09-09 16:12:25', 0, NULL, NULL, 'Chinese Restaurant', 1, 18, 1, NULL),
(7, 'NewTestCustomer', 'NewTestCompany', 'Manager', '2019999999999', 'test@gmail.com', 68, 251, 'Cairo', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-22 21:38:08', '2019-09-22 22:14:45', 1, '2019-09-22 22:13:19', NULL, NULL, 0, NULL, 1, NULL),
(8, 'Mohamed Borhan', 'Resturaaaaaaaaaaants', '213123213', '01154477447', 'borhan_m7amed@live.com', 68, 251, 'Egypt', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-10-17 22:54:09', '2019-10-17 22:54:21', 1, '2019-10-17 22:54:16', NULL, NULL, 0, NULL, 1, NULL),
(9, 'Amen', 'Test', 'Manager', '101010101010', 'amen@me.com', 68, 252, 'sds', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-06 01:13:56', '2019-11-06 01:14:07', 1, '2019-11-06 01:14:04', NULL, NULL, 0, NULL, 1, NULL),
(10, 'eid', 'ded', '', '', 'eid@pencil-designs.com', 0, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$srxqKV1U5XH1CV.rKpGt1.m2qsSRms9MdIUHRlb614qXYCjP8rk/W', '7KrOZXd4LXhJC0UKLwsh54Ml4AJVOG6LdVlBltnfxoly3W6qjCGvHSYOQAAo', '2019-11-06 01:19:40', '2019-11-06 01:19:40', 0, NULL, NULL, NULL, 0, NULL, 1, NULL),
(11, 'asd', 'asdasdasd', '', '', 'asd@asd.com', 0, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$JBTIjOfIcETs1tJLX1fEZ.tUY1tlkE9cdM2035qJJnwrwlpznzDLO', '9CvT6i3D39XtPXm3t6jGqzwPUGdDhU7Jz0nQlKtRScs5HEhmNV5HEdeEGdlo', '2019-11-06 01:20:20', '2019-11-06 01:20:20', 0, NULL, NULL, NULL, 0, NULL, 1, NULL),
(12, 'eid', 'de', '', '', 'eslam@pencil-designs.com', 0, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$Tn/SLMNHA/dK2mUOnjvSgeWjk3aST9VzjCMPWi0hkwOvThp.UECry', 'YzE0jxzeWYNBIE5SJzDyoai4KpHJDo9m6VFXkmA0i3WQKgEC9XYdvXYzf7Ux', '2019-11-06 01:20:56', '2019-11-06 01:20:56', 0, NULL, NULL, NULL, 0, NULL, 1, NULL),
(13, 'eid', 'de', '', '', 'tarek@pencil-designs.com', 0, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$bK9jyfjOXQOjE4yupcBNQur0niAF.9AonJPs6cJi8UlOQGoIY2m06', 'lyAMjZiifC6OqyLWzqORvSyAZsrgkGALtdx338V8eNZKxJcivC1jCE9oATMF', '2019-11-06 01:22:49', '2019-11-06 01:22:49', 0, NULL, NULL, NULL, 0, NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customer_users`
--

CREATE TABLE `customer_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customer_users`
--

INSERT INTO `customer_users` (`id`, `name`, `email`, `password`, `customer_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Mahmoud Abdelsamad', 'mahmoud@lucilles.com', '$2y$10$WFp54F.v5yjNt3p9IywpcehN/9EqatnQOcJ72iG48cc1ZgoCNpLAa', 1, '9SHyujys8VHxMH9isJ4gPAqUmdTqHAMP9ClmbvFCze42ITx41j3wJtUZFwUG', NULL, NULL),
(2, 'Tarek Mahfouz', 'tarek@lucilles.com', '$2y$10$kOa6SKV2qR/dM17lXQtLbOo8qYZxH7ZaWxrUrQBrfAE3MNMaPE6lS', 1, 'yNcqBifNuRHc2hYw9JGDBFM0CGyKnfSSOiN6JiyQfsbCIigMxPaDtuIEwe9Q', '2019-05-20 13:03:28', '2019-05-20 13:03:28'),
(3, 'asd', 'asd@asd.com', '$2y$10$QUcP3Tf/jo.sAPvGJgR/4eUYWu/DBjrt/tMbE3LQRsMg9kQ8FgiMW', 1, NULL, '2019-11-06 00:55:04', '2019-11-06 00:55:04'),
(4, 'asd', 'aasd@asd.com', '$2y$10$mXHCB6zWSVigZE7Xnn7zy.TDe7P/YIPoPf.2EJ5ReKUb1V5E8C6lu', 1, NULL, '2019-11-06 01:05:34', '2019-11-06 01:05:34'),
(5, 'asd', 'aasds@asd.com', '$2y$10$zXdYNkEghqp49bW1g.hDpe1ISfWbVBP25UAR4aLyXopZvutWeC2rO', 1, NULL, '2019-11-06 01:06:11', '2019-11-06 01:06:11');

-- --------------------------------------------------------

--
-- Table structure for table `devices`
--

CREATE TABLE `devices` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `uid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `app_version` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `os` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_connected` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `devices`
--

INSERT INTO `devices` (`id`, `customer_id`, `uid`, `model`, `app_version`, `os`, `last_connected`, `created_at`, `updated_at`) VALUES
(4, 1, 'null', '02:00:00:00:00:00', '0.0.7', 'android', '2019-11-05 16:31:36', '2019-11-05 22:31:36', '2019-11-05 22:31:36'),
(5, 1, '358240051111110', '02:00:00:00:00:00', '0.0.7', 'android', '2019-11-11 08:36:00', '2019-11-11 14:36:00', '2019-11-11 14:36:00');

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `form_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `form_header` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`id`, `customer_id`, `form_name`, `form_header`, `active`, `created_at`, `updated_at`) VALUES
(1, 1, 'Form 1', 'Form Header', 1, '2019-07-18 11:00:39', '2019-11-06 00:15:49'),
(2, 1, 'Form 2', 'Form 2 Header', 0, '2019-07-18 11:01:06', '2019-11-05 22:27:22'),
(3, 1, 'Form 3', 'Form 3 Header', 0, '2019-07-18 11:01:25', '2019-07-18 11:01:25'),
(4, 1, 'Form 4', 'Form 4 Heaedr', 0, '2019-07-18 11:01:48', '2019-07-18 11:01:48');

-- --------------------------------------------------------

--
-- Table structure for table `feedback_interactions`
--

CREATE TABLE `feedback_interactions` (
  `id` int(10) UNSIGNED NOT NULL,
  `identifier` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `customer_user_id` int(10) UNSIGNED NOT NULL,
  `form_id` int(10) UNSIGNED NOT NULL,
  `question_id` int(10) UNSIGNED NOT NULL,
  `answer` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `feedback_interactions`
--

INSERT INTO `feedback_interactions` (`id`, `identifier`, `customer_id`, `customer_user_id`, `form_id`, `question_id`, `answer`, `status`, `created_at`, `updated_at`) VALUES
(6, '123', 1, 1, 6, 17, 'Answer 2!', 1, '2019-06-27 10:55:22', '2019-07-10 08:11:13'),
(5, '123', 1, 1, 6, 18, 'Answer!', 1, '2019-06-27 10:55:22', '2019-07-10 08:11:13'),
(7, '1234', 1, 1, 7, 17, 'Answer 2!', 1, '2019-06-27 10:55:22', '2019-07-10 08:11:13'),
(8, '1234', 1, 1, 7, 17, 'Answer!', 1, '2019-06-27 10:55:22', '2019-07-10 08:11:13'),
(9, '1235', 1, 2, 1, 1, 'answer 1', 1, NULL, '2019-11-05 23:40:14'),
(10, '1235', 1, 2, 1, 1, 'answer 2', 1, NULL, '2019-11-05 23:40:14'),
(11, '1236', 1, 2, 1, 1, 'answer 1', 1, NULL, '2019-11-06 00:14:39'),
(12, '1236', 1, 2, 1, 1, 'answer 2', 1, NULL, '2019-11-06 00:14:39'),
(13, '1237', 1, 2, 1, 16, 'asdas', 0, NULL, '2019-11-05 23:38:11'),
(14, '1237', 1, 2, 1, 17, 'ddfds', 0, NULL, '2019-11-05 23:38:11'),
(15, '1237', 1, 2, 1, 18, '3', 0, NULL, '2019-11-05 23:38:11'),
(16, '1237', 1, 2, 1, 19, '0', 0, NULL, '2019-11-05 23:38:11'),
(17, '1237', 1, 2, 1, 20, '0', 0, NULL, '2019-11-05 23:38:11'),
(18, '1237', 1, 2, 1, 0, 'asdasd', 0, NULL, '2019-11-05 23:38:11'),
(19, '1237', 1, 2, 1, 0, '231231', 0, NULL, '2019-11-05 23:38:11'),
(20, '1238', 1, 2, 1, 16, '1', 0, NULL, '2019-11-05 23:38:11'),
(21, '1238', 1, 2, 1, 17, '1', 0, NULL, '2019-11-05 23:38:11'),
(22, '1238', 1, 2, 1, 18, '1', 0, NULL, '2019-11-05 23:38:11'),
(23, '1238', 1, 2, 1, 19, '1', 0, NULL, '2019-11-05 23:38:11'),
(24, '1238', 1, 2, 1, 20, '1', 0, NULL, '2019-11-05 23:38:11'),
(25, '1239', 1, 2, 1, 16, 'ghvjhgvgh', 0, NULL, '2019-11-05 23:38:11'),
(26, '1239', 1, 2, 1, 17, 'hvnhvvn', 0, NULL, '2019-11-05 23:38:11'),
(27, '1239', 1, 2, 1, 18, '3', 0, NULL, '2019-11-05 23:38:11'),
(28, '1239', 1, 2, 1, 19, '1', 0, NULL, '2019-11-05 23:38:11'),
(29, '1239', 1, 2, 1, 20, '1', 0, NULL, '2019-11-05 23:38:11'),
(30, '1240', 1, 2, 1, 16, 'jlknkjnkj', 0, NULL, '2019-11-05 23:38:11'),
(31, '1240', 1, 2, 1, 17, 'njknjkj', 0, NULL, '2019-11-05 23:38:11'),
(32, '1240', 1, 2, 1, 18, '1', 0, NULL, '2019-11-05 23:38:11'),
(33, '1240', 1, 2, 1, 19, '1', 0, NULL, '2019-11-05 23:38:11'),
(34, '1240', 1, 2, 1, 20, '1', 0, NULL, '2019-11-05 23:38:11'),
(35, '1241', 1, 2, 1, 16, 'dsfds', 0, NULL, '2019-11-05 23:38:11'),
(36, '1241', 1, 2, 1, 17, 'dsfdsf', 0, NULL, '2019-11-05 23:38:11'),
(37, '1241', 1, 2, 1, 18, '1', 0, NULL, '2019-11-05 23:38:11'),
(38, '1241', 1, 2, 1, 19, '1', 0, NULL, '2019-11-05 23:38:11'),
(39, '1241', 1, 2, 1, 20, '1', 0, NULL, '2019-11-05 23:38:11'),
(40, '1242', 1, 2, 1, 16, 'sadas', 0, NULL, '2019-11-05 23:38:11'),
(41, '1242', 1, 2, 1, 17, 'dsds', 0, NULL, '2019-11-05 23:38:11'),
(42, '1242', 1, 2, 1, 18, '1', 0, NULL, '2019-11-05 23:38:11'),
(43, '1242', 1, 2, 1, 19, '1', 0, NULL, '2019-11-05 23:38:11'),
(44, '1242', 1, 2, 1, 20, '1', 0, NULL, '2019-11-05 23:38:11'),
(45, '1243', 1, 2, 1, 16, 'sadasd', 0, NULL, '2019-11-05 23:38:11'),
(46, '1243', 1, 2, 1, 17, 'dsfsdfs', 0, NULL, '2019-11-05 23:38:11'),
(47, '1243', 1, 2, 1, 18, '1', 0, NULL, '2019-11-05 23:38:11'),
(48, '1243', 1, 2, 1, 19, '1', 0, NULL, '2019-11-05 23:38:11'),
(49, '1243', 1, 2, 1, 20, '1', 0, NULL, '2019-11-05 23:38:11'),
(50, '1244', 1, 2, 1, 16, 'dfsdf', 0, NULL, '2019-11-05 23:38:11'),
(51, '1244', 1, 2, 1, 17, 'fsdfs', 0, NULL, '2019-11-05 23:38:11'),
(52, '1244', 1, 2, 1, 18, '1', 0, NULL, '2019-11-05 23:38:11'),
(53, '1244', 1, 2, 1, 19, '1', 0, NULL, '2019-11-05 23:38:11'),
(54, '1244', 1, 2, 1, 20, '1', 0, NULL, '2019-11-05 23:38:11'),
(55, '1245', 1, 2, 1, 16, 'asdas', 0, NULL, '2019-11-05 23:38:11'),
(56, '1245', 1, 2, 1, 17, 'asdsa', 0, NULL, '2019-11-05 23:38:11'),
(57, '1245', 1, 2, 1, 18, '1', 0, NULL, '2019-11-05 23:38:11'),
(58, '1245', 1, 2, 1, 19, '1', 0, NULL, '2019-11-05 23:38:11'),
(59, '1245', 1, 2, 1, 20, '1', 0, NULL, '2019-11-05 23:38:11'),
(60, '1246', 1, 2, 1, 16, 'zcxzc', 0, NULL, '2019-11-05 23:38:11'),
(61, '1246', 1, 2, 1, 17, 'CSADSA', 0, NULL, '2019-11-05 23:38:11'),
(62, '1246', 1, 2, 1, 18, '3', 0, NULL, '2019-11-05 23:38:11'),
(63, '1246', 1, 2, 1, 19, '1', 0, NULL, '2019-11-05 23:38:11'),
(64, '1246', 1, 2, 1, 20, '1', 0, NULL, '2019-11-05 23:38:11'),
(65, '1247', 1, 2, 1, 16, 'asda', 0, NULL, '2019-11-05 23:38:11'),
(66, '1247', 1, 2, 1, 17, 'sdasd', 0, NULL, '2019-11-05 23:38:11'),
(67, '1247', 1, 2, 1, 18, '1', 0, NULL, '2019-11-05 23:38:11'),
(68, '1247', 1, 2, 1, 19, '1', 0, NULL, '2019-11-05 23:38:11'),
(69, '1247', 1, 2, 1, 20, '1', 0, NULL, '2019-11-05 23:38:11'),
(70, '1248', 1, 2, 1, 16, 'asda`', 0, NULL, '2019-11-05 23:38:11'),
(71, '1248', 1, 2, 1, 17, 'sdfdsf', 0, NULL, '2019-11-05 23:38:11'),
(72, '1248', 1, 2, 1, 18, '1', 0, NULL, '2019-11-05 23:38:11'),
(73, '1248', 1, 2, 1, 19, '1', 0, NULL, '2019-11-05 23:38:11'),
(74, '1248', 1, 2, 1, 20, '1', 0, NULL, '2019-11-05 23:38:11'),
(75, '1249', 1, 2, 1, 16, 'xczxcCZXC', 0, NULL, '2019-11-05 23:38:11'),
(76, '1249', 1, 2, 1, 17, 'csadasdasd', 0, NULL, '2019-11-05 23:38:11'),
(77, '1249', 1, 2, 1, 18, '0', 0, NULL, '2019-11-05 23:38:11'),
(78, '1249', 1, 2, 1, 19, 'false', 0, NULL, '2019-11-05 23:38:11'),
(79, '1249', 1, 2, 1, 20, 'false', 0, NULL, '2019-11-05 23:38:11'),
(80, '1250', 1, 2, 1, 16, 'asdada', 0, NULL, '2019-11-05 23:38:11'),
(81, '1250', 1, 2, 1, 17, 'sadasd', 0, NULL, '2019-11-05 23:38:11'),
(82, '1250', 1, 2, 1, 18, '0', 0, NULL, '2019-11-05 23:38:11'),
(83, '1250', 1, 2, 1, 19, '1', 0, NULL, '2019-11-05 23:38:11'),
(84, '1250', 1, 2, 1, 20, '1', 0, NULL, '2019-11-05 23:38:11'),
(85, '1251', 1, 2, 1, 16, 'zxczx', 0, NULL, '2019-11-05 23:38:11'),
(86, '1251', 1, 2, 1, 17, 'ccsds', 0, NULL, '2019-11-05 23:38:11'),
(87, '1251', 1, 2, 1, 18, '', 0, NULL, '2019-11-05 23:38:11'),
(88, '1251', 1, 2, 1, 19, 'false', 0, NULL, '2019-11-05 23:38:11'),
(89, '1251', 1, 2, 1, 20, 'false', 0, NULL, '2019-11-05 23:38:11'),
(90, '1252', 1, 2, 1, 16, 'zxczxXZC', 0, NULL, '2019-11-05 23:38:11'),
(91, '1252', 1, 2, 1, 17, 'dscc', 0, NULL, '2019-11-05 23:38:11'),
(92, '1252', 1, 2, 1, 18, '', 0, NULL, '2019-11-05 23:38:11'),
(93, '1252', 1, 2, 1, 19, '', 0, NULL, '2019-11-05 23:38:11'),
(94, '1252', 1, 2, 1, 20, '', 0, NULL, '2019-11-05 23:38:11'),
(95, '1253', 1, 2, 1, 16, 'werwe', 0, NULL, '2019-11-05 23:38:11'),
(96, '1253', 1, 2, 1, 17, 'jkenknk', 0, NULL, '2019-11-05 23:38:11'),
(97, '1253', 1, 2, 1, 18, '', 0, NULL, '2019-11-05 23:38:11'),
(98, '1253', 1, 2, 1, 19, 'false', 0, NULL, '2019-11-05 23:38:11'),
(99, '1253', 1, 2, 1, 20, 'false', 0, NULL, '2019-11-05 23:38:11'),
(100, '1254', 1, 2, 1, 16, 'sssdfds', 0, NULL, '2019-11-05 23:38:11'),
(101, '1254', 1, 2, 1, 17, 'sdfdsf', 0, NULL, '2019-11-05 23:38:11'),
(102, '1254', 1, 2, 1, 18, '3', 0, NULL, '2019-11-05 23:38:11'),
(103, '1254', 1, 2, 1, 19, '1', 0, NULL, '2019-11-05 23:38:11'),
(104, '1254', 1, 2, 1, 20, '1', 0, NULL, '2019-11-05 23:38:11'),
(105, '1255', 1, 2, 1, 16, 'adasd', 0, NULL, '2019-11-05 23:38:11'),
(106, '1255', 1, 2, 1, 17, 'adsasd', 0, NULL, '2019-11-05 23:38:11'),
(107, '1255', 1, 2, 1, 18, '4', 0, NULL, '2019-11-05 23:38:11'),
(108, '1255', 1, 2, 1, 19, '1', 0, NULL, '2019-11-05 23:38:11'),
(109, '1255', 1, 2, 1, 20, '1', 0, NULL, '2019-11-05 23:38:11'),
(110, '1256', 1, 2, 1, 16, 'xhdb', 1, NULL, NULL),
(111, '1256', 1, 2, 1, 17, 'dnxb', 1, NULL, NULL),
(112, '1256', 1, 2, 1, 18, '2', 1, NULL, NULL),
(113, '1256', 1, 2, 1, 19, '1', 1, NULL, NULL),
(114, '1256', 1, 2, 1, 20, '1', 1, NULL, NULL),
(115, '1257', 1, 2, 1, 16, 'anjus', 1, NULL, NULL),
(116, '1257', 1, 2, 1, 17, 'sbhz', 1, NULL, NULL),
(117, '1257', 1, 2, 1, 18, '3', 1, NULL, NULL),
(118, '1257', 1, 2, 1, 19, '1', 1, NULL, NULL),
(119, '1257', 1, 2, 1, 20, '1', 1, NULL, NULL),
(120, '1258', 1, 2, 1, 1, 'answer 1', 1, NULL, NULL),
(121, '1258', 1, 2, 1, 1, 'answer 2', 1, NULL, NULL),
(122, '1259', 1, 1, 1, 16, 'kkkk', 1, NULL, NULL),
(123, '1259', 1, 1, 1, 17, 'mmmmmmmm', 1, NULL, NULL),
(124, '1259', 1, 1, 1, 18, '3', 1, NULL, NULL),
(125, '1259', 1, 1, 1, 19, 'false', 1, NULL, NULL),
(126, '1259', 1, 1, 1, 20, 'false', 1, NULL, NULL),
(127, '1260', 1, 2, 1, 1, 'answer 1', 1, NULL, NULL),
(128, '1260', 1, 2, 1, 1, 'answer 2', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `feedback_questions`
--

CREATE TABLE `feedback_questions` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `feedback_id` int(10) UNSIGNED NOT NULL,
  `type` enum('text','rate','bool','') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'text',
  `mandatory` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `feedback_questions`
--

INSERT INTO `feedback_questions` (`id`, `customer_id`, `feedback_id`, `type`, `mandatory`, `created_at`, `updated_at`) VALUES
(16, 1, 1, 'rate', 0, '2019-07-29 12:45:37', '2019-11-06 00:15:49'),
(17, 1, 1, 'text', 0, '2019-07-29 12:45:37', '2019-11-06 00:15:49'),
(18, 1, 1, 'rate', 0, '2019-07-29 12:45:37', '2019-11-06 00:15:49'),
(19, 1, 1, 'bool', 0, '2019-07-29 12:45:37', '2019-11-06 00:15:49'),
(20, 1, 1, 'bool', 0, '2019-07-29 12:45:37', '2019-11-06 00:15:49');

-- --------------------------------------------------------

--
-- Table structure for table `feedback_translations`
--

CREATE TABLE `feedback_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `feedback_id` int(10) UNSIGNED NOT NULL,
  `form_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `form_header` text COLLATE utf8mb4_unicode_ci,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `feedback_translations`
--

INSERT INTO `feedback_translations` (`id`, `feedback_id`, `form_name`, `form_header`, `locale`, `created_at`, `updated_at`) VALUES
(1, 1, 'Form 6565', NULL, 'en', '2019-07-18 11:57:27', '2019-07-18 11:58:43'),
(2, 1, 'Form 98989', NULL, 'da', '2019-07-18 12:00:27', '2019-07-18 12:00:27');

-- --------------------------------------------------------

--
-- Table structure for table `ingredients_warnings`
--

CREATE TABLE `ingredients_warnings` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `ingredients_warnings` text COMMENT 'text field',
  `portion` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `preparation_time` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `calories` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `image_id` int(10) UNSIGNED DEFAULT NULL,
  `video_id` int(10) UNSIGNED DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `menu_id`, `name`, `description`, `ingredients_warnings`, `portion`, `preparation_time`, `calories`, `notes`, `image_id`, `video_id`, `is_active`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'Hollandaise Egg Benedict', 'The best in class Eggs tossed with our signature hollandaise sause.\r\nThe best start for your morning!', NULL, '2', '10', '120', NULL, 43, NULL, 0, '2019-10-24 21:55:12', '2019-09-01 19:55:49', '2019-10-24 21:55:12'),
(2, 2, 'Paradise Pie', 'Although it\'s called a pie, probably because of it\'s graham cracker crust, You\'ll want to stay tuned for a bite!', NULL, '200', '15 Minutes', '100', NULL, 55, NULL, 1, NULL, '2019-09-01 20:47:17', '2019-10-25 04:38:04'),
(3, 1, 'Egg in a Toast', 'The perfect combination of Eggs and a Toast in  a perfect match.', NULL, '1', '5', '75', NULL, 8, NULL, 0, '2019-10-24 21:55:29', '2019-09-01 21:12:31', '2019-10-24 21:55:29'),
(4, 1, 'Croissant Sandwich', 'Our signature french croissant stuffed with numerous thinly sliced smoked turkey, lettuce, scrambled eggs and a sprinkle of shredded cheese.', NULL, '2', '10', '50', NULL, 11, NULL, 0, '2019-10-24 21:56:00', '2019-09-01 21:26:32', '2019-10-24 21:56:00'),
(5, 2, NULL, NULL, NULL, '1', '30', '150', NULL, 12, NULL, 1, NULL, '2019-09-01 22:08:27', '2019-09-01 22:08:55'),
(6, 2, 'Profiterole', 'A classic french dessert made to our standards!', NULL, '3', '30', '130', NULL, 14, NULL, 0, '2019-10-24 19:32:31', '2019-09-01 22:14:41', '2019-10-24 19:32:31'),
(7, 8, 'Pizza Cheese', 'Yummy and Hot', NULL, '8', '30 minutes', '390', NULL, 30, NULL, 1, NULL, '2019-09-15 16:56:19', '2019-09-15 17:54:29'),
(8, 7, 'Burger 2', 'New and delicious', NULL, '1', '20 Minutes', '790', NULL, 24, NULL, 1, NULL, '2019-09-15 16:59:43', '2019-10-24 17:52:50'),
(9, 8, 'White Cheese Salad', 'New Cheese Salad', NULL, '1 Plate', '10 minutes', '300 Cal', NULL, 35, NULL, 1, NULL, '2019-09-15 17:04:01', '2019-09-15 18:21:39'),
(10, 8, 'Olivi Pizza', 'New and Hot', NULL, '1', '20 Minutes', '200', NULL, 41, NULL, 1, NULL, '2019-09-15 17:08:44', '2019-09-17 16:31:03'),
(11, 7, NULL, NULL, NULL, '1', '30 minutes', '200', NULL, 27, NULL, 1, '2019-10-24 19:48:13', '2019-09-15 17:35:56', '2019-10-24 19:48:13'),
(12, 7, NULL, NULL, NULL, '250 gram', '30 minutes', '300', NULL, 28, NULL, 1, '2019-10-24 19:49:00', '2019-09-15 17:39:17', '2019-10-24 19:49:00'),
(13, 1, 'Oak\'s Deviled Eggs', 'sfssfs', NULL, NULL, '20', '100', NULL, 45, NULL, 0, '2019-10-24 21:54:53', '2019-10-20 21:23:57', '2019-10-24 21:54:53'),
(14, 15, 'Oak\'s Deviled Egg', 'The one and only Egg dish that you will crave for after tasting it for the first time! \r\n- SIDE: Baby Potatoes, Hashbrown or French Fries.\r\n- DRINK: Coffee, Tea or Orange Juice.', 'Free Gelatin, Free Salt', '100', '20 Minutes', '140', NULL, 46, NULL, 1, '2019-11-05 23:50:46', '2019-10-24 17:54:53', '2019-11-05 23:50:46'),
(15, 1, 'OAK’s Big Breakfast', 'America’s favorite hash brown and beef sausages alongside grilled tomatoes, mushroom, and beef bacon. With your choice of either coffee or freshly squeezed orange juice. \r\n\r\n- SIDE: Baby Potatoes, Hashbrown or French Fries\r\n\r\n- DRINK: Coffee, Tea or Orange Juice', NULL, '300', '30 Minutes', '100', NULL, 47, NULL, 1, NULL, '2019-10-24 17:59:05', '2019-10-25 04:35:48'),
(16, 1, 'OAK’s Special Omelette', 'Comes with Veggies or Cheese. Served with baby potatoes, beef bacon, sour cream and toast.\r\n\r\n- SIDE: Baby Potatoes, Hashbrown or French Fries.\r\n\r\n- DRINK: Coffee, Tea or Orange Juice', NULL, '600', '10 Minutes', '100', NULL, 48, NULL, 1, NULL, '2019-10-24 18:01:06', '2019-10-25 04:36:04'),
(17, 1, 'Croissant Sandwichs', 'Our signature french croissant stuffed with numerous thinly sliced smoked turkey, lettuce, scrambled eggs and a sprinkle of shredded cheese.\r\n\r\n- SIDE: Baby Potatoes, Hashbrown or French Fries.\r\n\r\n- DRINK: Coffee, Tea or Orange Juice', NULL, '200', '10', '100', NULL, 49, NULL, 1, NULL, '2019-10-24 18:04:09', '2019-10-26 22:15:05'),
(18, 1, 'Labneh on Flat Bread', 'Flat bread with creamy lebneh on top along side olives, cucumber, mint and olive oil. All tossed with Palestinian zaatar.\r\n\r\n- SIDE: Baby Potatoes, Hashbrown or  French Fries.\r\n\r\n- DRINK: Coffee, Tea or Orange Juice', NULL, '200', '19 minutes', '100', NULL, 51, NULL, 1, NULL, '2019-10-24 18:22:16', '2019-10-25 04:36:40'),
(19, 2, 'Mama\'s Apple Pie', 'Freshly baked every day! We have chosen the best apples in the market to create this magnificent piece of dessert for the lovers of pie. Add a scoop of ice-cream to make it even better.', NULL, '300', '20 Minutes', '150', NULL, 53, NULL, 1, NULL, '2019-10-24 19:31:48', '2019-10-26 22:17:33'),
(20, 2, 'Creme Brulee', 'As french as it can get! The smooth and creamy texture of this sweet custard is highlighted by its candied top.', 'value,value1', '300', '20 Minutes', '190', NULL, 54, NULL, 1, NULL, '2019-10-24 19:35:20', '2019-10-25 04:37:28'),
(21, 2, 'Borwnie with Ice Cream', 'Brownie Goodness with a scoop of vannilla ice cream.', NULL, '200', '15 Minutes', '199', NULL, 56, NULL, 1, NULL, '2019-10-24 19:40:26', '2019-10-25 04:37:49'),
(22, 15, 'Oyster Gongi Fillet', 'A gongi experience! A chunk of hand chopped fillet, grilled to your appetite. Mounted with mushrooms, onions, colored peppers and green beans. Served with our unmistakable signature homemade oyster sauce and two sides of your choice.\r\n\r\nChoose your sides: Sauteed Vegetables | Plain Mashed potatoes | Loaded mashed potatoes | French Fries | Corn Cub | Oak’s Herbal Rice', NULL, '300', '30 Minutes', '500', NULL, 66, NULL, 1, NULL, '2019-10-24 21:00:18', '2019-10-26 21:07:17'),
(23, 15, 'Tokyo Fillet', 'A voyage to Tokyo! A tender piece of fillet mounted on green beans and spinach topped with our homemade teriyaki sauce. Served with two sides of your choice.\r\n\r\nChoose your sauce: Mushroom | Gravy | Rosemary | Pepper | Blue cheese sauce | Mustard\r\n\r\nChoose your sides: Sauteed Vegetables | Plain Mashed potatoes | Loaded mashed potatoes | French Fries | Corn Cub | Oak’s Herbal Rice', NULL, '300', '30 Minutes', '500', NULL, 67, NULL, 1, NULL, '2019-10-24 21:02:41', '2019-10-26 22:14:24'),
(24, 16, 'Original Monterey Chicken', 'A classic American dish that will keep getting you back to Oak Bay. Two grilled breasts, topped with beef bacon, cheese and tomatoes.', NULL, '200', '35 Minutes', '400', NULL, 68, NULL, 1, NULL, '2019-10-24 21:15:56', '2019-10-25 04:39:34'),
(25, 16, 'Orange Chicken Wok', 'A zesty touch to twitch your taste buds! Meticulously marinated chicken with our signature orange based marinade sitting on a bed of basmati rice, tossed with cashew, fried egg and Chinese style sautéed vegetables. Served with our signature homemade Oyster Sauce.', NULL, '300', '30 Minutes', '600', NULL, 69, NULL, 1, NULL, '2019-10-24 21:19:08', '2019-10-25 04:39:54'),
(26, 17, 'Oak’s Shrimp', 'An OAK BAY must try! Grilled Asian seasoned shrimp drizzled with hot chili sauce and served with fried seasoned rice.', NULL, '100', '30 Minutes', '300', NULL, 70, NULL, 1, NULL, '2019-10-24 21:23:14', '2019-10-25 04:40:27'),
(27, 17, 'Risotto Seafood', 'Italy says hello! A seafood fiesta merged with an Italian classic risotto. Topped with tomato sauce and parmesan cheese.', NULL, '300', '40 Minutes', '600', NULL, 71, NULL, 1, NULL, '2019-10-24 21:25:43', '2019-10-25 04:40:40'),
(28, 17, 'Haname Roll', 'Rolled salmon and shrimp tempura, cucumber and avocado. Topped with creamy cheese and crispy rice.', NULL, '200', '10 Minutes', '300', NULL, 72, NULL, 1, NULL, '2019-10-24 21:31:05', '2019-10-25 04:40:54'),
(29, 17, 'Exclusive Roll', 'Rolled shrimp tempura, cucumber and avocado. Topped with shrimp tartar.', NULL, '500', '30 Minutes', '300', NULL, 73, NULL, 1, NULL, '2019-10-24 21:34:38', '2019-10-25 04:41:09'),
(30, 1, NULL, NULL, NULL, '100', '20 Minutes', '140', NULL, 46, NULL, 1, NULL, '2019-11-05 23:42:33', '2019-11-05 23:42:33');

-- --------------------------------------------------------

--
-- Table structure for table `item_ingredient_warnings`
--

CREATE TABLE `item_ingredient_warnings` (
  `id` int(10) UNSIGNED NOT NULL,
  `item_id` int(10) UNSIGNED NOT NULL,
  `ingredient_warning_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `item_options`
--

CREATE TABLE `item_options` (
  `id` int(10) UNSIGNED NOT NULL,
  `item_id` int(10) UNSIGNED NOT NULL,
  `option_id` int(10) UNSIGNED NOT NULL,
  `type` enum('checkbox','slider') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'checkbox',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `item_options`
--

INSERT INTO `item_options` (`id`, `item_id`, `option_id`, `type`, `created_at`, `updated_at`) VALUES
(4, 22, 2, 'checkbox', '2019-10-26 21:07:17', '2019-10-26 21:07:17'),
(3, 22, 4, 'checkbox', '2019-10-26 21:07:17', '2019-10-26 21:07:17');

-- --------------------------------------------------------

--
-- Table structure for table `item_option_values`
--

CREATE TABLE `item_option_values` (
  `id` int(10) UNSIGNED NOT NULL,
  `item_option_id` int(10) UNSIGNED NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `item_option_values`
--

INSERT INTO `item_option_values` (`id`, `item_option_id`, `value`, `price`, `created_at`, `updated_at`) VALUES
(15, 4, 'Quark', 0, NULL, NULL),
(14, 4, 'Cottage Cheese', 0, NULL, NULL),
(13, 3, 'Hot', 10, NULL, NULL),
(12, 3, 'Butter', 7, NULL, NULL),
(11, 3, 'Browne', 6, NULL, NULL),
(10, 3, 'White', 4, NULL, NULL),
(9, 3, 'Red', 5, NULL, NULL),
(16, 4, 'Ricotta Cheese', 0, NULL, NULL),
(17, 4, 'Swiss', 0, NULL, NULL),
(18, 4, 'Mozzarella', 0, NULL, NULL),
(19, 4, 'Cheddar', 0, NULL, NULL),
(20, 4, 'Neufchatel', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `item_prices`
--

CREATE TABLE `item_prices` (
  `id` int(10) UNSIGNED NOT NULL,
  `item_id` int(10) UNSIGNED NOT NULL,
  `price` double(8,2) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `item_prices`
--

INSERT INTO `item_prices` (`id`, `item_id`, `price`, `description`, `created_at`, `updated_at`) VALUES
(183, 2, 69.00, 'Large', NULL, NULL),
(219, 30, 69.00, 'All inclusive', '2019-11-05 23:42:33', '2019-11-05 23:42:33'),
(6, 5, 70.00, 'Pie', NULL, NULL),
(182, 21, 75.00, 'Large', NULL, NULL),
(30, 7, 140.00, 'Large', NULL, NULL),
(29, 7, 120.00, 'Small', NULL, NULL),
(66, 8, 30.00, 'Small', NULL, NULL),
(65, 8, 40.00, 'Medium', NULL, NULL),
(64, 8, 50.00, 'Large', NULL, NULL),
(45, 9, 20.00, 'Small', NULL, NULL),
(44, 9, 40.00, 'Large', NULL, NULL),
(43, 9, 55.00, 'Super', NULL, NULL),
(49, 10, 70.00, 'Large', NULL, NULL),
(48, 10, 50.00, 'Small', NULL, NULL),
(207, 22, 300.00, '6 persons', NULL, NULL),
(179, 20, 65.00, 'Medium', NULL, NULL),
(180, 20, 99.00, 'Large', NULL, NULL),
(202, 28, 98.00, '5 Pcs', NULL, NULL),
(172, 15, 69.00, 'All inclusive', NULL, NULL),
(173, 16, 69.00, 'All inclusive', NULL, NULL),
(210, 17, 69.00, 'All inclusive', NULL, NULL),
(175, 18, 69.00, 'All inclusive', NULL, NULL),
(216, 19, 99.00, 'Large', NULL, NULL),
(215, 19, 69.00, 'Small', NULL, NULL),
(178, 20, 52.00, 'Small', NULL, NULL),
(181, 21, 55.00, 'Medium', NULL, NULL),
(206, 22, 260.00, '4 persons', NULL, NULL),
(205, 22, 224.00, '2 persons', NULL, NULL),
(209, 23, 269.00, 'Large', NULL, NULL),
(208, 23, 224.00, 'Small', NULL, NULL),
(190, 24, 160.00, 'Large', NULL, NULL),
(193, 25, 110.00, 'Small', NULL, NULL),
(192, 25, 150.00, 'Large', NULL, NULL),
(191, 25, 180.00, 'Super', NULL, NULL),
(189, 24, 126.00, 'Small', NULL, NULL),
(196, 26, 172.00, 'Regular', NULL, NULL),
(195, 26, 190.00, 'Fried', NULL, NULL),
(194, 26, 200.00, 'Grilled', NULL, NULL),
(199, 27, 180.00, 'Large', NULL, NULL),
(198, 27, 150.00, 'Medium', NULL, NULL),
(197, 27, 137.00, 'Small', NULL, NULL),
(200, 28, 120.00, '7 Pcs', NULL, NULL),
(204, 29, 127.00, '8 Pcs', NULL, NULL),
(203, 29, 69.00, '4 Pcs', NULL, NULL),
(201, 28, 170.00, '10 Pcs', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `item_side_dishes`
--

CREATE TABLE `item_side_dishes` (
  `id` int(10) UNSIGNED NOT NULL,
  `item_id` int(10) UNSIGNED NOT NULL,
  `side_dish_id` int(10) UNSIGNED NOT NULL,
  `price` double(8,2) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `media_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `item_side_dishes`
--

INSERT INTO `item_side_dishes` (`id`, `item_id`, `side_dish_id`, `price`, `name`, `media_id`, `created_at`, `updated_at`) VALUES
(44, 2, 5, 7.00, NULL, NULL, NULL, NULL),
(43, 21, 3, 6.00, NULL, NULL, NULL, NULL),
(52, 29, 3, 6.00, NULL, NULL, NULL, NULL),
(37, 15, 3, 6.00, NULL, NULL, NULL, NULL),
(38, 16, 5, 7.00, NULL, NULL, NULL, NULL),
(59, 17, 1, 7.00, NULL, NULL, NULL, NULL),
(40, 18, 2, 5.00, NULL, NULL, NULL, NULL),
(51, 28, 4, 20.00, NULL, NULL, NULL, NULL),
(15, 7, 3, 10.00, NULL, NULL, NULL, NULL),
(28, 10, 2, 5.00, NULL, NULL, NULL, NULL),
(27, 10, 1, 10.00, NULL, NULL, NULL, NULL),
(50, 27, 3, 20.00, NULL, NULL, NULL, NULL),
(49, 26, 1, 20.00, NULL, NULL, NULL, NULL),
(24, 9, 1, 5.00, NULL, NULL, NULL, NULL),
(42, 20, 1, 7.00, NULL, NULL, NULL, NULL),
(53, 22, 4, 6.00, NULL, NULL, NULL, NULL),
(56, 23, 1, 6.00, NULL, NULL, NULL, NULL),
(47, 24, 4, 20.00, NULL, NULL, NULL, NULL),
(48, 25, 4, 20.00, NULL, NULL, NULL, NULL),
(54, 22, 3, 9.00, NULL, NULL, NULL, NULL),
(55, 22, 5, 5.00, NULL, NULL, NULL, NULL),
(57, 23, 3, 9.00, NULL, NULL, NULL, NULL),
(58, 23, 5, 7.00, NULL, NULL, NULL, NULL),
(60, 17, 3, 6.00, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `item_translations`
--

CREATE TABLE `item_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `item_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `notes` text COLLATE utf8mb4_unicode_ci,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `item_translations`
--

INSERT INTO `item_translations` (`id`, `item_id`, `name`, `description`, `notes`, `locale`, `created_at`, `updated_at`) VALUES
(1, 1, 'Hollandaise Egg Benedict', 'The best in class Eggs tossed with our signature hollandaise sause.\r\nThe best start for your morning!', NULL, 'en', '2019-09-01 19:55:04', '2019-09-01 19:55:04'),
(2, 2, 'Paradise Pie', 'Although it\'s called a pie, probably because of it\'s graham cracker crust, You\'ll want to stay tuned for a bite!', NULL, 'en', '2019-09-01 19:55:49', '2019-10-24 19:37:33'),
(3, 3, 'Egg in a Toast', 'The perfect combination of Eggs and a Toast in  a perfect match.', NULL, 'en', '2019-09-01 21:12:31', '2019-09-01 21:12:31'),
(4, 4, 'Croissant Sandwich', 'Our signature french croissant stuffed with numerous thinly sliced smoked turkey, lettuce, scrambled eggs and a sprinkle of shredded cheese.', NULL, 'en', '2019-09-01 21:26:32', '2019-09-01 21:26:32'),
(5, 5, 'Paradise Pie', 'Although it\'s called a pie, probably because of it\'s graham cracker crust, You\'ll want to stay tuned for a bite!', NULL, 'en', '2019-09-01 22:08:27', '2019-09-01 22:08:27'),
(6, 6, 'Profiterole', 'A classic french dessert made to our standards!', NULL, 'en', '2019-09-01 22:14:41', '2019-09-01 22:14:41'),
(7, 7, 'Pizza Cheese', 'Yummy and Hot', NULL, 'en', '2019-09-15 16:56:19', '2019-09-15 16:56:19'),
(8, 8, 'Burger', 'New and delicious', NULL, 'en', '2019-09-15 16:59:43', '2019-09-15 16:59:43'),
(9, 9, 'White Cheese Salad', 'New Cheese Salad', NULL, 'en', '2019-09-15 17:04:01', '2019-09-15 17:04:01'),
(10, 10, 'Olivi Pizza', 'New and Hot', NULL, 'en', '2019-09-15 17:08:44', '2019-09-15 17:08:44'),
(11, 11, 'Meat with Botato', 'New and Delicious', NULL, 'en', '2019-09-15 17:35:56', '2019-09-15 17:35:56'),
(12, 12, 'Meat with Salad', 'New and Delicious', NULL, 'en', '2019-09-15 17:39:17', '2019-09-15 17:39:17'),
(13, 13, 'Testing Offline', 'sfssfs', NULL, 'en', '2019-10-20 21:23:57', '2019-10-20 21:23:57'),
(14, 14, 'Oak\'s Deviled Egg', 'The one and only Egg dish that you will crave for after tasting it for the first time! \r\n- SIDE: Baby Potatoes, Hashbrown or French Fries.\r\n- DRINK: Coffee, Tea or Orange Juice.', NULL, 'en', '2019-10-24 17:54:53', '2019-10-24 17:54:53'),
(15, 15, 'OAK’s Big Breakfast', 'America’s favorite hash brown and beef sausages alongside grilled tomatoes, mushroom, and beef bacon. With your choice of either coffee or freshly squeezed orange juice. \r\n\r\n- SIDE: Baby Potatoes, Hashbrown or French Fries\r\n\r\n- DRINK: Coffee, Tea or Orange Juice', NULL, 'en', '2019-10-24 17:59:05', '2019-10-24 17:59:05'),
(16, 16, 'OAK’s Special Omelette', 'Comes with Veggies or Cheese. Served with baby potatoes, beef bacon, sour cream and toast.\r\n\r\n- SIDE: Baby Potatoes, Hashbrown or French Fries.\r\n\r\n- DRINK: Coffee, Tea or Orange Juice', NULL, 'en', '2019-10-24 18:01:06', '2019-10-24 18:01:06'),
(17, 17, 'Croissant Sandwichs', 'Our signature french croissant stuffed with numerous thinly sliced smoked turkey, lettuce, scrambled eggs and a sprinkle of shredded cheese.\r\n\r\n- SIDE: Baby Potatoes, Hashbrown or French Fries.\r\n\r\n- DRINK: Coffee, Tea or Orange Juice', NULL, 'en', '2019-10-24 18:04:09', '2019-10-24 18:04:09'),
(18, 18, 'Labneh on Flat Bread', 'Flat bread with creamy lebneh on top along side olives, cucumber, mint and olive oil. All tossed with Palestinian zaatar.\r\n\r\n- SIDE: Baby Potatoes, Hashbrown or  French Fries.\r\n\r\n- DRINK: Coffee, Tea or Orange Juice', NULL, 'en', '2019-10-24 18:22:16', '2019-10-24 18:22:16'),
(19, 19, 'Mama\'s Apple Pie', 'Freshly baked every day! We have chosen the best apples in the market to create this magnificent piece of dessert for the lovers of pie. Add a scoop of ice-cream to make it even better.', NULL, 'en', '2019-10-24 19:31:48', '2019-10-24 19:31:48'),
(20, 20, 'Creme Brulee', 'As french as it can get! The smooth and creamy texture of this sweet custard is highlighted by its candied top.', NULL, 'en', '2019-10-24 19:35:20', '2019-10-24 19:35:20'),
(21, 21, 'Borwnie with Ice Cream', 'Brownie Goodness with a scoop of vannilla ice cream.', NULL, 'en', '2019-10-24 19:40:26', '2019-10-24 19:40:26'),
(22, 22, 'Oyster Gongi Fillet', 'A gongi experience! A chunk of hand chopped fillet, grilled to your appetite. Mounted with mushrooms, onions, colored peppers and green beans. Served with our unmistakable signature homemade oyster sauce and two sides of your choice.\r\n\r\nChoose your sides: Sauteed Vegetables | Plain Mashed potatoes | Loaded mashed potatoes | French Fries | Corn Cub | Oak’s Herbal Rice', NULL, 'en', '2019-10-24 21:00:18', '2019-10-24 21:00:18'),
(23, 23, 'Tokyo Fillet', 'A voyage to Tokyo! A tender piece of fillet mounted on green beans and spinach topped with our homemade teriyaki sauce. Served with two sides of your choice.\r\n\r\nChoose your sauce: Mushroom | Gravy | Rosemary | Pepper | Blue cheese sauce | Mustard\r\n\r\nChoose your sides: Sauteed Vegetables | Plain Mashed potatoes | Loaded mashed potatoes | French Fries | Corn Cub | Oak’s Herbal Rice', NULL, 'en', '2019-10-24 21:02:41', '2019-10-24 21:02:41'),
(24, 24, 'Original Monterey Chicken', 'A classic American dish that will keep getting you back to Oak Bay. Two grilled breasts, topped with beef bacon, cheese and tomatoes.', NULL, 'en', '2019-10-24 21:15:56', '2019-10-24 21:15:56'),
(25, 25, 'Orange Chicken Wok', 'A zesty touch to twitch your taste buds! Meticulously marinated chicken with our signature orange based marinade sitting on a bed of basmati rice, tossed with cashew, fried egg and Chinese style sautéed vegetables. Served with our signature homemade Oyster Sauce.', NULL, 'en', '2019-10-24 21:19:08', '2019-10-24 21:19:08'),
(26, 26, 'Oak’s Shrimp', 'An OAK BAY must try! Grilled Asian seasoned shrimp drizzled with hot chili sauce and served with fried seasoned rice.', NULL, 'en', '2019-10-24 21:23:14', '2019-10-24 21:23:14'),
(27, 27, 'Risotto Seafood', 'Italy says hello! A seafood fiesta merged with an Italian classic risotto. Topped with tomato sauce and parmesan cheese.', NULL, 'en', '2019-10-24 21:25:43', '2019-10-24 21:25:43'),
(28, 28, 'Haname Roll', 'Rolled salmon and shrimp tempura, cucumber and avocado. Topped with creamy cheese and crispy rice.', NULL, 'en', '2019-10-24 21:31:05', '2019-10-24 21:31:05'),
(29, 29, 'Exclusive Roll', 'Rolled shrimp tempura, cucumber and avocado. Topped with shrimp tartar.', NULL, 'en', '2019-10-24 21:34:38', '2019-10-24 21:34:38'),
(30, 30, 'Oak\'s Deviled Egg', 'The one and only Egg dish that you will crave for after tasting it for the first time! \r\n- SIDE: Baby Potatoes, Hashbrown or French Fries.\r\n- DRINK: Coffee, Tea or Orange Juice.', NULL, 'en', '2019-11-05 23:42:33', '2019-11-05 23:42:33');

-- --------------------------------------------------------

--
-- Table structure for table `item_views`
--

CREATE TABLE `item_views` (
  `id` int(10) UNSIGNED NOT NULL,
  `item_id` int(11) NOT NULL,
  `view` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `item_views`
--

INSERT INTO `item_views` (`id`, `item_id`, `view`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2019-09-01 20:34:13', '2019-09-01 20:34:13'),
(2, 1, 1, '2019-09-01 20:34:42', '2019-09-01 20:34:42'),
(3, 1, 1, '2019-09-01 20:34:58', '2019-09-01 20:34:58'),
(4, 1, 1, '2019-09-01 20:35:59', '2019-09-01 20:35:59'),
(5, 1, 1, '2019-09-01 21:06:20', '2019-09-01 21:06:20'),
(6, 1, 1, '2019-09-01 21:06:26', '2019-09-01 21:06:26'),
(7, 2, 1, '2019-09-01 21:06:34', '2019-09-01 21:06:34'),
(8, 3, 1, '2019-09-01 21:12:31', '2019-09-01 21:12:31'),
(9, 3, 1, '2019-09-01 21:19:43', '2019-09-01 21:19:43'),
(10, 3, 1, '2019-09-01 21:19:59', '2019-09-01 21:19:59'),
(11, 4, 1, '2019-09-01 21:26:32', '2019-09-01 21:26:32'),
(12, 4, 1, '2019-09-01 22:02:14', '2019-09-01 22:02:14'),
(13, 4, 1, '2019-09-01 22:02:53', '2019-09-01 22:02:53'),
(14, 5, 1, '2019-09-01 22:08:28', '2019-09-01 22:08:28'),
(15, 6, 1, '2019-09-01 22:14:41', '2019-09-01 22:14:41'),
(16, 6, 1, '2019-09-01 22:16:00', '2019-09-01 22:16:00'),
(17, 5, 1, '2019-09-01 23:34:58', '2019-09-01 23:34:58'),
(18, 4, 1, '2019-09-09 16:11:45', '2019-09-09 16:11:45'),
(19, 3, 1, '2019-09-11 20:15:17', '2019-09-11 20:15:17'),
(20, 4, 1, '2019-09-12 14:18:06', '2019-09-12 14:18:06'),
(21, 7, 1, '2019-09-15 16:56:19', '2019-09-15 16:56:19'),
(22, 7, 1, '2019-09-15 16:56:59', '2019-09-15 16:56:59'),
(23, 8, 1, '2019-09-15 16:59:44', '2019-09-15 16:59:44'),
(24, 8, 1, '2019-09-15 17:00:24', '2019-09-15 17:00:24'),
(25, 9, 1, '2019-09-15 17:04:01', '2019-09-15 17:04:01'),
(26, 10, 1, '2019-09-15 17:08:45', '2019-09-15 17:08:45'),
(27, 11, 1, '2019-09-15 17:35:57', '2019-09-15 17:35:57'),
(28, 12, 1, '2019-09-15 17:39:19', '2019-09-15 17:39:19'),
(29, 7, 1, '2019-09-15 17:46:53', '2019-09-15 17:46:53'),
(30, 7, 1, '2019-09-15 17:47:55', '2019-09-15 17:47:55'),
(31, 7, 1, '2019-09-15 17:54:06', '2019-09-15 17:54:06'),
(32, 7, 1, '2019-09-15 17:54:30', '2019-09-15 17:54:30'),
(33, 9, 1, '2019-09-15 17:54:38', '2019-09-15 17:54:38'),
(34, 9, 1, '2019-09-15 18:02:39', '2019-09-15 18:02:39'),
(35, 10, 1, '2019-09-15 18:02:55', '2019-09-15 18:02:55'),
(36, 10, 1, '2019-09-15 18:03:52', '2019-09-15 18:03:52'),
(37, 10, 1, '2019-09-15 18:10:11', '2019-09-15 18:10:11'),
(38, 9, 1, '2019-09-15 18:11:18', '2019-09-15 18:11:18'),
(39, 10, 1, '2019-09-15 18:11:37', '2019-09-15 18:11:37'),
(40, 10, 1, '2019-09-15 18:12:14', '2019-09-15 18:12:14'),
(41, 9, 1, '2019-09-15 18:20:32', '2019-09-15 18:20:32'),
(42, 9, 1, '2019-09-15 18:21:39', '2019-09-15 18:21:39'),
(43, 10, 1, '2019-09-17 16:28:48', '2019-09-17 16:28:48'),
(44, 10, 1, '2019-09-17 16:28:56', '2019-09-17 16:28:56'),
(45, 10, 1, '2019-09-17 16:31:04', '2019-09-17 16:31:04'),
(46, 3, 1, '2019-09-18 01:18:50', '2019-09-18 01:18:50'),
(47, 5, 1, '2019-09-18 02:42:51', '2019-09-18 02:42:51'),
(48, 3, 1, '2019-09-28 18:02:29', '2019-09-28 18:02:29'),
(49, 4, 1, '2019-10-08 16:53:51', '2019-10-08 16:53:51'),
(50, 4, 1, '2019-10-08 17:08:26', '2019-10-08 17:08:26'),
(51, 4, 1, '2019-10-08 17:13:12', '2019-10-08 17:13:12'),
(52, 4, 1, '2019-10-09 18:04:53', '2019-10-09 18:04:53'),
(53, 6, 1, '2019-10-09 18:05:14', '2019-10-09 18:05:14'),
(54, 4, 1, '2019-10-18 00:59:14', '2019-10-18 00:59:14'),
(55, 1, 1, '2019-10-20 21:01:21', '2019-10-20 21:01:21'),
(56, 1, 1, '2019-10-20 21:21:09', '2019-10-20 21:21:09'),
(57, 13, 1, '2019-10-20 21:23:58', '2019-10-20 21:23:58'),
(58, 13, 1, '2019-10-24 16:06:44', '2019-10-24 16:06:44'),
(59, 4, 1, '2019-10-24 17:34:46', '2019-10-24 17:34:46'),
(60, 4, 1, '2019-10-24 17:35:00', '2019-10-24 17:35:00'),
(61, 3, 1, '2019-10-24 17:35:09', '2019-10-24 17:35:09'),
(62, 3, 1, '2019-10-24 17:35:17', '2019-10-24 17:35:17'),
(63, 1, 1, '2019-10-24 17:35:40', '2019-10-24 17:35:40'),
(64, 1, 1, '2019-10-24 17:35:47', '2019-10-24 17:35:47'),
(65, 13, 1, '2019-10-24 17:35:57', '2019-10-24 17:35:57'),
(66, 13, 1, '2019-10-24 17:49:11', '2019-10-24 17:49:11'),
(67, 13, 1, '2019-10-24 17:49:43', '2019-10-24 17:49:43'),
(68, 13, 1, '2019-10-24 17:49:53', '2019-10-24 17:49:53'),
(69, 13, 1, '2019-10-24 17:51:28', '2019-10-24 17:51:28'),
(70, 8, 1, '2019-10-24 17:52:38', '2019-10-24 17:52:38'),
(71, 8, 1, '2019-10-24 17:52:50', '2019-10-24 17:52:50'),
(72, 14, 1, '2019-10-24 17:54:53', '2019-10-24 17:54:53'),
(73, 14, 1, '2019-10-24 17:56:25', '2019-10-24 17:56:25'),
(74, 15, 1, '2019-10-24 17:59:05', '2019-10-24 17:59:05'),
(75, 16, 1, '2019-10-24 18:01:07', '2019-10-24 18:01:07'),
(76, 17, 1, '2019-10-24 18:04:09', '2019-10-24 18:04:09'),
(77, 17, 1, '2019-10-24 18:05:13', '2019-10-24 18:05:13'),
(78, 14, 1, '2019-10-24 18:11:25', '2019-10-24 18:11:25'),
(79, 17, 1, '2019-10-24 18:14:40', '2019-10-24 18:14:40'),
(80, 18, 1, '2019-10-24 18:22:16', '2019-10-24 18:22:16'),
(81, 18, 1, '2019-10-24 19:23:02', '2019-10-24 19:23:02'),
(82, 18, 1, '2019-10-24 19:23:28', '2019-10-24 19:23:28'),
(83, 19, 1, '2019-10-24 19:31:48', '2019-10-24 19:31:48'),
(84, 6, 1, '2019-10-24 19:32:12', '2019-10-24 19:32:12'),
(85, 6, 1, '2019-10-24 19:32:22', '2019-10-24 19:32:22'),
(86, 19, 1, '2019-10-24 19:33:02', '2019-10-24 19:33:02'),
(87, 20, 1, '2019-10-24 19:35:21', '2019-10-24 19:35:21'),
(88, 2, 1, '2019-10-24 19:35:51', '2019-10-24 19:35:51'),
(89, 2, 1, '2019-10-24 19:37:34', '2019-10-24 19:37:34'),
(90, 2, 1, '2019-10-24 19:38:03', '2019-10-24 19:38:03'),
(91, 20, 1, '2019-10-24 19:38:35', '2019-10-24 19:38:35'),
(92, 21, 1, '2019-10-24 19:40:27', '2019-10-24 19:40:27'),
(93, 12, 1, '2019-10-24 19:48:40', '2019-10-24 19:48:40'),
(94, 20, 1, '2019-10-24 19:59:13', '2019-10-24 19:59:13'),
(95, 20, 1, '2019-10-24 19:59:36', '2019-10-24 19:59:36'),
(96, 20, 1, '2019-10-24 20:01:31', '2019-10-24 20:01:31'),
(97, 20, 1, '2019-10-24 20:18:56', '2019-10-24 20:18:56'),
(98, 20, 1, '2019-10-24 20:19:52', '2019-10-24 20:19:52'),
(99, 21, 1, '2019-10-24 20:19:58', '2019-10-24 20:19:58'),
(100, 2, 1, '2019-10-24 20:20:17', '2019-10-24 20:20:17'),
(101, 14, 1, '2019-10-24 20:28:01', '2019-10-24 20:28:01'),
(102, 14, 1, '2019-10-24 20:29:06', '2019-10-24 20:29:06'),
(103, 14, 1, '2019-10-24 20:36:36', '2019-10-24 20:36:36'),
(104, 14, 1, '2019-10-24 20:38:03', '2019-10-24 20:38:03'),
(105, 14, 1, '2019-10-24 20:38:41', '2019-10-24 20:38:41'),
(106, 15, 1, '2019-10-24 20:46:29', '2019-10-24 20:46:29'),
(107, 15, 1, '2019-10-24 20:46:57', '2019-10-24 20:46:57'),
(108, 16, 1, '2019-10-24 20:47:41', '2019-10-24 20:47:41'),
(109, 14, 1, '2019-10-24 20:47:56', '2019-10-24 20:47:56'),
(110, 14, 1, '2019-10-24 20:48:39', '2019-10-24 20:48:39'),
(111, 17, 1, '2019-10-24 20:48:51', '2019-10-24 20:48:51'),
(112, 17, 1, '2019-10-24 20:49:08', '2019-10-24 20:49:08'),
(113, 18, 1, '2019-10-24 20:50:16', '2019-10-24 20:50:16'),
(114, 18, 1, '2019-10-24 20:50:33', '2019-10-24 20:50:33'),
(115, 19, 1, '2019-10-24 20:51:44', '2019-10-24 20:51:44'),
(116, 20, 1, '2019-10-24 20:52:17', '2019-10-24 20:52:17'),
(117, 21, 1, '2019-10-24 20:52:24', '2019-10-24 20:52:24'),
(118, 21, 1, '2019-10-24 20:52:48', '2019-10-24 20:52:48'),
(119, 2, 1, '2019-10-24 20:52:58', '2019-10-24 20:52:58'),
(120, 2, 1, '2019-10-24 20:55:26', '2019-10-24 20:55:26'),
(121, 21, 1, '2019-10-24 20:55:34', '2019-10-24 20:55:34'),
(122, 21, 1, '2019-10-24 20:55:48', '2019-10-24 20:55:48'),
(123, 22, 1, '2019-10-24 21:00:19', '2019-10-24 21:00:19'),
(124, 23, 1, '2019-10-24 21:02:41', '2019-10-24 21:02:41'),
(125, 23, 1, '2019-10-24 21:02:58', '2019-10-24 21:02:58'),
(126, 22, 1, '2019-10-24 21:03:16', '2019-10-24 21:03:16'),
(127, 24, 1, '2019-10-24 21:15:57', '2019-10-24 21:15:57'),
(128, 25, 1, '2019-10-24 21:19:09', '2019-10-24 21:19:09'),
(129, 25, 1, '2019-10-24 21:19:23', '2019-10-24 21:19:23'),
(130, 24, 1, '2019-10-24 21:20:20', '2019-10-24 21:20:20'),
(131, 24, 1, '2019-10-24 21:20:44', '2019-10-24 21:20:44'),
(132, 26, 1, '2019-10-24 21:23:15', '2019-10-24 21:23:15'),
(133, 26, 1, '2019-10-24 21:23:45', '2019-10-24 21:23:45'),
(134, 26, 1, '2019-10-24 21:23:58', '2019-10-24 21:23:58'),
(135, 27, 1, '2019-10-24 21:25:44', '2019-10-24 21:25:44'),
(136, 27, 1, '2019-10-24 21:25:59', '2019-10-24 21:25:59'),
(137, 28, 1, '2019-10-24 21:31:05', '2019-10-24 21:31:05'),
(138, 29, 1, '2019-10-24 21:34:39', '2019-10-24 21:34:39'),
(139, 29, 1, '2019-10-24 21:35:02', '2019-10-24 21:35:02'),
(140, 28, 1, '2019-10-24 21:35:10', '2019-10-24 21:35:10'),
(141, 28, 1, '2019-10-24 21:35:43', '2019-10-24 21:35:43'),
(142, 27, 1, '2019-10-24 21:35:54', '2019-10-24 21:35:54'),
(143, 27, 1, '2019-10-24 21:36:06', '2019-10-24 21:36:06'),
(144, 26, 1, '2019-10-24 21:36:12', '2019-10-24 21:36:12'),
(145, 26, 1, '2019-10-24 21:36:21', '2019-10-24 21:36:21'),
(146, 22, 1, '2019-10-24 21:38:55', '2019-10-24 21:38:55'),
(147, 14, 1, '2019-10-24 21:39:53', '2019-10-24 21:39:53'),
(148, 14, 1, '2019-10-24 21:39:59', '2019-10-24 21:39:59'),
(149, 14, 1, '2019-10-24 21:41:58', '2019-10-24 21:41:58'),
(150, 3, 1, '2019-10-24 21:52:56', '2019-10-24 21:52:56'),
(151, 1, 1, '2019-10-24 21:53:15', '2019-10-24 21:53:15'),
(152, 14, 1, '2019-10-24 21:56:19', '2019-10-24 21:56:19'),
(153, 14, 1, '2019-10-24 21:57:45', '2019-10-24 21:57:45'),
(154, 26, 1, '2019-10-25 04:03:16', '2019-10-25 04:03:16'),
(155, 26, 1, '2019-10-25 04:33:45', '2019-10-25 04:33:45'),
(156, 27, 1, '2019-10-25 04:33:57', '2019-10-25 04:33:57'),
(157, 27, 1, '2019-10-25 04:34:09', '2019-10-25 04:34:09'),
(158, 28, 1, '2019-10-25 04:34:15', '2019-10-25 04:34:15'),
(159, 28, 1, '2019-10-25 04:34:24', '2019-10-25 04:34:24'),
(160, 29, 1, '2019-10-25 04:34:28', '2019-10-25 04:34:28'),
(161, 29, 1, '2019-10-25 04:34:45', '2019-10-25 04:34:45'),
(162, 27, 1, '2019-10-25 04:34:56', '2019-10-25 04:34:56'),
(163, 26, 1, '2019-10-25 04:35:00', '2019-10-25 04:35:00'),
(164, 28, 1, '2019-10-25 04:35:05', '2019-10-25 04:35:05'),
(165, 29, 1, '2019-10-25 04:35:12', '2019-10-25 04:35:12'),
(166, 14, 1, '2019-10-25 04:35:24', '2019-10-25 04:35:24'),
(167, 14, 1, '2019-10-25 04:35:33', '2019-10-25 04:35:33'),
(168, 15, 1, '2019-10-25 04:35:37', '2019-10-25 04:35:37'),
(169, 15, 1, '2019-10-25 04:35:48', '2019-10-25 04:35:48'),
(170, 16, 1, '2019-10-25 04:35:54', '2019-10-25 04:35:54'),
(171, 16, 1, '2019-10-25 04:36:05', '2019-10-25 04:36:05'),
(172, 17, 1, '2019-10-25 04:36:25', '2019-10-25 04:36:25'),
(173, 18, 1, '2019-10-25 04:36:33', '2019-10-25 04:36:33'),
(174, 18, 1, '2019-10-25 04:36:40', '2019-10-25 04:36:40'),
(175, 19, 1, '2019-10-25 04:36:56', '2019-10-25 04:36:56'),
(176, 19, 1, '2019-10-25 04:37:10', '2019-10-25 04:37:10'),
(177, 20, 1, '2019-10-25 04:37:17', '2019-10-25 04:37:17'),
(178, 20, 1, '2019-10-25 04:37:28', '2019-10-25 04:37:28'),
(179, 21, 1, '2019-10-25 04:37:49', '2019-10-25 04:37:49'),
(180, 2, 1, '2019-10-25 04:37:58', '2019-10-25 04:37:58'),
(181, 2, 1, '2019-10-25 04:38:04', '2019-10-25 04:38:04'),
(182, 22, 1, '2019-10-25 04:38:47', '2019-10-25 04:38:47'),
(183, 22, 1, '2019-10-25 04:38:55', '2019-10-25 04:38:55'),
(184, 23, 1, '2019-10-25 04:39:11', '2019-10-25 04:39:11'),
(185, 24, 1, '2019-10-25 04:39:29', '2019-10-25 04:39:29'),
(186, 24, 1, '2019-10-25 04:39:34', '2019-10-25 04:39:34'),
(187, 25, 1, '2019-10-25 04:39:41', '2019-10-25 04:39:41'),
(188, 25, 1, '2019-10-25 04:39:55', '2019-10-25 04:39:55'),
(189, 26, 1, '2019-10-25 04:40:21', '2019-10-25 04:40:21'),
(190, 26, 1, '2019-10-25 04:40:27', '2019-10-25 04:40:27'),
(191, 27, 1, '2019-10-25 04:40:33', '2019-10-25 04:40:33'),
(192, 27, 1, '2019-10-25 04:40:40', '2019-10-25 04:40:40'),
(193, 28, 1, '2019-10-25 04:40:47', '2019-10-25 04:40:47'),
(194, 28, 1, '2019-10-25 04:40:54', '2019-10-25 04:40:54'),
(195, 29, 1, '2019-10-25 04:41:01', '2019-10-25 04:41:01'),
(196, 29, 1, '2019-10-25 04:41:09', '2019-10-25 04:41:09'),
(197, 27, 1, '2019-10-25 04:41:16', '2019-10-25 04:41:16'),
(198, 20, 1, '2019-10-25 21:29:21', '2019-10-25 21:29:21'),
(199, 22, 1, '2019-10-26 21:07:18', '2019-10-26 21:07:18'),
(200, 23, 1, '2019-10-26 22:12:49', '2019-10-26 22:12:49'),
(201, 23, 1, '2019-10-26 22:14:24', '2019-10-26 22:14:24'),
(202, 17, 1, '2019-10-26 22:14:43', '2019-10-26 22:14:43'),
(203, 17, 1, '2019-10-26 22:15:06', '2019-10-26 22:15:06'),
(204, 14, 1, '2019-10-26 22:15:31', '2019-10-26 22:15:31'),
(205, 14, 1, '2019-10-26 22:15:50', '2019-10-26 22:15:50'),
(206, 14, 1, '2019-10-26 22:16:17', '2019-10-26 22:16:17'),
(207, 14, 1, '2019-10-26 22:16:46', '2019-10-26 22:16:46'),
(208, 19, 1, '2019-10-26 22:17:11', '2019-10-26 22:17:11'),
(209, 19, 1, '2019-10-26 22:17:34', '2019-10-26 22:17:34'),
(210, 14, 1, '2019-10-27 14:23:37', '2019-10-27 14:23:37'),
(211, 22, 1, '2019-10-27 18:09:18', '2019-10-27 18:09:18'),
(212, 14, 1, '2019-11-05 20:00:10', '2019-11-05 20:00:10'),
(213, 14, 1, '2019-11-05 20:00:40', '2019-11-05 20:00:40'),
(214, 14, 1, '2019-11-05 23:25:13', '2019-11-05 23:25:13'),
(215, 14, 1, '2019-11-05 23:25:58', '2019-11-05 23:25:58'),
(216, 24, 1, '2019-11-05 23:33:16', '2019-11-05 23:33:16'),
(217, 25, 1, '2019-11-05 23:33:28', '2019-11-05 23:33:28'),
(218, 14, 1, '2019-11-05 23:47:02', '2019-11-05 23:47:02'),
(219, 14, 1, '2019-11-05 23:50:36', '2019-11-05 23:50:36'),
(220, 22, 1, '2019-11-05 23:50:51', '2019-11-05 23:50:51'),
(221, 23, 1, '2019-11-06 00:17:41', '2019-11-06 00:17:41');

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(10) UNSIGNED DEFAULT NULL,
  `media_path` varchar(5000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `media_title` varchar(5000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `media_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `media_ext` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`id`, `customer_id`, `media_path`, `media_title`, `media_type`, `media_ext`, `created_at`, `updated_at`) VALUES
(1, 1, '36f3f58a7ddf300.jpg', 'Breakfast', 'image/jpeg', 'jpg', '2019-09-01 19:37:32', '2019-09-01 19:37:32'),
(2, 1, 'd5ef408ccfddd5b.jpeg', 'Desserts', 'image/jpeg', 'jpeg', '2019-09-01 19:39:41', '2019-09-01 19:39:41'),
(3, 1, '593a52f029ed4f3.jpg', 'Hollandaise Egg Benedict', 'image/jpeg', 'jpg', '2019-09-01 19:54:13', '2019-09-01 19:54:13'),
(4, 1, 'fd278687ad95d58.jpg', 'Hollandaise Egg Benedict', 'image/jpeg', 'jpg', '2019-09-01 19:55:04', '2019-09-01 19:55:04'),
(5, 1, '9f188a440560c27.jpg', 'Hollandaise Egg Benedict', 'image/jpeg', 'jpg', '2019-09-01 19:55:49', '2019-09-01 19:55:49'),
(6, 1, '4c41b74763f2e74.jpg', 'Cookie in a Pan', 'image/jpeg', 'jpg', '2019-09-01 20:47:17', '2019-09-01 20:47:17'),
(7, 1, '07f535366fd32b0.jpg', 'Egg in a Toast', 'image/jpeg', 'jpg', '2019-09-01 21:12:31', '2019-09-01 21:12:31'),
(8, 1, '884a5d635457035.jpg', 'Egg in a Toast', 'image/jpeg', 'jpg', '2019-09-01 21:19:58', '2019-09-01 21:19:58'),
(9, 1, '932735bda614ce4.jpg', 'Croissant Sandwich', 'image/jpeg', 'jpg', '2019-09-01 21:26:32', '2019-09-01 21:26:32'),
(10, 1, '9f6d55cd3fff623.jpg', 'Croissant Sandwich', 'image/jpeg', 'jpg', '2019-09-01 22:02:14', '2019-09-01 22:02:14'),
(11, 1, '4e00cee65dfa268.jpg', 'Croissant Sandwich', 'image/jpeg', 'jpg', '2019-09-01 22:02:52', '2019-09-01 22:02:52'),
(12, 1, '45a231557bbff7f.jpg', 'Paradise Pie', 'image/jpeg', 'jpg', '2019-09-01 22:08:27', '2019-09-01 22:08:27'),
(13, 1, '116f47d64c5969d.jpg', 'Profiterole', 'image/jpeg', 'jpg', '2019-09-01 22:14:41', '2019-09-01 22:14:41'),
(14, 1, 'c3a92b88441eebb.jpg', 'Profiterole', 'image/jpeg', 'jpg', '2019-09-01 22:15:59', '2019-09-01 22:15:59'),
(15, 1, '489e89c167a598c.jpg', 'Baby Botatos', 'image/jpeg', 'jpg', '2019-09-01 22:22:03', '2019-09-01 22:22:03'),
(16, 1, '829561897f4fd34.jpg', 'Hashbrown', 'image/jpeg', 'jpg', '2019-09-01 22:24:10', '2019-09-01 22:24:10'),
(17, 1, '822d09a9f365a18.jpg', 'French Fries', 'image/jpeg', 'jpg', '2019-09-01 22:25:08', '2019-09-01 22:25:08'),
(18, 6, 'f2c7f38d5038cc8.jpg', 'Samarqan Noodels', 'image/jpeg', 'jpg', '2019-09-09 16:12:25', '2019-09-09 16:12:25'),
(19, 1, '298b7958d98f8ce.jpg', 'Pizza Cheese', 'image/jpeg', 'jpg', '2019-09-11 20:12:32', '2019-09-11 20:12:32'),
(20, 1, 'be25c6389075405.jpg', 'Pizza', 'image/jpeg', 'jpg', '2019-09-11 20:24:58', '2019-09-11 20:24:58'),
(21, 1, '4c5d4ddd5095f55.jpg', 'Main Menu', 'image/jpeg', 'jpg', '2019-09-15 16:45:28', '2019-09-15 16:45:28'),
(22, 1, '7eab82303c9712b.jpg', 'Pizza', 'image/jpeg', 'jpg', '2019-09-15 16:51:45', '2019-09-15 16:51:45'),
(23, 1, '1bd0bd5943494b2.jpg', 'Pizza Cheese', 'image/jpeg', 'jpg', '2019-09-15 16:56:19', '2019-09-15 16:56:19'),
(24, 1, '4ec18f13a825b26.jpg', 'Burger', 'image/jpeg', 'jpg', '2019-09-15 17:00:23', '2019-09-15 17:00:23'),
(25, 1, 'ddf4bdbd3fbceb8.jpg', 'White Cheese Salad', 'image/jpeg', 'jpg', '2019-09-15 17:04:01', '2019-09-15 17:04:01'),
(26, 1, '48df8520206d9d5.jpg', 'Olivi Pizza', 'image/jpeg', 'jpg', '2019-09-15 17:08:44', '2019-09-15 17:08:44'),
(27, 1, '886355d77edfa78.jpg', 'Meat with Botato', 'image/jpeg', 'jpg', '2019-09-15 17:35:56', '2019-09-15 17:35:56'),
(28, 1, '675231ed37a5211.jpg', 'Meat with Salad', 'image/jpeg', 'jpg', '2019-09-15 17:39:17', '2019-09-15 17:39:17'),
(29, 1, 'fa92bd91dac0d4b.jpg', 'Pizza Cheese', 'image/jpeg', 'jpg', '2019-09-15 17:47:55', '2019-09-15 17:47:55'),
(30, 1, '8002d536b8ffb96.jpg', 'Pizza Cheese', 'image/jpeg', 'jpg', '2019-09-15 17:54:06', '2019-09-15 17:54:06'),
(31, 1, 'abf7a6eaa9dca16.jpg', 'White Cheese Salad', 'image/jpeg', 'jpg', '2019-09-15 17:59:29', '2019-09-15 17:59:29'),
(32, 1, '22650c0abed9f7e.jpg', 'White Cheese Salad', 'image/jpeg', 'jpg', '2019-09-15 18:02:38', '2019-09-15 18:02:38'),
(33, 1, '271913c7e1ea272.jpg', 'Olivi Pizza', 'image/jpeg', 'jpg', '2019-09-15 18:03:51', '2019-09-15 18:03:51'),
(34, 1, '38de84eea0e1f7e.jpg', 'Olivi Pizza', 'image/jpeg', 'jpg', '2019-09-15 18:10:10', '2019-09-15 18:10:10'),
(35, 1, 'a3aa6b389acc467.jpg', 'White Cheese Salad', 'image/jpeg', 'jpg', '2019-09-15 18:21:39', '2019-09-15 18:21:39'),
(36, 1, '4c5f40540b58099.jpg', 'Main Menu', 'image/jpeg', 'jpg', '2019-09-17 16:21:13', '2019-09-17 16:21:13'),
(37, 1, '04e52ec7b6292ed.jpg', 'Main Menu', 'image/jpeg', 'jpg', '2019-09-17 16:24:20', '2019-09-17 16:24:20'),
(38, 1, 'c06b89850f9dbff.jpg', 'Breakfast', 'image/jpeg', 'jpg', '2019-09-17 16:25:10', '2019-09-17 16:25:10'),
(39, 1, '8d043031fbd72b3.jpg', 'Main Menu', 'image/jpeg', 'jpg', '2019-09-17 16:26:37', '2019-09-17 16:26:37'),
(40, 1, '7517e07e421a8d9.jpg', 'Olivi Pizza', 'image/jpeg', 'jpg', '2019-09-17 16:28:56', '2019-09-17 16:28:56'),
(41, 1, 'cdb71644bb711da.jpg', 'Olivi Pizza', 'image/jpeg', 'jpg', '2019-09-17 16:31:03', '2019-09-17 16:31:03'),
(42, 1, '929edfe457725c6.jpg', 'Hollandaise Egg Benedict', 'image/jpeg', 'jpg', '2019-10-20 21:21:03', '2019-10-20 21:21:03'),
(43, 1, '6f58621fb22ed3c.jpg', 'Hollandaise Egg Benedict', 'image/jpeg', 'jpg', '2019-10-20 21:21:08', '2019-10-20 21:21:08'),
(44, 1, 'eb4a387e6b06c62.jpg', 'Testing Offline', 'image/jpeg', 'jpg', '2019-10-20 21:23:57', '2019-10-20 21:23:57'),
(45, 1, '48a3387a3c73b1e.jpg', 'Testing Offline', 'image/jpeg', 'jpg', '2019-10-24 16:06:44', '2019-10-24 16:06:44'),
(46, 1, '69b5d24302a0c97.jpg', 'Oak\'s Deviled Egg', 'image/jpeg', 'jpg', '2019-10-24 17:56:25', '2019-10-24 17:56:25'),
(47, 1, '2f4738fc6b25b50.jpg', 'OAK’s Big Breakfast', 'image/jpeg', 'jpg', '2019-10-24 17:59:05', '2019-10-24 17:59:05'),
(48, 1, '3b20f5fdac8a052.jpeg', 'OAK’s Special Omelette', 'image/jpeg', 'jpeg', '2019-10-24 18:01:06', '2019-10-24 18:01:06'),
(49, 1, '0da94dc50b2712f.jpg', 'Croissant Sandwichs', 'image/jpeg', 'jpg', '2019-10-24 18:04:09', '2019-10-24 18:04:09'),
(50, 1, '780489c2ef85b64.jpg', 'Breakfast', 'image/jpeg', 'jpg', '2019-10-24 18:15:48', '2019-10-24 18:15:48'),
(51, 1, 'd7b78e919a7bac7.jpg', 'Labneh on Flat Bread', 'image/jpeg', 'jpg', '2019-10-24 18:22:16', '2019-10-24 18:22:16'),
(52, 1, 'bba928ca44e8a1e.jpg', 'Desserts', 'image/jpeg', 'jpg', '2019-10-24 19:28:51', '2019-10-24 19:28:51'),
(53, 1, '4eeb3a7e826112b.jpg', 'Mama\'s Apple Pie', 'image/jpeg', 'jpg', '2019-10-24 19:31:48', '2019-10-24 19:31:48'),
(54, 1, '2a795c6e0d8c9a5.jpg', 'Creme Brulee', 'image/jpeg', 'jpg', '2019-10-24 19:35:20', '2019-10-24 19:35:20'),
(55, 1, 'f93fb739f4b6e8e.jpg', 'Paradise Pie', 'image/jpeg', 'jpg', '2019-10-24 19:37:33', '2019-10-24 19:37:33'),
(56, 1, '5be33a853b27637.jpg', 'Borwnie with Ice Cream', 'image/jpeg', 'jpg', '2019-10-24 19:40:26', '2019-10-24 19:40:26'),
(57, 1, 'cd75d724507dad0.jpg', 'MONK (cafe)', 'image/jpeg', 'jpg', '2019-10-24 19:42:12', '2019-10-24 19:42:12'),
(58, 1, 'fa6acafb504a762.jpg', 'Fresh Juices', 'image/jpeg', 'jpg', '2019-10-24 19:53:52', '2019-10-24 19:53:52'),
(59, 1, 'df5b4f2186cce95.jpg', 'Soft Drinks', 'image/jpeg', 'jpg', '2019-10-24 19:55:14', '2019-10-24 19:55:14'),
(60, 1, '1e8986c6749b641.jpg', 'Hot & Coffee', 'image/jpeg', 'jpg', '2019-10-24 19:56:07', '2019-10-24 19:56:07'),
(61, 1, '314e193c5b5aaf4.jpg', 'OAK BAY (Grillhouse)', 'image/jpeg', 'jpg', '2019-10-24 19:58:13', '2019-10-24 19:58:13'),
(62, 1, 'd4a40dc30e5bc49.jpg', 'Meat', 'image/jpeg', 'jpg', '2019-10-24 20:00:45', '2019-10-24 20:00:45'),
(63, 1, '066bb9f8806d3c3.jpg', 'Chicken', 'image/jpeg', 'jpg', '2019-10-24 20:02:16', '2019-10-24 20:02:16'),
(64, 1, 'c80248f8b0b5d2f.jpg', 'OAK BAY (Sea Food)', 'image/jpeg', 'jpg', '2019-10-24 20:05:01', '2019-10-24 20:05:01'),
(65, 1, 'e3f3755d0282665.jpg', 'Meat', 'image/jpeg', 'jpg', '2019-10-24 20:57:32', '2019-10-24 20:57:32'),
(66, 1, '8efd04998ecfebb.jpg', 'Oyster Gongi Fillet', 'image/jpeg', 'jpg', '2019-10-24 21:00:18', '2019-10-24 21:00:18'),
(67, 1, '06dcfc4a79b1f24.jpg', 'Tokyo Fillet', 'image/jpeg', 'jpg', '2019-10-24 21:02:41', '2019-10-24 21:02:41'),
(68, 1, 'c63c7dbc203b952.jpg', 'Original Monterey Chicken', 'image/jpeg', 'jpg', '2019-10-24 21:15:56', '2019-10-24 21:15:56'),
(69, 1, '34976834181209f.jpeg', 'Orange Chicken Wok', 'image/jpeg', 'jpeg', '2019-10-24 21:19:08', '2019-10-24 21:19:08'),
(70, 1, '3b8d5d68ca78643.jpg', 'Oak’s Shrimp', 'image/jpeg', 'jpg', '2019-10-24 21:23:45', '2019-10-24 21:23:45'),
(71, 1, 'b61e35aca162d1a.jpg', 'Risotto Seafood', 'image/jpeg', 'jpg', '2019-10-24 21:25:43', '2019-10-24 21:25:43'),
(72, 1, '7527888b4ac97a9.jpeg', 'Haname Roll', 'image/jpeg', 'jpeg', '2019-10-24 21:31:05', '2019-10-24 21:31:05'),
(73, 1, 'c5620744e169e65.jpeg', 'Exclusive Roll', 'image/jpeg', 'jpeg', '2019-10-24 21:34:38', '2019-10-24 21:34:38'),
(74, 1, 'a1cb511aa1dce98.jpg', 'Fries', 'image/jpeg', 'jpg', '2019-10-24 21:41:46', '2019-10-24 21:41:46'),
(75, 1, '62baeff96bc29ca.jpg', 'Corn Flix', 'image/jpeg', 'jpg', '2019-10-24 21:57:21', '2019-10-24 21:57:21'),
(76, 1, 'f337d7accc03c13.jpg', 'Fries', 'image/jpeg', 'jpg', '2019-10-25 01:10:22', '2019-10-25 01:10:22'),
(77, 1, '25ff07b78d51757.jpg', 'New Dish', 'image/jpeg', 'jpg', '2019-11-05 23:36:24', '2019-11-05 23:36:24'),
(78, 1, '5490b9ba33c9a87.jpg', 'New Dish', 'image/jpeg', 'jpg', '2019-11-05 23:40:57', '2019-11-05 23:40:57'),
(79, 1, '36ef70140c9e6f8.jpg', 'New Dish', 'image/jpeg', 'jpg', '2019-11-06 00:42:05', '2019-11-06 00:42:05');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(191) DEFAULT NULL,
  `description` text,
  `display_as` enum('grid','list') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `num_of_rows` tinyint(4) DEFAULT NULL,
  `grid_title_position` enum('top','middle','bottom') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `display_similar_items` tinyint(4) NOT NULL DEFAULT '0',
  `mark_as_new` tinyint(4) NOT NULL DEFAULT '0',
  `mark_as_signature` tinyint(4) NOT NULL DEFAULT '0',
  `additional_notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `media_id` int(10) UNSIGNED DEFAULT NULL,
  `video_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `customer_id`, `parent_id`, `section_id`, `name`, `description`, `display_as`, `num_of_rows`, `grid_title_position`, `display_similar_items`, `mark_as_new`, `mark_as_signature`, `additional_notes`, `media_id`, `video_id`, `created_at`, `updated_at`, `is_active`) VALUES
(1, 1, 0, NULL, 'Breakfast', 'All Dishes at 70.0 EGP all inclusive! Enjoy', 'grid', 4, 'top', 0, 0, 0, NULL, 50, NULL, '2019-09-01 19:37:32', '2019-10-24 19:25:05', 1),
(2, 1, 0, NULL, 'Desserts', 'There is a treat for everyone!', 'grid', 2, 'top', 0, 0, 0, NULL, 52, NULL, '2019-09-01 19:39:41', '2019-10-24 19:28:51', 1),
(3, 1, 1, NULL, NULL, NULL, 'grid', 2, 'bottom', 1, 0, 0, NULL, 19, NULL, '2019-09-11 20:12:32', '2019-09-11 20:12:32', 0),
(4, 1, 1, NULL, NULL, NULL, 'list', NULL, NULL, 0, 1, 0, NULL, NULL, NULL, '2019-09-11 20:15:56', '2019-09-11 20:15:56', 0),
(6, 1, 1, NULL, 'Pizza', 'New', 'grid', 2, 'bottom', 0, 0, 0, NULL, 20, NULL, '2019-09-11 20:24:58', '2019-10-24 17:35:31', 0),
(7, 1, 0, NULL, 'MONK (cafe)', 'Cafe', 'grid', 2, 'top', 1, 0, 0, NULL, 57, NULL, '2019-09-15 16:45:28', '2019-10-24 19:42:12', 1),
(10, 1, 0, NULL, 'OAK BAY (Grillhouse)', 'Grillhouse', 'grid', 2, 'top', 0, 0, 0, NULL, 61, NULL, '2019-10-09 20:03:21', '2019-10-24 19:58:19', 1),
(11, 1, 7, NULL, NULL, NULL, 'grid', 4, 'top', 1, 0, 0, NULL, NULL, NULL, '2019-10-24 19:47:17', '2019-10-24 19:47:23', 1),
(12, 1, 7, NULL, 'Hot & Coffee', 'Hot & Coffee', 'grid', 2, 'top', 0, 0, 0, NULL, 60, NULL, '2019-10-24 19:49:42', '2019-10-24 19:56:22', 1),
(13, 1, 7, NULL, 'Soft Drinks', 'Soft Drinks', 'grid', 2, 'top', 0, 0, 0, NULL, 59, NULL, '2019-10-24 19:51:26', '2019-10-24 19:55:14', 1),
(14, 1, 7, NULL, 'Fresh Juices', 'Fresh Juices', 'grid', 2, 'top', 0, 0, 0, NULL, 58, NULL, '2019-10-24 19:53:52', '2019-10-24 19:54:01', 1),
(15, 1, 10, NULL, 'Meat', 'meat', 'grid', 4, 'top', 0, 0, 0, NULL, 65, NULL, '2019-10-24 20:00:45', '2019-10-24 20:57:32', 1),
(16, 1, 10, NULL, NULL, NULL, 'grid', 2, 'top', 0, 0, 0, NULL, 63, NULL, '2019-10-24 20:02:16', '2019-10-24 20:02:23', 1),
(17, 1, 0, NULL, NULL, NULL, 'grid', 2, 'top', 0, 0, 0, NULL, 64, NULL, '2019-10-24 20:05:01', '2019-10-24 20:05:11', 1);

-- --------------------------------------------------------

--
-- Table structure for table `menu_sections`
--

CREATE TABLE `menu_sections` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(5000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menu_translations`
--

CREATE TABLE `menu_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `additional_notes` text COLLATE utf8mb4_unicode_ci,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_translations`
--

INSERT INTO `menu_translations` (`id`, `menu_id`, `name`, `description`, `additional_notes`, `locale`, `created_at`, `updated_at`) VALUES
(1, 1, 'Breakfast', 'All Dishes at 70.0 EGP all inclusive! Enjoy', NULL, 'en', '2019-09-01 19:37:32', '2019-10-24 19:25:05'),
(2, 2, 'Desserts', 'There is a treat for everyone!', NULL, 'en', '2019-09-01 19:39:41', '2019-09-01 19:39:41'),
(3, 3, 'Pizza Cheese', 'New', NULL, 'en', '2019-09-11 20:12:32', '2019-09-11 20:12:32'),
(4, 4, 'Test Sub Menu', 'Description', 'Note', 'en', '2019-09-11 20:15:56', '2019-09-11 20:15:56'),
(5, 5, 'Test Sub Menu', 'Description', 'Note', 'en', '2019-09-11 20:21:43', '2019-09-11 20:21:43'),
(6, 6, 'Pizza', 'New', NULL, 'en', '2019-09-11 20:24:58', '2019-09-11 20:24:58'),
(7, 7, 'MONK (cafe)', 'Cafe', NULL, 'en', '2019-09-15 16:45:28', '2019-10-24 19:42:12'),
(8, 8, 'Pizza', 'New Falvour', NULL, 'en', '2019-09-15 16:51:45', '2019-09-15 16:51:45'),
(9, 9, 'Testing', 'new', NULL, 'en', '2019-09-17 18:43:00', '2019-09-17 18:43:00'),
(10, 10, 'OAK BAY (Grillhouse)', 'Grillhouse', NULL, 'en', '2019-10-09 20:03:21', '2019-10-24 19:58:13'),
(11, 11, 'Hot & Coffee', 'Hot & Coffee', NULL, 'en', '2019-10-24 19:47:17', '2019-10-24 19:47:17'),
(12, 12, 'Hot & Coffee', 'Hot & Coffee', NULL, 'en', '2019-10-24 19:49:42', '2019-10-24 19:49:42'),
(13, 13, 'Soft Drinks', 'Soft Drinks', NULL, 'en', '2019-10-24 19:51:26', '2019-10-24 19:51:26'),
(14, 14, 'Fresh Juices', 'Fresh Juices', NULL, 'en', '2019-10-24 19:53:52', '2019-10-24 19:53:52'),
(15, 15, 'Meat', 'meat', NULL, 'en', '2019-10-24 20:00:45', '2019-10-24 20:00:45'),
(16, 16, 'Chicken', 'Chicken', NULL, 'en', '2019-10-24 20:02:16', '2019-10-24 20:02:16'),
(17, 17, 'OAK BAY (Sea Food)', 'Sea Food', NULL, 'en', '2019-10-24 20:05:01', '2019-10-24 20:05:01');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_admins_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2019_03_17_143538_create_countries_table', 1),
(9, '2019_03_17_143701_create_vendors_table', 1),
(10, '2019_03_25_104518_add_is_active_to_customer_table', 1),
(11, '2019_03_28_095700_create_customer_users_table', 1),
(12, '2019_03_28_110006_create_media_table', 1),
(13, '2019_03_28_114643_create_menus_table', 1),
(14, '2019_04_01_135157_add_customer_id_to_media_table', 1),
(15, '2019_04_07_140031_add_is_active_menus_table', 1),
(16, '2019_04_08_123601_create_menu_sections_table', 1),
(17, '2019_04_08_145206_add_section_id_to_menus_table', 1),
(18, '2019_05_14_091914_add_deleted_at_to_customers_table', 2),
(60, '2019_05_27_113114_create_item_option_values_table', 6),
(59, '2019_05_20_151135_create_cart_items_table', 5),
(58, '2019_05_20_020951_create_cart_item_options_table', 5),
(57, '2019_05_20_020919_create_cart_item_side_dishes_table', 5),
(56, '2019_05_20_020749_create_carts_table', 5),
(55, '2019_05_14_095800_create_item_prices_table', 5),
(54, '2019_05_14_095608_create_item_options_table', 5),
(29, '2019_05_19_152744_create_themes_table', 4),
(53, '2019_05_14_095418_create_options_table', 5),
(52, '2019_05_14_095157_create_item_sidedishes_table', 5),
(50, '2019_05_14_095105_create_item_ingredients_table', 5),
(51, '2019_05_14_095141_create_sidedishes_table', 5),
(49, '2019_05_14_095047_create_ingredients_table', 5),
(48, '2019_05_14_094942_create_items_table', 5),
(61, '2019_05_29_112126_create_orders_table', 7),
(64, '2019_05_10_122658_create_campaigns_table', 8),
(65, '2019_05_10_115332_add_fields_to_campaigns', 9),
(79, '2019_05_10_181233_create_feedback_table', 22),
(67, '2019_05_10_181500_create_feedback_questions_table', 11),
(68, '2019_05_10_181510_add_fields_end_to_campaigns', 12),
(69, '2019_05_11_103438_add_venue_fields_to_customers', 13),
(70, '2019_05_11_115746_add_logo_to_customers', 14),
(71, '2019_05_11_124848_create_plans_table', 15),
(72, '2019_05_11_153012_add_plan_id_to_customers', 16),
(73, '2019_05_11_155251_add_order_to_plans', 16),
(74, '2019_07_10_122856_create_arrangments_table', 17),
(82, '2019_07_15_130105_create_configrations_table', 25),
(76, '2019_07_16_105858_create_menu_translations_table', 19),
(77, '2019_07_16_105930_create_item_translations_table', 20),
(78, '2019_07_16_110151_create_feedback_translations_table', 21),
(80, '2019_07_22_163052_create_question_translations_table', 23),
(81, '2019_07_24_124956_add_waiter_login_to_customer_table', 24),
(84, '2019_07_24_143027_add_order_option_to_plans_table', 26),
(85, '2019_07_24_162752_add_customer_id_to_side_dishes_table', 27),
(86, '2019_07_30_164920_create_tables_table', 28);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Menioo Personal Access Client', 'xfmDmEQAcnqJDWG7c5e5MUNv0OtNT2DzETgrRybr', 'http://localhost', 1, 0, 0, '2019-05-15 07:35:21', '2019-05-15 07:35:21'),
(2, NULL, 'Menioo Password Grant Client', 'eqNpL10mwwaZBRHdG2wRDRPgHr8QSsBuOr3qFWIL', 'http://localhost', 0, 1, 0, '2019-05-15 07:35:21', '2019-05-15 07:35:21');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-05-15 07:35:21', '2019-05-15 07:35:21');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE `options` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `options`
--

INSERT INTO `options` (`id`, `customer_id`, `name`, `type`, `created_at`, `updated_at`) VALUES
(1, 1, 'Sauce', 'Checkbox', '2019-09-01 22:29:31', '2019-09-01 22:29:31'),
(2, 1, 'Cheese', 'Slider', '2019-09-01 22:35:00', '2019-09-01 22:35:00'),
(3, 1, 'Cooking Level', 'Slider', '2019-10-24 21:45:50', '2019-10-24 21:45:50'),
(4, 1, 'Sauces', 'Checkbox', '2019-10-24 21:47:49', '2019-10-24 21:47:49');

-- --------------------------------------------------------

--
-- Table structure for table `option_values`
--

CREATE TABLE `option_values` (
  `id` int(10) UNSIGNED NOT NULL,
  `option_id` int(10) UNSIGNED DEFAULT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `option_values`
--

INSERT INTO `option_values` (`id`, `option_id`, `value`, `price`, `created_at`, `updated_at`) VALUES
(1, 1, 'Hot Mustard', 0.00, '2019-09-01 22:29:31', '2019-09-01 22:29:31'),
(2, 1, 'Tangy BBQ', 0.00, '2019-09-01 22:29:31', '2019-09-01 22:29:31'),
(3, 1, 'Sweet \'n Sour', 0.00, '2019-09-01 22:29:31', '2019-09-01 22:29:31'),
(4, 1, 'Honey Mustard', 0.00, '2019-09-01 22:29:31', '2019-09-01 22:29:31'),
(5, 1, 'Habanero Ranch', 0.00, '2019-09-01 22:29:31', '2019-09-01 22:29:31'),
(6, 1, 'Creamy Ranch', 0.00, '2019-09-01 22:29:31', '2019-09-01 22:29:31'),
(7, 1, 'Spicy Buffalo', 0.00, '2019-09-01 22:29:31', '2019-09-01 22:29:31'),
(8, 2, 'Cottage Cheese', 0.00, '2019-09-01 22:35:00', '2019-09-01 22:35:00'),
(9, 2, 'Quark', 0.00, '2019-09-01 22:35:00', '2019-09-01 22:35:00'),
(10, 2, 'Ricotta Cheese', 0.00, '2019-09-01 22:35:00', '2019-09-01 22:35:00'),
(11, 2, 'Swiss', 0.00, '2019-09-01 22:35:00', '2019-09-01 22:35:00'),
(12, 2, 'Mozzarella', 0.00, '2019-09-01 22:35:00', '2019-09-01 22:35:00'),
(13, 2, 'Cheddar', 0.00, '2019-09-01 22:35:00', '2019-09-01 22:35:00'),
(14, 2, 'Neufchatel', 0.00, '2019-09-01 22:35:00', '2019-09-01 22:35:00'),
(15, 3, 'Well Done', NULL, '2019-10-24 21:45:50', '2019-10-24 21:45:50'),
(16, 3, 'Medium Well', NULL, '2019-10-24 21:45:50', '2019-10-24 21:45:50'),
(17, 3, 'Medium', NULL, '2019-10-24 21:45:50', '2019-10-24 21:45:50'),
(18, 3, 'Medium Rare', NULL, '2019-10-24 21:45:50', '2019-10-24 21:45:50'),
(19, 3, 'Rare', NULL, '2019-10-24 21:45:50', '2019-10-24 21:45:50'),
(20, 4, 'Red', 5.00, '2019-10-24 21:47:49', '2019-10-24 21:47:49'),
(21, 4, 'White', 4.00, '2019-10-24 21:47:49', '2019-10-24 21:47:49'),
(22, 4, 'Browne', 6.00, '2019-10-24 21:47:49', '2019-10-24 21:47:49'),
(23, 4, 'Butter', 7.00, '2019-10-24 21:47:49', '2019-10-24 21:47:49'),
(24, 4, 'Hot', 10.00, '2019-10-24 21:47:49', '2019-10-24 21:47:49');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `customer_user_id` int(10) UNSIGNED DEFAULT NULL,
  `cart_id` int(10) UNSIGNED NOT NULL,
  `table_number` int(11) DEFAULT NULL,
  `service_charge` double(8,2) NOT NULL DEFAULT '0.00',
  `tip` double(8,2) NOT NULL DEFAULT '0.00',
  `tax` double(8,2) NOT NULL DEFAULT '0.00',
  `total_cost` float NOT NULL DEFAULT '0',
  `status` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `customer_id`, `customer_user_id`, `cart_id`, `table_number`, `service_charge`, `tip`, `tax`, `total_cost`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, 1, 20, 4.80, 0.00, 0.00, 44.8, 'close', '2019-09-17 18:00:58', '2019-09-17 18:00:58'),
(2, 1, NULL, 2, 20, 4.80, 0.00, 5.60, 50.4, 'close', '2019-09-17 18:22:53', '2019-09-17 18:22:53'),
(3, 1, NULL, 3, 40, 9.00, 0.00, 10.50, 94.5, 'close', '2019-09-19 15:45:43', '2019-09-19 15:45:43'),
(4, 1, NULL, 4, 40, 2.40, 0.00, 2.80, 25.2, 'close', '2019-09-19 15:45:51', '2019-09-19 15:45:51'),
(5, 1, NULL, 5, 40, 5.40, 0.00, 6.30, 56.7, 'close', '2019-10-20 22:36:38', '2019-10-20 22:36:38'),
(6, 1, NULL, 6, 13, 36.36, 0.00, 42.42, 381.78, 'close', '2019-10-20 22:45:26', '2019-10-20 22:45:26'),
(7, 1, NULL, 7, 13, 8.40, 0.00, 9.80, 88.2, 'close', '2019-10-20 22:45:30', '2019-10-20 22:45:30'),
(8, 1, NULL, 8, 13, 8.40, 0.00, 9.80, 88.2, 'close', '2019-10-20 22:45:33', '2019-10-20 22:45:33'),
(9, 1, NULL, 9, 0, 7.80, 0.00, 9.10, 81.9, 'close', '2019-10-20 22:45:38', '2019-10-20 22:45:38'),
(10, 1, NULL, 10, 0, 8.40, 0.00, 9.80, 88.2, 'close', '2019-10-20 22:45:44', '2019-10-20 22:45:44'),
(11, 1, NULL, 11, 40, 8.40, 0.00, 9.80, 88.2, 'close', '2019-10-20 22:45:54', '2019-10-20 22:45:54'),
(12, 1, NULL, 12, 40, 8.40, 0.00, 9.80, 88.2, 'close', '2019-10-20 22:45:58', '2019-10-20 22:45:58'),
(13, 1, NULL, 13, 40, 8.40, 0.00, 9.80, 88.2, 'close', '2019-10-20 22:46:02', '2019-10-20 22:46:02'),
(14, 1, NULL, 14, 0, 8.40, 0.00, 9.80, 88.2, 'close', '2019-10-20 22:46:06', '2019-10-20 22:46:06'),
(15, 1, NULL, 15, 40, 9.00, 0.00, 10.50, 94.5, 'close', '2019-10-20 22:46:09', '2019-10-20 22:46:09'),
(16, 1, NULL, 16, 0, 16.80, 0.00, 19.60, 176.4, 'close', '2019-10-20 22:46:13', '2019-10-20 22:46:13'),
(17, 1, NULL, 17, 0, 16.80, 0.00, 19.60, 176.4, 'close', '2019-10-20 22:46:17', '2019-10-20 22:46:17'),
(18, 1, NULL, 18, 0, 8.76, 0.00, 10.22, 91.98, 'close', '2019-10-20 22:46:20', '2019-10-20 22:46:20'),
(19, 1, NULL, 19, 0, 6.00, 0.00, 7.00, 63, 'close', '2019-10-20 22:46:24', '2019-10-20 22:46:24'),
(20, 1, NULL, 20, 0, 10.80, 0.00, 12.60, 113.4, 'close', '2019-10-20 22:46:36', '2019-10-20 22:46:36'),
(21, 1, NULL, 21, 40, 4.80, 0.00, 5.60, 50.4, 'close', '2019-10-20 22:46:41', '2019-10-20 22:46:41'),
(22, 1, NULL, 22, 0, 18.00, 0.00, 21.00, 189, 'close', '2019-10-20 22:46:45', '2019-10-20 22:46:45'),
(23, 1, NULL, 23, 0, 21.48, 0.00, 25.06, 225.54, 'close', '2019-10-20 22:46:49', '2019-10-20 22:46:49'),
(24, 1, NULL, 24, 0, 4.80, 0.00, 5.60, 50.4, 'close', '2019-10-20 22:47:03', '2019-10-20 22:47:03'),
(25, 1, NULL, 25, 0, 2.76, 0.00, 3.22, 28.98, 'close', '2019-10-20 22:47:07', '2019-10-20 22:47:07'),
(26, 1, NULL, 26, 1, 0.00, 0.00, 0.00, 0, 'close', '2019-10-20 22:47:12', '2019-10-20 22:47:12'),
(27, 1, NULL, 27, 1, 0.00, 0.00, 0.00, 0, 'close', '2019-10-20 22:47:16', '2019-10-20 22:47:16'),
(28, 1, NULL, 28, 0, 25.20, 0.00, 29.40, 264.6, 'close', '2019-10-20 22:47:19', '2019-10-20 22:47:19'),
(29, 1, NULL, 29, 0, 5.40, 0.00, 6.30, 56.7, 'close', '2019-10-20 22:47:22', '2019-10-20 22:47:22'),
(30, 1, NULL, 30, 0, 5.40, 0.00, 6.30, 56.7, 'close', '2019-10-20 22:48:09', '2019-10-20 22:48:09'),
(31, 1, NULL, 31, 40, 4.80, 0.00, 5.60, 50.4, 'close', '2019-10-20 22:49:03', '2019-10-20 22:49:03'),
(32, 1, 1, 34, 0, 8.40, 0.00, 9.80, 88.2, 'close', '2019-10-22 11:02:02', '2019-10-22 11:02:02'),
(33, 1, NULL, 32, 40, 5.40, 0.00, 6.30, 56.7, 'close', '2019-10-22 17:44:08', '2019-10-22 17:44:08'),
(34, 1, NULL, 33, 40, 38.40, 0.00, 44.80, 403.2, 'close', '2019-10-22 20:44:23', '2019-10-22 20:44:23'),
(35, 1, NULL, 36, 0, 8.40, 0.00, 9.80, 88.2, 'close', '2019-10-26 22:37:07', '2019-10-26 22:37:07'),
(36, 1, NULL, 39, 0, 8.40, 0.00, 9.80, 88.2, 'close', '2019-10-26 22:37:18', '2019-10-26 22:37:18'),
(37, 1, NULL, 41, 0, 6.00, 0.00, 7.00, 63, 'close', '2019-10-26 22:44:24', '2019-10-26 22:44:24'),
(38, 1, NULL, 43, 0, 40.20, 0.00, 46.90, 422.1, 'close', '2019-10-27 19:01:36', '2019-10-27 19:01:36'),
(39, 1, NULL, 44, 0, 21.60, 0.00, 25.20, 226.8, 'close', '2019-10-27 19:07:41', '2019-10-27 19:07:41'),
(40, 1, NULL, 46, 0, 66.24, 0.00, 77.28, 695.52, 'close', '2019-10-27 19:07:45', '2019-10-27 19:07:45'),
(41, 1, NULL, 47, 0, 26.88, 0.00, 31.36, 282.24, 'close', '2019-10-27 19:07:49', '2019-10-27 19:07:49'),
(42, 1, NULL, 60, 0, 53.76, 0.00, 62.72, 564.48, 'close', '2019-10-27 19:11:09', '2019-10-27 19:11:09'),
(43, 1, NULL, 59, 0, 55.92, 0.00, 65.24, 587.16, 'close', '2019-10-27 19:12:11', '2019-10-27 19:12:11'),
(44, 1, NULL, 48, 0, 24.84, 0.00, 28.98, 260.82, 'close', '2019-10-27 19:14:32', '2019-10-27 19:14:32'),
(45, 1, NULL, 49, 0, 24.84, 0.00, 28.98, 260.82, 'close', '2019-11-06 00:58:23', '2019-11-06 00:58:23'),
(46, 1, NULL, 50, 0, 24.84, 0.00, 28.98, 260.82, 'close', '2019-11-06 01:00:05', '2019-11-06 01:00:05');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `plans`
--

CREATE TABLE `plans` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tablets_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `popular` tinyint(4) NOT NULL DEFAULT '0',
  `visible` tinyint(4) NOT NULL DEFAULT '1',
  `features` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1000000',
  `order_option` tinyint(4) NOT NULL DEFAULT '1',
  `billing_period` int(11) NOT NULL,
  `plan_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `plan_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `plans`
--

INSERT INTO `plans` (`id`, `name`, `type`, `price`, `tablets_number`, `popular`, `visible`, `features`, `created_at`, `updated_at`, `order`, `order_option`, `billing_period`, `plan_type`, `plan_id`) VALUES
(3, 'Basic', 'Normal', '0', '3', 0, 1, '[{\"name\":\"Unlimited Menus\",\"active\":\"on\"},{\"name\":\"24\\/7 Online Support\",\"active\":\"on\"},{\"name\":\"Free Onboarding\",\"active\":\"on\"},{\"name\":\"Menu Building\",\"active\":\"on\"},{\"name\":\"Customized Fonts\",\"active\":\"on\"},{\"name\":\"Instant Feedback\",\"active\":\"on\"},{\"name\":\"Ordering Feature\"},{\"name\":\"POS Integrations\"},{\"name\":\"Kitchen Printer Integrations\"}]', '2019-10-07 14:29:16', '2019-10-07 14:29:16', 1, 0, 1, 'month', 565740),
(4, 'Growing Business', 'Normal', '90.00', '10', 1, 1, '[{\"name\":\"Unlimited Menus\",\"active\":\"on\"},{\"name\":\"24\\/7 Online Support\",\"active\":\"on\"},{\"name\":\"Free Onboarding\",\"active\":\"on\"},{\"name\":\"Menu Building\",\"active\":\"on\"},{\"name\":\"Customized Fonts\",\"active\":\"on\"},{\"name\":\"Instant Feedback\",\"active\":\"on\"},{\"name\":\"Ordering Feature\",\"active\":\"on\"},{\"name\":\"POS Integrations\"},{\"name\":\"Kitchen Printer Integrations\"}]', '2019-10-07 15:18:36', '2019-10-07 15:18:36', 2, 0, 1, 'month', 565743),
(5, 'Expanding Chain', 'Normal', '140.00', '10', 0, 1, '[{\"name\":\"Unlimited Menus\",\"active\":\"on\"},{\"name\":\"24\\/7 Online Support\",\"active\":\"on\"},{\"name\":\"Free Onboarding\",\"active\":\"on\"},{\"name\":\"Menu Building\",\"active\":\"on\"},{\"name\":\"Customized Fonts\",\"active\":\"on\"},{\"name\":\"Instant Feedback\",\"active\":\"on\"},{\"name\":\"Ordering Feature\",\"active\":\"on\"},{\"name\":\"POS Integrations\"},{\"name\":\"Kitchen Printer Integrations\"}]', '2019-10-07 15:18:36', '2019-10-07 15:18:36', 3, 0, 1, 'month', 565744),
(6, 'Basic', 'Normal', '480.00', '1', 0, 1, '[{\"name\":\"Unlimited Menus\",\"active\":\"on\"},{\"name\":\"24\\/7 Online Support\",\"active\":\"on\"},{\"name\":\"Free Onboarding\",\"active\":\"on\"},{\"name\":\"Menu Building\",\"active\":\"on\"},{\"name\":\"Customized Fonts\",\"active\":\"on\"},{\"name\":\"Instant Feedback\",\"active\":\"on\"},{\"name\":\"Ordering Feature\"},{\"name\":\"POS Integrations\"},{\"name\":\"Kitchen Printer Integrations\"}]', '2019-10-07 15:18:36', '2019-10-07 15:18:36', 4, 0, 12, 'month', 565745),
(7, 'Growing Business', 'Normal', '864.00', '10', 1, 1, '[{\"name\":\"Unlimited Menus\",\"active\":\"on\"},{\"name\":\"24\\/7 Online Support\",\"active\":\"on\"},{\"name\":\"Free Onboarding\",\"active\":\"on\"},{\"name\":\"Menu Building\",\"active\":\"on\"},{\"name\":\"Customized Fonts\",\"active\":\"on\"},{\"name\":\"Instant Feedback\",\"active\":\"on\"},{\"name\":\"Ordering Feature\",\"active\":\"on\"},{\"name\":\"POS Integrations\"},{\"name\":\"Kitchen Printer Integrations\"}]', '2019-10-07 15:18:36', '2019-10-07 15:18:36', 5, 0, 12, 'month', 565746),
(8, 'Expanding Chain', 'Normal', '1344.00', '10', 0, 1, '[{\"name\":\"Unlimited Menus\",\"active\":\"on\"},{\"name\":\"24\\/7 Online Support\",\"active\":\"on\"},{\"name\":\"Free Onboarding\",\"active\":\"on\"},{\"name\":\"Menu Building\",\"active\":\"on\"},{\"name\":\"Customized Fonts\",\"active\":\"on\"},{\"name\":\"Instant Feedback\",\"active\":\"on\"},{\"name\":\"Ordering Feature\",\"active\":\"on\"},{\"name\":\"POS Integrations\"},{\"name\":\"Kitchen Printer Integrations\"}]', '2019-10-07 15:18:36', '2019-10-07 15:18:36', 6, 0, 12, 'month', 565747);

-- --------------------------------------------------------

--
-- Table structure for table `question_translations`
--

CREATE TABLE `question_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `feedback_question_id` int(10) UNSIGNED NOT NULL,
  `value` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `question_translations`
--

INSERT INTO `question_translations` (`id`, `feedback_question_id`, `value`, `locale`, `created_at`, `updated_at`) VALUES
(11, 16, 'ما رأيك فى جودة المكان؟', 'ar', NULL, NULL),
(12, 16, 'How do you see our service 55555?', 'en', NULL, '2019-11-05 23:37:28'),
(13, 17, 'ما هو اقتراحك لتحسين الجودة؟', 'ar', NULL, NULL),
(14, 17, 'What\'s your opinion to improve our service?', 'en', NULL, '2019-11-05 22:21:42'),
(15, 18, 'ما هو تقييمك لنظافة المكان', 'ar', NULL, NULL),
(16, 18, 'what\'s your rate for place cleanliness?', 'en', NULL, NULL),
(17, 19, 'هل تريد زيارة المكان مرة أخرى؟', 'ar', NULL, NULL),
(18, 19, 'will you visit us again?', 'en', NULL, NULL),
(19, 20, 'هل أنت راض عن جودة الطعام المقدم؟', 'ar', NULL, NULL),
(20, 20, 'Are you satisfied?', 'en', NULL, '2019-11-05 22:28:05');

-- --------------------------------------------------------

--
-- Table structure for table `recommended_items`
--

CREATE TABLE `recommended_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `item_id` int(10) UNSIGNED DEFAULT NULL,
  `recommended_item_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `recommended_items`
--

INSERT INTO `recommended_items` (`id`, `item_id`, `recommended_item_id`, `created_at`, `updated_at`) VALUES
(183, 14, 18, NULL, NULL),
(137, 2, 21, NULL, NULL),
(3, 5, 2, NULL, NULL),
(4, 5, 6, NULL, NULL),
(45, 6, 5, NULL, NULL),
(44, 6, 2, NULL, NULL),
(43, 1, 4, NULL, NULL),
(42, 1, 3, NULL, NULL),
(41, 3, 4, NULL, NULL),
(40, 3, 1, NULL, NULL),
(39, 4, 3, NULL, NULL),
(38, 4, 1, NULL, NULL),
(182, 14, 17, NULL, NULL),
(33, 10, 10, NULL, NULL),
(15, 11, 8, NULL, NULL),
(16, 12, 8, NULL, NULL),
(17, 12, 11, NULL, NULL),
(23, 7, 10, NULL, NULL),
(22, 7, 9, NULL, NULL),
(21, 7, 7, NULL, NULL),
(181, 14, 16, NULL, NULL),
(32, 10, 7, NULL, NULL),
(180, 14, 15, NULL, NULL),
(119, 15, 18, NULL, NULL),
(118, 15, 17, NULL, NULL),
(121, 16, 18, NULL, NULL),
(120, 16, 17, NULL, NULL),
(157, 17, 18, NULL, NULL),
(156, 17, 16, NULL, NULL),
(126, 18, 17, NULL, NULL),
(125, 18, 16, NULL, NULL),
(124, 18, 15, NULL, NULL),
(175, 19, 21, NULL, NULL),
(174, 19, 20, NULL, NULL),
(131, 20, 21, NULL, NULL),
(130, 20, 20, NULL, NULL),
(129, 20, 19, NULL, NULL),
(134, 21, 20, NULL, NULL),
(133, 21, 19, NULL, NULL),
(132, 21, 5, NULL, NULL),
(136, 2, 20, NULL, NULL),
(135, 2, 19, NULL, NULL),
(155, 23, 22, NULL, NULL),
(154, 22, 23, NULL, NULL),
(141, 25, 24, NULL, NULL),
(140, 24, 25, NULL, NULL),
(147, 27, 29, NULL, NULL),
(153, 29, 28, NULL, NULL),
(152, 29, 27, NULL, NULL),
(151, 29, 26, NULL, NULL),
(150, 28, 29, NULL, NULL),
(149, 28, 27, NULL, NULL),
(148, 28, 26, NULL, NULL),
(146, 27, 28, NULL, NULL),
(145, 27, 26, NULL, NULL),
(144, 26, 29, NULL, NULL),
(143, 26, 28, NULL, NULL),
(142, 26, 27, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `side_dishes`
--

CREATE TABLE `side_dishes` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL,
  `media_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `side_dishes`
--

INSERT INTO `side_dishes` (`id`, `name`, `price`, `media_id`, `created_at`, `updated_at`, `customer_id`) VALUES
(1, 'Baby Botatos', 7.00, 15, '2019-09-01 22:22:03', '2019-10-24 21:38:36', 1),
(2, 'Hashbrown', 5.00, 16, '2019-09-01 22:24:10', '2019-09-01 22:24:10', 1),
(3, 'French Fries', 6.00, 17, '2019-09-01 22:25:08', '2019-10-24 21:38:05', 1),
(4, 'Fries', 20.00, 76, '2019-10-24 21:41:46', '2019-10-25 01:10:22', 1),
(5, 'Corn Flix', 7.00, 75, '2019-10-24 21:57:21', '2019-10-24 21:57:21', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tables`
--

CREATE TABLE `tables` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `number` int(10) UNSIGNED NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tables`
--

INSERT INTO `tables` (`id`, `customer_id`, `number`, `description`, `created_at`, `updated_at`) VALUES
(1, 1, 40, 'Center', '2019-07-30 18:10:11', '2019-09-15 17:41:36'),
(2, 1, 13, 'Next Door', '2019-09-15 17:40:53', '2019-09-15 17:40:53'),
(3, 1, 32, 'Next Window', '2019-09-15 17:41:13', '2019-09-15 17:41:13'),
(4, 1, 20, 'Right Corner', '2019-09-15 17:42:00', '2019-09-15 17:42:00'),
(5, 1, 50, 'Window View', '2019-10-27 17:25:20', '2019-10-27 17:25:20');

-- --------------------------------------------------------

--
-- Table structure for table `themes`
--

CREATE TABLE `themes` (
  `id` int(10) UNSIGNED NOT NULL,
  `page_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `background_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `background_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `header_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_text_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `view_text_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `arrangments`
--
ALTER TABLE `arrangments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `arrangments_arrangmentable_type_arrangmentable_id_index` (`arrangmentable_type`,`arrangmentable_id`);

--
-- Indexes for table `campaigns`
--
ALTER TABLE `campaigns`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `carts_customer_id_foreign` (`customer_id`),
  ADD KEY `carts_customer_user_id_foreign` (`customer_user_id`);

--
-- Indexes for table `cart_items`
--
ALTER TABLE `cart_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cart_items_cart_id_foreign` (`cart_id`),
  ADD KEY `cart_items_item_id_foreign` (`item_id`);

--
-- Indexes for table `cart_item_options`
--
ALTER TABLE `cart_item_options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cart_item_options_item_id_foreign` (`item_id`),
  ADD KEY `cart_item_options_cart_id_foreign` (`cart_id`),
  ADD KEY `cart_item_options_item_option_id_foreign` (`item_option_id`);

--
-- Indexes for table `cart_item_side_dishes`
--
ALTER TABLE `cart_item_side_dishes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cart_item_side_dishes_item_id_foreign` (`item_id`),
  ADD KEY `cart_item_side_dishes_cart_id_foreign` (`cart_id`),
  ADD KEY `cart_item_side_dishes_item_side_dish_id_foreign` (`item_side_dish_id`);

--
-- Indexes for table `configrations`
--
ALTER TABLE `configrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customers_country_id_foreign` (`country_id`),
  ADD KEY `customers_city_id_foreign` (`city_id`),
  ADD KEY `customers_logo_id_foreign` (`logo_id`);

--
-- Indexes for table `customer_users`
--
ALTER TABLE `customer_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `customer_users_email_unique` (`email`),
  ADD KEY `customer_users_customer_id_foreign` (`customer_id`);

--
-- Indexes for table `devices`
--
ALTER TABLE `devices`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `devices_udid_unique` (`uid`),
  ADD KEY `devices_customer_id_foreign` (`customer_id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedback_interactions`
--
ALTER TABLE `feedback_interactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `feedback_interactions_customer_id_foreign` (`customer_id`),
  ADD KEY `feedback_interactions_customer_user_id_foreign` (`customer_user_id`),
  ADD KEY `feedback_interactions_form_id_foreign` (`form_id`),
  ADD KEY `feedback_interactions_question_id_foreign` (`question_id`);

--
-- Indexes for table `feedback_questions`
--
ALTER TABLE `feedback_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedback_translations`
--
ALTER TABLE `feedback_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `feedback_translations_feedback_id_locale_unique` (`feedback_id`,`locale`),
  ADD KEY `feedback_translations_locale_index` (`locale`);

--
-- Indexes for table `ingredients_warnings`
--
ALTER TABLE `ingredients_warnings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_ingredient_warnings`
--
ALTER TABLE `item_ingredient_warnings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `item_ingredient_warnings_item_id_foreign` (`item_id`),
  ADD KEY `item_ingredient_warnings_ingredient_warning_id_foreign` (`ingredient_warning_id`);

--
-- Indexes for table `item_options`
--
ALTER TABLE `item_options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `item_options_item_id_foreign` (`item_id`),
  ADD KEY `item_options_option_id_foreign` (`option_id`);

--
-- Indexes for table `item_option_values`
--
ALTER TABLE `item_option_values`
  ADD PRIMARY KEY (`id`),
  ADD KEY `item_option_values_item_option_id_foreign` (`item_option_id`);

--
-- Indexes for table `item_prices`
--
ALTER TABLE `item_prices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `item_prices_item_id_foreign` (`item_id`);

--
-- Indexes for table `item_side_dishes`
--
ALTER TABLE `item_side_dishes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `item_side_dishes_item_id_foreign` (`item_id`),
  ADD KEY `item_side_dishes_side_dish_id_foreign` (`side_dish_id`),
  ADD KEY `item_side_dishes_media_id_foreign` (`media_id`);

--
-- Indexes for table `item_translations`
--
ALTER TABLE `item_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `item_translations_item_id_locale_unique` (`item_id`,`locale`),
  ADD KEY `item_translations_locale_index` (`locale`);

--
-- Indexes for table `item_views`
--
ALTER TABLE `item_views`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `media_customer_id_foreign` (`customer_id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu_sections`
--
ALTER TABLE `menu_sections`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_sections_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `menu_translations`
--
ALTER TABLE `menu_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menu_translations_menu_id_locale_unique` (`menu_id`,`locale`),
  ADD KEY `menu_translations_locale_index` (`locale`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `option_values`
--
ALTER TABLE `option_values`
  ADD PRIMARY KEY (`id`),
  ADD KEY `option_values_option_id_foreign` (`option_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `plans`
--
ALTER TABLE `plans`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `plans_order_unique` (`order`);

--
-- Indexes for table `question_translations`
--
ALTER TABLE `question_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `question_translations_question_id_locale_unique` (`feedback_question_id`,`locale`),
  ADD KEY `question_translations_locale_index` (`locale`);

--
-- Indexes for table `recommended_items`
--
ALTER TABLE `recommended_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `recommended_items_item_id_foreign` (`item_id`),
  ADD KEY `recommended_items_recommended_item_id_foreign` (`recommended_item_id`);

--
-- Indexes for table `side_dishes`
--
ALTER TABLE `side_dishes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `side_dishes_media_id_foreign` (`media_id`),
  ADD KEY `side_dishes_customer_id_foreign` (`customer_id`);

--
-- Indexes for table `tables`
--
ALTER TABLE `tables`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tables_customer_id_foreign` (`customer_id`);

--
-- Indexes for table `themes`
--
ALTER TABLE `themes`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about`
--
ALTER TABLE `about`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `arrangments`
--
ALTER TABLE `arrangments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `campaigns`
--
ALTER TABLE `campaigns`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT for table `cart_items`
--
ALTER TABLE `cart_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;
--
-- AUTO_INCREMENT for table `cart_item_options`
--
ALTER TABLE `cart_item_options`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cart_item_side_dishes`
--
ALTER TABLE `cart_item_side_dishes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `configrations`
--
ALTER TABLE `configrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=254;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `customer_users`
--
ALTER TABLE `customer_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `devices`
--
ALTER TABLE `devices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `feedback_interactions`
--
ALTER TABLE `feedback_interactions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;
--
-- AUTO_INCREMENT for table `feedback_questions`
--
ALTER TABLE `feedback_questions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `feedback_translations`
--
ALTER TABLE `feedback_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ingredients_warnings`
--
ALTER TABLE `ingredients_warnings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `item_ingredient_warnings`
--
ALTER TABLE `item_ingredient_warnings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `item_options`
--
ALTER TABLE `item_options`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `item_option_values`
--
ALTER TABLE `item_option_values`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `item_prices`
--
ALTER TABLE `item_prices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=220;
--
-- AUTO_INCREMENT for table `item_side_dishes`
--
ALTER TABLE `item_side_dishes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT for table `item_translations`
--
ALTER TABLE `item_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `item_views`
--
ALTER TABLE `item_views`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=222;
--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;
--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `menu_sections`
--
ALTER TABLE `menu_sections`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `menu_translations`
--
ALTER TABLE `menu_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;
--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `options`
--
ALTER TABLE `options`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `option_values`
--
ALTER TABLE `option_values`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `plans`
--
ALTER TABLE `plans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `question_translations`
--
ALTER TABLE `question_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `recommended_items`
--
ALTER TABLE `recommended_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=184;
--
-- AUTO_INCREMENT for table `side_dishes`
--
ALTER TABLE `side_dishes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tables`
--
ALTER TABLE `tables`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `themes`
--
ALTER TABLE `themes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `question_translations`
--
ALTER TABLE `question_translations`
  ADD CONSTRAINT `question_translations_question_id_foreign` FOREIGN KEY (`feedback_question_id`) REFERENCES `feedback_questions` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
