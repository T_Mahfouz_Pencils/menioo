<?php

namespace Tests\Unit;

use Tests\TestCase;

class DinerLoginTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */

    public function test_it_can_login()
    {
        $data = [
            'email' => 'mahmoud@lucilles.com',
            'password' => 'admin'
        ];

        $this->post(route('api.login'), $data)
            ->assertStatus(200);
    }

    public function test_validation_error()
    {
        $data = [
            'email' => '',
            'password' => ''
        ];

        $this->post(route('api.login'), $data)
            ->assertStatus(422);
    }
}
