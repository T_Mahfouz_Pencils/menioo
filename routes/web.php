<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
view()->share('vuePath',url('').'/../resources/js/');
view()->share('resource',url('').'/');


Route::get('getplans', function () {
    $curl = curl_init();

    curl_setopt_array($curl, array(
    CURLOPT_URL => "https://vendors.paddle.com/api/2.0/subscription/plans",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => "vendor_id=52571&vendor_auth_code=76a7b9870e96b92ebfb9796e3699342788aa125567b37bcddd",
    CURLOPT_HTTPHEADER => array(
        "content-type: application/x-www-form-urlencoded"
    ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
    echo "cURL Error #:" . $err;
    } else {
        $response = json_decode($response);
        foreach($response->response as $key => $value){
            if($key > 0 )
            \App\Http\Models\Subscription::create(
                [
                    'name' => $value->name,
                    'type' => 'Normal',
                    'billing_period' => $value->billing_period,
                    'plan_type' => $value->billing_type,
                    'plan_id' => $value->id,
                    'price' => $value->recurring_price->USD,
                    'tablets_number' => rand(3,20),
                    'features' => null,
                    'order' => $key + 1 ,
                    'order_option' => 0,
                ]
            );
        }
        return \App\Http\Models\Subscription::all();
    // return  [json_decode($response)];
    }
});

Route::get('/curlsend', function(){
    $names =['Basic', 'Growing Business', 'Expanding Chain', 'Basic', 'Growing Business', 'Expanding Chain'];
    $lengths =[1, 1, 1, 12, 12, 12];
    $pricing =[50, 90, 140, 480, 864, 1344];
    $result =[];
    foreach($names as $key => $value){
        $result[] = $response = \Ixudra\Curl\Facades\Curl::to('https://vendors.paddle.com/api/2.0/subscription/plans_create ')
        ->withData( [
            'vendor_id' => '52571',
            'vendor_auth_code' => 'eb01818eaf2a0504e42311ccec39fb3298fdaaa66e5f3ef308',
            'plan_name' => $value,
            'plan_trial_days' => 0,
            'plan_length' => $lengths[$key],
            'plan_type' => 'month',
            'main_currency_code' => 'USD',
            'recurring_price_usd' => $pricing[$key]
        ] )
        ->asJson( true )
        ->post();
    }
    return $result;

});
Route::get('/arrangeMenus', function(){
    $menus = \App\Http\Models\Menu::all();
    foreach ($menus as $key => $menu) {
        \App\Http\Models\Arrangment::create([
            'menu_id' => $menu->parent_id,
            'customer_user' => $menu->customer_id,
            'order' => $key,
            'arrangmentable_type' => 'App\Http\Models\Menu',
            'arrangmentable_id' => $menu->id
        ]);
    }
    $items = \App\Http\Models\Item::all();
    foreach ($items as $key => $item) {
        \App\Http\Models\Arrangment::create([
            'menu_id' => $item->menu_id,
            'customer_user' => $item->menu->customer_id,
            'order' => $key,
            'arrangmentable_type' => 'App\Http\Models\Item',
            'arrangmentable_id' => $item->id
        ]);
    }
    return \App\Http\Models\Arrangment::all();
});

Route::group(['prefix' => 'admin', 'middleware' => 'web', 'namespace' => 'Admin', 'as' => 'admin.'], function () {

    Route::get('login', ['as' => 'login', 'uses' => 'AdminLoginController@login']);
    Route::post('login', 'AdminLoginController@doLogin');


    Route::group(['middleware' => ['auth:admin']], function () {
        Route::resource('customer', 'CustomerController');
        Route::resource('subscription', 'SubscriptionController');
        Route::get('login/customer/{id}', ['as' => 'as.diner.login', 'uses' => 'AdminLoginController@loginAsDiner']);


        /*
         * Ajax Requests
         */
        Route::group(['prefix' => 'ajax'], function () {
            Route::post('validation', 'AjaxController@validation');
            Route::get('cities', 'AjaxController@cities');
            Route::post('active_deactivate', 'AjaxController@activeDeactivate');
        });

        Route::any('logout', 'AdminLoginController@logout');
    });
});

Route::group(['middleware' => 'web', 'namespace' => 'Diner', 'as' => 'diner.'], function () {
    Route::get('login', ['as' => 'login', 'uses' => 'DinerLoginController@login']);
    Route::post('login', 'DinerLoginController@doLogin');
    Route::group(['prefix' => 'diner', 'middleware' => ['auth:diner']], function () {

        #Route::get('preview/{path}', 'HomeController@index')->where('path','([A-z\d\-\/_.]+)?');
        Route::get('getvenueMenu/{id}','CoustomerUserController@getvenueMenu');
        Route::get('preview','DinerController@preview');
        Route::get('preview/menus','DinerController@preview');
        Route::get('preview/{menuid}/item/{itemid}','DinerController@preview');
        Route::get('preview/sub-menus/{menuids}','DinerController@preview');
        Route::get('menu/{menu_id}/duplicate', 'MenuController@duplicate');

        Route::get('preview/items','DinerController@preview');
        Route::get('preview/login','DinerController@preview');
        Route::get('preview/items/{item_id}','DinerController@preview');

        Route::resource('/', 'DinerController');

        Route::get('/arrange/{id}','ArrangmentController@arrange');

        Route::resource('menu', 'MenuController');
        Route::resource('menu/{parent_id}/sub-menu', 'SubMenuController');
        Route::resource('menu/{menu_id}/item', 'ItemController');
        Route::resource('devices', 'DeviceController');
        Route::get('devices/deleteall/{ids}', 'DeviceController@deleteall');

        Route::resource('order', 'OrderController');

        Route::resource('venue','VenueController');
        Route::get('scan-qr', ['as'=>'scan-qr','uses'=>'VenueController@generateQrCode']);

        Route::get('localization','LocalizationController@index');
        Route::get('/getObjectTranslation/{id}/{type}','LocalizationController@getTranslation');
        Route::get('/updatelanguage','LocalizationController@updatelanguage');
        Route::get('updatesurveylanguage', 'LocalizationController@updatesurveylanguage');
        Route::get('cofigrations', 'ConfigrationController@index')->name('cofigrations.index');
        Route::post('configraton', 'ConfigrationController@store');
        Route::post('venue/import',['as' => 'venue.import','uses'=>'VenueController@import']);
        Route::post('menu/move',['as' => 'menu.move','uses'=>'VenueController@move']);

        Route::resource('campaign', 'CampaignController');
        Route::resource('feedback', 'FeedbackController');
        Route::get('getsurvey/{id}', 'FeedbackController@getsurvey');
        Route::resource('interaction', 'FeedbackInteractionController');
        Route::resource('sidedishes', 'SideDishesController');
        Route::resource('options', 'OptionController');
        Route::resource('tables', 'TableController');
        Route::get('getoption/{id}', 'OptionController@getoption');
        Route::get('sidedishes/destroy/{id}', 'SideDishesController@destroy');
        Route::get('feedback/open-close/{id}', ['as'=>'feedback.open-close','uses'=>'FeedbackController@openClose']);
        Route::get('interaction/open-close/{id}', ['as'=>'interaction.open-close','uses'=>'FeedbackInteractionController@openClose']);
        Route::get('interaction/open-close/{ids}/{status}', ['as'=>'interaction.open-close-all','uses'=>'FeedbackInteractionController@openCloseAll']);

        Route::get('modifiers',['as' => 'modifiers','uses' => 'SideDishesController@modifiers']);
        Route::get('edit-modifier/{id}',['as' => 'modifier.edit','uses' => 'SideDishesController@editModifier']);
        Route::post('edit-modifier/{id}',['as' => 'modifier.update','uses' => 'SideDishesController@updateModifier']);
        Route::get('add-modifier',['as' => 'modifier.add','uses' => 'SideDishesController@addModifier']);
        Route::post('add-modifier',['as' => 'modifier.save','uses' => 'SideDishesController@saveModifier']);
        Route::get('delete-modifier/{id}',['as' => 'modifier.delete','uses' => 'SideDishesController@deleteModifier']);

        Route::get('export_survey',['as'=> 'export.survey','uses'=>'FeedbackInteractionController@export']);
        Route::resource('waiters', 'CoustomerUserController');
       
        /*
         * Ajax Requests
        */
        Route::group(['prefix' => 'ajax'], function () {
            Route::post('validation', 'AjaxController@validation');
            Route::get('cities', 'AjaxController@cities');
            Route::post('menu/hide_show', 'AjaxController@HideShowMenu');
            Route::post('item/hide_show', 'AjaxController@HideShowItem');
            Route::post('section/create', 'AjaxController@sectionCreate');
            Route::post('section/remove', 'AjaxController@sectionRemove');
            Route::post('media/upload', 'AjaxController@upload');
            Route::post('edit/customer/plan', ['as' => 'ajax.edit.customer.plan', 'uses' => 'AjaxController@editCustomerPlan']);
        });

        Route::any('logout', 'DinerLoginController@logout');
    });


});

Route::group(['middleware' => 'web', 'namespace' => 'Landing', 'as' => 'landing.'], function () {

    Route::get('verify-first',['as' => 'verify-first', 'uses' => 'LandingController@verifyFirst']);
    Route::get('verify',['as' => 'verify.view', 'uses' => 'LandingController@verify']);

    //Route::get('login', ['as' => 'login', 'uses' => 'DinerLoginController@login']);
    //Route::post('login', 'DinerLoginController@doLogin');

    Route::get('/auth/redirect/{provider}',['as' => 'redirect.facebook','uses' => 'LandingController@redirect'] );
    Route::get('/callback/{provider}', ['as' => 'callback.facebook','uses' => 'LandingController@callback']);

    Route::get('/',['as' => 'index', 'uses' => 'LandingController@index']);
    Route::get('login',['as' => 'login', 'uses' => 'LandingController@login']);
    Route::post('login',['as' => 'submit.login', 'uses' => 'LandingController@submitLogin']);

    Route::get('sign-up',['as' => 'sign-up', 'uses' => 'LandingController@signUp']);
    Route::post('sign-up',['as' => 'submit.signing', 'uses' => 'LandingController@submitSigning']);

    Route::get('features',['as' => 'features', 'uses' => 'LandingController@features']);

    Route::get('blog',['as' => 'blog', 'uses' => 'LandingController@blog']);
    Route::get('blog1',['as' => 'blog1', 'uses' => 'LandingController@blog1']);
    Route::get('blog2',['as' => 'blog2', 'uses' => 'LandingController@blog2']);
    Route::get('blog3',['as' => 'blog3', 'uses' => 'LandingController@blog3']);

    Route::get('pricing',['as' => 'pricing', 'uses' => 'LandingController@pricing']);
    Route::get('terms',['as' => 'terms', 'uses' => 'LandingController@terms']);
    Route::get('about',['as' => 'about', 'uses' => 'LandingController@about']);
    Route::get('privacy',['as' => 'privacy', 'uses' => 'LandingController@privacy']);

    Route::get('get-quote-form',['as' => 'get.quote', 'uses' => 'LandingController@getQuote']);
    Route::post('send-quote',['as' => 'send.quote', 'uses' => 'LandingController@sendQuote']);

    Route::get('forget-password',['as' => 'forget.password.view', 'uses' => 'LandingController@forgetPassword']);
    Route::post('forget-password-request',['as' => 'forget.password.request', 'uses' => 'LandingController@sendForgetPasswordRequest']);
    Route::get('reset-password/{token}',['as' => 'reset.password.view', 'uses' => 'LandingController@resetPasswordView']);
    Route::post('reset-password',['as' => 'reset.password', 'uses' => 'LandingController@resetPassword']);
});


