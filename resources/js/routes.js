import Home from './components/Home.vue';
import Menus from './components/Menus';
import SubMenus from './components/SubMenus.vue';
import Item from './components/Item.vue';

export const routes = [
    { path: '/:menuid/item/:itemid', component: Item, name: 'item', props: true },
    { path: '/sub-menus/:menuid', component: SubMenus, name: 'sub-menus', props: true },
    { path: '/menus', component: Menus, name: 'menus', props: true },
    { path: '/*', component: Home, name: 'home', props: true },
    // { path: '/items', component: Home, props: true },
    // { path: '/items/:item_id', component: Home, props: true },
];
