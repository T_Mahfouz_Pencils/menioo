@extends('admin.layouts.master')
@section('styling')
    <link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/css/select2.min.css')}}">
@stop
@section('content')

    <div class="table-component table-responsive">
        <table class="table table-hover table-borderless" style="margin-top: 50px;">
            <thead>
            <tr>
                <th>Order</th>
                <th>Plan Name</th>
                <th>Plan Type</th>
                <th>Price</th>
                <th>No. of Tablets</th>
                <th>Subscribers</th>
            </tr>
            </thead>
            <tbody>
            @if($plans)
                @foreach($plans as $plan)
                    <tr class="viewPaln" data-href="{{URL::to('admin/subscription/'.$plan->id)}}">
                        <td>{{$plan->order}}</td>
                        <td>{{$plan->name}}</td>
                        <td>{{$plan->type}}</td>
                        <td>{{$plan->price}}</td>
                        <td>{{$plan->tablets_number ? $plan->tablets_number : 'Get A Quote'}}</td>
                        <td>{{count($plan->subscribers)}}</td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>

        <nav>
            <div class="float-right pagination-filter">
                <div>
                    <label>Showing</label>
                    <select class="form-control">
                        <option>10 items per page</option>
                    </select>
                </div>
            </div>
            {{$plans->links()}}
            {{--<ul class="pagination justify-content-center">--}}
            {{--<li class="page-item disabled">--}}
            {{--<a class="page-link" href="#" tabindex="-1"><i--}}
            {{--class="fas fa-angle-left"></i></a>--}}
            {{--</li>--}}
            {{--<li class="page-item active"><a class="page-link" href="#">1</a></li>--}}
            {{--<li class="page-item"><a class="page-link" href="#">2</a></li>--}}
            {{--<li class="page-item"><a class="page-link" href="#">3</a></li>--}}
            {{--<li class="page-item">--}}
            {{--<a class="page-link" href="#"><i class="fas fa-angle-right"></i></a>--}}
            {{--</li>--}}
            {{--</ul>--}}

        </nav>
    </div>
@stop

@section('scripts')
    <script src="{{asset('assets/js/select2.full.min.js')}}"></script>

    <script>

        $('#keep-on .dropdown-menu').on({
            "click": function (e) {
                e.stopPropagation();
            }
        });

        $(".dismissDD").on('click', function () {
            $(".dropdown-toggle").dropdown("hide");
        });

        $(document).ready(function () {
            $("#country_id").trigger("change");
        });

        $('#country_id').change(function () {

            id = $(this).val()
            if (id) {
                $.ajax({
                    method: "GET",
                    url: "{{URL::to('admin/ajax/cities')}}",
                    data: {id: id}
                })
                    .done(function (data) {
                        $('city_id').append(data)
                    });
            }
        });

        $(".select2").select2();


        $(".viewPaln").click(function () {
            window.location = $(this).data("href");
        });




        $('#sort_btn').click(function () {
            var sort_by = $('.sorting-buttons.selected').attr('id');
            if (sort_by) {
                url = updateQueryStringParameter(window.location.href, 'sort_by', sort_by);
                window.location = url;
            }
        });



        $('.sorting-buttons').click(function () {
            $('.sorting-buttons').each(function () {
                $(this).removeClass('selected');
            });

            $(this).addClass('selected');
        })
    </script>


@stop
