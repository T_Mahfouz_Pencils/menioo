@extends('admin.layouts.master')
@section('styling')
    <link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/css/select2.min.css')}}">
@stop
@section('content')
    {{Form::open(array('url'=>array('admin/customer') ,'method'=>'POST','files'=>'true','enctype'=>'multipart/form-data'))}}

    <div class="row">
        <div class="col-6 form-group floatlabel mb-4">
            <label class="label" for="first-name">Full Name</label>
            <input type="text" data-validation="required" name="name" value="{{old('name')}}" id="first-name"
                   class="form-control"/>
        </div>
        <div class="col-6 form-group floatlabel mb-4">
            <label class="label" for="email">Email</label>
            <input type="email" data-validation="required email server"
                   data-validation-url="{{URL::to('admin/ajax/validation')}}"
                   data-validation-req-params='{"model":"customer","_token":"{{csrf_token()}}"}' name="email"
                   value="{{old('email')}}" id="email"
                   class="form-control"/>
        </div>
        <div class="col-6 form-group floatlabel mb-4">
            <label class="label" for="mobile_number">Phone Number</label>
            <input type="text" data-validation="required number server"
                   data-validation-url="{{URL::to('admin/ajax/validation')}}"
                   data-validation-req-params='{"model":"customer","_token":"{{csrf_token()}}"}' class="form-control"
                   id="mobile_number"
                   value="{{old('mobile_number')}}"
                   name="mobile_number"/>
        </div>
        <div class="col-6 form-group floatlabel mb-4">
            <label class="label" for="company-name">Company Name</label>
            <input type="text" name="company_name" value="{{old('company_name')}}" data-validation="required"
                   id="company-name" class="form-control"/>
        </div>
        <div class="col-6 form-group floatlabel mb-4">
            <label class="label" for="occupation">Occupation</label>
            <input type="text" class="form-control" data-validation="required" value="{{old('occupation')}}"
                   id="occupation"
                   name="occupation"/>
        </div>
        <div class="col-6 form-group mb-4">
            <select class="form-control no-background pl-0 select2" data-validation="required number" name="country_id"
                    id="country_id">
                <option disabled selected>Country</option>
                @if(isset($countries) && count($countries))
                    @foreach($countries as $country)
                        <option value="{{$country->id}}" {{old('country_id') == $country->id ? 'selected' : ''}}>{{$country->name}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="col-6 form-group mb-4">
            <select class="form-control no-background pl-0 select2" name="city_id"
                    id="city_id">
                <option disabled selected>City</option>
            </select>
        </div>
        <div class="col-6 form-group floatlabel mb-4">
            <label class="label" for="state">State</label>
            <input type="text" name="state" value="{{old('state')}}" data-validation="required"
                   id="state" class="form-control"/>
        </div>
    </div>
    <div class="form-group text-center mt-5">
        <button type="submit" class="dark-btn">ADD CUSTOMER</button>
    </div>
    {{Form::close()}}
@stop

@section('scripts')
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
    <script src="{{asset('assets/js/select2.full.min.js')}}"></script>
    <script>

        $.validate({modules: 'security'});

        $("#country_id").trigger("change");

        $('#country_id').change(function () {
            id = $(this).val()
            if (id) {
                $.ajax({
                    method: "GET",
                    url: "{{URL::to('admin/ajax/cities')}}",
                    data: {id: id}
                })
                    .done(function (data) {
                        $('#city_id').append(data)
                        console.log(data)
                    });
            }
        })

        $(".select2").select2();

    </script>


@stop
