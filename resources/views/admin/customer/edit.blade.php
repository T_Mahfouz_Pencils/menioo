@extends('admin.layouts.master')
@section('styling')
    <link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/css/select2.min.css')}}">
@stop
@section('content')
    {{Form::open(array('url'=>array('admin/customer/'.$current_customer->id) ,'method'=>'PUT'))}}

    <div class="row">
        <div class="col-6 form-group floatlabel mb-4">
            <label class="label" for="first-name">Full Name</label>
            <input type="text" data-validation="required" name="name" value="{{old('name',$current_customer->name)}}"
                   id="first-name"
                   class="form-control"/>
        </div>
        <div class="col-6 form-group floatlabel mb-4">
            <label class="label" for="email">Email</label>
            <input type="email" data-validation="required email server"
                   data-validation-url="{{URL::to('admin/ajax/validation')}}"
                   data-validation-req-params='{"model":"customer","_token":"{{csrf_token()}}","id":"{{$current_customer->id}}"}'
                   name="email"
                   value="{{old('email',$current_customer->email)}}" id="email"
                   class="form-control"/>
        </div>
        <div class="col-6 form-group floatlabel mb-4">
            <label class="label" for="mobile_number">Phone Number</label>
            <input type="text" data-validation="required number server" class="form-control" id="mobile_number"
                   data-validation-url="{{URL::to('admin/ajax/validation')}}"
                   data-validation-req-params='{"model":"customer","_token":"{{csrf_token()}}","id":"{{$current_customer->id}}"}'
                   value="{{old('mobile_number',$current_customer->mobile_number)}}"
                   name="mobile_number"/>
        </div>
        <div class="col-6 form-group floatlabel mb-4">
            <label class="label" for="company-name">Company Name</label>
            <input type="text" name="company_name" value="{{old('company_name',$current_customer->company_name)}}"
                   data-validation="required"
                   id="company-name" class="form-control"/>
        </div>
        <div class="col-6 form-group floatlabel mb-4">
            <label class="label" for="occupation">Occupation</label>
            <input type="text" class="form-control" data-validation="required"
                   value="{{old('occupation',$current_customer->occupation)}}"
                   id="occupation"
                   name="occupation"/>
        </div>
        <div class="col-6 form-group mb-4">
            <select class="form-control no-background pl-0 select2" data-validation="required number" name="country_id"
                    id="country_id">
                <option disabled selected>Country</option>
                @if(isset($countries) && count($countries))
                    @foreach($countries as $country)
                        <option value="{{$country->id}}" {{old('country_id',$current_customer->country_id) == $country->id ? 'selected' : ''}}>{{$country->name}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="col-6 form-group mb-4">
            <select class="form-control no-background pl-0 select2" name="city_id"
                    id="city_id">
                <option disabled selected>City</option>
            </select>
        </div>
        <div class="col-6 form-group floatlabel mb-4">
            <label class="label" for="state">State</label>
            <input type="text" name="state" value="{{old('state',$current_customer->state)}}" data-validation="required"
                   id="state" class="form-control"/>
        </div>
    </div>
    <div class="form-group text-center mt-5">
        <button type="submit" class="dark-btn">SAVE CUSTOMER</button>
        <button type="button" data-toggle="modal" data-target="#deactivateAlert" id="active_deactivate_modal_btn"
                class="plain-btn text-muted"><i
                    class="far fa-eye-slash"></i> {{$current_customer->is_active == 1 ? 'DEACTIVATE' : 'ACTIVATE'}}
        </button>
        <button type="button" data-toggle="modal" data-target="#delAlert"
                class="plain-btn text-danger"><i class="far fa-trash-alt"></i> DELETE
        </button>
    </div>
    {{Form::close()}}
@stop

@section('scripts')
    <!-- Deactivate Alert -->
    <div class="modal fade" id="deactivateAlert" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content text-center">
                <div class="modal-body">
                    <h5 class="text-center">ARE YOU SURE?</h5>
                    <p>Are you sure you want to deactivate {{$current_customer->name}}?</p>

                </div>
                <div class="modal-footer">
                    <button type="button" class="plain-btn" data-dismiss="modal">CANCEL</button>
                    <button type="button" id="active_deactivate_btn"
                            class="dark-btn">{{$current_customer->is_active == 1 ? 'DEACTIVATE' : 'ACTIVATE'}}
                        CUSTOMER
                    </button>
                </div>
            </div>
        </div>
    </div>

    <!-- Delete Alert -->
    <div class="modal fade" id="delAlert" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            {{Form::open(['route'=>['admin.customer.destroy' , $current_customer->id ] , 'method'=>'delete'])}}

            <div class="modal-content text-center">
                <div class="modal-body">
                    <h5 class="text-center">ARE YOU SURE?</h5>
                    <p class="m-0">Are you sure you want to delete {{$current_customer->name}}?</p>
                    <small class="text-danger">This cannot be undone</small>
                </div>
                <div class="modal-footer">
                    <button type="button" class="plain-btn" data-dismiss="modal">CANCEL</button>
                    <button type="submit" class="dark-btn">DELETE CUSTOMER</button>
                </div>
            </div>

            {{Form::close()}}
        </div>
    </div>

    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
    <script src="{{asset('assets/js/select2.full.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            $("#country_id").trigger("change");
        });

        $.validate({modules: 'security'})

        $('#country_id').change(function () {

            id = $(this).val()
            if (id) {
                $.ajax({
                    method: "GET",
                    url: "{{URL::to('admin/ajax/cities')}}",
                    data: {id: id}
                })
                    .done(function (data) {
                        $('city_id').append(data)
                    });
            }
        });

        $('#active_deactivate_btn').click(function () {
            $.ajax({
                method: "POST",
                url: "{{URL::to('admin/ajax/active_deactivate')}}",
                data: {
                    _token: "{{csrf_token()}}",
                    id: "{{$current_customer->id}}"
                }
            }).done(function (data) {

                var data = jQuery.parseJSON(data);
                if (data.status == 2) {

                    html = '<div class="alert alert-danger text-center"> +data.error+</div>';
                    $('#active_deactivate_btn .modal-body').append(html);

                } else if (data.status == 1) {

                    html = data.data.is_active == 1 ? 'DEACTIVATE' : 'ACTIVATE';
                    $('#active_deactivate_modal_btn').html('<i class="far fa-eye-slash"></i>' + html);
                    $('#active_deactivate_btn').html(html + ' CUSTOMER');
                    $('#active_deactivate_btn .modal-body .alert-danger').remove();
                    $('#deactivateAlert').modal('hide');

                }
            });
        });

        $(".select2").select2();


    </script>


@stop