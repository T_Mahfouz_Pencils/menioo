<header>
    <div class="site-name">
        <a class="menu-toggle show" href="#"><i class="fas fa-bars"></i></a>
        <a href="index.html">{{config('app.name')}}</a>
    </div>

    <ul class="nav float-right">
        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Welcome, {{$current_user->name}}
                <b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li><a href="/user/preferences"><i class="icon-cog"></i> Preferences</a></li>
                <li><a href="/help/support"><i class="icon-envelope"></i> Contact Support</a></li>
                <li class="divider"></li>
                <li><a href="{{URL::to('admin/logout')}}"><i class="icon-off"></i> Logout</a></li>
            </ul>
        </li>
    </ul>
</header>