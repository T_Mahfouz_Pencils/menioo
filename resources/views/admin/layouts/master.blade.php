<!DOCTYPE html>
<html>
@include('admin.layouts.head')
@yield('styling')
<body>
<div class="container-fluid p-0">
    @include('admin.layouts.header')

    <div class="body">
        <!-- Side Menu -->

    @include('admin.layouts.left-nav')
    <!-- Content -->
        <div class="content">
            @include('admin.layouts.breadcrumb')
            <div class="inner-content mt-5">
                @include('admin.layouts.error')
                @yield('content')
            </div>
        </div>

    </div>
    <!-- Calendly inline widget begin -->
    {{-- <div class="calendly-inline-widget" data-url="https://calendly.com/mohamedelserag4488/30min" style="min-width:320px;height:630px;"></div>
    <script type="text/javascript" src="https://assets.calendly.com/assets/external/widget.js"></script> --}}
<!-- Calendly inline widget end -->
    {{-- <script type="text/javascript">window.$crisp=[];window.CRISP_WEBSITE_ID="c7a07e45-ec24-46e4-9262-299be561f291";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.chat/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();</script> --}}

</div>
@include('admin.layouts.scripts')
@yield('scripts')
</body>
</html>