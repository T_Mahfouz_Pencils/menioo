@extends('diner.layouts.master')

@section('content')


    <div class="container">
        {{Form::open(array('url'=>array('diner/menu/'.$parent_id.'/sub-menu'.'?venue_id='.$venue_id) ,'method'=>'POST','files'=>'true','enctype'=>'multipart/form-data'))}}
            <div class="row row-title">
                
                <div class="col-md-12 text-left">
                    <i class="fas fa-chevron-left"></i>&nbsp; Menus 
                </div>
                <div class="col-md-6 text-left"><h6>Create New SubMenu</h6></div>
                <div class="col-md-6 text-right">
                    {{-- <i class="fa fa-search"></i>  --}}
                    {{-- @if(count($mainMenus) || $user_branch->parent_id != 0)
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"><a href="#">Import Menu</a></button>
                    @endif --}}
                    <button class="btn btn-primary" type="submit">Save Menu</button>
                </div>
            </div>
            <div class="row menus-fBox">
                <div class="col-md-7">
                    <div class="create-inp">
                        <label for="">Menu Name</label>
                        <input type="text" name="name" value="{{old('name')}}" data-validation="required server"
                        data-validation-url="{{URL::to('diner/ajax/validation')}}"
                        data-validation-req-params='{"model":"menu","_token":"{{csrf_token()}}","parent_id":"0"}'
                        id="menu-name" placeholder="Add Menu title" class="form-control">
                    </div>
                    <div class="create-inp">
                        <label for="">Description</label>
                        <textarea name="description" placeholder="Type a menu description" class="form-control"></textarea>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="create-inp">
                                <label for="">Display as</label>
                                <br>
                                <select name="display_as" id="display_as">
                                    <option value="" class="form-control" disabled>select option</option>
                                    <option value="grid" {{old('display_as')=='grid' ? 'selected':''}}>Grid</option>
                                    <option value="list" {{old('display_as')=='list' ? 'selected':''}}>List</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6" id="num_of_rows">
                            <div class="create-inp">
                                <label for="">Number of Colums</label>
                                <br>
                                <select name="num_of_rows" >
                                    <option value="" disabled selected>Number of Columns</option>
                                    <option value="2" {{old('num_of_rows')=='2' ? 'selected':''}}>2</option>
                                    <option value="4" {{old('num_of_rows')=='4' ? 'selected':''}}>4</option>
                                    <option value="6" {{old('num_of_rows')=='6' ? 'selected':''}}>6</option>
                                    <option value="8" {{old('num_of_rows')=='8' ? 'selected':''}}>8</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="create-inp">
                        <label for="">Crid View Title Position</label>
                        <br>
                        <select name="grid_title_position" id="grid_title_position">
                            <option value="" disabled selected>Grid View Title Position</option>
                            <option value="top" {{old('grid_title_position')=='top' ? 'selected':''}}>Top</option>
                            <option value="bottom" {{old('grid_title_position')=='bottom' ? 'selected':''}}>Bottom</option>
                        </select>
                    </div>
                    <br>
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-9 col-10">
                                <h6>Display Similar Items</h6>
                                <div style="margin-top: -10px;">
                                    <small>Display other Items of the Category on each item Page</small>
                                </div>
                            </div>
                            <div class="col-md-3 col-2 text-right">
                                <input type="checkbox" name="display_similar_items"
                                {{old('display_similar_items') == 1? 'checked':''}} value="1" class="checkboxx">
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-5 col-6">
                        <div class="row">
                            <div class="col-md-9 col-11">
                                <h6>Mark Menu as New</h6>
                            </div>
                            <div class="col-md-3 col-1"><input name="mark_as_new" value="1"
                                id="mark_as_new" {{old('mark_as_new') == 1? 'checked':''}} type="checkbox" class="checkboxx"></div>
                        </div>
                    </div>
                    <div class="col-md-7 col-6 text-right">
                        <div class="row">
                            <div class="col-md-8 col-11">
                                <h6>Mark Menus as signature</h6>
                            </div>
                            <div class="col-md-4 col-1"><input name="mark_as_signature" value="1"
                                id="mark_as_signature" {{old('mark_as_signature') == 1? 'checked':''}} type="checkbox" class="checkboxx"></div>
                        </div>
                    </div>
                </div>
                <div class="create-inp">
                        <label for="">Add additional Notes</label>
                        <textarea name="additional_notes" placeholder="Type any Notes" class="form-control"></textarea>
                    </div>
                </div>
                
                <div class="col-md-5">
                    <div class="upload" id="imageDiv" upload-image="" style="box-shadow: 0px 0px 6px 0px rgba(0,0,0,.2); border-radius: 10px;">
                        <input type="file" id="uploadImage" name="file" class="input-file ng-pristine ng-valid ng-touched" files-model="" ng-model="project.fileList">
                        <label for="files" style="border: none">
                            <span class="add-image" style="color: #47e6fc">
                                <i class="far fa-image"></i> Upload Picture
                            </span>
                            <output id="list"></output>
                        </label>
                    </div>
                    <br>
                    <div class="upload" upload-image="" style="background: transparent">
                        <input type="file" id="files" name="video" class="input-file ng-pristine ng-valid ng-touched" files-model="" ng-model="project.fileList">
                        <label for="files" style="border: none">
                            <span class="add-image" style="color: #47e6fc">
                                <i class="far fa-file-video"></i> Add a Video
                            </span>
                            <output id="list"></output>
                        </label>
                    </div>
                </div>
                
            </div>  
            <br><br><br>
        {{ Form::close() }}
        <!--    MODEL  -->
        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <br><br><br><br>
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header import-head-modal">
                    <h4><b>Import Menus</b></h4>
                    </div>
                    {{ Form::open(array('url'=>route('diner.venue.import',['venue_id'=>$venue_id]) ,'method'=>'POST','files'=>'true','enctype'=>'multipart/form-data'))}}
                        <div class="modal-body create-menu-import">
                            <div class="create-inp">
                                <label for=""><b>Select a Venue</b></label>
                                <br>
                                <select name="customer_id" id="">
                                    <option value="" class="form-control">Classic's Utah</option>
                                    <option value="">1</option>
                                    <option value="">2</option>
                                </select>
                            </div>
                            <div class="create-inp">
                                <label for=""><b>Select a Menu</b></label>
                                <br>
                                <select name="menu_id" id="">
                                    <option value="" class="form-control">Utah's Menu</option>
                                    <option value="">1</option>
                                    <option value="">2</option>
                                </select>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-dafult" data-dismiss="modal" style="color: #ccc; font-weight: 500; letter-spacing: 1px">Cancel</button>
                            <button type="button" class="btn btn-primary" style="padding: 10px 30px"><a href="importmenu.html">Import</a></button>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>  
    </div>

    
@stop

@section('scripts')


    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>

    <script>
        $(document).ready(function () {
            $("#display_as").trigger("change");
        });

        $.validate({modules: 'security'});
        $('#display_as').change(function () {
            val = $(this).val();
            if (val == 'grid') {
                $('#num_of_rows').attr('data-validation', 'required');
                $('#grid_title_position').attr('data-validation', 'required');
                $('#num_of_rows').removeClass('d-none');
                $('#grid_title_position').removeClass('d-none');
            } else {
                $('#num_of_rows').val('');
                $('#grid_title_position').val('');
                $('#num_of_rows').addClass('d-none');
                $('#grid_title_position').addClass('d-none');
                $('#num_of_rows').removeAttr('data-validation');
                $('#grid_title_position').removeAttr('data-validation');
            }
        })

        $("#uploadImage").on("change", function(){
            // Get a reference to the fileList
            var files = !!this.files ? this.files : [];
            // If no files were selected, or no FileReader support, return
            if ( !files.length || !window.FileReader ) return;
            // Only proceed if the selected file is an image
            if ( /^image/.test( files[0].type ) ) {
                // Create a new instance of the FileReader
                var reader = new FileReader();
                // Read the local file as a DataURL
                reader.readAsDataURL( files[0] );

                // When loaded, set image data as background of page
                reader.onloadend = function(){
                    $("#imageDiv").css("background-image", 'url(' + this.result + ')');
                }
            }
        });

    </script>


@stop
