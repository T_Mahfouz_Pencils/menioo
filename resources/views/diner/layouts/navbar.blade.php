<nav class="navbar navbar-expand-md fixed-top">
    <a class="navbar-brand" href="index.html"><img src="{{ asset('imgs/menologo.png') }}" width="100"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"><i class="fa fa-bars"></i></span>
    </button>
  
    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
       <div class="dropdown create">
            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background-color: transparent">
              CLASSIC'S OHIO
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="#"><i class="fas fa-file-alt"></i> Add Page </a>
              <a class="dropdown-item" href="#"><i class="fas fa-pencil-alt"></i> Add Post</a>
              <a class="dropdown-item" href="#"><i class="fas fa-user"></i> Add User</a>
            </div>
        </div>
      <ul class="navbar-nav m-auto middle-nav">
            <li class="nav-item">
              <a class="nav-link" href="{{ $navigations['Waiters']->url }}">Waiters</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ $navigations['Tables']->url }}">Tables</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ $navigations['Devices']->url }}">Devices</a>
            </li>
             <li class="nav-item">
              <a class="nav-link" href="{{ $navigations['Modifiers']->url }}">Modifiers</a>
            </li>
      </ul>
       
       <ul class="navbar-nav ml-auto last-nav">
           <li class="nav-item">
              <a class="nav-link" href="#" data-toggle="modal" data-target="#parccode"><i class="fas fa-qrcode"></i> Scan QR Code</a>
            </li>
            
            <li class="nav-item">
              <a class="nav-link" href="#"><i class="fas fa-play"></i> Live Preview</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#"></a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <img src="{{ asset('imgs/avatar-icon-png-8.jpg') }}" width="20" height="20"> Welcome, {{ $current_user->name }}
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="{{ $navigations['App Configration']->url }}"><i class="fas fa-wrench"></i> App Config</a>
                <a class="dropdown-item" href="{{ $navigations['Localization']->url }}"><i class="fas fa-globe"></i> Localization</a>
                <a class="dropdown-item" href="#"><i class="fas fa-cog"></i> Account Setting</a>
                <a class="dropdown-item" href="#"><i class="fas fa-info-circle"></i> Support</a>
                <a class="dropdown-item" href="{{ URL::to('logout') }}"><i class="fas fa-sign-out-alt"></i> Logout</a>
              </div>
            </li>
      </ul>
    </div>
</nav> 