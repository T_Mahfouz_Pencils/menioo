<div class="modal fade" id="parccode" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <br><br><br><br>
    <div class="modal-dialog" role="document" style="min-width: 75%;">
        <div class="modal-content" style="height: 600px;">
            <div class="modal-header import-head-modal">
            <h4 style="padding-bottom: 0px !important;"><b>SCAN QR CODE</b></h4>
            </div>
            <div class="modal-body create-menu-import" style="height: 100%;">
                <iframe style="height: 100%;" class="col-md-12" src="{{ route('diner.scan-qr') }}" frameborder="0"></iframe>
                {{--<div class="text-center">--}}
                    {{--<img src="{{ asset('imgs/ParCode.png') }}" width="40%">--}}
                {{--</div><br>--}}
                {{--<div class="create-inp" style="width: 250px; margin: auto;">--}}
                {{--<label for="">Email Address</label>--}}
                {{--<b style="padding-left: 5px;">info@flavoursco.com</b>--}}
                {{--<br><br>--}}
                {{--<label for="">Branch ID</label><br>--}}
                {{--<b style="padding-left: 5px;">12537</b>--}}
                {{--</div>--}}
            </div><br>
        </div>
    </div>
</div>