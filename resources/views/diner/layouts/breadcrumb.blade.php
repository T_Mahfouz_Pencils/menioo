<div class="page-header">

    @if(isset($title->back))
        <a href="{{$title->back->url}}" class="text-muted float-left">
            <small><i class="fas fa-long-arrow-alt-left"></i>{{$title->back->name}}</small>
        </a>
        <div class="clearfix"></div>
    @endif
    <h1 class="float-left">{{$title->name}}</h1>

    @if(isset($title->child))
        <button onclick="window.open('{{$title->child->url}}','_self')"
                class="dark-btn float-right">{{$title->child->name}}</button>
    @endif
    @if(isset($title->extra) && isset($title->extra->subtitle))
        <button style="margin-right: 5px;" onclick="window.open('{{$title->extra->url}}','_self')"
                class="dark-btn float-right">{{$title->extra->name}}</button>
    @endif
    @if(isset($title->additional))
        <button class="{{$title->additional->class}} float-right mr-3">{{$title->additional->name}}</button>

    @endif
    @if(isset($title->child) && isset($title->child->subtitle))
        <div class="clearfix"></div>
        <small class="text-muted">{{$title->child->subtitle}}</small>
    @elseif(isset($title->url))
        <button onclick="window.open('{{$title->url}}','_self')" class="plain-btn float-right mr-3">Cancel</button>

    @endif
    <div class="clearfix"></div>

</div>
