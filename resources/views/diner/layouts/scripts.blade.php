<script src="{{asset('assets/js/jquery-3.3.1.min.js')}}"></script>
{{-- <script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script> --}}
<script src="{{ asset('js/popper.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>

{{-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> --}}
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script> --}}
<script src="{{asset('assets/js/float-label.js')}}"></script>
<script src="{{asset('assets/js/side-nav.js')}}"></script>
<script src="{{asset('assets/js/main.js')}}"></script>
<script>
    showMessages();

    function editVenue(id,current) {
        if(current != 0)
          window.open('{{URL::to('diner/venue')}}/' + id + '/edit?venue_id='+current, '_self')
        else
          window.open('{{URL::to('diner/venue')}}/' + id + '/edit', '_self')
    }
</script>

@if(Auth::check())
<script>
  window.intercomSettings = {
    app_id: "j13srl2r",
    name: "<?php echo $current_user->name ?>", // Full name
    email: "<?php echo $current_user->email ?>", // Email address
    created_at: "<?php echo strtotime($current_user->created_at) ?>", // Signup date as a Unix timestamp
    "company": "<?php echo $current_user->company_name ?? '' ?>", // company_name
    "phone": "<?php echo $current_user->mobile_number ?? '' ?>", // mobile_number
    "country": "<?php echo $current_user->Country->name ?? '' ?>", // Country
    "city": "<?php echo $current_user->City->name ?? '' ?>", // city
    "state": "<?php echo $current_user->state ?? '' ?>", // state
    "subscription": "<?php echo $current_user->plan->name ?? '' ?>", // Plan
    "subscription_price": "<?php echo $current_user->plan->price ?? '' ?>", // Signup date as a Unix timestamp
    "subscription_at": "<?php echo strtotime($current_user->created_at) ?? '' ?>", // Signup date as a Unix timestamp
    "subscription_ended_at": "<?php echo strtotime($current_user->created_at) ?? '' ?>", // Signup date as a Unix timestamp
    "tablet_number": "<?php echo $current_user->plan->tablets_number ?? '' ?>", // tablets_number
    "menus": "<?php echo count($current_user->Menus)?? 0 ?>", // Menus Count
    "branches": "<?php echo count($current_user->venues) ?? 0 ?>" // venues Count
  };
</script>
@endif


{{-- Menu Scripts --}}
<script>
  $('.active_deactivate_btn').change(function () {
    var menuId = $(this).attr('data-id');
            $.ajax({
                method: "POST",
                url: "{{URL::to('diner/ajax/menu/hide_show')}}",
                data: {
                    _token: "{{csrf_token()}}",
                    id: menuId,
                }
            }).done(function (data) {
              // location.reload();
                
            });
    });
  $('.movebtnclass').click(function(e){
    $('#menu_id_input').val($(this).attr('data-id'));
    $('#type_input').val($(this).attr('data-type'));
  })

</script>

<script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',w.intercomSettings);}else{var d=document;var i=function(){i.c(arguments);};i.q=[];i.c=function(args){i.q.push(args);};w.Intercom=i;var l=function(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/j13srl2r';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);};if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>
