<div class="fixed-menu">
    <i class="fas fa-angle-left"></i>
    <ul>
        <li class="{{ Request::is('*diner?venue_id*') || Request::is('*diner') ? 'liactive' :'' }}"><a href="{{ $navigations['Dashbord']->url }}"><img src="{{ asset('imgs/dashicon.png') }}" width="30" height="30" alt=""> Dashboard</a></li>
        <li class="{{ Request::is('*menu*')? 'liactive' :'' }}"><a href="{{ $navigations['Menus']->url }}"><img src="{{ asset('imgs/menuicon.png') }}" width="30" height="30"> Menus</a></li>
        <li><a href="#"><img src="{{ asset('imgs/designicon.png') }}" width="30" height="30"> Design</a></li>
        <li class="{{ Request::is('*order*')? 'liactive' :'' }}"><a href="{{ $navigations['Orders']->url }}"><img src="{{ asset('imgs/ordericon.png') }}" width="30" height="30"> Orders</a></li>
        <li class="{{ Request::is('*feedback*')? 'liactive' :'' }}"><a href="{{ $navigations['Survey Forms']->url }}"><img src="{{ asset('imgs/feedbackicon.png') }}" width="30" height="30"> Feedback</a></li>
        <li class="{{ Request::is('*campaign*')? 'liactive' :'' }}"><a href="{{ $navigations['Campaigns']->url }}"><img src="{{ asset('imgs/campaignicon.png') }}" width="30" height="30"> Campaigns</a></li>
    </ul>
</div>