<!DOCTYPE html>
<html>
@include('diner.layouts.head')
@yield('styling')
<body>
    @include('diner.layouts.navbar')

    @include('diner.layouts.left-nav')
    <br><br><br>
    <div class="content">

        @yield('content')
        
    </div>
    @include('diner.layouts.scripts')

    @include('diner.layouts.qrmodal')

@yield('scripts')
</body>
</html>
