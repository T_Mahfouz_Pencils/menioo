@extends('diner.layouts.master')
@section('styling')
    <link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/css/select2.min.css')}}">
@stop
@section('content')

    {{Form::open(array('method'=>'GET'))}}
    <input type="hidden" name="venue_id" value="{{request()->venue_id}}">
    <div class="form-group col-md-8">
        <div class="row">
            <div class="col-3">
                <label>From</label>
                <input type="date" placeholder="From"
                       value="{{\Request::Input('interaction_from') ? date('Y-m-d',strtotime(\Request::Input('interaction_from'))) :'' }}"
                       id="interaction_from" class="form-control" name="interaction_from">
            </div>
            <div class="col-3">
                <label>To</label>
                <input type="date" placeholder="To"
                       value="{{\Request::Input('interaction_to') ? date('Y-m-d',strtotime(\Request::Input('interaction_to'))) :'' }}"
                       id="interaction_to" class="form-control" name="interaction_to">
            </div>
            <div class="col-2">
                <label>Name</label>
                <input type="text" placeholder="Name"
                        value="{{ \Request::Input('interaction_name') ? \Request::Input('interaction_name') :'' }}"
                        id="interaction_name" class="form-control" name="interaction_name">
            </div>
            <div class="col-2">
                <label>Status</label>
                <select name="interaction_status" class="form-control">
                    <option disabled>Status </option>
                    <option value="0" {{ Request::Input('interaction_status') == 0 ? 'selected' : '' }}>Close</option>
                    <option value="1" {{ Request::Input('interaction_status') == 1 ? 'selected' : '' }}>Open</option>
                </select>
            </div>

            <div class="form-group col-md-2">
                <button style="margin-top: 30px;" type="submit" class="btn btn-danger">Filter</button>
            </div>

            <div class="col-2">
                <a style="margin-top: 30px;" href="{{route('diner.export.survey').'?venue_id='.request()->venue_id}}" class="btn btn-success">Export</a>
            </div>

        </div>
    </div>

    {{Form::close()}}
    <div>
        
        <div class="custom-control custom-checkbox">
            <div class="row" style="width:50%;">
                
                <div class="col sm-4">
                    <input type="checkbox" class="custom-control-input" id="selectallbtn">
                    <label class="custom-control-label" for="selectallbtn">Select All</label>
                </div>
                <div class="col sm-2">
                    <button class="btn btn-danger text-white statusbtn" style="display:none;" data-status="0">Close</button>
                </div>
                <div class="col sm-2">
                    <button class="btn btn-warning text-white statusbtn" style="display:none;" data-status="1">Open</button>
                </div>
                    
            </div>
            
        </div>
    </div>
    <div class="table-component table-responsive">
        <table class="table table-hover table-borderless" style="margin-top: 50px;">
            <thead>
            <tr>
                <th>..</th>
                <th>Date</th>
                <th>Status</th>
                <th>Form Name</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @if(count($interactions) && count($interactions[0]->interactions))
                @foreach($interactions as $interaction)
                
                    <tr class="viewPaln">
                        <td>
                            <div class="custom-control custom-checkbox">
                                <input value="{{ $interaction->identifier }}" name="interactions[]" type="checkbox" class="custom-control-input checkboxinput" id="checkbox_{{ $interaction->identifier }}">
                                <label class="custom-control-label" for="checkbox_{{ $interaction->identifier }}">Select</label>
                            </div>
                        </td>
                        
                        <td>{{$interaction->date.' '.$interaction->time}}</td>
                        <td>
                            <span class="badge badge-{{$interaction->status ? 'primary' : 'danger'}}">{{$interaction->status ? 'OPEN' : 'CLOSE'}}</span>

                        </td>
                        <td>{{$interaction->form_name}}</td>

                        <td>
                            <button class="btn btn-danger" data-toggle="modal" data-target="#viewModal_{{$interaction->id}}">View</button>
                            <a class="btn btn-warning text-white" href="{{route('diner.interaction.open-close',$interaction->identifier)}}">{{$interaction->status ? 'Close' : 'Open'}}</a>
                        </td>
                    </tr>

                    <!-- VIEW MODAL -->
                    <div class="modal fade" id="viewModal_{{$interaction->id}}" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog" role="document" style="min-width: 60%;">
                            <div class="modal-content text-center">
                                <div class="modal-body">
                                    <div class="float-left">
                                        <p style="float: left;"><b>Feedback {{$interaction->identifier}}</b></p>
                                        <div class="clearfix"></div>
                                        <p style="float: left;"><b>Date:</b> {{$interaction->date.', '.$interaction->time}}</p>
                                        <div class="clearfix"></div>
                                        <p style="float: left;"><b>Form:</b> {{$interaction->form_name}}</p>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <p style="float: left;margin-top: 50px;margin-left: 10px;"><b>Answers</b></p>
                                    <div class="clearfix"></div>
                                    <hr>
                                    <div class="col-md-12">
                                        @if(count($interaction->interactions))
                                            @foreach($interaction->interactions as $qa)
                                                <p style="float: left;"><b>{{$qa->question ? $qa->question->value : ''}}</b></p>
                                                @if($qa->question &&  $qa->question->type == 'rating')
                                                    <p style="float: right;">
                                                    @for($i = 0; $i <= (int)$qa->answer-1; $i++)
                                                        <i class="fas fa-star px-1"></i>
                                                    @endfor
                                                    @for($i = 0; $i <= 4-(int)$qa->answer; $i++)
                                                        <i class="far fa-star px-1"></i>
                                                    @endfor
                                                    </p>
                                                @else
                                                    <p style="float: right;">{{$qa->answer}}</p>
                                                @endif
                                                <div class="clearfix"></div>

                                            @endforeach
                                        @endif

                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="plain-btn" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- OPEN CLOSE Alert -->
                    <div class="modal fade" id="delAlert_{{$interaction->id}}" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            {{Form::open(['route'=>['diner.interaction.update' , $interaction->id ] , 'method'=>'PUT'])}}

                            <div class="modal-content text-center">
                                <div class="modal-body">
                                    <h5 class="text-center">ARE YOU SURE?</h5>
                                    <p class="m-0">Are you sure you want to delete {{$interaction->form_name}}?</p>
                                    <small class="text-danger">This cannot be undone</small>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="plain-btn" data-dismiss="modal">CANCEL</button>
                                    <button type="submit" class="dark-btn">DELETE FEEDBACK</button>
                                </div>
                            </div>

                            {{Form::close()}}
                        </div>
                    </div>
                @endforeach
            @endif
            </tbody>
        </table>

        <nav>
            {{--<div class="float-right pagination-filter">--}}
                {{--<div>--}}
                    {{--<label>Showing</label>--}}
                    {{--<select class="form-control">--}}
                        {{--<option>10 items per page</option>--}}
                    {{--</select>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{-- $interactions->links() --}}
            {{--<ul class="pagination justify-content-center">--}}
            {{--<li class="page-item disabled">--}}
            {{--<a class="page-link" href="#" tabindex="-1"><i--}}
            {{--class="fas fa-angle-left"></i></a>--}}
            {{--</li>--}}
            {{--<li class="page-item active"><a class="page-link" href="#">1</a></li>--}}
            {{--<li class="page-item"><a class="page-link" href="#">2</a></li>--}}
            {{--<li class="page-item"><a class="page-link" href="#">3</a></li>--}}
            {{--<li class="page-item">--}}
            {{--<a class="page-link" href="#"><i class="fas fa-angle-right"></i></a>--}}
            {{--</li>--}}
            {{--</ul>--}}

        </nav>
    </div>
@stop

@section('scripts')
    <script src="{{asset('assets/js/select2.full.min.js')}}"></script>

    <script>

        // $('#filter_btn').click(function () {
        //     var status = $('#status').is(":checked") ? 'open' : 'close';
        //     var interaction_from = $('#interaction_from').val()
        //     var interaction_to = $('#interaction_to').val();
        //
        //     url = updateQueryStringParameter(window.location.href, 'status', status);
        //     url = updateQueryStringParameter(url, 'interaction_from', interaction_from);
        //     url = updateQueryStringParameter(url, 'interaction_to', interaction_to);
        //     window.location = url;
        // });

        $('.edit-campaign').on('click',function(){
            id = $(this).attr('data-href');
            window.open('{{URL::to('diner/feedback')}}/' + id + '/edit?venue_id={{$venue_id}}' , '_self');
        });

        $('#keep-on .dropdown-menu').on({
            "click": function (e) {
                e.stopPropagation();
            }
        });

        $(".dismissDD").on('click', function () {
            $(".dropdown-toggle").dropdown("hide");
        });

        $(document).ready(function () {
            $("#country_id").trigger("change");
        });

        $('#country_id').change(function () {

            id = $(this).val()
            if (id) {
                $.ajax({
                    method: "GET",
                    url: "{{URL::to('admin/ajax/cities')}}",
                    data: {id: id}
                })
                    .done(function (data) {
                        $('city_id').append(data)
                    });
            }
        });

        $(".select2").select2();




        $('#sort_btn').click(function () {
            var sort_by = $('.sorting-buttons.selected').attr('id');
            if (sort_by) {
                url = updateQueryStringParameter(window.location.href, 'sort_by', sort_by);
                window.location = url;
            }
        });



        $('.sorting-buttons').click(function () {
            $('.sorting-buttons').each(function () {
                $(this).removeClass('selected');
            });

            $(this).addClass('selected');
        })
    </script>

    <script>
        $(document).on('click','#selectallbtn', function(event){
            if(this.checked){
                $('.checkboxinput').each(function(){
                    this.checked = true;
                });
                $('.statusbtn').css('display','block');
            }else{
                $('.checkboxinput').each(function(){
                    this.checked = false;
                });
                $('.statusbtn').css('display','none');

            }
        });

        $(document).on('click', '.statusbtn', function(){
            var status = $(this).attr('data-status');
            var interactions = [];
            $.each($(".checkboxinput:checkbox:checked"), function(){            
                interactions.push($(this).val());
            });
            $.get('/diner/interaction/open-close/'+ interactions +'/' + status ,function(data){
                if(data.success){
                    location.reload();
                }
                else{
                    alert(data.fail);

                }
            })
        });
    </script>


@stop
