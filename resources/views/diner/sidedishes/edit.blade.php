@extends('diner.layouts.master')

@section('content')

    {{ Form::open([ 'route' => ['diner.sidedishes.update',$sidedish->id], 'method' => 'put']) }}
    <div class="container">
        <div class="row row-title">
            <div class="col-md-12"><i class="fas fa-chevron-left"></i> &nbsp; Side Dishes</div>
            <div class="col-md-6 text-left"><h6>Edit Side Dish</h6></div>
            <div class="col-md-6 text-right">
                <button type="submit" class="btn btn-primary">SAVE CHANGES</button>
            </div>
        </div>
        <div class="row menus-fBox">
        </div>
        @include('diner.layouts.error')
        <div class="container">
            <div class="ano-sec">
                <div class="row ano-sec ano-sec-edi">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-1 col-2"><img src="{{ $sidedish->image_path }}" width="100%" style="padding-top: 30px;"></div>
                            <div class="col-md-5 col-10">
                                <div class="create-inp" style="padding: 10px;">
                                    <label for="">Side Dish Name</label>
                                    <input name="name" value="{{ old('name', $sidedish->name) }}" type="text" class="form-control" placeholder="Add a slider">
                                </div>
                            </div>
                            <div class="col-md-5 col-10">
                                <div class="create-inp" style="padding: 10px;">
                                    <label for="">Added Price</label>
                                    <input name="price" value="{{ old('price', $sidedish->price) }}" type="text" class="form-control" placeholder="10EGP">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    {{ Form::close() }}
@stop

