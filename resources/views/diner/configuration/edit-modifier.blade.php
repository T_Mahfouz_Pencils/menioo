@extends('diner.layouts.master')

@section('content')
    @include('diner.layouts.error')
    {{ Form::open([ 'route' => ['diner.modifier.update',$modifier->id], 'method' => 'post']) }}
    <div class="container">
        <div class="row row-title">
            <div class="col-md-6 text-left"><h6>Edit Modifier</h6></div>
            <div class="col-md-6 text-right">

                <button type="submit" class="btn btn-primary">SAVE MODIFIER</button>
            </div>
        </div>
        <div class="row menus-fBox">
            <div class="col-md-6">
                <div class="create-inp">
                    <label for="modifierType">Modifier Type</label>
                    <select onchange="toggleCheckBoxContainer()" name="type" id="modifierType" class="form-control">
                        <option value="slider">Slider</option>
                        <option {{ $modifier->type == 'checkbox' ? 'selected':'' }} value="checkbox">Checkbox</option>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="create-inp">
                    <label for="">Modifier Name</label>
                    <input name="name" type="text" class="form-control" placeholder="Sauces" value="{{ old('name',$modifier->name) }}">
                </div>
            </div>
        </div>
        <div id="checkboxesContainer" style=" {{ $modifier->type == 'slider' ? 'display: none;':'' }}" class="container">
            <div class="ano-sec">
                <div class="row ano-sec ano-sec-edi">
                    <div class="col-md-6"><h6><b>Checkboxes</b></h6></div>
                    <div class="col-md-6 text-right"><i onclick="addCheckBoxRow()" class="fas fa-plus" style="color: #00e0ff;cursor: pointer;"></i></div>
                    <div id="checkboxesItems" class="col-md-12">
                        @foreach($modifier->values as $index => $modifierValue)
                        <div id="itemRow{{$index}}" class="row">
                            <div class="col-md-5">
                                <div class="create-inp" style="padding: 10px;">
                                    <label for="">Checkbox Name</label>
                                    <input name="optionValues[{{$index}}][name]" type="text" class="form-control" placeholder="Butter Sauce" value="{{$modifierValue->value}}" >
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="create-inp" style="padding: 10px;">
                                    <label for="">Added Price</label>
                                    <input name="optionValues[{{$index}}][price]" type="text" class="form-control" placeholder="10EGP" value="{{$modifierValue->price}}" >
                                </div>
                            </div>
                            @if($index > 1)
                            <div class="col-md-2" style="padding-top: 40px;">
                                <span class="text-danger"><i onclick="deleteCheckBoxRow({{$index}})" class="far fa-trash-alt" style="font-size: 25px;"></i></span>
                            </div>
                            @endif
                        </div>
                        @endforeach
                        @if(!count($modifier->values))
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="create-inp" style="padding: 10px;">
                                            <label for="">Checkbox Name</label>
                                            <input name="optionValues[0][name]" type="text" class="form-control" placeholder="Butter Sauce">
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="create-inp" style="padding: 10px;">
                                            <label for="">Added Price</label>
                                            <input name="optionValues[0][price]" type="text" class="form-control" placeholder="10EGP">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="create-inp" style="padding: 10px;">
                                            <label for="">Checkbox Name</label>
                                            <input name="optionValues[1][name]" type="text" class="form-control" placeholder="Butter Sauce" >
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="create-inp" style="padding: 10px;">
                                            <label for="">Added Price</label>
                                            <input name="optionValues[1][price]" type="text" class="form-control" placeholder="10EGP">
                                        </div>
                                    </div>
                                </div>
                        @endif
                    </div>
                </div>

            </div>
        </div>
    </div>
    {{ Form::close() }}
@stop

@section('scripts')
    <script>
        /*$(document).ready(function(){
            toggleCheckBoxContainer();
        });*/
        var counter = {{count($modifier->values)}};
        function toggleCheckBoxContainer() {
            var type = $('#modifierType').val();
            switch(type) {
                case 'slider':  $('#checkboxesContainer').css('display','none');
                    break;
                case 'checkbox':  $('#checkboxesContainer').css('display','block');
                    break;
            }
        }
        function addCheckBoxRow() {
            counter++;
            var itemsContainer = $('#checkboxesItems');
            var item = `
                <div id="itemRow`+counter+`" class="row">
                    <div class="col-md-5">
                        <div class="create-inp" style="padding: 10px;">
                            <label for="">Name</label>
                            <input name="optionValues[`+counter+`][name]" type="text" class="form-control" placeholder="Butter Sauce">
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="create-inp" style="padding: 10px;">
                            <label for="">Added Price</label>
                            <input name="optionValues[`+counter+`][price]" type="text" class="form-control" placeholder="10EGP">
                        </div>
                    </div>
                    <div class="col-md-2" style="padding-top: 40px;">
                        <span class="text-danger"><i onclick="deleteCheckBoxRow(`+counter+`)" class="far fa-trash-alt" style="font-size: 25px;"></i></span>
                    </div>
                </div>
            `;
            itemsContainer.append(item);

        }
        function deleteCheckBoxRow(count) {
            $('#itemRow'+count).remove();
        }
    </script>
@endsection
