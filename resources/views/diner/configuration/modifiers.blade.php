@extends('diner.layouts.master')

@section('content')
    <div class="container">
        <div class="row row-title">
            <div class="col-md-6 text-left"><h6>Modifiers</h6></div>
            <div class="col-md-6 text-right">
                <button class="btn btn-primary"><a href="{{ route('diner.modifier.add') }}">ADD Modifiers</a></button>
            </div>
        </div>
        <div class="row menus-fBox">
            <div class="col-md-12">
                <div class="dyn-links modf-link">
                    <ul>
                        <li><a href="{{ route('diner.modifiers') }}">Modifiers</a></li>
                        <li class="live"><a href="{{ URL::to('diner/sidedishes') }}" class="live">Side Dishes</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row menus-fBox">
            <table class="table" style="box-shadow: 0px 1px 5px 1px rgba(0,0,0,.2)">
                <thead>
                <tr style="background: #fff">
                    {{--<th scope="col"><input type="checkbox" class="checkboxx"></th>--}}
                    <th scope="col">Name</th>
                    <th scope="col">Type</th>
                    <th scope="col">Modifiers</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                @foreach($modifiers as $modifier)
                <tr>
                    {{--<th scope="row"><input type="checkbox" class="checkboxx"></th>--}}
                    <td>{{ $modifier->name }}</td>
                    <td>{{ $modifier->type }}</td>
                    <td>{{ count($modifier->values) }}</td>
                    <td>
                        <button class="btn btn-warning">
                            <a href="{{ route('diner.modifier.edit',[$modifier->id]) }}">edit</a>
                        </button>&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="{{ route('diner.modifier.delete',[$modifier->id]) }}" class="text-danger">delete </a></td>
                </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
@stop