@extends('diner.layouts.master')
@section('styling')
    <link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/css/select2.min.css')}}">
@stop
@section('content')


    <div class="row">

        <div class="col-6 form-group floatlabel mb-4" style="margin-top: 50px;">
            <label class="label" for="order">PLAN ORDER</label>
            <input readonly data-validation="required" name="order" value="{{old('order',1)}}" id="order"
                   class="form-control"/>
        </div>

        <div class="col-6 form-group floatlabel mb-4" style="margin-top: 50px;">
            <label class="label" for="type">PLAN TYPE</label>
            <input readonly data-validation="required" value="{{$current_plan->type}}" id="type"
                   class="form-control"/>
        </div>

        <div class="col-6 form-group floatlabel mb-4">
            <label class="label" for="first-name">PLAN NAME</label>
            <input readonly data-validation="required" value="{{$current_plan->order}}" id="first-name"
                   class="form-control"/>
        </div>

        <div class="col-6 form-group floatlabel mb-4">
            <label class="label" for="price">PRICE</label>
            <input id="price" readonly name="price" value="{{$current_plan->price}}" class="form-control"/>
        </div>

        <div class="col-6 form-group floatlabel mb-4">
            <label class="label" for="features">FEATURES</label>
            <textarea id="features" readonly name="features" rows="10" class="form-control">{{$current_plan->features}}</textarea>
        </div>

        <div class="col-6 form-group floatlabel mb-4" id="tablets_number_container">
            <label class="label" for="tablets_number">NO. OF TABLETS</label>
            <input id="tablets_number" readonly name="tablets_number" value="{{$current_plan->tablets_number}}" class="form-control"/>
        </div>


    </div>

@stop

@section('scripts')
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
    <script src="{{asset('assets/js/select2.full.min.js')}}"></script>
    <script>

        $.validate({modules: 'security'});

        $("#country_id").trigger("change");

        $('#country_id').change(function () {
            id = $(this).val()
            if (id) {
                $.ajax({
                    method: "GET",
                    url: "{{URL::to('admin/ajax/cities')}}",
                    data: {id: id}
                })
                    .done(function (data) {
                        $('#city_id').append(data)
                        console.log(data)
                    });
            }
        });

        $('#type').on('change',function(){
            let val = $('#type').val();
            if(val == 'Premium') {
                $('#price').val('Get A Quote');
                $('#price').attr('readonly', true);
                $('#tablets_number').val(0);
                $('#tablets_number_container').css('display','none');
            } else {
                $('#price').val('');
                $('#price').attr('readonly', false);
                $('#tablets_number').val('');
                $('#tablets_number_container').css('display','block');
            }
        });
        $(".select2").select2();

    </script>


@stop
