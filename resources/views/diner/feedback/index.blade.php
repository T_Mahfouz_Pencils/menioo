@extends('diner.layouts.master')
@section('styling')
    <link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/css/select2.min.css')}}">
@stop
@section('content')

    <div class="table-component table-responsive">
        <table class="table table-hover table-borderless" style="margin-top: 50px;">
            <thead>
            <tr>
                <th>Form Name</th>
                <th>Form Header</th>
                <th>Publish</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @if($feedbacks)
                @foreach($feedbacks as $feedback)
                    <tr class="viewPaln">
                        <td>{{$feedback->form_name}}</td>
                        <td>{{$feedback->form_header}}</td>
                        <td>
                            <label class="switch">
                                <input data-id="{{$feedback->id}}" type="checkbox" id="status"
                                       {{$feedback->active ?'checked':''}} value="close"
                                       class="primary change-status">
                                <span class="slider round"></span>
                            </label>
                        </td>

                        <td>
                            <button class="btn btn-danger edit-campaign" data-href="{{$feedback->id}}">EDIT</button>
                            <button class="btn btn-secondary"  data-toggle="modal" data-target="#delAlert_{{$feedback->id}}" class="">DELETE</button>
                        </td>
                    </tr>
                    <!-- Delete Alert -->
                    <div class="modal fade" id="delAlert_{{$feedback->id}}" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            {{Form::open(['route'=>['diner.feedback.destroy' , $feedback->id ] , 'method'=>'delete'])}}

                            <div class="modal-content text-center">
                                <div class="modal-body">
                                    <h5 class="text-center">ARE YOU SURE?</h5>
                                    <p class="m-0">Are you sure you want to delete {{$feedback->form_name}}?</p>
                                    <small class="text-danger">This cannot be undone</small>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="plain-btn" data-dismiss="modal">CANCEL</button>
                                    <button type="submit" class="dark-btn">DELETE FEEDBACK</button>
                                </div>
                            </div>

                            {{Form::close()}}
                        </div>
                    </div>
                @endforeach
            @endif
            </tbody>
        </table>

        <nav>
            <div class="float-right pagination-filter">
                <div>
                    <label>Showing</label>
                    <select class="form-control">
                        <option>10 items per page</option>
                    </select>
                </div>
            </div>
            {{$feedbacks->links()}}
            {{--<ul class="pagination justify-content-center">--}}
            {{--<li class="page-item disabled">--}}
            {{--<a class="page-link" href="#" tabindex="-1"><i--}}
            {{--class="fas fa-angle-left"></i></a>--}}
            {{--</li>--}}
            {{--<li class="page-item active"><a class="page-link" href="#">1</a></li>--}}
            {{--<li class="page-item"><a class="page-link" href="#">2</a></li>--}}
            {{--<li class="page-item"><a class="page-link" href="#">3</a></li>--}}
            {{--<li class="page-item">--}}
            {{--<a class="page-link" href="#"><i class="fas fa-angle-right"></i></a>--}}
            {{--</li>--}}
            {{--</ul>--}}

        </nav>
    </div>
@stop

@section('scripts')
    <script src="{{asset('assets/js/select2.full.min.js')}}"></script>

    <script>

        $('.edit-campaign').on('click',function(){
            id = $(this).attr('data-href');
            window.open('{{URL::to('diner/feedback')}}/' + id + '/edit?venue_id={{$venue_id}}' , '_self');
        });

        $('.change-status').on('click',function(){
            id = $(this).attr('data-id');
            window.open('{{URL::to('diner/feedback/open-close')}}/' + id , '_self');
        });

        $('#keep-on .dropdown-menu').on({
            "click": function (e) {
                e.stopPropagation();
            }
        });

        $(".dismissDD").on('click', function () {
            $(".dropdown-toggle").dropdown("hide");
        });

        $(document).ready(function () {
            $("#country_id").trigger("change");
        });

        $('#country_id').change(function () {

            id = $(this).val()
            if (id) {
                $.ajax({
                    method: "GET",
                    url: "{{URL::to('admin/ajax/cities')}}",
                    data: {id: id}
                })
                    .done(function (data) {
                        $('city_id').append(data)
                    });
            }
        });

        $(".select2").select2();




        $('#sort_btn').click(function () {
            var sort_by = $('.sorting-buttons.selected').attr('id');
            if (sort_by) {
                url = updateQueryStringParameter(window.location.href, 'sort_by', sort_by);
                window.location = url;
            }
        });



        $('.sorting-buttons').click(function () {
            $('.sorting-buttons').each(function () {
                $(this).removeClass('selected');
            });

            $(this).addClass('selected');
        })
    </script>


@stop
