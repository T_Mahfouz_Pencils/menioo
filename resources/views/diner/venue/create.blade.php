@extends('diner.layouts.master')
@section('styling')
    <link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/css/select2.min.css')}}">
@stop
@section('content')
    {{Form::open(array('url'=>array('diner/venue') ,'method'=>'POST','files'=>'true','enctype'=>'multipart/form-data'))}}

    <div class="row">

        <div class="col-9 float-left">
            <div class="col-8 form-group floatlabel mb-4">
                <label class="label" for="first-name">Venue Name</label>
                <input type="text" data-validation="required" name="name" value="{{old('name')}}" id="first-name"
                       class="form-control"/>
            </div>

            <div class="col-8 form-group floatlabel mb-4">
                <label class="label" for="first-name">Description</label>
                <textarea type="text"  name="description" id="first-name"
                       class="form-control">{{old('description')}}</textarea>
            </div>

            <div class="col-8 form-group floatlabel mb-4">
              <label class="label" for="address">Address</label>
              <input type="text" value="{{old('address')}}" name="address" id="address"
                 class="form-control" />
            </div>

            <div class="col-8 form-group floatlabel mb-4">
                  <label class="label" for="state">Region</label>
                  <input type="text" value="{{old('state')}}" name="state" id="state"
                     class="form-control" />
            </div>

            <div class="col-8 form-group floatlabel mb-4">
              <label class="label" for="timezone">Timezone</label>
              <input type="text" value="{{old('timezone')}}" name="timezone" id="timezone"
                 class="form-control" />
            </div>

            <div class="col-8 form-group floatlabel mb-4">
              <label class="label" for="website">Website</label>
              <input type="text" value="{{old('website')}}" name="website" id="website"
                 class="form-control" />
            </div>

            <div class="col-8 form-group floatlabel mb-4">
              <label class="label" for="instagram">Instagram</label>
              <input type="text" value="{{old('instagram')}}" name="instagram" id="instagram"
                 class="form-control" />
            </div>

            <div class="col-8 form-group floatlabel mb-4">
              <label class="label" for="facebook">Facebook</label>
              <input type="text" value="{{old('facebook')}}" name="facebook" id="facebook"
                 class="form-control" />
            </div>

            <div class="col-8 form-group floatlabel mb-4">
              <label class="label" for="twitter">Twitter</label>
              <input type="text" value="{{old('twitter')}}" name="twitter" id="twitter"
                 class="form-control" />
            </div>

            <div class="col-8 form-group floatlabel mb-4">
              <label class="label" for="foursquare">Foursquare</label>
              <input type="text" value="{{old('foursquare')}}" name="foursquare" id="foursquare"
                 class="form-control" />
            </div>

        </div>

        <div class="col-3 float-right">
            <div class="uploaded-pic">
                <img id="imageDiv" src="">
                <button class="change-pic-btn">Add Logo</button>
                <input id="uploadImage" name="logo" type="file" class="custom-file-input">
            </div>
        </div>

    </div>
    <div class="form-group text-center mt-5">
        <button type="submit" class="dark-btn">ADD Venue</button>
    </div>
    {{Form::close()}}
@stop

@section('scripts')
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
    <script src="{{asset('assets/js/select2.full.min.js')}}"></script>
    <script>
        $.validate({modules: 'security'});

        $("#uploadImage").on("change", function(){
            // Get a reference to the fileList
            var files = !!this.files ? this.files : [];
            // If no files were selected, or no FileReader support, return
            if ( !files.length || !window.FileReader ) return;
            // Only proceed if the selected file is an image
            if ( /^image/.test( files[0].type ) ) {
                // Create a new instance of the FileReader
                var reader = new FileReader();
                // Read the local file as a DataURL
                reader.readAsDataURL( files[0] );
                // When loaded, set image data as background of page
                reader.onloadend = function(){
                    $("#imageDiv").attr("src", this.result);
                }
            }
        });

    </script>


@stop
