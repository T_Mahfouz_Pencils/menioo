@extends('admin.layouts.master')

@section('content')
    <div class="row">
        <div class="col-sm-6 col-12">
            <h2 class="mb-4">Details</h2>
            <ul class="flex-menu-half">
                <li>
                    <label>Vendor Name</label>
                    <p>{{$current_customer->name}}</p>
                </li>
                <li>
                    <label>Customer ID</label>
                    <p>{{$current_customer->id}}</p>
                </li>
                <li>
                    <label>Company Name</label>
                    <p>{{$current_customer->company_name}}</p>
                </li>
                <li>
                    <label>Occupation</label>
                    <p>{{$current_customer->occupation}}</p>
                </li>
                <li>
                    <label>Email</label>
                    <p>{{$current_customer->email}}</p>
                </li>
                <li>
                    <label>Country</label>
                    <p>{{$current_customer->Country ? $current_customer->Country->name : '' }}</p>
                </li>
                <li>
                    <label>Phone Number</label>
                    <p>{{$current_customer->mobile_number}}</p>
                </li>
                <li>
                    <label>City</label>
                    <p>{{$current_customer->City ? $current_customer->City->name : '' }}</p>
                </li>
                <li>
                    <label>Region</label>
                    <p>{{$current_customer->state}}</p>
                </li>
            </ul>
            <hr>

            <h2 class="mb-4">Subscription</h2>
            <ul class="flex-menu-half">
                <li>
                    <label>Subscription Start Date</label>
                    <p>06 Aug 2019</p>
                </li>
                <li>
                    <label>Subscription End Date</label>
                    <p>22 May 2019</p>
                </li>
            </ul>
            <hr>
            <h2 class="mb-4">History</h2>
            <div class="table-component table-responsive">
                <table class="table table-hover table-borderless">
                    <thead>
                    <tr>
                        <th>Date</th>
                        <th>Amount</th>
                        <th>Method</th>
                        <th>Reciept</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>18 Mar 2019</td>
                        <td>200$</td>
                        <td>
                            <small class="text-muted">Credit Card</small>
                            <p>**4565</p>
                        </td>
                        <td><a href="#"><i class="fas fa-file-download"></i></a></td>
                    </tr>
                    <tr>
                        <td>18 Mar 2019</td>
                        <td>200$</td>
                        <td>
                            <small class="text-muted">Credit Card</small>
                            <p>**4565</p>
                        </td>
                        <td><a href="#"><i class="fas fa-file-download"></i></a></td>
                    </tr>
                    </tbody>
                </table>
            </div>

        </div>
        <div class="col-sm-6 col-12">
            <h2 class="mb-4">Branches</h2>
            <div class="bg-white p-4 mb-4">
                <ul class="flex-menu-half">
                    <li>
                        <label>Branch Name</label>
                        <p>Doddie</p>
                    </li>
                    <li>
                        <label>Branch ID</label>
                        <p>Doddie</p>
                    </li>
                    <li>
                        <label>Website</label>
                        <p>Doddie</p>
                    </li>
                    <li>
                        <label>Email</label>
                        <p>name@domain.com</p>
                    </li>
                    <li>
                        <label>Country</label>
                        <p>Egypt</p>
                    </li>
                    <li>
                        <label>City</label>
                        <p>Cairo</p>
                    </li>
                    <li>
                        <label>Region</label>
                        <p>Maadi</p>
                    </li>
                    <li>
                        <label>Address</label>
                        <p>Road 12</p>
                    </li>
                    <li>
                        <label>Time Zone</label>
                        <p>Cair (+2:00 GMT)</p>
                    </li>
                    <li>
                        <label>Facebook</label>
                        <p>fb.com/Doddie</p>
                    </li>
                    <li>
                        <label>Twitter</label>
                        <p>Maadi</p>
                    </li>
                    <li>
                        <label>Instagram</label>
                        <p>Maadi</p>
                    </li>
                    <li>
                        <label>Foursquare</label>
                        <p>Maadi</p>
                    </li>
                </ul>
            </div>

            <div class="bg-white p-4 mb-4">
                <ul class="flex-menu-half">
                    <li>
                        <label>Branch Name</label>
                        <p>Doddie</p>
                    </li>
                    <li>
                        <label>Branch ID</label>
                        <p>Doddie</p>
                    </li>
                    <li>
                        <label>Website</label>
                        <p>Doddie</p>
                    </li>
                    <li>
                        <label>Email</label>
                        <p>name@domain.com</p>
                    </li>
                    <li>
                        <label>Country</label>
                        <p>Egypt</p>
                    </li>
                    <li>
                        <label>City</label>
                        <p>Cairo</p>
                    </li>
                    <li>
                        <label>Region</label>
                        <p>Maadi</p>
                    </li>
                    <li>
                        <label>Address</label>
                        <p>Road 12</p>
                    </li>
                    <li>
                        <label>Time Zone</label>
                        <p>Cair (+2:00 GMT)</p>
                    </li>
                    <li>
                        <label>Facebook</label>
                        <p>fb.com/Doddie</p>
                    </li>
                    <li>
                        <label>Twitter</label>
                        <p>Maadi</p>
                    </li>
                    <li>
                        <label>Instagram</label>
                        <p>Maadi</p>
                    </li>
                    <li>
                        <label>Foursquare</label>
                        <p>Maadi</p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
@stop