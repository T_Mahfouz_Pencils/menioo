@extends('diner.layouts.master')

@section('content')
    <div class="row">
        <div class="col-sm-6 col-12">
            <h2 class="mb-4">Details</h2>
            <ul class="flex-menu-half">
                <li>
                    <label>Waiter Name</label>
                    <p>{{$current_customer->name}}</p>
                </li>
                <li>
                    <label>Waiter ID</label>
                    <p>{{$current_customer->id}}</p>
                </li>
                <li>
                    <label>Email</label>
                    <p>{{$current_customer->email}}</p>
                </li>
            </ul>

        </div>
    </div>
@stop
