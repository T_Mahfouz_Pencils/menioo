@extends('diner.layouts.master')
@section('styling')
    <link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/css/select2.min.css')}}">
@stop
@section('content')

{{-- 
    <div class="search-field">
        {{Form::open(array('url'=>array('diner/waiters') ,'method'=>'GET','class'=>"form-inline md-form form-sm mt-0"))}}
        <input class="form-control form-control-sm ml-3 w-75" value="{{\Request::Input('keyword')}}" type="text"
               name="keyword" placeholder="Search"
               aria-label="Search">
        <i class="fas fa-search" aria-hidden="true"></i>
        {{Form::close()}}
    </div> --}}
{{-- 
    <div class="sort-option float-right">
        <ul id="keep-on">
            <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">FILTER
                    <b class="caret"></b></a>
                <div class="dropdown-menu">
                    <h3>Filter</h3>

                    <div class="form-group">
                        <label>Plan</label>
                        <select class="form-control" id="plan">
                            <option>Basic</option>
                            <option>Fasic</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Location</label>
                        <div class="row">
                            <div class="col-6">
                                <select class="form-control select2" id="country_id" name="country_id">
                                    <option value="">Select Country</option>
                                    @if($countries)
                                        @foreach($countries as $country)
                                            <option value="{{$country->id}}" {{\Request::Input('country_id') == $country->id ? 'selected':''}}>{{$country->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="col-6">
                                <select class="form-control select2" name="city_id" id="city_id">
                                    <option value="">Select City</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Start Subscription Date</label>
                        <div class="row">
                            <div class="col-6">
                                <input type="date" placeholder="From"
                                       value="{{\Request::Input('start_sub_date_from') ? date('Y-m-d',strtotime(\Request::Input('start_sub_date_from'))) :'' }}"
                                       id="start_sub_date_from" class="form-control">
                            </div>
                            <div class="col-6">
                                <input type="date" placeholder="To"
                                       value="{{\Request::Input('start_sub_date_to') ? date('Y-m-d',strtotime(\Request::Input('start_sub_date_to'))) :'' }}"
                                       id="start_sub_date_to" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>End Subscription Date</label>
                        <div class="row">
                            <div class="col-6">
                                <input type="date" placeholder="From"
                                       value="{{\Request::Input('end_sub_date_from') ? date('Y-m-d',strtotime(\Request::Input('end_sub_date_from'))) :'' }}"
                                       id="end_sub_date_from" class="form-control">
                            </div>
                            <div class="col-6">
                                <input type="date" placeholder="To"
                                       value="{{\Request::Input('end_sub_date_from') ? date('Y-m-d',strtotime(\Request::Input('end_sub_date_from'))) :'' }}"
                                       id="end_sub_date_to" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Status</label>

                        <div class="d-flex justify-content-between">
                            <div class="text-muted">
                                Inactive
                            </div>
                            <div>
                                <label class="switch">
                                    <input type="checkbox" id="is_active"
                                           {{\Request::Input('is_active') && \Request::Input('is_active')==1 ?'checked':''}} value="1"
                                           class="primary">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                            <div class="text-muted">
                                Active
                            </div>
                        </div>

                    </div>

                    <div class="form-group">
                        <label>Last Active</label>
                        <select class="form-control">
                            <option>3 Weeks</option>
                            <option>4 Weeks</option>
                        </select>
                    </div>

                    <div class="form-group d-flex justify-content-between mt-3">
                        <button class="dark-btn h-40" id="filter_btn">FILTER</button>
                        <button class="plain-btn h-40 dismissDD">CANCEL</button>
                    </div>
                </div>
            </li>
            <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">SORT
                    <b class="caret"></b></a>
                <div class="dropdown-menu">
                    <h3>Sort</h3>

                    <ul>
                        <li><a href="javascript:void(0)" id="name"
                               class="sorting-buttons {{\Request::Input('sort_by') == 'name' ? 'selected':''}}">Customer
                                Name</a></li>
                        <li><a href="javascript:void(0)" id="num_of_tablets"
                               class="sorting-buttons {{\Request::Input('sort_by') == 'num_of_tablets' ? 'selected':''}}">Number
                                of
                                Tablets</a></li>
                        <li><a href="javascript:void(0)" id="location"
                               class="sorting-buttons {{\Request::Input('sort_by') == 'location' ? 'selected':''}}">Location</a>
                        </li>
                        <li><a href="javascript:void(0)" id="plan"
                               class="sorting-buttons {{\Request::Input('sort_by') == 'plan' ? 'selected':''}}">Plan</a>
                        </li>
                        <li><a href="javascript:void(0)" id="sub_date"
                               class="sorting-buttons {{\Request::Input('sort_by') == 'sub_date' ? 'selected':''}}">Subscription
                                Date</a>
                        </li>
                    </ul>

                    <div class="form-group d-flex justify-content-between mt-2">
                        <button class="dark-btn h-40" id="sort_btn">SORT</button>
                        <button class="plain-btn h-40 dismissDD">CANCEL</button>
                    </div>
                </div>
            </li>
        </ul>
    </div> --}}


    <div class="table-component table-responsive">
        <table class="table table-hover table-borderless">
            <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
            </tr>
            </thead>
            <tbody>
            @if($customers)
                @foreach($customers as $customer)
                    <tr class="viewCustomer" data-href="{{URL::to('diner/waiters/'.$customer->id)}}">
                        <td>{{$customer->name}}</td>
                        <td>{{$customer->email}}</td>
                       
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>

        <nav>
            <div class="float-right pagination-filter">
                <div>
                    <label>Showing</label>
                    <select class="form-control">
                        <option>10 items per page</option>
                    </select>
                </div>
            </div>
            {{$customers->links()}}
            {{--<ul class="pagination justify-content-center">--}}
            {{--<li class="page-item disabled">--}}
            {{--<a class="page-link" href="#" tabindex="-1"><i--}}
            {{--class="fas fa-angle-left"></i></a>--}}
            {{--</li>--}}
            {{--<li class="page-item active"><a class="page-link" href="#">1</a></li>--}}
            {{--<li class="page-item"><a class="page-link" href="#">2</a></li>--}}
            {{--<li class="page-item"><a class="page-link" href="#">3</a></li>--}}
            {{--<li class="page-item">--}}
            {{--<a class="page-link" href="#"><i class="fas fa-angle-right"></i></a>--}}
            {{--</li>--}}
            {{--</ul>--}}

        </nav>
    </div>
@stop

@section('scripts')
    <script src="{{asset('assets/js/select2.full.min.js')}}"></script>

    <script>

        $('#keep-on .dropdown-menu').on({
            "click": function (e) {
                e.stopPropagation();
            }
        });

        $(".dismissDD").on('click', function () {
            $(".dropdown-toggle").dropdown("hide");
        });

        $(document).ready(function () {
            $("#country_id").trigger("change");
        });

        $('#country_id').change(function () {

            id = $(this).val()
            if (id) {
                $.ajax({
                    method: "GET",
                    url: "{{URL::to('admin/ajax/cities')}}",
                    data: {id: id}
                })
                    .done(function (data) {
                        $('city_id').append(data)
                    });
            }
        });

        $(".select2").select2();


        $(".viewCustomer").click(function () {
            window.location = $(this).data("href");
        });

        $('#filter_btn').click(function () {
            var is_active = $('#is_active').is(":checked") ? 1 : 0;
            var country_id = $('#country_id').val();
            var city_id = $('#city_id').val();
            var start_sub_date_from = $('#start_sub_date_from').val();
            var start_sub_date_to = $('#start_sub_date_to').val();
            var end_sub_date_from = $('#end_sub_date_from').val();
            var end_sub_date_to = $('#end_sub_date_to').val();
            url = updateQueryStringParameter(window.location.href, 'country_id', country_id);
            url = updateQueryStringParameter(url, 'city_id', city_id);
            url = updateQueryStringParameter(url, 'start_sub_date_from', start_sub_date_from);
            url = updateQueryStringParameter(url, 'start_sub_date_to', start_sub_date_to);
            url = updateQueryStringParameter(url, 'end_sub_date_from', end_sub_date_from);
            url = updateQueryStringParameter(url, 'end_sub_date_to', end_sub_date_to);
            url = updateQueryStringParameter(url, 'is_active', is_active);
            window.location = url;
        });


        $('#sort_btn').click(function () {
            var sort_by = $('.sorting-buttons.selected').attr('id');
            if (sort_by) {
                url = updateQueryStringParameter(window.location.href, 'sort_by', sort_by);
                window.location = url;
            }
        });

        function updateQueryStringParameter(url, key, value) {
            if (!url) {
                uri = window.location.href;
            } else {
                uri = url;
            }

            var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
            var separator = uri.indexOf('?') !== -1 ? "&" : "?";
            if (uri.match(re)) {
                return uri.replace(re, '$1' + key + "=" + value + '$2');
            } else {
                return uri + separator + key + "=" + value;
            }
        }

        $('.sorting-buttons').click(function () {
            $('.sorting-buttons').each(function () {
                $(this).removeClass('selected');
            });

            $(this).addClass('selected');
        })
    </script>


@stop
