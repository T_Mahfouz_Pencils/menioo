@extends('diner.layouts.master')
@section('styling')
    <link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/css/select2.min.css')}}">
@stop
@section('content')
    {{Form::open(array('url'=>array('diner/waiters') ,'method'=>'POST','files'=>'true','enctype'=>'multipart/form-data'))}}

    <div class="row">
        <div class="col-6 form-group floatlabel mb-4">
            <label class="label" for="first-name">Full Name</label>
            <input type="text" data-validation="required" name="name" value="{{old('name')}}" id="first-name"
                   class="form-control"/>
        </div>
        <div class="col-6 form-group floatlabel mb-4">
            <label class="label" for="email">Email</label>
            <input type="email" data-validation="required email"  name="email" value="{{old('email')}}" id="email" class="form-control"/>
        </div>
        <div class="col-6 form-group floatlabel mb-4">
            <label class="label" for="password">Password</label>
            <input id="password" name="password" type="password"  required class="form-control">

        </div>

        <div class="col-6 form-group floatlabel mb-4">
            <label class="label" for="password-confirm" >Confirm Password</label>
            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
            
        </div>
    </div>
    <div class="form-group text-center mt-5">
        <button type="submit" class="dark-btn">ADD Waiter</button>
    </div>
    {{Form::close()}}
@stop

@section('scripts')
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
    <script src="{{asset('assets/js/select2.full.min.js')}}"></script>
    <script>

        $.validate({modules: 'security'});

        
        $(".select2").select2();

    </script>


@stop
