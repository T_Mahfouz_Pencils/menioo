@extends('diner.layouts.master')

@section('styling')
    <link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/css/select2.min.css')}}">
@stop
@section('content')

    <div class="search-field">
        {{Form::open(array('url'=>route('diner.order.index',['venue_id'=>$venue_id]) ,'method'=>'GET','class'=>"form-inline md-form form-sm mt-0"))}}
        <input class="form-control form-control-sm ml-3 w-75" value="{{\Request::Input('keyword')}}" type="text"
               name="keyword" placeholder="Search Ticket ID"
               aria-label="Search">
        <i class="fas fa-search" aria-hidden="true"></i>
        {{Form::close()}}
    </div>

    <div class="sort-option float-right">
        <ul id="keep-on">
            <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">FILTER
                    <b class="caret"></b></a>
                <div class="dropdown-menu">
                    <h3>Filter</h3>


                    <div class="form-group">
                        <label>Order Date</label>
                        <div class="row">
                            <div class="col-6">
                                <input type="date" placeholder="From"
                                       value="{{\Request::Input('order_date_from') ? date('Y-m-d',strtotime(\Request::Input('order_date_from'))) :'' }}"
                                       id="order_date_from" class="form-control">
                            </div>
                            <div class="col-6">
                                <input type="date" placeholder="To"
                                       value="{{\Request::Input('order_date_to') ? date('Y-m-d',strtotime(\Request::Input('order_date_to'))) :'' }}"
                                       id="order_date_to" class="form-control">
                            </div>
                        </div>
                    </div>


                    <div class="form-group">
                        <label>Status</label>

                        <div class="d-flex justify-content-between">
                            <div class="text-muted">
                                Close
                            </div>
                            <div>
                                <label class="switch">
                                    <input type="checkbox" id="status"
                                           {{\Request::Input('status') && \Request::Input('status')==1 ?'checked':''}} value="close"
                                           class="primary">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                            <div class="text-muted">
                                Open
                            </div>
                        </div>

                    </div>
                    <div class="form-group d-flex justify-content-between mt-3">
                        <button class="dark-btn h-40" id="filter_btn">FILTER</button>
                        <button class="plain-btn h-40 dismissDD">CANCEL</button>
                    </div>
                </div>
            </li>

        </ul>
    </div>


    <div class="table-component table-responsive">
        <table class="table table-hover table-borderless">
            <thead>
            <tr>
                <th>Status</th>
                <th>Ticket ID</th>
                <th>Table Number</th>
                <th>Update Time</th>
                <th>Sent Time</th>
            </tr>
            </thead>
            <tbody>
            @if($orders)
                @foreach($orders as $order)
                    <tr class="viewCustomer" data-href="{{URL::to('diner/order/'.$order->id.'?venue_id='.$venue_id)}}">
                        <td>{{$order->status}}</td>
                        <td>{{$order->id}}</td>
                        <td>{{$order->table_number}}</td>
                        <td>{{$order->updated_at}}</td>
                        <td>{{$order->created_at}}</td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>

        <nav>
            <div class="float-right pagination-filter">
                <div>
                    <label>Showing</label>
                    <select class="form-control">
                        <option>10 items per page</option>
                    </select>
                </div>
            </div>
            {{$orders->links()}}
            {{--<ul class="pagination justify-content-center">--}}
            {{--<li class="page-item disabled">--}}
            {{--<a class="page-link" href="#" tabindex="-1"><i--}}
            {{--class="fas fa-angle-left"></i></a>--}}
            {{--</li>--}}
            {{--<li class="page-item active"><a class="page-link" href="#">1</a></li>--}}
            {{--<li class="page-item"><a class="page-link" href="#">2</a></li>--}}
            {{--<li class="page-item"><a class="page-link" href="#">3</a></li>--}}
            {{--<li class="page-item">--}}
            {{--<a class="page-link" href="#"><i class="fas fa-angle-right"></i></a>--}}
            {{--</li>--}}
            {{--</ul>--}}

        </nav>
    </div>
@stop

@section('scripts')
    <script src="{{asset('assets/js/select2.full.min.js')}}"></script>

    <script>

        $('#keep-on .dropdown-menu').on({
            "click": function (e) {
                e.stopPropagation();
            }
        });

        $(".dismissDD").on('click', function () {
            $(".dropdown-toggle").dropdown("hide");
        });

        $(document).ready(function () {
            $("#country_id").trigger("change");
        });

        $('#country_id').change(function () {

            id = $(this).val()
            if (id) {
                $.ajax({
                    method: "GET",
                    url: "{{URL::to('admin/ajax/cities')}}",
                    data: {id: id}
                })
                    .done(function (data) {
                        $('city_id').append(data)
                    });
            }
        });

        $(".select2").select2();


        $(".viewCustomer").click(function () {
            window.location = $(this).data("href");
        });

        $('#filter_btn').click(function () {
            var status = $('#status').is(":checked") ? 'open' : 'close';
            var order_date_from = $('#order_date_from').val()
            var order_date_to = $('#order_date_to').val();

            url = updateQueryStringParameter(window.location.href, 'status', status);
            url = updateQueryStringParameter(url, 'order_date_from', order_date_from);
            url = updateQueryStringParameter(url, 'order_date_to', order_date_to);
            window.location = url;
        });


        $('#sort_btn').click(function () {
            var sort_by = $('.sorting-buttons.selected').attr('id');
            if (sort_by) {
                url = updateQueryStringParameter(window.location.href, 'sort_by', sort_by);
                window.location = url;
            }
        });

        function updateQueryStringParameter(url, key, value) {
            if (!url) {
                uri = window.location.href;
            } else {
                uri = url;
            }

            var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
            var separator = uri.indexOf('?') !== -1 ? "&" : "?";
            if (uri.match(re)) {
                return uri.replace(re, '$1' + key + "=" + value + '$2');
            } else {
                return uri + separator + key + "=" + value;
            }
        }

        $('.sorting-buttons').click(function () {
            $('.sorting-buttons').each(function () {
                $(this).removeClass('selected');
            });

            $(this).addClass('selected');
        })
    </script>


@stop
