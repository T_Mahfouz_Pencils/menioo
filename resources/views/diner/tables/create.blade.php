@extends('diner.layouts.master')
@section('styling')
    <link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/css/select2.min.css')}}">
@stop
@section('content')
    {{Form::open(array('url'=>array('diner/tables') ,'method'=>'POST','files'=>'true'))}}

    <div class="row">

        <input type="hidden" name="customer_id" value="{{$customer_id}}" />


        <div class="col-8 form-group floatlabel mb-4">
            <label class="label" for="number">Number</label>
            <input type="text" data-validation="required" name="number" value="{{old('number')}}" id="number"
                   class="form-control"/>
        </div>

        <div class="col-8 form-group floatlabel mb-4">
            <label class="label" for="description">Type a table description</label>
            <textarea type="text" name="description" id="description"
                   class="form-control">{{old('description')}}</textarea>
        </div>

    </div>
    <div class="form-group text-center mt-5">
        <button type="submit" class="dark-btn">ADD TABLE</button>
    </div>
    {{Form::close()}}
@stop

@section('scripts')
    <style>
        #content_option_video, #trigger_option_item {display: none;}
    </style>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
    <script src="{{asset('assets/js/select2.full.min.js')}}"></script>
    <script>

        $.validate({modules: 'security'});

    </script>

@stop
