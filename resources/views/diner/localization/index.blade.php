@extends('diner.layouts.master')

@section('content')
    <div class="">
        <ul class="nav nav-pills nav-fill justify-content-center" id="tabs">
                    
            <li class="nav-item active border">
                <a class="bold nav-link active" href="#menus" data-toggle="tab">
                    Menus
                </a>
            </li>
            
            <li class="nav-item  border">
                <a class="bold nav-link " href="#survey" data-toggle="tab">
                    Survey Forms
                </a>
            </li>
        </ul>
        <div class="tab-content" style="margin-top: 20px !important;">
            <div class="tab-pane active" id="menus">
                <div class="row">
                    <div class="col-md-4">
                        <div id="treeview-expandible" class="">
    
                        </div>
                    </div>
                    <div class="col-md-8" >
                        <div class="row" >
                            @if(isset($config))
                                @if(isset(config('translatable.locales')[$config->default_language]))
                                    <div class="col-xl-3">
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input language" data-lang="{{ $config->default_language }}" id="checke_{{ $config->default_language }}" checked>
                                            <label class="form-check-label" for="checke_{{ $config->default_language }}">{{ config('translatable.locales')[$config->default_language] }}</label>
                                        </div>
                                    </div>
                                @endif
                            @endif
                            @foreach (json_decode($config->other_languages) as $value)
                                <div class="col-sm-3 ">
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input language" data-lang="{{ $value }}" id="checke_{{ $value }}" checked>
                                        <label class="form-check-label" for="checke_{{ $value }}">{{ config('translatable.locales')[$value] }}</label>
                                    </div>
                                </div>
                                
                            @endforeach
                                
                        </div>
                        @if(isset($config))
                            <div class="row">
                                <div class="col-md-4" style="display: inline-block; float: none;" id="div_{{ $config->default_language }}">
                                    <h3>{{ config('translatable.locales')[$config->default_language] }}</h3>
                                    <hr>
                                    <div class="row languageDiv">
                                        <div class="form-group col-12 floatlabel mb-4">
                                            <label class="label" for="item-name">Item Name</label>
                                            <input type="text" data-type="menu" data-id="" data-lang="{{ $config->default_language }}" data-col="name" name="name" value=""  class="form-control menu-control" id="item-name"/>
                                        </div>
                                        <div class="form-group col-12 floatlabel mb-4">
                                            <label class="label" for="description">Description</label>
                                            <textarea data-type="menu" data-id="" data-lang="{{ $config->default_language }}" data-col="description" name="description"  class="form-control menu-control" id="description"></textarea>
                                        </div>
                                        <div class="form-group col-12 floatlabel mb-4">
                                            <label class="label" for="item-name">Note</label>
                                            <input type="text" data-type="menu" data-id="" data-lang="{{ $config->default_language }}" data-col="note" name="note" value=""  class="form-control menu-control" id="notes"/>
                                        </div>
                                    </div>
                                </div>
                                @foreach (json_decode($config->other_languages) as $value)
                                    <div class="col-md-4" id="div_{{ $value }}">
                                        <h3>{{ config('translatable.locales')[$value] }}</h3>
                                        <hr>
                                        <div class="row languageDiv">
                                            <div class="form-group col-12 floatlabel mb-4">
                                                <label class="label" for="item-name">Item Name</label>
                                                <input type="text" data-type="Menu" data-id="" data-lang="{{ $value }}" data-col="name" name="name" value="" class="form-control menu-control" id="item-name"/>
                                            </div>
                                            <div class="form-group col-12 floatlabel mb-4">
                                                <label class="label" for="description">Description</label>
                                                <textarea name="description" data-type="Menu" data-id="" data-lang="{{ $value }}" data-col="description" class="form-control menu-control" id="description"></textarea>
                                            </div>
                                            <div class="form-group col-12 floatlabel mb-4">
                                                <label class="label" for="item-name">Note</label>
                                                <input type="text" data-type="Menu" data-id="" data-lang="{{ $value }}" data-col="note" name="note" value=""  class="form-control menu-control" id="notes"/>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>  
                        @endif
                    </div>
                    
                </div>
            </div>
            <div class="tab-pane " id="survey">
                <div class="row">
                        <div class="col-md-4">
                            <ul class="list-group">
                                @foreach ($survey_forms as $survey)
                                    <li class="list-group-item surveyform" data-id="{{ $survey->id }}">
                                        <span class="icon node-icon"></span>{{ $survey->form_name }}
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="col-md-8" >
                            <div class="row" >
                                @if(isset($config))
                                    @if(isset(config('translatable.locales')[$config->default_language]))
                                        <div class="col-xl-3">
                                            <div class="form-check">
                                                <input type="checkbox" class="form-check-input surfeyCheck" data-lang="{{ $config->default_language }}" id="checke_surfey_{{ $config->default_language }}" checked>
                                                <label class="form-check-label" for="checke_surfey_{{ $config->default_language }}">{{ config('translatable.locales')[$config->default_language] }}</label>
                                            </div>
                                        </div>
                                    @endif
                                @endif
                                @if(isset($config))
                                    @foreach (json_decode($config->other_languages) as $value)
                                        <div class="col-sm-3 ">
                                            <div class="form-check">
                                                <input type="checkbox" class="form-check-input surfeyCheck" data-lang="{{ $value }}" id="checke_surfey_{{ $value }}" checked>
                                                <label class="form-check-label" for="checke_surfey_{{ $value }}">{{ config('translatable.locales')[$value] }}</label>
                                            </div>
                                        </div>
                                        
                                    @endforeach
                                @endif 
                            </div>
                            @if(isset($config))
                                <div class="row">
                                    <div class="col-md-4" style="display: inline-block; float: none;" id="div_survey_{{ $config->default_language }}">
                                        <h3>{{ config('translatable.locales')[$config->default_language] }}</h3>
                                        <hr>
                                        <div class="row surveyDiv" data-lang="{{ $config->default_language }}">
                                            
                                        </div>
                                    </div>
                                    @foreach (json_decode($config->other_languages) as $value)
                                        <div class="col-md-4" id="div_survey_{{ $value }}">
                                            <h3>{{ config('translatable.locales')[$value] }}</h3>
                                            <hr>
                                            <div class="row surveyDiv" data-lang="{{ $value }}">
                                                
                                            </div>
                                        </div>
                                    @endforeach
                                </div>  
                            @endif
                        </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('scripts')
    <style>
        .glyphicon-plus:before {
            content: "\2b";
        }
        .glyphicon-minus:before {
            content: "\2212";
        }
        

        

    </style>
    <script src="{{ asset('js/tree.js') }}"></script>
    <script>
        var defaultData = JSON.parse('{!! $json_menu_data !!}');
        var $expandibleTree = $('#treeview-expandible').treeview({
            data: defaultData,
            onNodeSelected: function(event, node) {
                var objId = node.id;
                var objType = node.type;
                $.get('getObjectTranslation/' + objId + '/' + objType, function(data){
                    $(".languageDiv input[name=name]").val(data.name);
                    $(".languageDiv input[name=name]").attr('data-type',objType);
                    $(".languageDiv input[name=name]").attr('data-id',data.id);
                    $(".languageDiv textarea[name=description]").val(data.description);
                    $(".languageDiv textarea[name=description]").attr('data-type',objType);
                    $(".languageDiv textarea[name=description]").attr('data-id',data.id);
                    objType == 'Menu' ? $(".languageDiv input[name=note]").val(data.additional_notes) : $(".languageDiv input[name=note]").val(data.note);
                    $(".languageDiv input[name=note]").attr('data-type',objType);
                    $(".languageDiv input[name=note]").attr('data-id',data.id);
                    var languages = data.translate;
                    if(languages.length){
                        for(var i=0; i<languages.length; i++){
                            var lang = languages[i].locale;
                            if(languages[i].name)
                                $("#div_"+ lang+" input[name=name]").val(languages[i].name);
                            if(languages[i].description)
                                $("#div_"+ lang+" textarea[name=description]").val(languages[i].description);
                            if(objType == 'Menu')
                                if(languages[i].additional_notes)
                                    $("#div_"+ lang+" input[name=note]").val(languages[i].additional_notes );
                            else
                                if(languages[i].note)
                                    $("#div_"+ lang+" input[name=note]").val(languages[i].note)
                        }
                    }
                });
            },
            onNodeCollapsed: function(event, node) {

            },
            onNodeExpanded: function (event, node) {

            },
        });

        $(document).on('click', '.language', function(event){
            var lang = $(this).attr('data-lang');
            if(this.checked){
                $('#div_' + lang).css('display','block');
            }else{
                $('#div_' + lang).css('display','none');

            }
        });

        $(document).on('click', '.surfeyCheck', function(event){
            var lang = $(this).attr('data-lang');
            if(this.checked){
                $('#div_survey_' + lang).css('display','block');
            }else{
                $('#div_survey_' + lang).css('display','none');

            }
        });

        $(document).on('change' , '.menu-control', function(event){
            var data = {
                'type' : $(this).attr('data-type'),
                'id' : $(this).attr('data-id'),
                'lang' : $(this).attr('data-lang'),
                'col' : $(this).attr('data-col'),
                'value' : $(this).val(),
            };
            $.get('updatelanguage',{data : data},function(data){
                console.log(data);
            })
        })

        $(document).on('click', '.surveyform', function(){
            $( ".surveyDiv" ).html('')
            $('.surveyform').css('color','#000');
            $('.surveyform').css('background-color','#FFF');
            $(this).css('color','#FFFFFF');
            $(this).css('background-color','#428bca');
            var dataId = $(this).attr('data-id');
            $.get('getsurvey/' + dataId,function(data){
                data.questions.forEach(element => {
                    appendQuestion(element);
                });
            })
        })
        function appendQuestion(element){
            $( ".surveyDiv" ).each(function() {
                var lang = $( this ).attr('data-lang');
                $( this ).append(   '<div class="form-group col-12 floatlabel mb-4" id="'+lang + '_' + element.id+'">'+
                                        '<input type="text" data-id="'+element.id+'" data-lang="'+$( this ).attr('data-lang') +'" class="form-control survey-control" value="'+element.value+'" name="question_name"/>'+
                                    '</div>'
                                );
                console.log(element.translations)
                element.translations.forEach(language => {
                    if(lang == language.locale){
                        $( '#'+language.locale+'_'+ element.id).html(
                                                '<input type="text" data-id="'+element.id+'" data-lang="'+$( this ).attr('data-lang') +'" class="form-control survey-control" value="'+language.value+'" name="question_name"/>'
                                        );
                    }
                });

            });
        }
        $(document).on('change' , '.survey-control', function(event){
            var data = {
                'id' : $(this).attr('data-id'),
                'lang' : $(this).attr('data-lang'),
                'value' : $(this).val(),
            };
            $.get('updatesurveylanguage',{data : data},function(data){
                console.log(data);
            })
        })



    </script>
@stop
