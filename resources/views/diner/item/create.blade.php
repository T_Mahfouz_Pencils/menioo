@extends('diner.layouts.master')

@section('content')
    {{Form::open(array('url'=>array('diner/menu/'.$menu_id.'/item') ,'method'=>'POST','files'=>'true','enctype'=>'multipart/form-data'))}}

    <div class="row">
        <div class="col-md-6 col-12">
            <div class="form-group col-12 floatlabel mb-4">
                <label class="label" for="item-name">Item Name</label>
                <input type="text" name="name" value="{{old('name')}}" id="item-name" class="form-control"/>
            </div>
            <div class="form-group col-12 floatlabel mb-4">
                <label class="label" for="description">Description</label>
                <textarea name="description" id="description" class="form-control">{{old('description')}}</textarea>
            </div>

            <div class="row m-0">
                <div class="form-group col-6 floatlabel mb-4">
                  <label class="label" for="calories">Calories</label>
                  <input type="text" name="calories" value="{{old('calories')}}" id="calories" class="form-control"/>
                </div>
                <div class="form-group col-6 floatlabel mb-4">
                    <label class="label" for="portion">Portion</label>
                    <input type="text" name="portion" value="{{old('portion')}}" id="portion" class="form-control"/>
                </div>
            </div>

            <div class="form-group col-12 floatlabel mb-4">
                <label class="label" for="preparation_time">Preparation Time</label>
                <input type="text" name="preparation_time" value="{{old('preparation_time')}}" id="preparation_time" class="form-control"/>
            </div>

            <h5 class="mb-3" style="margin-left:10px;">Prices And Tags
                <button type="button" class="addpricetag float-right btn-link btn btn-sm">
                  <i class="fas fa-plus"></i>
                </button>
            </h5>

            <div class="pricetagDiv">
                <div class="row m-0 pricetagRow">
                    <div class="form-group col-6 floatlabel ">
                        <input placeholder="Price" type="text" name="prices[]"   class="form-control"/>
                    </div>
                    <div class="form-group col-6 floatlabel ">
                        <input placeholder="Price Tag" type="text" name="price_tags[]"   class="form-control"/>
                    </div>
                </div>
                
            </div>
            


            <div class="col-12 form-group floatlabel mb-4">
                <select class="form-control no-background" multiple name="ingredient_warning_ids[]" id="ingredient_warning_id">
                    <option value="" disabled>Ingredient Warnings</option>
                    @foreach($ingredientWarnings as $iw)
                    <option value="{{$iw->id}}">{{$iw->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-6 col-12">
            <div class="uploaded-pic">
                <img id="imageDiv" src="">
                <button class="change-pic-btn">Add Picture</button>
                <input id="uploadImage" name="image" type="file" class="custom-file-input">
            </div>

            <div class="uploaded-video">
                <!-- <iframe id="vidDiv" src="" width="" height=""></iframe> -->
                <input id="uploadVideo" name="video" type="file" class="custom-file-input">
                <button class="plain-btn btn-block">
                  <i class="fas fa-film"></i> Add a video
                </button>
            </div>

            <div class="form-group col-12 floatlabel my-4">
                <label class="label" for="notes">Note</label>
                <textarea name="notes" id="notes" class="form-control">{{old('notes')}}</textarea>
            </div>
        </div>
    </div>


    <h5 class="mb-3" style="margin-left:10px;">Side Dishes
        <button type="button" class="addDish float-right btn-link btn btn-sm">
          <i class="fas fa-plus"></i>
        </button>
    </h5>

    <div class="dishAppend">
        <div class="dishRow">
            <div class="form-group col-5 floatlabel mb-4">
                <select class="form-control sidedishselect" name="side_dishes[0][id]">
                    <option value="">Select Side Dish</option>
                    @foreach($sideDishes as $sideDish)
                    <option value="{{$sideDish->id}}" style="background-image:url({{  $sideDish->FullThumbnailImagePath()}});" data-price="{{ $sideDish->price }}">{{$sideDish->name}}</option>
                    @endforeach
                </select>

            </div>
            <div class="form-group col-5 floatlabel mb-4">
                <input placeholder="Add Price" type="text" name="side_dishes[0][price]" id="added-price-1" class="form-control"/>
            </div>
            <div class="form-group col-2 mb-4">
                <button type="button" class="plain-btn float-right removeItem text-danger">
                  <i class="far fa-trash-alt"></i>
                </button>
            </div>
        </div>
    </div>

    <h5 class="mb-3" style="margin-left: 12px;">Options
        <button type="button"  class="addOption float-right btn-link btn btn-sm">
          <i class="fas fa-plus"></i>
        </button>
    </h5>
    <div class="optionsSet">
      <div class="optionAppend">
          <div class="optionRow">
              <div class="form-group col-5 floatlabel mb-4">
                  <select type="text" name="options[0][id]" id="option0" class="form-control select2 Selectoption">
                    <option value="">Select Option</option>                    
                    @foreach($options as $option)
                    <option value="{{$option->id}}">{{$option->name}}</option>
                    @endforeach
                  </select>
              </div>
              <div class="form-group col-5 floatlabel mb-4">
                  <select id="onSelectOption" disabled class="form-control no-background" name="options[0][type]" id="option1_type">
                      <option value="">Type</option>
                      <option value="Checkbox">Checkboxes</option>
                      <option value="Slider">Slider</option>
                  </select>
              </div>
          </div>
          <div class="sliderOption onSlider">
              <div class="row m-0">
              </div>
          </div>
      </div>
    </div>

    <h5 class="mb-3" style="margin-left:10px;">Recommended Item
        <button type="button" class="addrecommend float-right btn-link btn btn-sm">
            <i class="fas fa-plus"></i>
        </button>
    </h5>
    
    <div class="recommendDiv">
        <div class="recommendRow">
            <div class="form-group col-4 floatlabel mb-4">
                <select class="form-control sidedishselect select2" multiple  name="recommend[]">
                    <option value="" disabled>Select Item</option>
                    @foreach($items as $item)
                    <option value="{{ $item->id}}" style="background-image:url({{  $item->FullThumbnailImagePath()}});" >{{$item->name}}</option>
                    @endforeach
                </select>

            </div>
        </div>
    </div>


    <div class="form-group text-center mt-5">
        <button type="submit" class="dark-btn">SAVE ITEM</button>
    </div>

    {{Form::close()}}
@stop




@section('scripts')
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>

    <script>

        $(document).ready(function () {
            $("#display_as").trigger("change");
            $('.Selectoption').change();
        });

        $.validate({modules: 'security'});

        var dishIndex = 0, optionIndex = 0;

        // add dish
        $(".addDish").click(function () {
            dishIndex = dishIndex + 1;
            let clonedDish =
            `<div class="dishRow">
                <div class="form-group col-5 floatlabel mb-4">
                    <select name="side_dishes[`+dishIndex+`][id]" id="side_dishes_id_`+dishIndex+`" class="form-control sidedishselect">
                      <option value="">Select Side Dish</option>
                      @foreach($sideDishes as $sideDish)
                        <option value="{{$sideDish->id}}" data-price="{{ $sideDish->price }}">{{$sideDish->name}}</option>
                      @endforeach
                    </select>
                </div>
                <div class="form-group col-5 floatlabel mb-4">
                    <input placeholder="Add Price" type="number" min="1" name="side_dishes[`+dishIndex+`][price]" id="side_dishes_price_`+dishIndex+`" class="form-control"/>
                </div>
                <div class="form-group col-2 mb-4">
                    <button type="button" class="plain-btn float-right removeItem text-danger">
                      <i class="far fa-trash-alt"></i>
                    </button>
                </div>
            </div>`;

            $(".dishAppend").append(clonedDish);
            initFloatLabels();

            // remove dish
            $(".removeItem").click(function () {
                this.closest('div.dishRow').remove();
            })
        });
        //Add price tag
        $(".addpricetag").click(function () {
            dishIndex = dishIndex + 1;
            let clonedDish =
            `<div class="row m-0 pricetagRow">
                <div class="form-group col-5 floatlabel ">
                    <input placeholder="Price" type="number" min="1" name="prices[]"   class="form-control"/>
                </div>
                <div class="form-group col-5 floatlabel ">
                    <input placeholder="Price Tag" type="text" name="price_tags[]"   class="form-control"/>
                </div>
                <div class="form-group col-2 ">
                    <button type="button" class="plain-btn float-right removerecpricetag text-danger">
                        <i class="far fa-trash-alt"></i>
                    </button>
                </div>
            </div>`;

            $(".pricetagDiv").append(clonedDish);
            initFloatLabels();

            // remove dish
            $(".removerecpricetag").click(function () {
                this.closest('div.pricetagRow').remove();
            })
        });

        //Add recommended
        // $(".addrecommend").click(function () {
        //     dishIndex = dishIndex + 1;
        //     let clonedDish =
        //     `<div class=" recommendRow">
        //         <div class="form-group col-4 floatlabel mb-4">
        //             <select class="form-control sidedishselect" name="recommend[]">
        //                 <option value="">Select Item</option>
        //                 @foreach($items as $item)
        //                 <option value="{{ $item->id}}" style="background-image:url({{  $item->FullThumbnailImagePath()}});" >{{$item->name}}</option>
        //                 @endforeach
        //             </select>

        //         </div>
        //         <div class="form-group col-2 mb-4">
        //             <button type="button" class="plain-btn float-right removerecItem text-danger">
        //                 <i class="far fa-trash-alt"></i>
        //             </button>
        //         </div>
        //     </div>`;

        //     $(".recommendDiv").append(clonedDish);
        //     initFloatLabels();

        //     // remove dish
        //     $(".removerecItem").click(function () {
        //         this.closest('div.recommendRow').remove();
        //     })
        // });
        // add option
        $(".addOption").click(function () {
            optionIndex = optionIndex + 1;
            let clonedOption =
                `<div class="optionAppend">
                    <div class="optionRow">
                        <div class="form-group col-5 floatlabel mb-4">
                        <select type="text" required name="options[`+optionIndex+`][id]" id="option`+optionIndex+`" class="form-control Selectoption">
                          @foreach($options as $option)
                          <option value="{{$option->id}}">{{$option->name}}</option>
                          @endforeach
                        </select>
                        </div>
                        <div class="form-group col-5 floatlabel mb-4">
                            <select id="onSelectOption" disabled class="form-control no-background" name="options[`+optionIndex+`][type]" id="option1_type">

                                <option value="Checkbox">Checkboxes</option>
                                <option value="Slider">Slider</option>
                            </select>
                        </div>
                        <div class="form-group col-2 mb-4">
                            <button type="button" class="plain-btn float-right removeOption text-danger"><i class="far fa-trash-alt"></i></button>
                        </div>
                    </div>
                    <div class="sliderOption onSlider">
                        <div class="row m-0">
                        </div>
                    </div>
                </div>`

            $(".optionsSet").append(clonedOption);
            initFloatLabels();
            $('.optionsSet').children().last().children().children().children().change();
            // remove dish
            
        });
        $(document).on('click', ".removeOption", function () {
            this.closest('div.optionAppend').remove();
        })

        $("#uploadImage").on("change", function(){
            // Get a reference to the fileList
            var files = !!this.files ? this.files : [];
            // If no files were selected, or no FileReader support, return
            if ( !files.length || !window.FileReader ) return;
            // Only proceed if the selected file is an image
            if ( /^image/.test( files[0].type ) ) {
                // Create a new instance of the FileReader
                var reader = new FileReader();
                // Read the local file as a DataURL
                reader.readAsDataURL( files[0] );
                // When loaded, set image data as background of page
                reader.onloadend = function(){
                    $("#imageDiv").attr("src", this.result);
                }
            }
        });



        $("#uploadVideo").on("change", function(){
            // Get a reference to the fileList
            var files = !!this.files ? this.files : [];
            // If no files were selected, or no FileReader support, return
            if ( !files.length || !window.FileReader ) return;
            // Only proceed if the selected file is an image
            if ( /^image/.test( files[0].type ) ) {
                // Create a new instance of the FileReader
                var reader = new FileReader();
                // Read the local file as a DataURL
                reader.readAsDataURL( files[0] );
                // When loaded, set image data as background of page
                reader.onloadend = function(){
                    $("#vidDiv").attr("src", this.result);
                }
            }
        });

        // on select an option
        // $("#onSelectOption").on('change', (el) = > {
        //     if(el.target.value === 'slider')
        // {
        //     $(".onSlider").show();
        //     $(".onCheckBox").hide()
        // }
        // else
        // if (el.target.value === 'checkbox') {
        //     $(".onSlider").hide();
        //     $(".onCheckBox").show()
        // }
        // })
    </script>

    <script>
        $(document).on('change', '.sidedishselect', function(event){
            var price = $('option:selected', this).attr('data-price');
            $(this).parent().next().children().val(price);
        })
    </script>
    <script>
        $(document).on('change', '.Selectoption', function(event){
            var option = $(this).val();
            var that = $(this);
            if(option){
                $.get('/diner/getoption/'+ option, function(data){  
                    that.parent().next().children().val(data.type);
                    that.attr('name','options['+data.id+'][id]')
                    that.parent().next().children().attr('name','options['+data.id+'][type]');
                    if(data.type == 'Slider'){
                        AppendSliderValue(data.values, that);
                    }
                    else{
                        AppendCheckboxValue(data.values, that);

                    }
                });
            }
            else{
                that.parent().parent().next().children().html('');
            }
            
        });

        function AppendSliderValue(values, div){
            var html = '';
            for(var i = 0 ; i < values.length; i++){
                html += '<div class="form-group col-6 floatlabel mb-4">'+
                            '<label class="label" for="option'+values[i].option_id+'_value_'+values[i].id+'">'+ values[i].value +'</label>'+
                            '<input required type="text" id="option'+values[i].option_id+'_value_'+values[i].id+'" name="options[' + values[i].option_id + '][values][]" class="form-control"/>'+
                        '</div>';
            }
            div.parent().parent().next().children().html('');
            div.parent().parent().next().children().append(html);
            
        }
        function AppendCheckboxValue(values, div){
            var html = '';
            for(var i = 0 ; i < values.length; i++){
                html += '<div class="form-group col-6 floatlabel mb-4">'+
                            '<label class="label" for="option'+values[i].option_id+'_value_'+values[i].id+'">' + values[i].value + '</label>'+
                            '<input required type="text" id="option'+values[i].option_id+'_value_'+values[i].id+'" name="options[' + values[i].option_id + '][values][]"  class="form-control"/>'+
                        '</div>'+
                        '<div class="form-group col-6 floatlabel mb-4">'+
                            '<label class="label" for="option'+values[i].option_id+'_value_price'+values[i].id+'">Price</label>'+
                            '<input type="number" min="1" id="option'+values[i].option_id+'_value_price'+values[i].id+'" name="options[' + values[i].option_id + '][prices][]" value="'+ values[i].price +'" class="form-control"/>'+
                        '</div>';
            }
            div.parent().parent().next().children().html('');
            div.parent().parent().next().children().append(html);
            
        }
    </script>


@stop
