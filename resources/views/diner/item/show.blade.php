@extends('diner.layouts.master')

@section('content')

    <div class="row" style="margin-right: -45px;">
        <div class="col-md-6 pull-left item-display-info">
            <div class="col-md-12 item-display-info-icons">
                <span class="col-md-4 float-left">
                    <i class="fa fa-fire-alt"></i>
                    <span>{{$item->calories?$item->calories:'--'}}</span>
                </span>
                <span class="col-md-4 float-left">
                    <i class="fas fa-balance-scale"></i>
                    <span>{{$item->portion?$item->portion:'--'}}</span>
                </span>
                <span class="col-md-4 float-left">
                    <i class="fa fa-clock"></i>
                    <span>{{$item->preparation_time}}</span>
                </span>
            </div>

            <div class="col-md-12 item-display-info-description">
                <p>{{$item->description}}</p>
            </div>

            <div class="col-md-12 item-display-info-price">
                @if(count($item->prices))
                  @foreach($item->prices as $price)
                      <div class="col-md-3 float-left">
                        <small>{{$price->description}}</small><br>
                        <b>EGP {{$price->price}}</b>
                      </div>
                  @endforeach
                @endif
            </div>

            <div class="clearfix"></div>



            <div class="col-md-12 item-display-info-sidedishes">
              @if(count($item->sideDishes))
                <b>Side Dishes</b><br><br>
                    @foreach($item->sideDishes as $sideDishe)
                    <div class="col-md-12 item-display-info-sidedishes-single">
                        <div class="col-md-1 float-left item-display-info-sidedishes-single-image">
                          <img src="{{$sideDishe->sideDishe_image}}" alt="">
                        </div>
                        <div class="col-md-9 float-right" class="item-display-info-sidedishes-single-info">
                          <p><b>{{$sideDishe->name}}</b></p>
                          <p class="item-display-info-sidedishes-single-price">+EGP {{$sideDishe->price}}</p>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    @endforeach
                @endif
            </div>


        </div>

        <div class="col-md-6 pull-right item-display-image">
          <img src="{{$item->item_image}}">
          @if($item->item_video)
          <video width="500" height="300" controls style="margin-top:10px;">
            <source src="{{$item->item_video}}" type="video/mp4">
          </video>
          @endif
        </div>

    </div>
@stop
