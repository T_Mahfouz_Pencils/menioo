@extends('diner.layouts.master')

@section('content')
    {{Form::open(array('url'=>array('diner/menu/'.$current_item->menu_id.'/item/'.$current_item->id) ,'method'=>'PUT','files'=>'true','enctype'=>'multipart/form-data'))}}

    <div class="row">
        <div class="col-md-6 col-12">
            <div class="form-group col-12 floatlabel mb-4">
                <label class="label" for="item-name">Item Name</label>
                <input type="text" name="name" value="{{$current_item->name}}" id="item-name" class="form-control"/>
            </div>
            <div class="form-group col-12 floatlabel mb-4">
                <label class="label" for="description">Description</label>
                <textarea name="description" rows="5" id="description" class="form-control">{{$current_item->description}}</textarea>
            </div>

            <div class="row m-0">
                <div class="form-group col-6 floatlabel mb-4">
                  <label class="label" for="calories">Calories</label>
                  <input type="text" name="calories" value="{{$current_item->calories}}" id="calories" class="form-control"/>
                </div>
                <div class="form-group col-6 floatlabel mb-4">
                    <label class="label" for="portion">Portion</label>
                    <input type="text" name="portion" value="{{$current_item->portion}}" id="portion" class="form-control"/>
                </div>
            </div>

            <div class="form-group col-12 floatlabel mb-4">
                <label class="label" for="preparation_time">Preparation Time</label>
                <input type="text" name="preparation_time" value="{{$current_item->preparation_time}}" id="preparation_time" class="form-control"/>
            </div>

            <h5 class="mb-3" style="margin-left:10px;margin-top: 50px;">Prices & Tags
                <button type="button" class="addpricetag float-right btn-link btn btn-sm">
                    <i class="fas fa-plus"></i>
                </button>
            </h5>
            <div class=" pricetagDiv">
                @forelse($current_item->priceList as $index => $price)
                    <div class="row m-0 pricetagRow ">
                        <div class="form-group col-5 floatlabel mb-4">
                            <input type="number" min="1" placeholder="Price #{{$index+1}}" name="prices[]" value="{{ $price->price}}"  class="form-control"/>
                        </div>
                        <div class="form-group col-5 floatlabel mb-4">
                            <input type="text" placeholder="Price #{{$index+1}} Tag" name="price_tags[]" value="{{ $price->description}}"  class="form-control"/>
                        </div>
                        <div class="form-group col-2 ">
                            <button type="button" class="plain-btn float-right removerecpricetag text-danger">
                                <i class="far fa-trash-alt"></i>
                            </button>
                        </div>
                    </div>
                @empty
                    <div class="row m-0 pricetagRow col-12">
                        <div class="form-group col-6 floatlabel ">
                            <input placeholder="Price" type="text" name="prices[]"   class="form-control"/>
                        </div>
                        <div class="form-group col-6 floatlabel ">
                            <input placeholder="Price Tag" type="text" name="price_tags[]"   class="form-control"/>
                        </div>
                    </div>  
                @endforelse
            </div>


            <div class="col-12 form-group floatlabel mb-4">
                <label class="label" for="ing-warn">Ingredient Warnings</label>
                <select class="form-control no-background" multiple name="ingredient_warning_ids[]" id="ingredient_warning_id">
                    <option value="" disabled></option>
                    @foreach($ingredientWarnings as $iw)
                    <option value="{{ $iw->id }}" {{ (count($itemIngredientWarnings) && in_array($iw->id, $itemIngredientWarnings->pluck('id')->toArray()) )? 'selected' : '' }}>{{ $iw->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-6 col-12">
            <div class="uploaded-pic">
                <img id="imageDiv" src="{{$current_item->FullImagePath()}}">
                <button class="change-pic-btn">Change Picture</button>
                <input id="uploadImage" name="image" type="file" class="custom-file-input">
            </div>

            <div class="uploaded-video">
                <!-- <iframe id="vidDiv" src="" width="" height=""></iframe> -->
                <input id="uploadVideo" type="file" name="video" class="custom-file-input">
                <button class="plain-btn btn-block">
                  <i class="fas fa-film"></i> Add a video
                </button>
            </div>

            <div class="form-group col-12 floatlabel my-4">
                <label class="label" for="notes">Note</label>
                <textarea name="notes" rows="5" id="notes" class="form-control">{{$current_item->notes}}</textarea>
            </div>
        </div>
    </div>


    <h5 class="mb-3" style="margin-left:10px;">Side Dishes
        <button type="button" class="addDish float-right btn-link btn btn-sm">
          <i class="fas fa-plus"></i>
        </button>
    </h5>

    <!-- SIDE DISHES -->
    <div class="dishAppend">
        @forelse($current_item['sideDishes'] as $index => $sideDish)
        <div class="dishRow">
            <div class="form-group col-5 floatlabel mb-4">
                <select class="form-control" name="side_dishes[{{$index}}][id]">
                    <option value="">Select Side Dish</option>
                    @foreach($sideDishes as $intiSideDish)
                    <option value="{{$intiSideDish->id}}" {{$intiSideDish->id == $sideDish->side_dish_id ? 'selected' : ''}}>{{$intiSideDish->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-5 floatlabel mb-4">
                <input type="number" min="1" placeholder="Added Price" name="side_dishes[{{$index}}][price]" id="added-price-1" class="form-control" value="{{$sideDish->price}}"/>
            </div>
            <div class="form-group col-2 mb-4">
                <button type="button" class="plain-btn float-right text-danger removeItem">
                  <i class="far fa-trash-alt"></i>
                </button>
            </div>
        </div>
        @empty
        <div class="dishRow">
            <div class="form-group col-5 floatlabel mb-4">
                <select class="form-control sidedishselect" name="side_dishes[0][id]">
                    <option value="">Select Side Dish</option>
                    @foreach($sideDishes as $sideDish)
                    <option value="{{$sideDish->id}}" style="background-image:url({{$sideDish->FullThumbnailImagePath()}});" data-price="{{ $sideDish->price }}">{{$sideDish->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-5 floatlabel mb-4">
                <input type="number" min="1" placeholder="Added Price" name="side_dishes[0][price]" id="added-price-1" class="form-control"/>
            </div>
            <div class="form-group col-2 mb-4">
                <button type="button" class="plain-btn float-right text-danger removeItem">
                  <i class="far fa-trash-alt"></i>
                </button>
            </div>
        </div>
        @endforelse
    </div>
    <!-- END SIDE DISHES -->

    <h5 class="mb-3" style="margin-left: 12px;">Options
        <button type="button"  class="addOption float-right btn-link btn btn-sm">
          <i class="fas fa-plus"></i>
        </button>
    </h5>
    <!-- OPTIONS -->
    <div class="optionsSet">

        @foreach ($options as $index => $option)
            <div class="optionAppend">
                <div class="optionRow">
                    <div class="form-group col-5 floatlabel mb-4">
                        <select type="text" name="options[{{ $option->option_id }}][id]" id="option0" class="form-control Selectoption">
                            <option value="">Select Option</option>                                                
                            @foreach($availableOptions as $availableOption)
                            <option value="{{  $availableOption->id}}" {{ $option->option_id == $availableOption->id ? 'selected' : ''}}>{{  $availableOption->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-5 floatlabel mb-4">
                        <select id="onSelectOption" disabled class="form-control no-background" name="options[{{  $index}}][type]" id="option1_type">
                            <option value="Checkbox" {{ $option->option->type == 'Checkbox'? 'selected' :'' }}>Checkboxes</option>
                            <option value="Slider" {{ $option->option->type == 'Slider'? 'selected' :'' }}>Slider</option>
                        </select>
                    </div>
                    <div class="form-group col-2 mb-4">
                        <button type="button" class="plain-btn float-right text-danger removeOption"><i class="far fa-trash-alt"></i></button>
                    </div>
                </div>
                <div class="sliderOption onSlider">
                    <div class="row m-0">
                            @if($option->option->type == 'Slider')
                                @foreach ($option->optionValues as $value)
                                    <div class="form-group col-6 floatlabel mb-4">
                                        <label class="label" for="option{{ $option->option_id }}_value{{ $value->id }}">{{ $value->value }}</label>
                                        <input type="text" name="options[{{ $option->option_id }}][values][]" id="option{{ $option->option_id }}_value{{ $value->id }}" class="form-control" value="{{ $value->value }}"/>
                                    </div>
                                @endforeach
                                
                            @else
                                @foreach ($option->optionValues as $value)
                                    <div class="form-group col-6 floatlabel mb-4">
                                        <label class="label" for="option{{ $option->option_id }}_value{{ $value->id }}">{{ $value->value }}</label>
                                        <input type="text" name="options[{{ $option->option_id }}][values][]" id="option{{ $option->option_id }}_value{{ $value->id }}" class="form-control" value="{{ $value->value }}"/>
                                    </div>
                                    <div class="form-group col-6 floatlabel mb-4">
                                        <label class="label" for="option{{ $option->option_id }}_value{{ $value->id }}">Price</label>
                                        <input type="number" min="1" name="options[{{ $option->option_id }}][prices][]" id="option{{ $option->option_id }}_value{{ $value->id }}" class="form-control" value="{{ $value->price }}"/>
                                    </div>
                                @endforeach
                            @endif
                    </div>
                </div>
            </div>
        @endforeach

        @unless (count($options))
        <div class="optionAppend">
            <div class="optionRow">
                <div class="form-group col-5 floatlabel mb-4">
                    <select type="text" name="options[0][id]" id="option0" class="form-control">
                        <option value="">Select Option</option>                                                                        
                        @foreach($availableOptions as $availableOption)
                        <option value="{{$availableOption->id}}">{{$availableOption->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-5 floatlabel mb-4">
                    <select id="onSelectOption" class="form-control no-background" id="option1_type">
                        <option value="Checkbox">Checkboxes</option>
                        <option value="Slider">Slider</option>
                    </select>
                </div>
                <div class="form-group col-2 mb-4">
                    <button type="button" class="plain-btn float-right text-danger removeOption"><i class="far fa-trash-alt"></i></button>
                </div>
            </div>
            <div class="sliderOption onSlider">
                <div class="row m-0">
                </div>
            </div>
        </div>
        @endunless

    </div>
    <!-- END OPTIONS -->
    <!-- Recommended -->

    <h5 class="mb-3" style="margin-left:10px;">Recommended Item
        <button type="button" class="addrecommend float-right btn-link btn btn-sm">
            <i class="fas fa-plus"></i>
        </button>
    </h5>
        
    <div class="recommendDiv">
            <div class="recommendRow">
                <div class="form-group col-4 floatlabel mb-4">
                    <select class="form-control sidedishselect select2" multiple name="recommend[]">
                        <option value="" disabled>Select Item</option>
                        @foreach($items as $item)
                        <option value="{{ $item->id}}" {{ (count($current_item->recommendeditems) && in_array($item->id, $current_item->recommendeditems->pluck('id')->toArray()))? 'selected' : '' }} style="background-image:url({{  $item->FullThumbnailImagePath()}});" >{{$item->name}}</option>
                        @endforeach
                    </select>
    
                </div>
                <div class="form-group col-2 mb-4">
                    <button type="button" class="plain-btn float-right removerecItem text-danger">
                        <i class="far fa-trash-alt"></i>
                    </button>
                </div>
            </div>
        
    </div>
    <!-- END Recommended -->

    <div class="form-group text-center mt-5">
        <button type="submit" class="dark-btn">EDIT ITEM</button>
        <button type="button" data-toggle="modal" data-target="#deactivateAlert" id="active_deactivate_modal_btn"
                class="plain-btn text-muted"><i
                class="far fa-eye-slash"></i> {{$current_item->is_active == 1 ? 'HIDE' : 'SHOW'}}
        </button>
        <button type="button" data-toggle="modal" data-target="#delAlert"
                class="plain-btn text-danger"><i class="far fa-trash-alt"></i> DELETE
        </button>
    </div>

    {{Form::close()}}
@stop




@section('scripts')

    <!-- Deactivate Alert -->
    <div class="modal fade" id="deactivateAlert" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content text-center">
            <div class="modal-body">
                <h5 class="text-center">ARE YOU SURE?</h5>
                <p id="modal_msg">Are you sure you want
                    to {{$current_item->is_active == 1 ? 'HIDE' : 'SHOW'}} {{$current_item->name}}?</p>

            </div>
            <div class="modal-footer">
                <button type="button" class="plain-btn" data-dismiss="modal">CANCEL</button>
                <button type="button" id="active_deactivate_btn"
                        class="dark-btn">{{$current_item->is_active == 1 ? 'HIDE' : 'SHOW'}}
                    ITEM
                </button>
            </div>
        </div>
    </div>
    </div>

    <!-- Delete Alert -->
    <div class="modal fade" id="delAlert" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog" role="document">
        {{Form::open(['route'=>['diner.item.destroy', $current_item->menu_id, $current_item->id] , 'method'=>'delete'])}}

        <div class="modal-content text-center">
            <div class="modal-body">
                <h5 class="text-center">ARE YOU SURE?</h5>
                <p class="m-0">Are you sure you want to delete {{$current_item->name}}?</p>
                <small class="text-danger">This cannot be undone</small>
            </div>
            <div class="modal-footer">
                <button type="button" class="plain-btn" data-dismiss="modal">CANCEL</button>
                <button type="submit" class="dark-btn">DELETE ITEM</button>
            </div>
        </div>

        {{Form::close()}}
    </div>
    </div>

    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
    <script>
      $(document).ready(function () {
          $("#display_as").trigger("change");
      });

      $.validate({modules: 'security'});

      $('#display_as').change(function () {
          val = $(this).val();
          if (val == 'grid') {
              $('#num_of_rows').attr('data-validation', 'required');
              $('#grid_title_position').attr('data-validation', 'required');
              $('#num_of_rows').removeClass('d-none');
              $('#grid_title_position').removeClass('d-none');
          } else {
              $('#num_of_rows').val('');
              $('#grid_title_position').val('');
              $('#num_of_rows').addClass('d-none');
              $('#grid_title_position').addClass('d-none');
              $('#num_of_rows').removeAttr('data-validation');
              $('#grid_title_position').removeAttr('data-validation');
          }
      })


      $('#active_deactivate_btn').click(function () {
          $.ajax({
              method: "POST",
              url: "{{URL::to('diner/ajax/item/hide_show')}}",
              data: {
                  _token: "{{csrf_token()}}",
                  id: "{{$current_item->id}}"
              }
          }).done(function (data) {

              var data = jQuery.parseJSON(data);
              if (data.status == 2) {
                  html = '<div class="alert alert-danger text-center"> +data.error+</div>';
                  $('#active_deactivate_btn .modal-body').append(html);

              } else if (data.status == 1) {

                  html = data.data.is_active == 1 ? 'HIDE' : 'SHOW';

                  $('#active_deactivate_modal_btn').html('<i class="far fa-eye-slash"></i>' + html);
                  $('#active_deactivate_btn').html(html + ' MENU');
                  $('#active_deactivate_btn .modal-body .alert-danger').remove();
                  $('#deactivateAlert').modal('hide');
                  $('#modal_msg').text("Are you sure you want to " + html + " {{$current_item->name}}?");

              }
          });
      });

      $("#uploadImage").on("change", function(){
          // Get a reference to the fileList
          var files = !!this.files ? this.files : [];
          // If no files were selected, or no FileReader support, return
          if ( !files.length || !window.FileReader ) return;
          // Only proceed if the selected file is an image
          if ( /^image/.test( files[0].type ) ) {
              // Create a new instance of the FileReader
              var reader = new FileReader();
              // Read the local file as a DataURL
              reader.readAsDataURL( files[0] );

              // When loaded, set image data as background of page
              reader.onloadend = function(){
                  $("#imageDiv").attr("src", this.result);
              }
          }
      });
    </script>

    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
    <script>

        $(document).ready(function () {
            $("#display_as").trigger("change");
        });

        $.validate({modules: 'security'});

        var dishIndex = {{count($current_item['sideDishes'])-1}}, optionIndex = {{count($options)-1}};

        // add dish
        $(".addDish").click(function () {
            dishIndex = dishIndex + 1;
            let clonedDish =
            `<div class="dishRow">
                <div class="form-group col-5 floatlabel mb-4">
                    <select name="side_dishes[`+dishIndex+`][id]" id="side_dishes_id_`+dishIndex+`" class="form-control sidedishselect">
                      <option value="">Select Side Dish</option>
                      @foreach($sideDishes as $sideDish)
                        <option value="{{$sideDish->id}}" data-price="{{ $sideDish->price }}">{{$sideDish->name}}</option>
                      @endforeach
                    </select>
                </div>
                <div class="form-group col-5 floatlabel mb-4">
                    <input placeholder="Add Price" type="number" min="1" name="side_dishes[`+dishIndex+`][price]" id="side_dishes_price_`+dishIndex+`" class="form-control"/>
                </div>
                <div class="form-group col-2 mb-4">
                    <button type="button" class="plain-btn float-right removeItem text-danger">
                      <i class="far fa-trash-alt"></i>
                    </button>
                </div>
            </div>`;
            $(".dishAppend").append(clonedDish);
            initFloatLabels();

            // remove dish
            $(".removeItem").click(function () {
                this.closest('div.dishRow').remove();
            })
        });

        

        // add option
        $(".addOption").click(function () {
            optionIndex = optionIndex + 1;
            let clonedOption =
                `<div class="optionAppend">
                    <div class="optionRow">
                        <div class="form-group col-5 floatlabel mb-4">
                        <select type="text" name="options[`+optionIndex+`][id]" id="option`+optionIndex+`" class="form-control Selectoption">
                          @foreach($availableOptions as $option)
                          <option value="{{ $option->id}}">{{ $option->name}}</option>
                          @endforeach
                        </select>
                        </div>
                        <div class="form-group col-5 floatlabel mb-4">
                            <select id="onSelectOption" disabled class="form-control no-background" name="options[`+optionIndex+`][type]" id="option1_type">

                                <option value="Checkbox">Checkboxes</option>
                                <option value="Slider">Slider</option>
                            </select>
                        </div>
                        <div class="form-group col-2 mb-4">
                            <button type="button" class="plain-btn float-right removeOption text-danger"><i class="far fa-trash-alt"></i></button>
                        </div>
                    </div>
                    <div class="sliderOption onSlider">
                        <div class="row m-0">
                        </div>
                    </div>
                </div>`

            $(".optionsSet").append(clonedOption);
            initFloatLabels();
            $('.optionsSet').children().last().children().children().children().change();
            // remove dish
            
        });

        //Add price tag
        $(".addpricetag").click(function () {
            dishIndex = dishIndex + 1;
            let clonedDish =
            `<div class="row m-0 pricetagRow">
                <div class="form-group col-5 floatlabel ">
                    <input placeholder="Price" type="text" name="prices[]"   class="form-control"/>
                </div>
                <div class="form-group col-5 floatlabel ">
                    <input placeholder="Price Tag" type="number" min="1" name="price_tags[]"   class="form-control"/>
                </div>
                <div class="form-group col-2 ">
                    <button type="button" class="plain-btn float-right removerecpricetag text-danger">
                        <i class="far fa-trash-alt"></i>
                    </button>
                </div>
            </div>`;

            $(".pricetagDiv").append(clonedDish);
            initFloatLabels();

            // remove dish
            
        });
        $(document).on('click', ".removerecpricetag", function () {
            this.closest('div.pricetagRow').remove();
        })


        //Add recommended
        // $(".addrecommend").click(function () {
        //     dishIndex = dishIndex + 1;
        //     let clonedDish =
        //     `<div class=" recommendRow">
        //         <div class="form-group col-4 floatlabel mb-4">
        //             <select required class="form-control sidedishselect select2" multiple name="recommend[]">
        //                 <option value="">Select Item</option>
        //                 @foreach($items as $item)
        //                 <option value="{{ $item->id}}" style="background-image:url({{  $item->FullThumbnailImagePath()}});" >{{$item->name}}</option>
        //                 @endforeach
        //             </select>

        //         </div>
        //         <div class="form-group col-2 mb-4">
        //             <button type="button" class="plain-btn float-right removerecItem text-danger">
        //                 <i class="far fa-trash-alt"></i>
        //             </button>
        //         </div>
        //     </div>`;

        //     $(".recommendDiv").append(clonedDish);
        //     initFloatLabels();

        //     // remove dish
        //     $(".removerecItem").click(function () {
        //         this.closest('div.recommendRow').remove();
        //     })
        // });
        $(document).on('click', ".removeOption", function () {
            this.closest('div.optionAppend').remove();
        })

        $("#uploadImage").on("change", function(){
            // Get a reference to the fileList
            var files = !!this.files ? this.files : [];
            // If no files were selected, or no FileReader support, return
            if ( !files.length || !window.FileReader ) return;
            // Only proceed if the selected file is an image
            if ( /^image/.test( files[0].type ) ) {
                // Create a new instance of the FileReader
                var reader = new FileReader();
                // Read the local file as a DataURL
                reader.readAsDataURL( files[0] );
                // When loaded, set image data as background of page
                reader.onloadend = function(){
                    $("#imageDiv").attr("src", this.result);
                }
            }
        });


        $("#uploadVideo").on("change", function(){
            // Get a reference to the fileList
            var files = !!this.files ? this.files : [];
            // If no files were selected, or no FileReader support, return
            if ( !files.length || !window.FileReader ) return;
            // Only proceed if the selected file is an image
            if ( /^image/.test( files[0].type ) ) {
                // Create a new instance of the FileReader
                var reader = new FileReader();
                // Read the local file as a DataURL
                reader.readAsDataURL( files[0] );
                // When loaded, set image data as background of page
                reader.onloadend = function(){
                    $("#vidDiv").attr("src", this.result);
                }
            }
        });

        // on select an option
        // $("#onSelectOption").on('change', (el) = > {
        //     if(el.target.value === 'slider')
        // {
        //     $(".onSlider").show();
        //     $(".onCheckBox").hide()
        // }
        // else
        // if (el.target.value === 'checkbox') {
        //     $(".onSlider").hide();
        //     $(".onCheckBox").show()
        // }
        // })
    </script>
    <script>
        $(document).on('change', '.sidedishselect', function(event){
            var price = $('option:selected', this).attr('data-price');
            $(this).parent().next().children().val(price);
        })
    </script>
    <script>
        $(document).on('change', '.Selectoption', function(event){
            var option = $(this).val();
            var that = $(this);
            $.get('/diner/getoption/'+ option, function(data){  
                that.parent().next().children().val(data.type);
                that.parent().next().children().change();
                that.attr('name','options['+data.id+'][id]')
                that.parent().next().children().attr('name','options['+data.id+'][type]');
                if(data.type == 'Slider'){
                    AppendSliderValue(data.values, that);
                }
                else{
                    AppendCheckboxValue(data.values, that);

                }
            });
        });

        function AppendSliderValue(values, div){
            var html = '';
            for(var i = 0 ; i < values.length; i++){
                html += '<div class="form-group col-6 floatlabel mb-4">'+
                            '<label class="label" for="option'+values[i].option_id+'_value_'+values[i].id+'">'+ values[i].value +'</label>'+
                            '<input required type="text" id="option'+values[i].option_id+'_value_'+values[i].id+'" name="options[' + values[i].option_id + '][values][]" class="form-control"/>'+
                        '</div>';
            }
            div.parent().parent().next().children().html('');
            div.parent().parent().next().children().append(html);
            
        }
        function AppendCheckboxValue(values, div){
            var html = '';
            for(var i = 0 ; i < values.length; i++){
                html += '<div class="form-group col-6 floatlabel mb-4">'+
                            '<label class="label" for="option'+values[i].option_id+'_value_'+values[i].id+'">' + values[i].value + '</label>'+
                            '<input required type="text" id="option'+values[i].option_id+'_value_'+values[i].id+'" name="options[' + values[i].option_id + '][values][]"  class="form-control"/>'+
                        '</div>'+
                        '<div class="form-group col-6 floatlabel mb-4">'+
                            '<label class="label" for="option'+values[i].option_id+'_value_price'+values[i].id+'">Price</label>'+
                            '<input type="number" min="1" id="option'+values[i].option_id+'_value_price'+values[i].id+'" name="options[' + values[i].option_id + '][prices][]" value="'+ values[i].price +'" class="form-control"/>'+
                        '</div>';
            }
            div.parent().parent().next().children().html('');
            div.parent().parent().next().children().append(html);
            
        }
    </script>



@stop
