<!DOCTYPE html>
<html>

@include('diner.layouts.head')

<body>
<div class="text-center align-items-center d-flex form-container">
    @include('diner.layouts.error')
    {{Form::open(array('url'=>array('login') ,'method'=>'POST','class'=>"form-signin"))}}

    <img class="mb-4" src="https://getbootstrap.com/docs/4.0/assets/brand/bootstrap-solid.svg" alt="" width="72"
         height="72">
    <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
    <label for="inputEmail" class="sr-only">Email address</label>
    <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" required
           autofocus>
    <label for="inputPassword" class="sr-only">Password</label>
    <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required>
    <div class="checkbox mb-3">
        <label>
            <input type="checkbox" value="1" name="remember"> Remember me
        </label>
    </div>
    <button class="btn btn-lg btn-primary btn-block" type="submit">Sign
        in
    </button>
    {{Form::close()}}

</div>


@include('diner.layouts.scripts')

</body>

</html>