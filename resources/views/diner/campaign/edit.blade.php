@extends('diner.layouts.master')
@section('styling')
    <link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/css/select2.min.css')}}">
@stop
@section('content')
    {{Form::open(array('url'=>array('diner/campaign/'.$current_campaign->id) ,'method'=>'PUT','files'=>'true','enctype'=>'multipart/form-data'))}}

    <div class="row">

        <input type="hidden" name="customer_id" value="{{$customer_id}}" />


        <div class="col-8 form-group floatlabel mb-4">
            <label class="label" for="title">Title</label>
            <input type="text" data-validation="required" name="title" value="{{old('title',$current_campaign->title)}}" id="title"
                   class="form-control"/>
        </div>

        <div class="col-8 form-group floatlabel mb-4">
            <label class="label" for="description">Type a campaign description</label>
            <textarea type="text" name="description" id="description"
                      class="form-control">{{old('description',$current_campaign->description)}}</textarea>
        </div>


        <div class="col-8 form-group mb-6 float-left">
            <label class="label" for="first-name">Automatically end displaying the campaign</label>
            <label class="switch">
                <input type="hidden" id="automatically_end" name="automatically_end" value="1">
                <input type="checkbox" id="automatic_end" {{$current_campaign->automatically_end ? 'checked' : ''}} value="0" class="danger">
                <span class="slider round"></span>
            </label>
        </div>
        <div class="col-8 form-group mb-6" id="end_after_container" style="padding: unset;margin-top: 25px;">
            <label class="label col-md-3 float-left" for="price">End after</label>
            <input id="end_after" type="text" name="end_after" value="{{old('end_after',$current_campaign->end_after)}}" class="form-control col-md-3 float-left"/>
            <label class="label col-md-3 float-left" for="price">Sec</label>
        </div>


        <div class="col-8 form-group mb-6 float-left" style="padding: unset;margin-top: 25px;">
            <label class="label col-md-6 float-left" for="first-name">Campaign will be triggered after:</label>
            <select class="form-control col-md-6 float-left" name="" id="triggered_after_option">
                <option value="time">A certain time</option>
                <option {{$current_campaign->item_id ? 'selected' : ''}} value="item">A product selected</option>
            </select>
        </div>

        <div id="trigger_option_time" class="col-md-12" style="padding: unset;margin-top: 25px;">
            <div class="col-8 form-group mb-6" style="padding: unset;">
                <label class="label col-md-3 float-left" for="triggered_after">Campaign starts after</label>
                <input id="triggered_after" type="text" name="triggered_after" value="{{old('triggered_after',$current_campaign->triggered_after)}}" class="form-control col-md-3 float-left"/>
                <label class="label col-md-3 float-left" for="price">Sec</label>
            </div>
        </div>

        <div id="trigger_option_time" class="col-md-12" style="padding: unset;margin-top: 25px;">
            <div class="col-8 form-group mb-6" id="end_after_container" style="padding: unset;">
                <label class="label col-md-3 float-left" for="repetation">Campaign Repetation</label>
                <input  type="text" name="repetation" value="{{old('repetation',$current_campaign->repetation)}}" class="form-control col-md-3 float-left"/>
                <label class="label col-md-3 float-left" for="repetation">Times</label>
            </div>
        </div>
        <div id="trigger_option_item" class="col-md-12" style="padding: unset;margin-top: 25px;">
            <div class="col-8 form-group mb-6" style="padding: unset;">
                <label class="label col-md-3 float-left" for="price">Campaign starts when</label>
                <select class="form-control col-md-6 float-left" name="item_id" id="item_id">
                    @foreach($items as $item)
                    <option {{$current_campaign->item_id == $item->id ? 'selected' : ''}} value="{{$item->id}}">{{$item->name}}</option>
                    @endforeach
                </select>
                <label class="label col-md-3 float-left" for="price">Selected</label>
            </div>
        </div>

        <div class="col-md-12" style="padding: unset;margin-top: 25px;">
            <div class="col-8 form-group mb-6" style="padding: unset;">
                <label class="label col-md-3 float-left" for="price">Content</label>
                <select id="content_option" class="form-control col-md-6 float-left" name="content_type" id="content">
                    {{-- <option value="url">URL</option> --}}
                    <option value="image">Image</option>
                    <option value="video">Video</option>
                </select>
            </div>
        </div>

        {{-- <div id="content_option_url" class="col-md-12" style="padding: unset;margin-top: 25px;">
            <div class="col-8 form-group mb-6" style="padding: unset;">
                <label class="label col-md-3 float-left" for="price">URL</label>
                <input id="content_option_url_content" type="text" name="url" value="{{old('url',$current_campaign->url)}}" class="form-control col-md-9 float-left"/>
            </div>
        </div> --}}
        <div id="content_option_image" class="col-md-12" style="padding: unset;margin-top: 25px;">
            <div class="col-8 form-group mb-6" style="padding: unset;">
                <label class="label col-md-3 float-left" for="price">Image</label>
                <img id="imageDiv" src="">
                <input id="content_option_image_content" name="image" type="file" class="form-control">
            </div>
        </div>
        <div id="content_option_video" class="col-md-12" style="padding: unset;margin-top: 25px;">
            <div class="col-8 form-group mb-6" style="padding: unset;">
                <label class="label col-md-3 float-left" for="price">Video</label>
                <img id="imageDiv" src="">
                <input id="content_option_video_content" name="video" type="file" class="form-control">
            </div>
        </div>

        <div class="col-2 form-group mb-6 float-left" style="padding: unset;margin-top: 50px;">
            <label class="label col-md-3" for="first-name">Publish</label>
            <label class="switch">
                <input type="hidden" name="active" id="active" value="1">
                <input type="checkbox" id="publish" {{$current_campaign->active ? 'checked' : ''}} value="1" class="danger col-md-3">
                <span class="slider round"></span>
            </label>
        </div>

    </div>
    <div class="form-group text-center mt-5">
        <button type="submit" class="dark-btn">SAVE CAMPAIGN</button>
    </div>
    {{Form::close()}}
@stop

@section('scripts')
    <style>
        /* #content_option_image {
            {{!$current_campaign->image_id ? 'display:none;' : ''}}
        } */
        #content_option_video{
            {{!$current_campaign->video_id ? 'display:none;' : ''}}
        }
        #trigger_option_time {
            {{!$current_campaign->triggered_after ? 'display:none;' : ''}}
        }
         #trigger_option_item {
            {{!$current_campaign->item_id ? 'display:none;' : ''}}
        }
    </style>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
    <script src="{{asset('assets/js/select2.full.min.js')}}"></script>
    <script>

        $.validate({modules: 'security'});

    </script>

    <script>
        $('#content_option').on('change',function(){
            let content = $('#content_option').val();
            switch(content) {
                case 'url' : {
                    $('#content_option_url').css('display','inline-block');
                    $('#content_option_image').css('display','none');
                    $('#content_option_video').css('display','none');

                    $('#content_option_image_content').val('');
                    $('#content_option_video_content').val('');

                }   break;
                case 'image' : {
                    $('#content_option_image').css('display','inline-block');
                    $('#content_option_url').css('display','none');
                    $('#content_option_video').css('display','none');

                    $('#content_option_url_content').val('');
                    $('#content_option_video_content').val('');
                }   break;
                case 'video' : {
                    $('#content_option_video').css('display','inline-block');
                    $('#content_option_url').css('display','none');
                    $('#content_option_image').css('display','none');

                    $('#content_option_url_content').val('');
                    $('#content_option_image_content').val('');
                }   break;
            }
        });
        $('#triggered_after_option').on('change',function(){
            let trigger = $('#triggered_after_option').val();
            switch(trigger) {
                case 'time' : {
                    $('#trigger_option_time').css('display','inline-block');
                    $('#trigger_option_item').css('display','none');

                    $('#item_id').val('');
                }   break;
                case 'item' : {
                    $('#trigger_option_item').css('display','inline-block');
                    $('#trigger_option_time').css('display','none');

                    $('#triggered_after').val('');
                }   break;
            }
        });

        var automatic_end = $('#automatic_end').is(":checked") ? '1' : '0';
        $('#automatically_end').val(automatic_end);
        $('#automatic_end').on('click',function(){
            automatic_end = $('#automatic_end').is(":checked") ? '1' : '0';
            $('#automatically_end').val(automatic_end);

            if(automatic_end === '0') {
                $('#end_after_container').css('display','none');
                $('#end_after').val(0);
            } else {
                $('#end_after_container').css('display','inline-block');
            }
        });

        var active = $('#publish').is(":checked") ? '1' : '0';
        $('#active').val(active);
        $('#publish').on('click',function(){
            active = $('#publish').is(":checked") ? '1' : '0';
            $('#active').val(active);
        });

    </script>

@stop
