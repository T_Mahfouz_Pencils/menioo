@extends('diner.layouts.master')

@section('content')

<div class="container">
        {{ Form::open(array('url'=>array('diner/menu?venue_id='.$venue_id) ,'method'=>'POST','files'=>'true','enctype'=>'multipart/form-data'))}}
            <div class="row row-title">
                
                <div class="col-md-12 text-left">
                    <i class="fas fa-chevron-left"></i>&nbsp; Menus &nbsp; 
                    <i class="fas fa-chevron-left"></i>&nbsp; {{ $current_menu->name }}
                </div>
                <div class="col-md-6 text-left"><h6>Duplicate Menu</h6></div>
                <div class="col-md-6 text-right">
                    {{-- <i class="fa fa-search"></i>  --}}
                    <a id="active_deactivate_btn" class="fa-eye-slas" data-id="{{ $current_menu->id }}"><i class="fas {{ !$current_menu->is_active ? 'fa-eye':'fa-eye-slash' }}"></i> {{  $current_menu->is_active == 1 ? 'HIDE' : 'SHOW'}}</a>&nbsp;&nbsp;&nbsp;&nbsp;
                    <a data-toggle="modal" data-target="#delAlert" class="text-danger"><i class="fas fa-trash-alt"></i> DELETE</a>&nbsp;&nbsp;&nbsp;&nbsp;
                    <button class="btn btn-primary" type="submit">Save Menu</button>
                </div>
            </div>
            <div class="row menus-fBox">
                <div class="col-md-7">
                    <div class="create-inp">
                        <label for="">Menu Name</label>
                        <input type="text" name="name" value="{{ $current_menu->name }}"
                        id="menu-name" placeholder="Add Menu title" class="form-control">
                    </div>
                    <div class="create-inp">
                        <label for="">Description</label>
                        <textarea name="description" placeholder="Type a menu description" class="form-control">{{ $current_menu->description }}</textarea>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="create-inp">
                                <label for="">Display as</label>
                                <br>
                                <select name="display_as" id="display_as">
                                    <option value="" class="form-control" disabled>select option</option>
                                    <option value="grid" {{ $current_menu->display_as=='grid' ? 'selected':''}}>Grid</option>
                                    <option value="list" {{ $current_menu->display_as=='list' ? 'selected':''}}>List</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6" id="num_of_rows">
                            <div class="create-inp">
                                <label for="">Number of Colums</label>
                                <br>
                                <select name="num_of_rows" id="">
                                    <option value="" disabled selected>Number of Columns</option>
                                    <option value="2" {{ $current_menu->num_of_rows=='2' ? 'selected':''}}>2</option>
                                    <option value="4" {{ $current_menu->num_of_rows=='4' ? 'selected':''}}>4</option>
                                    <option value="6" {{ $current_menu->num_of_rows=='6' ? 'selected':''}}>6</option>
                                    <option value="8" {{ $current_menu->num_of_rows=='8' ? 'selected':''}}>8</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="create-inp">
                        <label for="">Crid View Title Position</label>
                        <br>
                        <select name="grid_title_position" id="grid_title_position">
                            <option value="" disabled selected>Grid View Title Position</option>
                            <option value="top" {{ $current_menu->grid_title_position=='top' ? 'selected':''}}>Top</option>
                            <option value="bottom" {{ $current_menu->grid_title_position=='bottom' ? 'selected':''}}>Bottom</option>
                        </select>
                    </div>
                    <br>
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-9 col-10">
                                <h6>Display Similar Items</h6>
                                <div style="margin-top: -10px;">
                                    <small>Display other Items of the Category on each item Page</small>
                                </div>
                            </div>
                            <div class="col-md-3 col-2 text-right">
                                <input type="checkbox" name="display_similar_items"
                                {{ $current_menu->display_similar_items == 1? 'checked':''}} value="1" class="checkboxx">
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-5 col-6">
                        <div class="row">
                            <div class="col-md-9 col-11">
                                <h6>Mark Menu as New</h6>
                            </div>
                            <div class="col-md-3 col-1"><input name="mark_as_new" value="1"
                                id="mark_as_new" {{ $current_menu->mark_as_new == 1? 'checked':''}} type="checkbox" class="checkboxx"></div>
                        </div>
                    </div>
                    <div class="col-md-7 col-6 text-right">
                        <div class="row">
                            <div class="col-md-8 col-11">
                                <h6>Mark Menus as signature</h6>
                            </div>
                            <div class="col-md-4 col-1"><input name="mark_as_signature" value="1"
                                id="mark_as_signature" {{ $current_menu->mark_as_signature == 1? 'checked':''}} type="checkbox" class="checkboxx"></div>
                        </div>
                    </div>
                </div>
                <div class="create-inp">
                        <label for="">Add additional Notes</label>
                        <textarea name="additional_notes" placeholder="Type any Notes" class="form-control">{{ $current_menu->additional_notes }}</textarea>
                    </div>
                </div>
                
                <div class="col-md-5">
                    <div class="show-choose-img" style="position: relative">
                        <img id="imageDiv" src="{{ $current_menu->FullThumbnailImagePath('517X255') }}" width="100%" height="180px" alt="">
                        <div class="overlayy" style="position: absolute; bottom: 0; left: 0; background-color: #40aebe91; width: 100%; height: 25%">
                            <div class="upload" upload-image="" style="background-color: transparent;">
                                <input type="file" id="uploadImage" name="file" class="input-file ng-pristine ng-valid ng-touched" files-model="" ng-model="project.fileList">
                                <label for="files" style="border: none; color: #fff;">
        
                                    <span class="add-image" style="margin: 8px auto; font-size: 15px;">
                                        Change Image
                                    </span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="upload" upload-image="" style="background: transparent">
                        <input type="file" id="files" name="video" class="input-file ng-pristine ng-valid ng-touched" files-model="" ng-model="project.fileList">
                        <label for="files" style="border: none">
                            <span class="add-image" style="color: #47e6fc">
                                <i class="far fa-file-video"></i> Add a Video
                            </span>
                            <output id="list"></output>
                        </label>
                    </div>
                </div>
                
            </div>  
            <br><br><br>
        {{ Form::close() }}
        <!--    MODEL  -->
        <!-- Modal -->
         
    </div>
@stop

@section('scripts')
    <!-- Deactivate Alert -->
    
    <!-- Delete Alert -->
    <div class="modal fade" id="delAlert" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            {{Form::open(['route'=>['diner.menu.destroy' , $current_menu->id ] , 'method'=>'delete'])}}

            <div class="modal-content text-center">
                <div class="modal-body">
                    <h5 class="text-center">ARE YOU SURE?</h5>
                    <p class="m-0">Are you sure you want to delete {{$current_menu->name}}?</p>
                    <small class="text-danger">This cannot be undone</small>
                </div>
                <div class="modal-footer">
                    <button type="button" class="plain-btn" data-dismiss="modal">CANCEL</button>
                    <button type="submit" class="dark-btn">DELETE MENU</button>
                </div>
            </div>

            {{Form::close()}}
        </div>
    </div>

    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>

    <script>
        $(document).ready(function () {
            $("#display_as").trigger("change");
        });

        $.validate({modules: 'security'});

        $('#display_as').change(function () {
            val = $(this).val();
            if (val == 'grid') {
                $('#num_of_rows').attr('data-validation', 'required');
                $('#grid_title_position').attr('data-validation', 'required');
                $('#num_of_rows').removeClass('d-none');
                $('#grid_title_position').removeClass('d-none');
            } else {
                $('#num_of_rows').val('');
                $('#grid_title_position').val('');
                $('#num_of_rows').addClass('d-none');
                $('#grid_title_position').addClass('d-none');
                $('#num_of_rows').removeAttr('data-validation');
                $('#grid_title_position').removeAttr('data-validation');
            }
        })


        $('#active_deactivate_btn').click(function () {
            $.ajax({
                method: "POST",
                url: "{{URL::to('diner/ajax/menu/hide_show')}}",
                data: {
                    _token: "{{csrf_token()}}",
                    id: "{{$current_menu->id}}",
                }
            }).done(function (data) {
                location.reload();
            });
        });
        

        $("#uploadImage").on("change", function(){
            // Get a reference to the fileList
            var files = !!this.files ? this.files : [];
            // If no files were selected, or no FileReader support, return
            if ( !files.length || !window.FileReader ) return;
            // Only proceed if the selected file is an image
            if ( /^image/.test( files[0].type ) ) {
                // Create a new instance of the FileReader
                var reader = new FileReader();
                // Read the local file as a DataURL
                reader.readAsDataURL( files[0] );

                // When loaded, set image data as background of page
                reader.onloadend = function(){
                    $("#imageDiv").attr("src", this.result);
                }
            }
        });
    </script>


@stop
