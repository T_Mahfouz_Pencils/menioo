<div class="modal fade" id="MoveModal" tabindex="-1" role="dialog" aria-labelledby="MoveModalLabel" aria-hidden="true">
        <br><br><br><br>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header import-head-modal">
                <h4><b>Move</b></h4>
                </div>
                {{Form::open(array('url'=>route('diner.menu.move',['venue_id'=>$venue_id]) ,'method'=>'POST','files'=>'true','enctype'=>'multipart/form-data'))}}
                    <div class="modal-body create-menu-import">
                        <input type="hidden" name="menu_id" id="menu_id_input" >
                        <input type="hidden" name="type" id="type_input" >
                        <div class="create-inp">
                            <label for=""><b>Select a Menu</b></label>
                            <br>
                            <select name="menu_id" id="menu_import">
                                @foreach ($current_user->Menus as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }} ({{ $item->MenuParent->name ?? 'Main Menu' }})</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-dafult" data-dismiss="modal" style="color: #ccc; font-weight: 500; letter-spacing: 1px">Cancel</button>
                        <button type="submit" class="btn btn-primary" style="padding: 10px 30px"><a href="importmenu.html">Import</a></button>
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>