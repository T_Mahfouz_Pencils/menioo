<header>
    <nav class="navbar navbar-expand-md navbar-dark fixed-top">
        <a class="navbar-brand ml-5" href="{{route('landing.index') }}"><img src="{{asset('landing/images/logo.png')}}"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav m-auto">
                <li class="nav-item {{ request()->route()->getName() == "landing.features" ? 'active' : '' }}">
                    <a class="nav-link mr-3" href="{{route('landing.features') }}">Features</a>
                </li>
                <li class="nav-item {{ request()->route()->getName() == "landing.pricing" ? 'active' : '' }}">
                    <a class="nav-link mr-3" href="{{route('landing.pricing') }}">Pricing</a>
                </li>
                <li class="nav-item {{ request()->route()->getName() == "landing.blog" ? 'active' : '' }}">
                    <a class="nav-link mr-3" href="{{route('landing.blog') }}">Blog</a>
                </li>
                <li class="nav-item {{ request()->route()->getName() == "landing.about" ? 'active' : '' }}">
                    <a class="nav-link" href="{{route('landing.about') }}">About</a>
                </li>
            </ul>

            <ul class="d-flex align-items-center mr-5">
                @if(auth()->guard('diner')->check())
                    <li><a class="btn  btn-white py-0 px-4 font-weight-bold" href="{{url('diner/menu') }}">Console</a></li>
                    <li><a class="text-theme ml-4" href="{{url('diner/logout') }}">Log out</a></li>
                @else
                    <li><a class="text-light mr-4" href="{{route('landing.login') }}">Login</a></li>
                    <li><a class="btn btn-white py-0 px-4 font-weight-bold" href="{{route('landing.sign-up') }}">Get Started</a></li>
                @endif
            </ul>
        </div>
    </nav>
</header>