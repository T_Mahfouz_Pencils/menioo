<footer class="inner-footer p-4"><br>
    <div class="container-fluid pl-4">
        <div class="row foot">
            <div class="col-lg-2 col-md-2 col-sm-4 col-4">
                <img src="{{asset('landing/images/logo.png')}}" width="90%">
            </div>
            <div class="col-lg-2 col-md-2 col-sm-4 col-4">
                <p class="font-weight-bold">Services</p>
                <p><a href="{{ route('landing.features') }}">Features</a></p>
                <p><a href="{{ route('landing.pricing') }}">Pricing Plans</a></p>
                <p><a href="{{ route('landing.about') }}">How it works</a></p>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-4 col-4">
                <p class="font-weight-bold">Company</p>
                <p><a href="{{ route('landing.about') }}"> About us </a></p>
                <p><a href="#"> Contact us </a></p>
                <p><a href="{{ route('landing.privacy') }}"> Privacy Policy </a></p>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-4 col-4">
                <p class="font-weight-bold">Support</p>
                <p><a href="#"> FAQ </a></p>
                <p><a href="#"> Help Center </a></p>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-4 col-4">
                <p class="font-weight-bold">Account</p>
                <p><a href="{{ route('landing.login') }}"> Login </a></p>
                <p><a href="{{ route('landing.sign-up') }}"> Register </a></p>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-4 col-4">
                <a class="btn request px-4 font-weight-bold" href="#">Request a Demo</a>
            </div>
        </div><br>
        <div class="fontawsem">
            <a href="#"><img src="{{asset('landing/images/facebook-btn.png')}}"></a>
            <a href="#"><img src="{{asset('landing/images/instagram-btn.png')}}"></a>
        </div><br>
        <p class="text-center copyRight">Copyright © 2018 KingsTeck OÜ | All Rights Reserved</p>
    </div>
</footer>



