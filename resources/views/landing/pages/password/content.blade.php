<div class="login header-height mb-5">
    <div class="row m-0">
        <div class="col-md-6"></div>
        <div class="col-md-6">
            <h4 class="text-theme text-center mb-4">Forgot your password?</h4>
            <p class="text-theme text-center">Enter your email address and we’ll send you <br> a link to reset your
                password.</p>
        </div>
    </div>
    <div class="row m-0">
        <div class="col-md-1"></div>
        <div class="col-md-5 col-12 mt-5">

            @include('landing.layouts.error')
            {{ Form::open([ 'url' => route('landing.forget.password.request'), 'method' => 'post']) }}
            <div class="form p-5">
                <h4 class="text-theme text-center mb-4">Forget Password</h4>
                <div class="form-group">
                    <label>Email Address<span class="required">*</span></label>
                    <input name="email" type="email" class="form-control" placeholder="James.dean@email.com">
                </div>

                <button class="btn btn-block btn-primary rounded-0">Send Reset Email</button>
                <p class="d-flex align-items-center justify-content-center">
                    <strong>Go back to </strong>
                    <a href="{{ route('landing.login') }}" class="text-theme btn btn-link">Log in</a>
                </p>
            </div>
            {{ Form::close() }}

        </div>
        <div class="col-md-6 col-12 text-right">
            <img src="{{asset('landing/images/header-img.png')}}" class="img-fluid mt-5">
        </div>
    </div>
</div>
