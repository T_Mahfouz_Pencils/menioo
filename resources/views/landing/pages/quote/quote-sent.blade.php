<div class="login header-height mb-5">
    <div class="row m-0 pt-5">
        <div class="col-md-1"></div>
        <div class="col-md-10 col-12 wow zoomIn getQoteForm" data-wow-duration="2s" data-wow-delay="0.4s">
            <h2 class="text-theme text-center mb-3">Your Quote Request Has Been Sent!</h2>
            <p class="text-theme textGetQote text-center mb-5">We will email you with a quote request information</p>
            <a href="{{ route('landing.get.quote') }}" class="btn d-flex mx-auto px-5 btnSend rounded-0"> Get another quote </a>
            <p class="text-theme textGetQote text-center mb-5 mt-3"><a href="{{ route('landing.index') }}">Back to the Homepage</a> </p>
        </div>
    </div>
</div>
