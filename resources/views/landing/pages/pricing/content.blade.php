<!-- content -->
<section class="my-5 p-5">
    <h2 class="text-theme mt-5 text-center">Start using menioo for free for up to three tablets</h2>
    <p class="text-theme mb-5 text-center">Get your menioo now, pick a plan later</p>

    <div class="switcher mb-5">
        <ul>
            <li class="{{ (request()->type && request()->type == 1) ? 'active' : '' }}"><a href="?type=1">Billed Monthly</a></li>
            <li class="{{ (!request()->type || request()->type == 12) ? 'active' : '' }}"><a href="?type=12">Billed Yearly</a></li>
        </ul>
        <span class="notice">Save 20%</small>
            <span class="switch-border"></span>
    </div>


    <div class="row pricing-grid mt-5 align-items-center">

        @foreach ($plans as $plan)
            <div class=" {{count($plans) < 4 ? 'col-md-4' : 'col-md-3'}} col-12 p-0">
                <div class="grid {{ $plan->popular ? 'best-plan':'' }}">
                    @if($plan->popular)
                    <div class="best-plan-badge"><span>Most Popular</span></div>
                    @endif
                    <h2>{{ $plan->name }}</h2>
                    <h3><span>${{ $plan->price }}</span>/{{ $plan->billing_period > 11 ? 'Yr': 'Mo'  }}</h3>
                    <ul>
                        @if($plan->features)
                            @foreach (json_decode($plan->features) as $feature)
                                <li class="{{ isset($feature->active) ? 'included':'' }}">{{ $feature->name }}</li>
                            @endforeach
                        @endif

                        <li class="mt-5 included">
                            <strong>Up to {{ $plan->tablets_number }} Tablets</strong>
                            <small class="d-block v-hidden">$5 per tablet per month</small>
                        </li>

                        @if (\Auth::guard('diner')->check())
                            @if(auth()->guard('diner')->user()->status)
                                <li><a href="#!" class="btn btn-primary px-4 my-5 shadow" onclick="checkout({{ $plan->plan_id }})" data-product="{{ $plan->plan_id }}">Go Starter</a></li>
                            @else
                                <li><a onclick="alert('Check your email to verify your account')" href="" class="btn btn-primary px-4 my-5 shadow">Go {{ $plan->name }}</a></li>
                            @endif
                        @else
                            <li><a href="{{ route('landing.login') }}" class="btn btn-primary px-4 my-5 shadow">Go {{ $plan->name }}</a></li>
                        @endif
                    </ul>
                </div>
            </div>
        @endforeach

    </div>
</section>

<section class="my-5">
    <div class="row shadow shitty-box">
        <div class="col-md-4 col-12">
            <h2 class="text-theme">ENTERPRISE</h2>
            <h6 class="text-theme">As many tablets as you need</h6>
            <a href="{{ route('landing.get.quote') }}" class="btn btn-primary px-5">Get A Quote</a>
        </div>
        <div class="col-md-8 col-12 custom-bg">
            <ul>
                <li><img width="30" src="{{asset('landing/images/check-ico.svg')}}"> Unlimited Menus</li>
                <li><img width="30" src="{{asset('landing/images/check-ico.svg')}}"> Instant Feedback</li>
                <li><img width="30" src="{{asset('landing/images/check-ico.svg')}}"> 24/7 Online Support</li>
                <li><img width="30" src="{{asset('landing/images/check-ico.svg')}}"> Ordering Feature</li>
                <li><img width="30" src="{{asset('landing/images/check-ico.svg')}}"> Free Onboarding</li>
                <li><img width="30" src="{{asset('landing/images/check-ico.svg')}}"> POS Integrations</li>
                <li><img width="30" src="{{asset('landing/images/check-ico.svg')}}"> Menu Building</li>
                <li><img width="30" src="{{asset('landing/images/check-ico.svg')}}"> Customized Fonts</li>
                <li><img width="30" src="{{asset('landing/images/check-ico.svg')}}"> Kitchen Printer Integrations</li>
            </ul>
        </div>
    </div>
</section>


<section id="resources" class="mt-5 p-5">
    <h2 class="text-theme my-3 mx-auto text-center w-75">Download the app on iPad or Android tablet for
        stunning dinning experience for your guests!</h2>

    <ul class="d-flex flex-wrap justify-content-center my-5">
        <li class="mx-3 my-2"><a href="#"><img src="{{asset('landing/images/ios-btn.png')}}"></a></li>
        <li class="mx-3 my-2"><a href="#"><img src="{{asset('landing/images/android-btn.png')}}"></a></li>
    </ul>

</section>


@include('landing.common.signupsection')

@push('scripts')
    <script src="https://cdn.paddle.com/paddle/paddle.js"></script>
    <script type="text/javascript">

        var token = "{{Session::token()}}";
        var url = "{{route('diner.ajax.edit.customer.plan')}}";
        var diner = "{{url('diner/menu')}}";

        Paddle.Setup({ vendor: 52571 });
        function checkout(product_id){
            Paddle.Checkout.open({
                product: product_id,
                successCallback: function(data) {
                    var checkoutId = data.checkout.id;
                    Paddle.Order.details(checkoutId, function(data) {
                        console.log(data);
                    });
                    $.ajax({
                        url: url,
                        method: 'POST',
                        data: {_token: token, 'plan_id': product_id}
                    }).done(function(data) {
                        window.location.href = diner;
                    }).fail(function(error) {
                        alert(error.message)
                    });
                }
            });
        }
    </script>
@endpush
