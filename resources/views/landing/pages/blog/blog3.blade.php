<!-- content -->
<section class="container my-5 p-5">
    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-10">
            <h2 class="text-theme mt-5 text-center">Adding Calorie Count! An up-seller? Or a down-seller?</h2>
            <p class="mt-3 mb-5 text-center textGide">It not only helps you comply with government regulations; it helps you upsell as well!</p>
        </div>
    </div>

    <div class="text-center mb-4">
        <img src="{{asset('landing/images/blog-3.png')}}" width="100%">
    </div>
    <div class="d-flex align-items-center mb-3">
        <span class="capitalize mt-0">L</span>
        <p class="ml-2 mt-3 txtBlog">
            et’s agree on one fact, guests who don’t care about their calorie
            intake will not even read that information on your menu. However,
            your health-conscious guests will definitely read it and appreciate that
            you’ve provided the information. Adding the calorie count gives you a clear
            opportunity to upsell. You can offer low-calorie alternatives on your
            menu at slightly higher prices.
        </p>
    </div>
    <p class="txtBlog mb-5">
        You can improve the costing on these items while ensuring that you offer
        the correct calorie counts. Doing this will gain the trust of your guests
        as well as their confidence in the quality of information you’ve given them.
    </p>
    <h4 class="my-4">Why it is important to have a healthy option on your menu?</h4>
    <p class="txtBlog">
        If you are not offering healthy options on your menu, you are definitely
        missing a great opportunity. Healthy eating is on the rise clear across
        the world. You need to join the trend to ensure that your health-conscious
        guests continue dining at your restaurant and have the clarity they need
        to order the healthier options if they wish.
    </p>
    <p class="txtBlog mb-5">
        Adding the calorie count to each menu item couldn’t be easier on menioo.
        All you have to do is login, go to menu setup, enter the calorie count of
        each item on your menu and sync it to your tablets.
    </p>

    <hr>

    <div class="row">
        <div class="col-md-6 col-12">
            <img src="{{asset('landing/images/blog.png')}}" class="img-fluid">
            <h4 class="my-2">Solving the Ticket Time Riddle</h4>
            <p class="pt-5">
                Restaurant owners and managers all know that
                collecting feedback from guests can be a challenge,
                especially honest real-time feedback…
            </p>
            <a href="blog1" class="btn btn-link p-0 readon">Read More</a>
        </div>
        <div class="col-md-6 col-12">
            <img src="{{asset('landing/images/blog-2.png')}}" class="img-fluid">
            <h4 class="my-2">Adding Calorie Count! An up-seller? Or a
                down-seller?</h4>
            <p class="pt-3">
                Let’s agree on one fact, guests who don’t care about
                their calorie intake will not even read that information
                on your menu…
            </p>
            <a href="blog2" class="btn btn-link p-0 readon">Read More</a>
        </div>
    </div>

</section>

<section id="resources" class="mt-5 p-5">
    <h2 class="text-theme my-3 mx-auto text-center w-75">Download the app on iPad or Android tablet for
        stunning dinning experience for your guests!</h2>

    <ul class="d-flex flex-wrap justify-content-center my-5">
        <li class="mx-3 my-2"><a href="#"><img src="{{asset('landing/images/ios-btn.png')}}"></a></li>
        <li class="mx-3 my-2"><a href="#"><img src="{{asset('landing/images/android-btn.png')}}"></a></li>
    </ul>

</section>


@include('landing.common.signupsection')
