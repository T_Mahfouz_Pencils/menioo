<!-- content -->
<section class="container my-5 p-5">
    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <h2 class="text-theme my-5 text-center">Solving the Ticket Time Riddle</h2>
        </div>
    </div>


    <div class="text-center mb-4">
        <img src="{{asset('landing/images/blog-2.png')}}" width="100%">
    </div>
    <div class="d-flex align-items-center mb-3">
        <span class="capitalize mt-0">R</span>
        <p class="ml-2 mt-3 txtBlog">
            estaurant owners and managers all know that collecting feedback
            from guests can be a challenge, especially honest real-time feedback.
            Guests are reluctant to reveal what they really think of the food and service,
            even if given a piece of paper to write their review out.
            They wonder what the point is, thinking that no one will read it anyway,
            and if someone does, it might be the person they’re criticizing.
        </p>
    </div>
    <h4 class="my-4">How to work in an effective ticket time riddle process?</h4>
    <p class="txtBlog">
        Our goal was to make collecting feedback tamper proof for restaurants.
        We wanted to ensure that the feedback from every guest who took the
        time to provide it would directly reach management without any
        interference from staff. Once our guests started using the feedback
        feature on the digital tablet menu, the number of guests filling
        out the form increased by 32%. Presumably, because they were
        more confident that it will reach management.
        The quality of the feedback improved greatly and made a
        lot more sense as well.
    </p>
    <p class="txtBlog mb-5">
        One complaint we kept getting in our feedback had to do with our
        long ticket times. But we were having difficulty identifying the
        actual source of the problem. When we implemented the real-time
        feedback feature in Menioo.com and our guests were confident that
        what they wrote would not be tampered with, we were able to get very
        insightful information that helped us solve the riddle.
    </p>
    <p class="txtBlog mb-5">
        After analyzing the orders placed by all the guests complaining about
        the ticket time, we found they all had one item in common on their order
        – a specific type of pizza. We went back to the kitchen to investigate
        and realized that one of the ingredients had to be defrosted before
        being added on top of the pizza dough. We then adjusted the process to
        ensure that sufficient quantities of this ingredient were brought out
        to defrost throughout the day, reducing the ticket time for this
        type of pizza.
    </p>
    <h4>In Conclusion</h4>
    <p class="mt-4 txtBlog">
        Once we took these actions the ticket time issue disappeared.
        We would never have found out what we needed to know without having a
        full record of our feedback and the specific orders placed by those guests.
    </p>
    <p class="mt-4 txtBlog">
        When restaurants can collect real-time feedback that is tamper proof,
        they can quickly implement the changes needed to improve their food and
        service. I would never stop using this feature on menioo.
    </p>
    <hr>

    <div class="row">
        <div class="col-md-6 col-12">
            <img src="{{asset('landing/images/blog.png')}}" class="img-fluid">
            <h4 class="my-2">Solving the Ticket Time Riddle</h4>
            <p class="pt-5">
                Restaurant owners and managers all know that
                collecting feedback from guests can be a challenge,
                especially honest real-time feedback…
            </p>
            <a href="blog1" class="btn btn-link p-0 readon">Read More</a>
        </div>
        <div class="col-md-6 col-12">
            <img src="{{asset('landing/images/blog-3.png')}}" class="img-fluid">
            <h4 class="my-2">Adding Calorie Count! An up-seller? Or a
                down-seller?</h4>
            <p class="pt-3">
                Let’s agree on one fact, guests who don’t care about
                their calorie intake will not even read that information
                on your menu…
            </p>
            <a href="blog3" class="btn btn-link p-0 readon">Read More</a>
        </div>
    </div>

</section>

<section id="resources" class="mt-5 p-5">
    <h2 class="text-theme my-3 mx-auto text-center w-75">Download the app on iPad or Android tablet for
        stunning dinning experience for your guests!</h2>

    <ul class="d-flex flex-wrap justify-content-center my-5">
        <li class="mx-3 my-2"><a href="#"><img src="{{asset('landing/images/ios-btn.png')}}"></a></li>
        <li class="mx-3 my-2"><a href="#"><img src="{{asset('landing/images/android-btn.png')}}"></a></li>
    </ul>

</section>


@include('landing.common.signupsection')
