<!-- content -->
<section class="container my-5 p-5">
    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <h2 class="text-theme my-5 text-center">Why did we develop menioo?</h2>
        </div>
    </div>


    <div class="text-center mb-4">
        <img src="{{asset('landing/images/blog.png')}}" class="img-fluid">
    </div>
    <div class="d-flex align-items-center mb-3">
        <span class="capitalize mt-0">B</span>
        <p class="ml-2 mt-3 txtBlog">eing in the restaurant business ourselves, we are very aware of the difficulty involved in
            developing a menu and how much of a pain it can be to change a price or add an item
            after the menu has already been printed. We were determined to figure out a way for
            restaurants to quickly and easily change prices, add items and make any other
            necessary changes as well.</p>
    </div>
    <p class="txtBlog mb-5">We all know that the cost of raw materials can change suddenly, even day to day. It’s unlikely that
        you would need to change your prices daily, but at least with a digital menu on a tablet you would
        have the freedom to change prices whenever the need arose, avoiding the time and money it would
        take to reprint an entire restaurant menu. </p>
    <h4 class="my-4">Customize the Theme of a Restaurant’s Menu According to the Season</h4>
    <p class="txtBlog">
        Another very important feature that motivated us to develop menioo was that we wanted the ability
        to customize the theme of our restaurant’s menu according to the season. Why should we be stuck
        with just one theme for the entire year? We wanted to celebrate Christmas with my Christian guests,
        Hanukkah with my Jewish guests, Eid with my Muslim guests and Diwali with my Indian guests. And
        with my restaurant menu app, to the delight of my customers I can do this!
    </p>
    <p class="txtBlog mb-5">With the cost of tablets now being far more affordable than in the past, I felt that by developing this
        app I would have the freedom and flexibility I’ve been seeking. In the process, new features and
        usages started to present themselves, such as the ability for my restaurant guests to order directly
        from the tablet menu. Their order could then be sent to my POS or to the printers in the kitchen.
        I would also be able to add preparation times, calorie counts, ingredient warnings, etc. The scope
        and quality of the information my menu app provides my guests is incredible compared to my old
        printed restaurant menu.</p>
    <h4>In Conclusion</h4>
    <p class="mt-4 txtBlog">
        Honestly, the most important added value that menioo provided my business was that it unleased
        the creativity of my chef. We can now develop a plate today, work on the costing, agree on the
        presentation and have it on my tablet menu the next day, allowing my guests to try it immediately.
        There is no need to print inserts or wait until the next menu print.
    </p>
    <hr>

    <div class="row">
        <div class="col-md-6 col-12">
            <img src="{{asset('landing/images/blog-2.png')}}" class="img-fluid">
            <h4 class="my-2">Solving the Ticket Time Riddle</h4>
            <p class="pt-5">
                Restaurant owners and managers all know that
                collecting feedback from guests can be a challenge,
                especially honest real-time feedback…
            </p>
            <a href="blog2" class="btn btn-link p-0 readon">Read More</a>
        </div>
        <div class="col-md-6 col-12">
            <img src="{{asset('landing/images/blog-3.png')}}" class="img-fluid">
            <h4 class="my-2">Adding Calorie Count! An up-seller? Or a
                down-seller?</h4>
            <p class="pt-3">
                Let’s agree on one fact, guests who don’t care about
                their calorie intake will not even read that information
                on your menu…
            </p>
            <a href="blog3" class="btn btn-link p-0 readon">Read More</a>
        </div>
    </div>

</section>

<section id="resources" class="mt-5 p-5">
    <h2 class="text-theme my-3 mx-auto text-center w-75">Download the app on iPad or Android tablet for
        stunning dinning experience for your guests!</h2>

    <ul class="d-flex flex-wrap justify-content-center my-5">
        <li class="mx-3 my-2"><a href="#"><img src="{{asset('landing/images/ios-btn.png')}}"></a></li>
        <li class="mx-3 my-2"><a href="#"><img src="{{asset('landing/images/android-btn.png')}}"></a></li>
    </ul>

</section>


@include('landing.common.signupsection')
